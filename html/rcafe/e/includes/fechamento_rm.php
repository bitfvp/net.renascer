<?php
try{
    $sql = "SELECT * "
        ."FROM "
        ."ren_entradas "
        ."WHERE "
        ."ren_entradas.id <> 0 and ren_entradas.romaneio_tipo=1 ";

    $sql .="AND ren_entradas.ocr=:ocr ";

    $sql .="order by ren_entradas.data_ts DESC LIMIT 0,500 ";

    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":ocr", $fechamento['nr']);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<div class="card mt-2">
    <div class="card-header bg-info text-light">
        Romaneios pertencentes ao fechamento
    </div>
    <div class="card-body">

        <?php
        foreach ($entradas as $dados){
            $en_id = $dados["id"];
            $romaneio = $dados["romaneio"];
            $romaneio_tipo = $dados["romaneio_tipo"];
            $romaneio_tipo=fncgetromaneiotipo($dados["romaneio_tipo"]);
            $data_ts = $dados["data_ts"];

            ?>
            <table class="table table-stripe table-sm table-hover table-condensed">
                <thead class="thead-dark">
                <tr>
                    <th scope="col"><small>ROMANEIO</small></th>
                    <th scope="col"><small>DATA</small></th>
                    <th scope="col"><small>DOCUMENTOS</small></th>
                </tr>
                </thead>
                <tbody>
                <tr id="<?php echo $en_id;?>" class="">
                    <th scope="row" style="white-space: nowrap;">
                        <h5 class="badge badge-info text-dark">
                            <?php echo $romaneio_tipo.$romaneio; ?>
                        </h5>
                    </th>
                    <td><?php echo dataRetiraHora($data_ts); ?></td>
                    <td>
                        <div class="btn-group" role="group" aria-label="">
                            <a href="index.php?pg=Vrm_print1&id_e=<?php echo $en_id; ?>" target="_blank" class="btn btn-sm btn-primary mx-1">
                                <i class="fas fa-print"> pesagens</i>
                            </a>

                            <a href="index.php?pg=Vrm_print2&id_e=<?php echo $en_id; ?>" target="_blank" class="btn btn-sm btn-primary mx-1">
                                <i class="fas fa-print"> Lotes</i>
                            </a>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
            <?php
            // Captura os dados do cliente solicitado
            $sql = "SELECT * \n"
                . "FROM ren_entradas_lotes \n"
                . "WHERE (((ren_entradas_lotes.romaneio)=?) and status=1)\n"
                . "ORDER BY ren_entradas_lotes.data_ts ASC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $en_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $lotes = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            foreach ($lotes as $lt) {
                $entrada=fncgetentrada($lt['romaneio']);
                $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

                $cordalinha = "info ";
                if ($lt['peso_atual']==null or $lt['peso_atual']==0 or $lt['peso_atual']==""){
                    $cordalinha = "dark ";
                }else{
                    if ($lt['p_bo']==1){
                        $cordalinha = "warning ";
                    }
                }
                $letra=fncgetletra($lt["letra"]);
                $id_l = $lt["id"];
                $peso_entrada = $lt["peso_entrada"];
                $sacas_entrada=$peso_entrada/60;
                $sacas_entrada=number_format($sacas_entrada, 2, ',', ' ');
                $p_bo = $lt["p_bo"];
                $bo = $lt["bo"];
                $obs = $lt["obs"];
                $responsavel = fncgetpessoa($lt["responsavel"])['nome'];
                ?>
                <hr>
                <blockquote class="blockquote blockquote-<?php echo $cordalinha; ?>">
                    <a href="?pg=Vl&id_l=<?php echo $lt['id'];?>" class="float-right">
                        <strong class='badge badge-success'><h5><?php echo $romaneio_tipo.$entrada['romaneio']. " ".$letra;?></h5></strong>
                    </a>
                    Fornecedor: <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?>&nbsp&nbsp</strong><br>
                    Produto: <strong class="text-info">
                        <?php echo fncgetprodutos($lt['tipo_cafe'])['nome']; ?>
                    </strong><br>
                    Entrada: <strong class="text-success"><?php echo $peso_entrada. "KG ou ".$sacas_entrada."V "; ?>&nbsp&nbsp</strong><br>
                    <?php
                    if ($p_bo==1){
                        echo "B.O.: <strong class='text-info'>{$bo}&nbsp&nbsp</strong><br>";
                    }
                    ?>
                    Obs gerais: <strong class="text-info"><?php echo $obs; ?>&nbsp&nbsp</strong><br>
                    <strong class="text-info" title=""><?php echo datahoraBanco2data($lt['data_ts']); ?>&nbsp;&nbsp;</strong>

                    <footer class="blockquote-footer">
                        <?php
                        $us=fncgetusuario($lt['usuario']);
                        echo $us['nome'];
                        ?>
                    </footer>
                </blockquote>
                <?php
            }


        }
        ?>

    </div>
</div>