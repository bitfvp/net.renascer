<?php
// Recebe
if (isset($_GET['id_e'])) {
    $rm_id = $_GET['id_e'];
    //existe um id e se ele é numérico
    if (!empty($rm_id) && is_numeric($rm_id)) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT * \n"
            . "FROM rcafe_entradas_lotes \n"
            . "WHERE (((rcafe_entradas_lotes.romaneio)=?) and status=1)\n"
            . "ORDER BY rcafe_entradas_lotes.data_ts ASC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $rm_id);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $lotes = $consulta->fetchAll();
        $sql = null;
        $consulta = null;
    }
}
?>

<div class="card mt-2">
    <div id="pointofview" class="card-header bg-info text-light">
        Lotes desse romaneio
    </div>
    <div class="card-body">
        <a href="index.php?pg=Vlt_print1&id_e=<?php echo $_GET['id_e'];?>" target="_blank">
            <span class="fa fa-print text-warning" aria-hidden="true"> Impressão de lotes</span>
        </a>
        <?php
        foreach ($lotes as $lt) {
            $entrada=fncgetentrada($lt['romaneio']);
            $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

            $cordalinha = "info ";
            if ($lt['peso_atual']==null or $lt['peso_atual']==0 or $lt['peso_atual']==""){
                $cordalinha = "dark ";
            }else{
                if ($lt['p_bo']==1){
                    $cordalinha = "warning ";
                }
            }
            $letra=fncgetletra($lt["letra"]);
            $id_l = $lt["id"];
            $peso_entrada = $lt["peso_entrada"];
            $sacas_entrada=$peso_entrada/60;
            $sacas_entrada=number_format($sacas_entrada, 2, '.', ',');
            $bags_entrada = $lt["bags_entrada"];
            $localizacao = $lt["localizacao"];
            $localizacao_obs = $lt["localizacao_obs"];
            $peso_atual = $lt["peso_atual"];
            $sacas_atual=$peso_atual/60;
            $sacas_atual=number_format($sacas_atual, 2, '.', ',');
            $bags_atual = $lt["bags_atual"];
            $p_bo = $lt["p_bo"];
            $bo = $lt["bo"];
            $obs = $lt["obs"];
            $valor_saca = $lt["valor_saca"];
            $responsavel = fncgetpessoa($lt["responsavel"])['nome'];
            if ($lt["cata"]>0){
                $cata=$lt["cata"]."%";
            }else{
                $cata="N.D.";
            }
            ?>
            <hr>
            <blockquote class="blockquote blockquote-<?php echo $cordalinha; ?>">
                <a href="?pg=Vl&id_l=<?php echo $lt['id'];?>" class="float-right">
                    <strong class='badge badge-success'><h3><?php echo $romaneio_tipo.$entrada['romaneio']. " ".$letra;?></h3></strong>
                </a>
                Fornecedor: <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?>&nbsp&nbsp</strong><br>
                Produto: <strong class="text-info">
                    <?php echo fncgetprodutos($lt['tipo_cafe'])['nome']; ?>
                </strong><br>
                Cata: <strong class="text-info"><?php echo $cata; ?>&nbsp&nbsp</strong><br>
                Peso inicial: <strong class="text-success"><?php echo $peso_entrada. "KG ou ".$sacas_entrada." volumes"; ?>&nbsp&nbsp</strong><br>
                Valor por saca: R$<strong class="text-info"><?php echo $valor_saca; ?> </strong><br>

                Localização: <strong class="text-info"><?php echo fncgetlocal($localizacao)['nome']." ". $localizacao_obs; ?>&nbsp&nbsp</strong><br>

                Peso atual: <strong class="text-danger"><?php echo $peso_atual. "KG ou ".$sacas_atual." volumes"; ?>&nbsp&nbsp</strong><br>

                <?php
                if ($p_bo==1){
                    echo "B.O.: <strong class='text-info'>{$bo}&nbsp&nbsp</strong><br>";
                }
                ?>
                Obs gerais: <strong class="text-info"><?php echo $obs; ?>&nbsp&nbsp</strong><br>
                Responsável: <strong class="text-info"><?php echo strtoupper($responsavel); ?>&nbsp&nbsp</strong><br>
                <strong class="text-info" title=""><?php echo datahoraBanco2data($lt['data_ts']); ?>&nbsp;&nbsp;</strong>

                <?php
                if ($lt["alterado"]!=0){
                    echo "<br><i class='text-danger'>Esse lote já sofreu alteração e não pode ser excluido ou editado!!</i>";
                }else{

                }
                ?>

                <footer class="blockquote-footer">
                    <?php
                    $us=fncgetusuario($lt['usuario']);
                    echo $us['nome'];
                    ?>
                </footer>
            </blockquote>
            <?php
            echo "</h6>";
        }
        ?>
    </div>
</div>