<?php

$venda=fncgetvenda($fechamento['id_venda']);
$cliente=fncgetpessoa($venda['cliente']);
?>

<div class="card mt-2">
    <div class="card-header bg-info text-light">
        Venda pertencente ao fechamento
    </div>
    <div class="card-body">


            <table class="table table-stripe table-sm table-hover table-condensed">
                <thead class="thead-dark">
                <tr>
                    <th scope="col"><small>CONTRATO</small></th>
                    <th scope="col"><small>CLIENTE</small></th>
                </tr>
                </thead>
                <tbody>
                <tr id="" class="">
                    <th scope="row" style="white-space: nowrap;">
                        <a href="index.php?pg=Vvvv_print1&id_v=<?php echo $venda['id']; ?>" target="_blank" title="comprovante de carregamento" class="">
                        <h5 class="badge badge-info text-dark">
                                <?php echo $venda['prefixo']."-".$venda['contrato']; ?>
                        </h5>
                        </a>
                    </th>
                    <td><?php echo strtoupper($cliente['nome']); ?></td>
                </tr>
                </tbody>
            </table>
            <?php
            // Captura os dados do cliente solicitado
            $sql = "SELECT * FROM "
                ."ren_vendas_pesagens "
                ."WHERE venda=:venda "
                ."order by ren_vendas_pesagens.data_ts DESC ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindValue(":venda", $venda['id']);
            $consulta->execute();
            $pesagens = $consulta->fetchAll();
            $pesagens_quant = $consulta->rowCount();

            $peso_total=0;
            $temp_coo=0;
            foreach ($pesagens as $dados) {
                $temp_coo++;
                $peso_liquido=$dados["peso_saida"]-$dados["peso_entrada"];
                $peso_total=$peso_total+$peso_liquido;
                $sacas=$peso_liquido/60;
                ?>
                <hr>
                <blockquote class="blockquote ">
                    Pesagem <?php echo $temp_coo;?>:<br>
                    <?php
                    echo number_format($peso_liquido,2,',',' ')."Kg Liquido ";
                    echo number_format($sacas,2,',',' ')."V ";
                    ?>
                </blockquote>
                <?php
            }


        ?>

    </div>
</div>