<?php
//inicia cookies
ob_start();
//inicia sessions
session_start();

//reports
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/net.renascer/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/net.renascer/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();
/*
*   inclui e inicia a classe de configuração de ambiente que é herdada de uma classe abaixo da raiz em models/Env.class.php
*   $env e seus atributos sera usada por todo o sistema
*/
include_once("models/ConfigMod.class.php");
$env=new ConfigMod();

/*
*   inclui e inicia a classe de configuração de drive pdo/mysql
*   $pdo e seus atributos sera usada por todas as querys
*/
include_once("{$env->env_root}models/Db.class.php");////conecção com o db
$pdo = Db::conn();


use Sinergi\BrowserDetector\Browser;
use Sinergi\BrowserDetector\Os;
use Sinergi\BrowserDetector\Device;
use Sinergi\BrowserDetector\Language;
$browser = new Browser();
$os = new Os();
$device = new Device();
$language = new Language();

//classe para debugar e salvar sqls ao banco de dados
include_once("{$env->env_root}models/LogQuery.class.php");//classe de log de query
$LQ = new LogQuery();


//inclui um controle contendo a funcão que apaga a sessao e desloga
//será chamada da seguinte maneira
//killSession();
include_once("{$env->env_root}controllers/Ks.php");//se ha uma acao

//inclui um controle que ativa uma acao
include_once("{$env->env_root}controllers/action.php");//se ha uma acao

//funcoes diversas
include_once ("{$env->env_root}includes/funcoes.php");//funcoes

////troca tema
include_once("{$env->env_root}controllers/trocatema.php");
////logout
include_once("{$env->env_root}controllers/logout.php");

//valida o token e gera array
include_once("{$env->env_root}controllers/validaToken.php");

//valida se há manutenção
include_once("{$env->env_root}controllers/validaManutencao.php");

//chat msg
include_once("{$env->env_root}models/rcafe/Chat_Msg.class.php");
include_once("{$env->env_root}controllers/rcafe/chat_msg.php");

/* inicio do Bloco dedidado*/
if (isset($_SESSION['logado']) and $_SESSION['logado']=="1"){

    include_once("{$env->env_root}controllers/usuario_lista.php");

    include_once("models/ClsTexto.class.php");

    //pessoa
    include_once("models/Pessoa.class.php");
    include_once("controllers/pessoa_carregar.php");
    include_once("controllers/pessoa_edicao.php");
    include_once("controllers/pessoa_novo.php");
    include_once("controllers/pessoa_apagar.php");
    include_once ("{$env->env_root}includes/rcafe/rank_pessoa.php");

    //locais
    include_once("controllers/locais_carregar.php");

    //produtos
    include_once("controllers/produtos_carregar.php");

    //letra
    include_once("controllers/letra_carregar.php");

    //tipo de lançamento
    include_once("controllers/tipolancamento_carregar.php");

    //romaneiotipo
    include_once("controllers/romaneiotipo_carregar.php");


    //lotes
    include_once("models/Lotes.class.php");
    include_once("controllers/lotes_carregar.php");
    include_once("controllers/lote_edicao.php");

    include_once("controllers/entrada_carregar.php");


    include_once("models/Historico.class.php");
    include_once("controllers/historico_load.php");
    include_once("controllers/historico_insert.php");
    include_once("controllers/historico_update.php");
    include_once("controllers/historico_delete.php");

    include_once("models/Fonte.class.php");
    include_once("controllers/fonte_load.php");
    include_once("controllers/fonte_insert.php");
    include_once("controllers/fonte_update.php");
    include_once("controllers/fonte_delete.php");

    include_once("models/Corretor.class.php");
    include_once("controllers/corretor_load.php");
    include_once("controllers/corretor_insert.php");
    include_once("controllers/corretor_update.php");
    include_once("controllers/corretor_delete.php");

    include_once("models/Fechamento.class.php");
    include_once("controllers/fechamento_load.php");
    include_once("controllers/fechamento_insert.php");
    include_once("controllers/fechamento_update.php");
    include_once("controllers/fechamento_delete.php");

    //venda
    include_once("controllers/venda_carregar.php");


    include_once("controllers/roteiro_imprimir.php");
    include_once("controllers/roteiro_resetimprimir.php");

    include_once("models/Fechamento_lote.class.php");
    include_once("controllers/fechamento_lote_load.php");
    include_once("controllers/fechamento_lote_insert.php");
    include_once("controllers/fechamento_lote_update.php");
    include_once("controllers/fechamento_lote_delete.php");

    include_once("models/Caixa_lancamento.class.php");
    include_once("controllers/caixa_lancamento_load.php");
    include_once("controllers/caixa_lancamento_insert.php");
    include_once("controllers/caixa_lancamento_update.php");
    include_once("controllers/caixa_lancamento_delete.php");

    //
    include_once("controllers/comissao_insert.php");

    //arquivos
    include_once("controllers/arquivo_insert.php");
    include_once("controllers/arquivo_delete.php");

}
/* fim do bloco dedicado*/

//metodo de checar usuario
include_once("{$env->env_root}controllers/confirmUser.php");