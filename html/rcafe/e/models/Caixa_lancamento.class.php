<?php
class Caixa_lancamento{
    public function fnccaixa_lancamentoinsert($fonte, $fechamento_id, $tipo, $valor, $previsao, $historico, $identificador, $descricao, $finalizado, $obs, $usuario ){

        if ($finalizado==1){
            $finalizado_data=date('Y-m-d H:i:s', time());
        }else{
            $finalizado_data=null;
        }
//inserção no banco
        try{
            $sql="INSERT INTO rcafe_caixa_lancamentos ";
            $sql.="(id, fonte, fechamento_id, tipo, valor, previsao, historico, identificador, descricao, finalizado, finalizado_data, obs, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :fonte, :fechamento_id, :tipo, :valor, :previsao, :historico, :identificador, :descricao, :finalizado, :finalizado_data, :obs, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":fonte", $fonte);
            $insere->bindValue(":fechamento_id", $fechamento_id);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":valor", $valor);
            $insere->bindValue(":previsao", $previsao);
            $insere->bindValue(":historico", $historico);
            $insere->bindValue(":identificador", $identificador);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":finalizado", $finalizado);
            $insere->bindValue(":finalizado_data", $finalizado_data);
            $insere->bindValue(":obs", $obs);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
///
///
    public function fnccaixa_lancamentoupdate($id, $finalizado, $obs ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="rcafe_caixa_lancamentos ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){

            if ($finalizado==1){
                $finalizado_data=date('Y-m-d H:i:s', time());
            }else{
                $finalizado_data=null;
            }
//comecar o update
            try {
                $sql="UPDATE rcafe_caixa_lancamentos ";
                $sql.="SET ";
                $sql .= "
finalizado=:finalizado, finalizado_data=:finalizado_data, obs=:obs
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":finalizado", $finalizado);
                $atualiza->bindValue(":finalizado_data", $finalizado_data);
                $atualiza->bindValue(":obs", $obs);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fnccaixa_lancamentodelete($tabela_id,$usuario_off ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="rcafe_caixa_lancamentos ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $tabela_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE rcafe_caixa_lancamentos ";
                $sql.="SET ";
                $sql .= "status=0,
usuario_off=:usuario_off,
data_off=CURRENT_TIMESTAMP
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":usuario_off", $usuario_off);
                $atualiza->bindValue(":id", $tabela_id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse registro cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Desativada Com Sucesso!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=V{pagina}&id={id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

}//fim da classe