<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<main class="container-fluid">
    <div class="row">

        <?php
        try{
            $sql = "SELECT "
            . "ren_entradas_lotes.id, "
            . "ren_entradas.id as id_e, "
            . "ren_entradas.romaneio, "
            . "ren_entradas_lotes.data_ts, "
            . "ren_entradas_lotes.letra, "
            . "ren_entradas_lotes.tipo_cafe, "
            . "ren_entradas_lotes.verificado, "
            . "ren_entradas_lotes.peso_entrada, "
            . "ren_entradas_lotes.bags_entrada, "
            . "ren_entradas_lotes.localizacao, "
            . "ren_entradas_lotes.localizacao_obs, "
            . "ren_entradas_lotes.p_bo, "
            . "ren_entradas_lotes.bo, "
            . "ren_entradas_lotes.peso_atual, "
            . "ren_entradas_lotes.bags_atual, "
            . "ren_entradas_lotes.obs, "
            . "ren_entradas_lotes.valor_saca, "
            . "ren_entradas.romaneio_tipo, "
            . "ren_pessoas.nome AS fornecedor "
            . "FROM "
            . "ren_entradas_lotes "
            . "INNER JOIN ren_entradas ON ren_entradas_lotes.romaneio = ren_entradas.id "
            . "INNER JOIN ren_pessoas ON ren_entradas.fornecedor = ren_pessoas.id ";
            $sql_where = "WHERE ren_entradas_lotes.id>0 and ren_entradas.fornecedor={$_GET['id']} ";
            $sql_orderby= "ORDER BY ren_entradas_lotes.data_ts DESC LIMIT 0,1000";
            global $pdo;
            $consulta=$pdo->prepare($sql.$sql_where.$sql_orderby);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $lotes = $consulta->fetchAll();
        $lotes_quant = $consulta->rowCount();
        $sql = null;
        $consulta = null;
        ?>

        <div class="col-md-9">
            <a href="index.php?pg=Vpessoa_lprint1&id=<?php echo $_GET['id'];?>" target="_blank" class="btn btn-sm btn-info">
                <span class="fa fa-print text-warning" aria-hidden="true"> Impressão de histórico de lotes</span>
            </a>
            <table class="table table-striped table-hover table-sm">
                <thead class="thead-dark">
                <tr>
                    <th scope="col"><small>LOTE</small></th>
                    <th scope="col"><small>DATA</small></th>
                    <th scope="col"><small>VALOR</small></th>
                    <th scope="col"><small>PRODUTO</small></th>
                    <th scope="col"><small>PESO ENTRADA</small></th>
                    <th scope="col"><small>PESO ATUAL</small></th>
                    <th scope="col"><small>LOCALIZAÇÃO</small></th>
                    <th scope="col"><small>B.O.</small></th>
                </tr>
                </thead>

                <?php

                // vamos criar a visualização,
                foreach ($lotes as $dados){
                $cordalinha = "  ";
                if ($dados['peso_atual']==null or $dados['peso_atual']==0 or $dados['peso_atual']==""){
                    $cordalinha = " text-warning bg-dark ";
                }else{
                    if ($dados['p_bo']==1){
                        $cordalinha = " text-dark bg-warning ";
                    }
                }

                $id_l = $dados["id"];
                $romaneio = $dados["romaneio"];
                $romaneio_tipo=fncgetromaneiotipo($dados["romaneio_tipo"]);

                $letra=fncgetletra($dados["letra"]);

                $entrada_id = $dados["id_e"];
                $data_l = $dados["data_ts"];

                $tipo_cafe = $dados["tipo_cafe"];
                $tipo_cafe_entrada = $dados["tipo_cafe_entrada"];
                if ($dados["verificado"]==0){
                    $bebida="<small class='text-danger'>NÃO VERIFICADA</small>";
                }else{
                    $bebida="<small class='text-success'>VERIFICADA</small>";
                }
                $peso_entrada = $dados["peso_entrada"];
                $sacas_entrada=$peso_entrada/60;
                $sacas_entrada=number_format($sacas_entrada, 1, '.', ',');
                $peso_entrada.=" Kg";
                $bags_entrada = $dados["bags_entrada"];
                $localizacao = $dados["localizacao"];
                $localizacao_obs = $dados["localizacao_obs"];
                $p_bo = $dados["p_bo"];
                $bo = $dados["bo"];
                $peso_atual = $dados["peso_atual"];
                $sacas_atual=$peso_atual/60;
                $sacas_atual=number_format($sacas_atual, 2, '.', ',');
                $peso_atual.=" Kg";
                $bags_atual = $dados["bags_atual"];
                $obs = $dados["obs"];
                $fornecedor = strtoupper($dados["fornecedor"]);
                $valor=$dados["valor_saca"];

                ?>
                <tbody>
                <tr class="small <?php echo $cordalinha; ?>" id='<?php echo $romaneio_tipo; ?>'>
                    <th scope="row" id="<?php echo $id_l;  ?>" style="white-space: nowrap;">
                        <a href="?pg=Vl&id_l=<?php echo $id_l;?>" title="Ver">
                            <?php
                                echo $romaneio_tipo.$romaneio." ".$letra;
                            ?>
                        </a>
                    </th>
                    <td title="<?php echo datahoraBanco2data($dados["data_ts"]);?>">
                        <?php
                        echo dataRetiraHora($dados["data_ts"]);
                        ?>
                    </th>
                    <td>
                        <?php echo number_format($valor,2); ?>R$
                    </td>
                    <td>
                        <?php echo fncgetprodutos($tipo_cafe)['abrev'];?>
                    </td>

                    <td>
                            <?php echo $peso_entrada. ", ".$sacas_entrada."v";?>
                    </td>

                    <td>
                        <?php echo $peso_atual. ", ".$sacas_atual."v";?>
                    </td>

                    <td>
                        <?php echo fncgetlocal($localizacao)['nome']. " ". $localizacao_obs;?>
                    </td>

                    <td>
                        <?php echo $bo;?>
                    </td>
                </tr>
                <?php
                }
                ?>
                </tbody>
            </table>




        </div>


        <div class="col-md-3">
            <section class="sidebar-offcanvas" id="sidebar">
                <div class="list-group">
                    <a href="?pg=Vpessoa&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fas fa-home"></i>    PESSOA</a>
                    <a href="?pg=Vpessoa_l&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fas fa-boxes"></i>    LOTES</a>
                </div>
            </section>
            <script type="application/javascript">
                var offset = $('#sidebar').offset().top;
                var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance
                $(document).on('scroll', function () {
                    if (offset <= $(window).scrollTop()) {
                        $meuMenu.addClass('fixarmenu');
                    } else {
                        $meuMenu.removeClass('fixarmenu');
                    }
                });
            </script>
        </div>

    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>