<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_9"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vfechamento_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT "
        ."rcafe_fechamentos.id, "
        ."rcafe_fechamentos.`status`, "
        ."rcafe_fechamentos.data_ts, "
        ."rcafe_fechamentos.nr, "
        ."rcafe_fechamentos.tipo_fechamento, "
        ."rcafe_fechamentos.imprimir, "
        ."rcafe_fechamentos.corretagem_c, "
        ."rcafe_fechamentos.corretagem_v, "
        ."rcafe_fechamentos.ordem_compra, "
        ."rcafe_fechamentos.descarga, "
        ."rcafe_fechamentos.descricao, "
        ."rcafe_fechamentos.condicao_pag, "
        ."rcafe_fechamentos.condicao_entrega, "
        ."rcafe_fechamentos.prazo_entrega, "
        ."rcafe_fechamentos.forma_pag, "
        ."rcafe_fechamentos.observacao, "
        ."pessoas_comprador.nome AS comprador, "
        ."pessoas_vendedor.nome AS vendedor "
        ."FROM "
        ."rcafe_fechamentos "
        ."INNER JOIN ren_pessoas AS pessoas_comprador ON pessoas_comprador.id = rcafe_fechamentos.comprador "
        ."INNER JOIN ren_pessoas AS pessoas_vendedor ON pessoas_vendedor.id = rcafe_fechamentos.vendedor "
        ."WHERE "
        ."rcafe_fechamentos.id <> 0 ";

    if (isset($_GET['sca']) and $_GET['sca']!='') {
        $sca=$_GET['sca'];
        $sql .=" AND pessoas_comprador.nome LIKE '%$sca%' ";
    }

    if (isset($_GET['scb']) and $_GET['scb']!='') {
        $scb=$_GET['scb'];
        $sql .=" AND pessoas_vendedor.nome LIKE '%$scb%' ";
    }

    $tempinicio=" 00:00:01";
    $tempfinal=" 23:59:59";
    if (isset($_GET['scc']) and $_GET['scc']!=0 and $_GET['scc']!='') {
        $inicial=$_GET['scc'].$tempinicio;
        $final=$_GET['scc'].$tempfinal;
    }else{
        if (((isset($_GET['sca']) and $_GET['sca']!='') or (isset($_GET['scb']) and $_GET['scb']!='')) or ((isset($_GET['scc']) and $_GET['scc']!='') or (isset($_GET['scc']) and $_GET['scc']!=''))){
            //tempo todo
            $inicial="2021-01-01".$tempinicio;
            $final=date("Y-m-d").$tempfinal;
        }else{
            //apenas dia anterior
            $inicial=date("Y-m-d",strtotime("-90 days")).$tempinicio;
            $final=date("Y-m-d").$tempfinal;
        }

    }
    $sql .=" AND ((rcafe_fechamentos.data_ts)>=:inicial) And ((rcafe_fechamentos.data_ts)<=:final) ";


    $sql .="order by rcafe_fechamentos.data_ts DESC LIMIT 0,50 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container-fluid">
    <h3 class="form-cadastro-heading">
        <a href="index.php?pg=Vfechamento_lista" class="text-decoration-none text-dark">FECHAMENTOS</a>
    </h3>
    <hr>

    <div class="row">
        <div class="col-md-8">
            <form action="index.php" method="get">
                <div class="input-group mb-3 col-md-12 float-left">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
                    </div>
                    <input name="pg" value="Vfechamento_lista" hidden/>
                    <input type="text" name="sca" id="sca" placeholder="comprador" autocomplete="off" class="form-control" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                    <input type="text" name="scb" id="scb" placeholder="vendedor" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
                    <input type="date" name="scc" id="scc" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
                </div>
            </form>

            <script type="text/javascript">
                function selecionaTexto()
                {
                    document.getElementById("sca").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>
        <div class="col-md-4">
            <a href="index.php?pg=Vfechamento_editar" class="btn btn-block btn-primary mb-2" ><i class="fas fa-level-down-alt"></i> Novo</a>
        </div>
    </div>
<!--    <div class="row">-->
<!---->
<!--        <div class="col-md-2">-->
<!--            <a href="index.php?pg=Vfechamento_roteiro" class="btn btn-block btn-success mb-2" target="_blank"> Roteiro</a>-->
<!--        </div>-->
<!--        <div class="col-md-2">-->
<!--            <a href='index.php?pg=Vfechamento_lista&aca=resetroimprimir' class="btn btn-block btn-warning btn-block mb-2"> Desmarcar Todos</a>-->
<!--        </div>-->
<!--    </div>-->

    <table class="table table-sm table-stripe table-hover table-bordered">
        <thead class="thead-dark">
        <tr>
            <th scope="col"><small>TIPO</small></th>
            <th scope="col"><small>NR</small></th>
            <th scope="col"><small>OCE</small></th>
            <th scope="col"><small>EMISSÃO</small></th>
            <th scope="col"><small>COMPRADOR</small></th>
            <th scope="col"><small>VENDEDOR</small></th>
            <th scope="col"><small>SACAS</small></th>
            <th scope="col"><small>VALOR</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr class="bg-warning">
            <th colspan="4" class="bg-info text-right"></th>
            <th colspan="5" class="bg-info text-right"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</th>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);
        }
        if($_GET['scb']!="" and isset($_GET['scb'])) {
            $stb = strtoupper($_GET['scb']);
            define('CSB', $stb);
        }
        foreach ($entradas as $dados){
            $fe_id = $dados["id"];
            $ordem_compra = $dados["ordem_compra"];
            $comprador = strtoupper($dados["comprador"]);
            $vendedor = strtoupper($dados["vendedor"]);
            $data_ts = dataRetiraHora($dados["data_ts"]);
            if ($dados['tipo_fechamento']==1){
                $tipo_fechamento="C";
            }
            if ($dados['tipo_fechamento']==2){
                $tipo_fechamento="V";
            }
            if ($dados['tipo_fechamento']==3){
                $tipo_fechamento="$";
            }
            $data_ts = dataRetiraHora($dados["data_ts"]);
            $usuario = $dados["usuario"];

            if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                $corlinha="";
            }else{
                $corlinha=" bg-secondary text-warning ";
            }


                $sql = "SELECT id FROM rcafe_caixa_lancamentos WHERE fechamento_id=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $fe_id);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $countlancamentos = $consulta->rowCount();
                $sql = null;
                $consulta = null;

            $sql = "SELECT * FROM rcafe_fechamentos_lotes WHERE id_fechamento=? and status=1";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $fe_id);
            $consulta->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $lotes = $consulta->fetchAll();
            $countlotes = $consulta->rowCount();
            $sql = null;
            $consulta = null;



            $info1="";
            $info2="";
            if ($countlotes>0){
//                $info.= "<i class='fas fa-boxes fa-2x' title='possui lotes'></i> ";

                $tempinfosacas=0;
                $tempinfop=0;
                foreach ($lotes as $lote){
                    $tempinfop+=$lote['sacas']*$lote['preco'];
                    $tempinfosacas+=$lote['sacas'];
                }
                $tempinfopreco=$tempinfop/$tempinfosacas;

                $info1= "<i class='' title=''>".number_format($tempinfosacas,1)." sc</i> ";
                $info2= "<i class='' title=''>R$ ".number_format($tempinfopreco,2,',',' ')."</i> ";

            }
            if ($countlancamentos>0){
                $info3= "<i class='fas fa-dollar-sign fa-2x' title='possui lançamentos'></i>";
            }



            $imprimir=$dados["imprimir"];
            ?>

            <tr id="<?php echo $fe_id;?>" class="<?php echo $corlinha;?>">
                <td><?php
                    echo $tipo_fechamento;
                    ?>
                </td>
                <td><?php echo utf8_encode(strftime('%Y', strtotime("{$dados['data_ts']}")))."-".$dados['nr']; ?></td>
                <td><?php echo $ordem_compra; ?></td>
                <td><?php echo $data_ts; ?></td>
                <td><?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $aaa = $comprador;
                        $aa = explode(CSA, $aaa);
                        $a = implode("<span class='text-danger'>{$sta}</span>", $aa);
                        echo $a;
                    }else{
                        echo $comprador;
                    }
                    ?>
                </td>

                <td><?php
                    if($_GET['scb']!="" and isset($_GET['scb'])) {
                        $stb = CSB;
                        $aaa = $vendedor;
                        $aa = explode(CSB, $aaa);
                        $a = implode("<span class='text-danger'>{$stb}</span>", $aa);
                        echo $a;
                    }else{
                        echo $vendedor;
                    }
                    ?>
                </td>

                <td><?php echo $info1; ?></td>
                <td><?php echo $info2; ?></td>

                <td class="text-center">

                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vfechamento&id=<?php echo $fe_id; ?>" title="acessar" class="btn btn-sm btn-warning fas fa-search-plus text-dark"> ACESSAR</a>

                        <a href="index.php?pg=Vfechamento_editar&id=<?php echo $fe_id; ?>" title="Editar fechamento" class="btn btn-sm btn-primary">
                            <i class="fas fa-pen"></i>
                        </a>

                        <?php
                        if($imprimir==0){
//                                echo "<a href='index.php?pg=Vfechamento_lista&id={$fe_id}&aca=imprimirrosim&sca={$_GET['sca']}&b={$_GET['scb']}&scc={$_GET['scc']}' title='Click para alterar para sim' class='btn btn-outline-danger text-danger fas fa-print'></a>";
                        }
                        if($imprimir==1){
//                                echo "<a href='index.php?pg=Vfechamento_lista&id={$fe_id}&aca=imprimirronao&sca={$_GET['sca']}&b={$_GET['scb']}&scc={$_GET['scc']}' title='Click para alterar para não' class='btn btn-outline-success fas fa-print text-success'></a>";
                        }

                        echo "<a href='index.php?pg=Vfechamento_print1&id={$fe_id}' title='' class='btn btn-success fas fa-print' target='_blank'></a>";

                        if ($info1=="" and $info2==""){?>
                            <div class="dropdown show">
                                <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-trash"> EXCLUIR</i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#">Não</a>
                                    <a class="dropdown-item bg-danger" href="index.php?pg=Vfechamento_lista&aca=fechamentodelete&id=<?php echo $fe_id; ?>">Apagar</a>
                                </div>
                            </div>
                        <?php }
                        ?>
                    </div>

                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>