<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_9"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vcaixa_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT "
        ."* "
        ."FROM "
        ."rcafe_caixa_lancamentos "
        ."WHERE "
        ."rcafe_caixa_lancamentos.id <> 0 ";

    if (isset($_GET['sca']) and $_GET['sca']!='') {
        $sca=$_GET['sca'];
        $sql .=" AND identificador LIKE '%$sca%' ";
    }

    if (isset($_GET['scb']) and $_GET['scb']>0 and is_numeric($_GET['scb'])) {
        $scb=$_GET['scb'];
        $sql .=" AND historico='$scb' ";
    }


    if (isset($_GET['scc']) and $_GET['scc']!=0 and $_GET['scc']!='') {
        $inicial=$_GET['scc'];
        $inicial.=" 00:00:01";
        $final=$_GET['scc'];
        $final.=" 23:59:59";
        $sql .=" AND ((rcafe_caixa_lancamentos.data_ts)>=:inicial) And ((rcafe_caixa_lancamentos.data_ts)<=:final) ";
    }else{
        if ((isset($_GET['sca']) and $_GET['sca']!='') or ((isset($_GET['scc']) and $_GET['scc']!='') or (isset($_GET['scc']) and $_GET['scc']!=''))){
            $inicial="2021-01-01";
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((data_ts)>=:inicial) And ((data_ts)<=:final) ";
        }else{
            //apenas dia anterior
            $inicial=date("Y-m-d",strtotime("-90 days"));
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((data_ts)>=:inicial) And ((data_ts)<=:final) ";
        }

    }

    $sql .="order by finalizado, previsao asc LIMIT 0,50 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
//    if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
//        $consulta->bindValue(":romaneio", "%".$_GET['sca']."%");
//    }
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$lancamentos = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container-fluid"><!--todo conteudo-->
    <h3 class="form-cadastro-heading">
        <a href="index.php?pg=Vcaixa_lista" class="text-decoration-none">
            CAIXA
        </a>
    </h3>
    <hr>

    <div class="row">
        <div class="col-md-8">
            <form action="index.php" method="get">
                <div class="input-group mb-3 col-md-9 float-left">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i>Buscar</button>
                    </div>
                    <input name="pg" value="Vcaixa_lista" hidden/>
                    <input type="text" name="sca" id="sca" placeholder="Identificação" autocomplete="off" class="form-control" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                    <select name="scb" id="scb" class="form-control">
                        <option selected="" value="<?php if (isset($_GET['scb']) and $_GET['scb']>0 and is_numeric($_GET['scb'])){ echo $_GET['scb'];}else{ echo 0;} ?>">
                            <?php
                            if (isset($_GET['scb']) and $_GET['scb']>0 and is_numeric($_GET['scb'])){
                                echo fncgethistorico($_GET['scb'])['historico'];
                            }else{
                                echo "Todos os históricos";
                            }
                            ?>
                        </option>
                        <?php
                        foreach(fnchistoricolist() as $item){?>
                            <option value="<?php echo $item['id']; ?>"><?php echo $item['historico']; ?></option>
                        <?php } ?>

                        <option value="0">Todos os históricos</option>
                    </select>

                    <input type="date" name="scc" id="scc" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
                </div>
            </form>

            <script type="text/javascript">
                function selecionaTexto()
                {
                    document.getElementById("sca").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <a href="index.php?pg=Vlancamento_editar2" class="btn btn-block btn-primary mb-2" ><i class="fas fa-level-down-alt"></i> NOVO LANÇAMENTO INDEPENDENTE </a>
        </div>

        <div class="col-md-4">
            <a href='index.php?pg=Vcaixa' class="btn btn-block btn-secondary btn-block mb-2"> BALANÇO</a>
        </div>
    </div>

    <table class="table table-sm table-stripe table-hover table-bordered text-uppercase">
        <thead class="thead-dark">
            <tr>
                <th class="text-center">TIPO</th>
                <th>VALOR</th>
                <th>IDENTIFICAÇÃO</th>
                <th>PREVISÃO/HISTÓRICO</th>
                <th>AÇÕES</th>
            </tr>
        </thead>
        <tfoot>
        <tr class="bg-warning">
            <th colspan="3" class="bg-info text-right"></th>
            <th colspan="3" class="bg-info text-right"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</th>
        </tr>
        </tfoot>



        <tbody>
        <?php
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);
        }
        foreach ($lancamentos as $dado){
            if ($dado['tipo']==1){$tempcor="success";$temptipo="<i class='fas fa-angle-up text-success'></i> Crédito";}
            if ($dado['tipo']==2){$tempcor="danger";$temptipo="<i class='fas fa-angle-down text-danger'></i> Débito";}

        if ($dado['finalizado']==1){
            $cor_back="bg-secondary text-light";
        }else{
            $cor_back="";
        }


            echo "<tr class='{$cor_back}'>";

            echo "<td class='text-center'>";
            echo $temptipo;
            echo " - ".dataRetiraHora($dado['data_ts']);
            echo "</td>";

            echo "<td style='white-space: nowrap;'>R$ ".number_format($dado['valor'],2, ',', ' ')."</td>";

            echo "<td class=''>";
            if($_GET['sca']!="" and isset($_GET['sca'])) {
                $sta = CSA;
                $ccc = strtoupper($dado['identificador']);
                $cc = explode(CSA, $ccc);
                $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                echo $c;
            }else{
                echo strtoupper($dado['identificador']);
            }
            echo "<br>";
            echo $dado['descricao'];
            echo "</td>";



        if ($dado['finalizado']==1){
            echo "<td class='info'>";
        }else {
            if ($dado['previsao'] <= date('Y-m-d')) {
                echo "<td class='text-danger'>";
            } else {
                echo "<td class='text-success'>";
            }
        }
                echo dataBanco2data($dado['previsao']);
        echo "<br>";
                echo fncgethistorico($dado['historico'])['historico'];
            echo "</td>";

            echo "<td class=''>";
            if ($dado['finalizado']==1){
                echo "<i class='fas fa-info-circle' title='{$dado['obs']}'></i> Finalizado em ".dataRetiraHora($dado['finalizado_data']);
                ?>
                <br>
                <div class="btn-group mt-1">

                    <a href='index.php?pg=Vcaixa_lista_comprovante&id=<?php echo $dado['id'];?>' class='btn btn-sm btn-light' target='_blank'>Comprovante</a>

                    <div class="dropdown show">
                        <a class="btn btn-sm btn-info dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-pen"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item bg-info" href="index.php?pg=Vlancamento_editar&id=<?php echo $dado['id']; ?>">Alterar concluir</a>
                        </div>
                    </div>

                    <div class="dropdown show">
                        <a class="btn btn-sm btn-danger dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-trash"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <!--                        <a class="dropdown-item" href="#">Não</a>-->
                            <a class="dropdown-item bg-danger" href="index.php?pg=Vcaixa_lista&aca=caixa_lancamentodelete2&id=<?php echo $dado['id']; ?>">Excluir</a>
                        </div>
                    </div>
                </div>

            <?php
            }else{
                ?>
        <div class="btn-group mt-1">

                    <div class="dropdown show">
                        <a class="btn btn-sm btn-info dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Concluir
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item bg-info" href="index.php?pg=Vlancamento_editar&id=<?php echo $dado['id']; ?>">Concluir</a>
                            <a class="dropdown-item bg-primary" href="index.php?pg=Vcaixa_lista&id=<?php echo $dado['id']; ?>&aca=caixa_lancamentoupdate_rapido">Concluir rápido</a>
                        </div>
                    </div>

                <div class="dropdown show">
                    <a class="btn btn-sm btn-danger dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        APAGAR
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
<!--                        <a class="dropdown-item" href="#">Não</a>-->
                        <a class="dropdown-item bg-danger" href="index.php?pg=Vcaixa_lista&aca=caixa_lancamentodelete2&id=<?php echo $dado['id']; ?>">Excluir</a>
                    </div>
                </div>
        </div>
                <?php
            }
            echo "</td>";

            echo "</tr>";

        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>