<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vcaixa_lista'>";
include_once("includes/topo.php");

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <!-- =============================começa conteudo======================================= -->
            <div class="card mt-2">
                <div class="card-header bg-info text-light">
                    Lançamento independente
                </div>
                <div class="card-body">
                    <form class="form-signin" action="index.php?pg=Vcaixa_lista&aca=caixa_lancamentoinsert2" method="post" id="formx">
                        <div class="row">

                            <input id="fechamento_id" type="hidden" class="" name="fechamento_id" value="0"/>

                            <div class="col-md-4">
                                <label for="historico">HISTÓRICO:</label>
                                <select name="historico" id="historico" class="form-control input-sm" required autofocus>

                                    <option selected value="">
                                        Nada selecionado
                                    </option>
                                    <?php
                                    foreach (fnchistoricolist() as $item) {
                                        ?>
                                        <option value="<?php echo $item['id'];?>">
                                            <?php echo $item['historico']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label for="valor">VALOR:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">R$</span>
                                    </div>
                                    <input autocomplete="off" id="valor" placeholder="" type="number" class="form-control" name="valor" value="" min="0.5" step="any" required />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="">PREVISÃO</label>
                                <div class="input-group">
                                    <input  id="previsao" type="date" class="form-control" name="previsao" value="<?php echo date('Y-m-d');?>" required/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="tipo">TIPO:</label>
                                <select name="tipo" id="tipo" class="form-control input-sm" required>
                                    <option value="1">
                                        Crédito
                                    </option>
                                    <option selected value="2">
                                        Débito
                                    </option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="fonte">CONTA:</label>
                                <select name="fonte" id="fonte" class="form-control input-sm" required>

                                    <?php $temp_conta=fncgetfonte(1);?>
                                    <option selected value="<?php echo $temp_conta['id'];?>">
                                        <?php echo $temp_conta['titular'];?>
                                    </option>
                                    <?php
                                    foreach (fncfontelist() as $item) {
                                        ?>
                                        <option value="<?php echo $item['id'];?>">
                                            <?php echo $item['titular']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <label for="identificador">IDENTIFICAÇÃO:</label>
                                <div class="input-group">
                                    <input  id="identificador" type="text" class="form-control" name="identificador" placeholder="Beneficiário..." value="<?php echo $identificador;?>" required/>
                                </div>
                            </div>


                            <div class="col-md-9">
                                <label for="descricao">DESCRIÇÃO:</label>
                                <div class="input-group">
                                    <input  id="descricao" type="text" class="form-control" name="descricao" value=""/>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <label for="finalizado" class="text-danger">(*)OPÇÃO DE CONCLUIR:</label>
                                <select name="finalizado" id="finalizado" class="form-control input-sm">
                                    <option selected value="0">
                                        Manter em aberto
                                    </option>
                                    <option value="1">
                                        Marcar como finalizado
                                    </option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <input type="submit" name="enviar" id="enviar" class="btn btn-lg btn-success btn-block mt-3" value="SALVAR"/>
                            </div>
                            <script>
                                var formID = document.getElementById("formx");
                                var send = $("#enviar");

                                $(formID).submit(function(event){
                                    if (formID.checkValidity()) {
                                        send.attr('disabled', 'disabled');
                                        send.attr('value', 'AGUARDE...');
                                    }
                                });
                            </script>
                        </div>
                    </form>

                    </p>
                </div>
            </div>


            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3"></div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>