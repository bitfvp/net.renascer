<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de lançamentos-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$final=$_POST['data_final'];
$tipo=$_POST['tipo'];
$fonte=$_POST['fonte'];
$historico=$_POST['historico'];
$finalizado=$_POST['finalizado'];



    $sql = "SELECT * FROM "
        ."rcafe_caixa_lancamentos "
        ."WHERE "
        ."rcafe_caixa_lancamentos.`previsao` >= :inicial ";

            if (isset($_POST['identificacao']) and $_POST['identificacao']!='') {
                $identificacao=$_POST['identificacao'];
                $sql .=" AND identificador LIKE '%$identificacao%' ";
            }

            if ($tipo!=0){
                $sql .="AND tipo = {$tipo} ";
            }

            if ($fonte!=0){
                $sql .="AND fonte = {$fonte} ";
            }

            if ($historico!=0){
                $sql .="AND historico = {$historico} ";
            }
        $sql .="AND finalizado = :finalizado "
        ."AND rcafe_caixa_lancamentos.`previsao` <= :final "
        ."ORDER BY rcafe_caixa_lancamentos.previsao ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":finalizado",$finalizado);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $lancamentos = $consulta->fetchAll();
    $sql=null;
    $consulta=null;


?>
<main class="container">

    <?php
    if ($_GET['cab']==0 or $_GET['cab']==''){
        include_once("../includes/renascer_cab.php");
    }else{
        $cab=fncgetcab($_GET['cab']);
        echo "<div class='row'>";
        echo "<div class='col-10 offset-1 text-center'>";
        echo "<h3 class='mt-1'>".$cab['empresa']."</h3>";
        echo "<h6>".$cab['linha1']."</h6>";
        echo "<h6>".$cab['linha2']."</h6>";
        echo "</div>";
        echo "</div>";

    }
    ?>


    <div class="row">
        <div class="col-12 text-center">
            <h3 class="text-uppercase">RELATÓRIO DE LANÇAMENTOS</h3>
            <h5>
                <?php
                if ($finalizado==0){
                    echo "Em aberto ";
                }
                if ($finalizado==1){
                    echo "Finalizado ";
                }
                ?>
            </h5>
            <h6>Filtros:
                <?php

                if ($tipo!=0){
                    echo "Tipo: ".fncgettipolancamento($tipo)['tipo'].", ";
                }else{
                    echo "Todos os Tipo, ";
                }

                if ($fonte!=0){
                    echo "Conta: ".fncgetfonte($fonte)['titular'].", ";
                }else{
                    echo "Todos as contas, ";
                }

                if ($historico!=0){
                    echo "Histórico: ".fncgethistorico($historico)['historico']." ";
                }else{
                    echo "Todos os históricos ";
                }

                if (isset($identificacao) and $identificacao!='') {
                    echo "Contem: ".$identificacao." ";
                }


                ?>
            </h6>
            <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
        </div>
    </div>


    <table class="table table-sm">
        <thead class="thead-dark">
            <tr>
                <th>TIPO</th>
                <th>VALOR</th>
                <th>PREVISÃO/HISTÓRICO</th>
                <th>IDENTIFICAÇÃO</th>
                <th>CONTA</th>
            </tr>
        </thead>

        <tbody>
        <?php
        $cre=0;
        $deb=0;
        foreach ($lancamentos as $dado){
            if ($dado['tipo']==1){
                $tempcor="success";
                $temptipo="<i class='fas fa-angle-up text-success'></i> Crédito";
            $cre+=$dado['valor'];
            }
            if ($dado['tipo']==2){
                $tempcor="danger";
                $temptipo="<i class='fas fa-angle-down text-danger'></i> Débito";
                $deb+=$dado['valor'];
            }


            echo "<tr>";

            echo "<td class=''>";
            echo $temptipo;
            echo " - ".dataRetiraHora($dado['data_ts']);
            echo "</td>";

            echo "<td class=''>R$ ".number_format($dado['valor'],2, ',', ' ')."</td>";

            if ($dado['finalizado']==1){
                echo "<td class='info'>";
            }else {
                if ($dado['previsao'] <= date('Y-m-d')) {
                    echo "<td class='text-danger'>";
                } else {
                    echo "<td class='text-success'>";
                }
            }
            echo dataBanco2data($dado['previsao']);
            echo "<br>";
            echo fncgethistorico($dado['historico'])['historico'];
            echo "</td>";

            echo "<td class=''>";
            echo $dado['identificador'];
            echo "<br>";
            echo $dado['descricao'];
            echo "</td>";

            echo "<td class=''>";
            echo fncgetfonte($dado['fonte'])['titular'];

        if ($dado['finalizado']==1) {
            echo "<br>";
            echo $dado['obs'] . " " . dataRetiraHora($dado['finalizado_data']);
        }

            echo "</td>";

            echo "</tr>";

        }
        ?>
        </tbody>

    </table>

    <div class="row">
        <?php
        if ($tipo==0 or $tipo==1){//Crédito
            echo " <div class='col-md-3 border'>Créditos ";
            echo "R$ ".number_format($cre,2, ',', ' ');
            echo "</div> ";
        }

        if ($tipo==0 or $tipo==2){//Débito
            echo " <div class='col-md-3 border'>Débitos ";
            echo "R$ ".number_format($deb,2, ',', ' ');
            echo "</div> ";
        }
        ?>
    </div>



    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
        <div class="col-12">
            <strong> Concordo com os dados prescritos acima e dou fé.</strong>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura
            </h4>
        </div>
    </div>


</main>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>