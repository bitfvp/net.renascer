<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar corretor-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="corretorupdate";
    $corretor=fncgetcorretor($_GET['id']);
}else{
    $a="corretorinsert";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vcorretor_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de corretor</h3>
        <hr>
        <div class="row">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $corretor['id']; ?>"/>
            <div class="col-md-12">
                <label for="categoria">Corretor:</label>
                <input type="text" name="corretor" id="corretor" class="form-control" autocomplete="off" placeholder="nome completo" value="<?php echo $corretor['corretor']; ?>"/>
            </div>
            <div class="col-md-6 d-none">
                <label for="corretagem_c">Comissão:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">%</div>
                    </div>
                    <input id="comissao" type="number" autocomplete="off" class="form-control" name="comissao" value="<?php echo $corretor['comissao']; ?>" step="0.1" min="0" max="10" placeholder="use vírgula" />
                </div>
            </div>
            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>