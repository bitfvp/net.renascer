<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        //validação das permissoes
//        if ($allow["allow_4"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id_l']) and is_numeric($_GET['id_l'])){
    $a="lotevalorsave";
    $lote=fncgetlote($_GET['id_l']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vl_lista");
    exit();
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid">
    <div class="row">

        <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header bg-info text-light">
                        Edição de lote
                    </div>
                    <div class="card-body">
                        <form action="<?php echo "index.php?pg=Vl_editarvalor&id_l={$_GET['id_l']}&aca={$a}"; ?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                 <div class="col-md-12">
                                     <input id="id_l" type="hidden" class="txt bradius" name="id_l" value="<?php echo $lote['id']; ?>"/>
                                     <input id="pg" type="hidden" class="txt bradius" name="pg" value="<?php echo $_GET['pg']; ?>"/>

                                     <label for="valor_saca">VALOR POR SACA:</label>
                                     <div class="input-group">
                                         <input autocomplete="off" id="valor_saca" placeholder="" type="text" class="form-control" name="valor_saca" value="<?php echo $lote['valor_saca']; ?>" required />
                                         <div class="input-group-append">
                                             <span class="input-group-text">,00 R$</span>
                                         </div>
                                     </div>
                                     <script>
                                         $(document).ready(function(){
                                             $('#valor_saca').mask('000000', {reverse: true});
                                         });
                                     </script>
                                </div>

                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>


    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>