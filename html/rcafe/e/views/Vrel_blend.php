<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes

    }
}

$page="Relatorio de vendas-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container">
<div class="row">
<div class="col-md-2"></div>
    <div class="col-md-6">
<!-- =============================começa conteudo======================================= -->
        <div class="card">
            <div class="card-header bg-info text-light text-uppercase">
            Relatório de estoque atual
            </div>
            <div class="card-body">

                <form action="index.php?pg=Vrel_blendprint" method="post" target="_blank" name="form1">


                    <div class="form-group">
                        <label for="tipo_cafe">TIPO DE CAFÉ:</label>
                        <select name="tipo_cafe" id="tipo_cafe" class="form-control input-sm" data-live-search="true">
                            <?php
                            foreach (fncprodutoslist() as $item) {
                                ?>
                                <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                    <?php echo $item['nome']; ?>
                                </option>
                                <?php
                            }
                            ?>
                            <option data-tokens="" value="0" selected>
                                TODOS OS TIPO DE CAFÉ
                            </option>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="valor">MOSTRAR SEM VALOR:</label>
                        <select name="valor" id="valor" class="form-control input-sm" data-live-search="true">
                            <option data-tokens="" value="1" selected>
                                SIM
                            </option>
                            <option data-tokens="" value="0">
                                NÃO
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="cata">MOSTRAR SEM CATA:</label>
                        <select name="cata" id="cata" class="form-control input-sm" data-live-search="true">
                            <option data-tokens="" value="1" selected>
                                SIM
                            </option>
                            <option data-tokens="" value="0">
                                NÃO
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="ordem">ORDEM POR:</label>
                        <select name="ordem" id="ordem" class="form-control input-sm" data-live-search="true">
                            <option data-tokens="" value="1" selected>
                                TIPO DE CAFÉ
                            </option>
                            <option data-tokens="" value="2">
                                DATA
                            </option>
                            <option data-tokens="" value="3">
                                VALOR DA SACA
                            </option>
                            <option data-tokens="" value="4">
                                FORNECEDOR
                            </option>
                        </select>
                    </div>


                    <input type="submit"id="btnsub" class="btn btn-lg btn-success btn-block" value="GERAR RELATÓRIO"/>

                </form>
            </div>
        </div>

    </div>
    <div class="col-md-2"></div>
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>