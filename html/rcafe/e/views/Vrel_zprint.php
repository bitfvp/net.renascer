<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de balanço-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicialh=$inicial." 00:00:00";
$final=$_POST['data_final'];
$finalh=$final." 23:59:59";

?>
<main class="container text-uppercase">

    <?php
        include_once("../includes/renascer_cab.php");
    ?>



    <div class="row">
        <div class="col-12 text-center">
            <h3 class="text-uppercase">RELATÓRIO DE BALANÇO</h3>
            <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
        </div>
    </div>

    <?php

    ?>



    <?php

    ?>


    <div class="row">
        <h3>Vendas</h3>

        <div class="col-md-12">Valor em vendas:
        <?php
        $sql = "SELECT "
            ."rcafe_caixa_lancamentos.id, "
            ."rcafe_caixa_lancamentos.data_ts, "
            ."rcafe_caixa_lancamentos.usuario, "
            ."rcafe_caixa_lancamentos.`status`, "
            ."rcafe_caixa_lancamentos.data_off, "
            ."rcafe_caixa_lancamentos.usuario_off, "
            ."rcafe_caixa_lancamentos.fonte, "
            ."rcafe_caixa_lancamentos.fechamento_id, "
            ."rcafe_caixa_lancamentos.tipo, "
            ."rcafe_caixa_lancamentos.valor, "
            ."rcafe_caixa_lancamentos.previsao, "
            ."rcafe_caixa_lancamentos.historico, "
            ."rcafe_caixa_lancamentos.quem_paga, "
            ."rcafe_caixa_lancamentos.identificador, "
            ."rcafe_caixa_lancamentos.descricao, "
            ."rcafe_caixa_lancamentos.obs, "
            ."rcafe_caixa_lancamentos.finalizado, "
            ."rcafe_caixa_lancamentos.finalizado_data, "
            ."rcafe_caixa_lancamentos.codigo_contagem "
            ."FROM "
            ."rcafe_caixa_lancamentos INNER JOIN rcafe_fechamentos ON rcafe_caixa_lancamentos.fechamento_id = rcafe_fechamentos.id "
            ."WHERE "
            ."rcafe_fechamentos.tipo_fechamento = 2 "
            ."AND rcafe_fechamentos.id_venda <> 0 "
            ."AND rcafe_caixa_lancamentos.previsao >= :inicial "
            ."AND rcafe_caixa_lancamentos.previsao <= :final "
            ."ORDER BY rcafe_fechamentos.data_ts ASC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial",$inicial);
        $consulta->bindValue(":final",$final);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $lancamentos = $consulta->fetchAll();
        $sql=null;
        $consulta=null;


        $valortotalvenda=0;
        foreach ($lancamentos as $item){
            $valortotalvenda+=$item['valor'];
        }
        echo "R$ ".number_format($valortotalvenda,2, ',', ' ');
        ?>
        </div>








        <div class="col-md-12">Custos:
        <?php
        $sql = "SELECT * FROM "
            ."rcafe_fechamentos "
            ."WHERE "
            ."rcafe_fechamentos.tipo_fechamento = 2 "
            ."AND rcafe_fechamentos.id_venda <> 0 "
            ."AND rcafe_fechamentos.`data_ts` >= :inicial "
            ."AND rcafe_fechamentos.`data_ts` <= :final "
            ."ORDER BY rcafe_fechamentos.data_ts ASC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial",$inicialh);
        $consulta->bindValue(":final",$finalh);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $fechamentos = $consulta->fetchAll();
        $sql=null;
        $consulta=null;


        foreach ($fechamentos as $item){
//        $item['id_venda'];
            $sql = "SELECT * FROM "
                ."ren_entradas_lotes_saidas "
                ."WHERE id_destino=:id_destino and tipo_saida=1 "
                ."order by ren_entradas_lotes_saidas.data_ts DESC ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindValue(":id_destino", $item['id_venda']);
            $consulta->execute();
            $pesagens = $consulta->fetchAll();
            $sql = null;
            $consulta = null;
            $peso_total=0;
            $totalvalorcusto=0;
            foreach ($pesagens as $dados){
                $lote = fncgetlote($dados["lote"]);
                $peso_total=$peso_total+$dados["peso"];

                $valor_saca=$lote["valor_saca"];
                $subtotalvalor=$valor_saca*($dados["peso"]/60);
                $totalvalorcusto=$totalvalorcusto+$subtotalvalor;
            }
            $sacastotais=$peso_total/60;

            $mediavalor=@($totalvalorcusto/$sacastotais);

        }




        echo "R$ ".number_format($totalvalorcusto,2,',',' ');
        ?>
        </div>


        <div class="col-md-12">Saldo:
        <?php
        $valortotalsaldo=$valortotalvenda-$totalvalorcusto;
        echo "R$ ".number_format($valortotalsaldo,2,',',' ');
        ?>
        </div>
    </div>
    <hr>







    <div class="row">
        <h3>Despesas</h3>
        <div class="col-md-12">
            <?php
            $historicos=fnchistoricodespesalist();

            $itensvalortotal=0;
            foreach (fnchistoricodespesalist() as $item){
                $sql = "SELECT * FROM "
                    ."rcafe_caixa_lancamentos "
                    ."WHERE "
                    ."rcafe_caixa_lancamentos.tipo = 2 "
                    ."AND rcafe_caixa_lancamentos.historico= :historico "
                    ."AND rcafe_caixa_lancamentos.previsao >= :inicial "
                    ."AND rcafe_caixa_lancamentos.previsao <= :final "
                    ."ORDER BY rcafe_caixa_lancamentos.data_ts ASC ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":historico",$item['id']);
                $consulta->bindValue(":inicial",$inicial);
                $consulta->bindValue(":final",$final);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lancamentos = $consulta->fetchAll();
                $sql=null;
                $consulta=null;


                $valor=0;
                foreach ($lancamentos as $dado){
                    $valor+=$dado['valor'];
                    $itensvalortotal+=$valor;
                }
                if ($valor>0){
                    echo "<p>";
                    echo $item['historico'].": R$ ".number_format($valor,2, ',', ' ');
                    echo "</p>";
                }
            }

            ?>

        </div>
        <div class="col-md-12">Total:
        <?php
        echo "R$ ".number_format($itensvalortotal,2,',',' ');
        ?>
        </div>
    </div>





    <hr>
    <div class="row">
        <h3>Outros créditos</h3>
        <div class="col-md-12">
            <?php
            $historicos=fnchistoricodespesalist();

            $itensvalortotal2=0;
            foreach (fnchistoricodespesalist() as $item){
                $sql = "SELECT * FROM "
                    ."rcafe_caixa_lancamentos "
                    ."WHERE "
                    ."rcafe_caixa_lancamentos.tipo = 1 "
                    ."AND rcafe_caixa_lancamentos.historico= :historico "
                    ."AND rcafe_caixa_lancamentos.previsao >= :inicial "
                    ."AND rcafe_caixa_lancamentos.previsao <= :final "
                    ."ORDER BY rcafe_caixa_lancamentos.data_ts ASC ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":historico",$item['id']);
                $consulta->bindValue(":inicial",$inicial);
                $consulta->bindValue(":final",$final);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $lancamentos = $consulta->fetchAll();
                $sql=null;
                $consulta=null;


                $valor=0;
                foreach ($lancamentos as $dado){
                    $valor+=$dado['valor'];
                    $itensvalortotal2+=$valor;
                }
                if ($valor>0){
                    echo "<p>";
                    echo $item['historico']." R$ ".number_format($valor,2, ',', ' ');
                    echo "</p>";
                }
            }

            ?>

        </div>
        <div class="col-md-12">Total:
            <?php
            echo "R$ ".number_format($itensvalortotal2,2,',',' ');
            ?>
        </div>
    </div>
    <hr class="hrgrosso">

    <div class="row">
        <h3>Total geral </h3>
        <div class="col-md-12">
            <?php
            $temptotal=$valortotalsaldo - $itensvalortotal +$itensvalortotal2;
            echo "<h4>";
            echo "R$ ".number_format($temptotal,2,',',' ');
            echo "</h4>";

            if ($temptotal>0){
                echo "<small> (";
                $tempmoeda=number_format($temptotal,2,',','');
                echo ClsTexto::converte($tempmoeda, true, false);
                echo ")</small>";
            }
            ?>
        </div>
    </div>

    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
    </div>
    <br>


</main>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>