<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vhome'>";
include_once("includes/topo.php");

?>

<main class="container-fluid"><!--todo conteudo-->


    <div class="row">

        <?php
        try{
            $date_temp = date("Y-m-d");
            $sql="SELECT * FROM ";
            $sql.="rcafe_caixa_lancamentos ";
            $sql.="WHERE finalizado=0 and status=1 and previsao<:date_temp order by previsao asc";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":date_temp", $date_temp);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $lancamentos=$consulta->fetchAll();
        ?>

        <div class="col-md-12 bg-light">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Lançamentos Vencidos
                </div>
                <div class="card-body">
                    <table class="table table-sm text-uppercase">
                        <thead class="thead-dark">
                        <tr>
                            <th>TIPO</th>
                            <th>VALOR</th>
                            <th>IDENTIFICAÇÃO</th>
                            <th>CONTA</th>
                            <th>PREVISÃO</th>
                            <th>HISTÓRICO</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach ($lancamentos as $dado){
                            if ($dado['tipo']==1){$tempcor="success";$temptipo="<i class='fas fa-angle-up text-success'></i> Crédito";}
                            if ($dado['tipo']==2){$tempcor="danger";$temptipo="<i class='fas fa-angle-down text-danger'></i> Débito";}


                            echo "<tr>";

                            echo "<td class='border-left border-{$tempcor}'>";
                            echo $temptipo;
                            echo "</td>";

                            echo "<td class=''>R$ ".number_format($dado['valor'],2, ',', ' ')."</td>";

                            echo "<td class=''>";
                            echo $dado['identificador'];
                            echo "</td>";

                            echo "<td class=''>";
                            echo fncgetfonte($dado['fonte'])['titular'];
                            echo "</td>";

                            echo "<td class='text-danger'>";
                            if ($dado['finalizado']==1){
                                echo "Finalizado";
                            }else{
                                echo dataBanco2data($dado['previsao']);
                            }
                            echo "</td>";

                            echo "<td class='border-{$tempcor}'>";
                            echo fncgethistorico($dado['historico'])['historico'];
                            echo "</td>";

                            echo "</tr>";

                            echo "<tr>";

                            echo "<td colspan='3' class='border-top-0 border-bottom border-left border-{$tempcor}'>";

                            if ($dado['fechamento_id']>0) {
                                echo "REFERENTE AO FECHAMENTO: ";

                                $fechamento = fncgetfechamento($dado['fechamento_id']);

                                echo "<a href='index.php?pg=Vfechamento2&id={$dado['fechamento_id']}'>";
                                if ($fechamento['tipo_fechamento'] == 1) {
                                    echo "COMPRA";
                                }
                                if ($fechamento['tipo_fechamento'] == 2) {
                                    echo "VENDA";
                                }
                                echo " -- ";
                                echo utf8_encode(strftime('%Y', strtotime("{$fechamento['data_ts']}"))) . "-" . $fechamento['id'] . " --- " . dataRetiraHora($fechamento['data_ts']);
                                echo "</a>";
                            }
                            echo "</td>";

                            echo "<td colspan='3' class='border-top-0 border-bottom border-{$tempcor}'>";
                            echo $dado['descricao'];
                            echo "</td>";

                            echo "</tr>";

                        }
                        ?>
                        </tbody>

                    </table>

                </div>
            </div>
        </div>
    </div>



</main>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>