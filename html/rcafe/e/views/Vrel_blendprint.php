<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes

    }
}

$page="Relatório de estoque-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");


$sql_base='SELECT
ren_entradas_lotes.id,
ren_entradas_lotes.`status`,
ren_entradas_lotes.data_ts,
ren_entradas_lotes.usuario,
ren_entradas_lotes.alterado,
ren_entradas_lotes.romaneio,
ren_entradas_lotes.letra,
ren_entradas_lotes.tipo_cafe,
ren_entradas_lotes.tipo_cafe_entrada,
ren_entradas_lotes.cata,
ren_entradas_lotes.peso_entrada,
ren_entradas_lotes.bags_entrada,
ren_entradas_lotes.localizacao,
ren_entradas_lotes.localizacao_obs,
ren_entradas_lotes.p_bo,
ren_entradas_lotes.bo,
ren_entradas_lotes.peso_atual,
ren_entradas_lotes.bags_atual,
ren_entradas_lotes.obs,
ren_entradas_lotes.responsavel,
ren_entradas_lotes.verificado,
ren_entradas_lotes.valor_saca
FROM
ren_entradas
INNER JOIN ren_entradas_lotes ON ren_entradas_lotes.romaneio = ren_entradas.id
INNER JOIN ren_pessoas ON ren_entradas.fornecedor = ren_pessoas.id ';

$sql_where=' where ren_entradas_lotes.peso_atual > 0 ';
if (isset($_POST['tipo_cafe']) and is_numeric($_POST['tipo_cafe']) and $_POST['tipo_cafe']>0){
    //
    $sql_where.= 'and ren_entradas_lotes.tipo_cafe='.$_POST['tipo_cafe'].' ';
}

if ($_POST['valor']!=1){
    $sql_where.= " and ren_entradas_lotes.valor_saca>0 ";
}
if ($_POST['cata']!=1){
    $sql_where.= " and ren_entradas_lotes.cata>0 ";
}

$temp_ordem=" ORDER BY ";
if ($_POST['ordem']=='1'){//tipo cafe
    $temp_ordem.='ren_entradas_lotes.tipo_cafe ASC, ren_entradas_lotes.data_ts ASC';
    $iifo='tipo de café';
}
if ($_POST['ordem']=='2'){//DATA
    $temp_ordem.='ren_entradas_lotes.data_ts ASC';
    $iifo='data de entrada do lote';
}
if ($_POST['ordem']=='3'){//VALOR DA SACA
    $temp_ordem.='ren_entradas_lotes.valor_saca ASC, ren_entradas_lotes.data_ts ASC';
    $iifo='Valor da saca';
}
if ($_POST['ordem']=='4'){//FORNECEDOR
    $temp_ordem.='ren_pessoas.nome ASC, ren_entradas_lotes.data_ts ASC';
    $iifo='fornecedor';
}


    $sql = $sql_base.$sql_where.$temp_ordem;

    global $pdo;
    $consulta = $pdo->prepare($sql);

    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $lotes = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>
<div class="container-fluid">
    <?php
    if (isset($_POST['tipo_cafe']) and is_numeric($_POST['tipo_cafe']) and $_POST['tipo_cafe']>0){
        $pprod=fncgetprodutos($_POST['tipo_cafe']);

        echo "<h3 class='text-uppercase my-0'>".$pprod['nome']."</h3>";
    }else{
        echo "<h3 class='text-uppercase my-0'>Todos os tipos de café</h3>";
    }

    if ($_POST['valor']!=1){
        echo "<h6 class='text-uppercase my-0'>Não mostrar sem valor</h6>";
    }
    if ($_POST['cata']!=1){
        echo "<h6 class='text-uppercase my-0'>Não mostrar sem cata</h6>";
    }

    echo "<h5 class='text-uppercase my-0'>ordenado por: ".$iifo."</h5>";

    ?>
    <h5>Gerado em: <?php echo datahoraBanco2data(dataNow());?></h5>


    <table class="table table-bordered table-hover table-sm">
        <thead>
        <th>PRODUTO</th>
        <th>CATA</th>
        <th>FORNECEDOR</th>
        <th>LOTE</th>
        <th>VOLUMES</th>
        <th>VALOR/SACA</th>
        <th>BO</th>

        </thead>
        <tbody>
        <?php
        $infovolumes=0;
        $infovalor=0;
        foreach ($lotes as $lote){
            $entrada=fncgetentrada($lote['romaneio']);
            $cordalinha = "  ";
                if ($lote['p_bo']==1){
                    $cordalinha = " text-dark bg-warning ";
                }
            $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

            $letra=fncgetletra($lote["letra"]);

            if ($lote["cata"]>0){
                $cata=$lote["cata"]."%";
            }else{
                $cata="N.D.";
            }

            $valor_saca = $lote["valor_saca"];

            $peso_atual = $lote["peso_atual"];
            $sacas_atual=$peso_atual/60;


            $valortemp=($valor_saca/60)*$peso_atual;

            $infovolumes=$infovolumes+$sacas_atual;
            $infovalor=$infovalor+$valortemp;

            echo "<tr class='{$cordalinha}'>";
            echo "<td><small>".fncgetprodutos($lote['tipo_cafe'])['abrev']."</small></td>";
            echo "<td><small>".$cata."</small></td>";
            echo "<td><small>".strtoupper(fncgetpessoa($entrada['fornecedor'])['nome'])."</small></td>";
            echo "<td style='white-space: nowrap;'><small>".$romaneio_tipo.$entrada['romaneio']."-".$letra." ".dataRetiraHora($lote['data_ts'])."</small></td>";
            echo "<td><small>".number_format($sacas_atual, 2, ',', '.')."v,   ".$peso_atual."kg </small></td>";
            echo "<td class='text-center'>R$ ".number_format($valor_saca,2,',',' ')."</td>";

            echo "<td>".$lote["bo"]."</td>";
            echo "</tr>";
        }
        $infovalor1=@($infovalor/$infovolumes);
        $infovalor1=number_format($infovalor1, 2, ',', ' ');
        $infovalor2=$infovalor;
        $infovalor2=number_format($infovalor2, 2, ',', ' ');
        ?>
        </tbody>
    </table>


    <table class="table table-bordered table-hover table-sm">
        <tbody>
        <tr>
            <td>TOTAL EM VOLUMES: <strong><?php echo number_format($infovolumes,1,',',' '); ?></strong></td>
            <td>MÉDIA POR VOLUMES: R$ <strong><?php echo $infovalor1; ?></strong></td>
            <td>TOTAL APROXIMADO: R$ <strong><?php echo $infovalor2; ?></strong></td>
        </tr>
        </tbody>
    </table>


    <?php
    $sql = "SELECT sum(`peso_atual`) FROM ren_entradas_lotes ".$sql_where;
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $estoquetudo = $consulta->fetch();
    ?>
    <table class="table table-sm text-left table-responsive table-bordered">
        <thead>
        <tr>
            <td>TIPO</td>
            <td>QUANTIDADE</td>
            <td>CATA MÉDIA</td>
            <td>VALOR MÉDIO</td>
            <td>TOTAL</td>
        </tr>
        </thead>
        <tr class="">
            <td>CAFÉ EM ESTOQUE</td>
            <td><?php
                $sacas=$estoquetudo[0]/60;
                $sacas=number_format($sacas, 1, ',', ' ');
                $peso=number_format($estoquetudo[0],1,',',' ')." Kg";
                echo $sacas." saca(s) ou ".$peso;
                ?></td>
        </tr>

        <?php
        ///======
        if (isset($_POST['tipo_cafe']) and is_numeric($_POST['tipo_cafe']) and $_POST['tipo_cafe']>0){
            $sql = "SELECT * FROM ren_produtos where id=".$_POST['tipo_cafe']." ORDER BY nome";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $produtosx = $consulta->fetchAll();
            $sql=null;
            $consulta=null;
        }else{
            $sql = "SELECT * FROM ren_produtos ORDER BY nome";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $produtosx = $consulta->fetchAll();
            $sql=null;
            $consulta=null;
        }

        $sql_where='';
        if ($_POST['valor']!=1){
            $sql_where.= " and ren_entradas_lotes.valor_saca>0 ";
        }
        if ($_POST['cata']!=1){
            $sql_where.= " and ren_entradas_lotes.cata>0 ";
        }

        foreach ($produtosx as $produtos){


            $sql = "SELECT * FROM ren_entradas_lotes WHERE status=1 and peso_atual<>0 and tipo_cafe=? ".$sql_where;
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$produtos['id']);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $lotes = $consulta->fetchAll();

            $infovolumes=0;
            $infovalor=0;
            $infocata=0;
            foreach ($lotes as $lote){
                $sacas_atual=$lote["peso_atual"]/60;
                $infocata+=$sacas_atual*$lote["cata"];
                $valortemp=($lote["valor_saca"]/60)*$lote["peso_atual"];

                $infovolumes=$infovolumes+$sacas_atual;
                $infovalor=$infovalor+$valortemp;
            }
            $infocata=@($infocata/$infovolumes);
            $infovalor1=@($infovalor/$infovolumes);
            $infovalor1=number_format($infovalor1, 2, ',', ' ');
            $infovalor2=$infovalor;
            $infovalor2=number_format($infovalor2, 2, ',', ' ');

            if ($infovolumes==0 or $infovolumes==null or $infovolumes=="" or !is_numeric($infovolumes)){
                $exib= " d-none";
            }else{
                $exib= " ";
            }

            echo "<tr class='{$exib}'>";
            echo "<td>";
            echo $produtos['nome'];
            echo "</td>";
            echo "<td>";
            echo number_format($infovolumes,1,',',' ')." saca(s) ";
            echo "</td>";

            echo "<td>";
            echo number_format($infocata,0)."% ";
            echo "</td>";

            echo "<td>R$ ";
            echo $infovalor1;
            echo "</td>";
            echo "<td>R$ ";
            echo $infovalor2;
            echo "</td>";
            echo "</tr>";

        }
        ?>

    </table>

</div>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>