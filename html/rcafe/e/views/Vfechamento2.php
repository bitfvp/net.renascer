<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_1"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vfechamento_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $fechamento=fncgetfechamento($_GET['id']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vfechamento_lista");
    exit();
}

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid">
    <div class="row">

        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Fechamento
                </div>
                <div class="card-body">
                    <a href="index.php?pg=Vfechamento&id=<?php echo $_GET['id'];?>" class="btn btn-warning btn-block"><i class="fas fa-arrow-left"></i> IR PARA DETALHES </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <header>
                                NEGOCIAÇÃO DO TIPO:
                                <strong class="text-info text-uppercase">
                                    <?php
                                    $temp_negociacao="";
                                    if ($fechamento['tipo_fechamento']==1){
                                        $temp_negociacao.="COMPRA";
                                    }
                                    if ($fechamento['tipo_fechamento']==2){
                                        $temp_negociacao.="VENDA";
                                    }
                                    if ($fechamento['tipo_fechamento']==3){
                                        $temp_negociacao.="CORRETAGEM";
                                    }
                                    $temp_negociacao.=" -- NR:";
                                    $temp_negociacao.=utf8_encode(strftime('%Y', strtotime("{$fechamento['data_ts']}")))."-".$fechamento['nr'];
                                    echo $temp_negociacao."<br>";


                                    if (strlen($fechamento['ordem_compra'])>0){
                                        echo "<p class='text-dark mb-1'>ORDEM DE COMPRA EXTERNA: ".strtoupper($fechamento['ordem_compra'])."</p>";
                                    }

                                    if ($fechamento['id_venda']!=0){
                                        $venda=fncgetvenda($fechamento['id_venda']);
                                        echo "<small class='text-dark'>".utf8_encode(strftime('%Y', strtotime("{$venda['data_ts']}")))."-".$venda['id']." / ".fncgetpessoa($venda['cliente'])['nome']. " / CONTRATO:".$venda['prefixo']."-".$venda['contrato']." ".$venda['descricao']."</small><br>";
                                    }else{
                                        echo "";
                                    }
                                    echo datahoraBanco2data($fechamento['data_ts']);
                                    ?>
                                </strong>
                            </header>

                            <h6>
                                VENDEDOR:
                                <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['vendedor'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                                COMPRADOR:
                                <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['comprador'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                                LOCAL DE DESCARGA:
                                <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['descarga'])['nome']); ?>&nbsp;&nbsp;</strong><br>

                                <hr>

                                <?php
                                try{
                                    $sql="SELECT * FROM ";
                                    $sql.="rcafe_fechamentos_lotes ";
                                    $sql.="WHERE id_fechamento=:id and status=1";
                                    global $pdo;
                                    $consulta=$pdo->prepare($sql);
                                    $consulta->bindValue(":id", $_GET['id']);
                                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                }catch ( PDOException $error_msg){
                                    echo 'Erroff'. $error_msg->getMessage();
                                }
                                $lotes=$consulta->fetchAll();

                                $sum=0;
                                foreach ($lotes as $dados){
                                    $sum=$sum+($dados['sacas']*$dados['preco']);
                                }
                                ?>


                                DESCRIÇÃO:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['descricao']); ?>&nbsp;&nbsp;</strong><br>
                                CONDIÇÃO DE PAGAMENTO:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['condicao_pag']); ?>&nbsp;&nbsp;</strong><br>
                                FORMA DE PAGAMENTO:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['forma_pag']); ?>&nbsp;&nbsp;</strong><br>
                                CONDIÇÃO DE ENTREGA:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['condicao_entrega']); ?>&nbsp;&nbsp;</strong><br>
                                PRAZO DE ENTREGA:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['prazo_entrega']); ?>&nbsp;&nbsp;</strong><br>

                                CORRETOR:
                                <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetcorretor($fechamento['corretor'])['corretor']); ?>&nbsp;&nbsp;</strong><br>
                                OBSERVAÇÃO:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['observacao']); ?>&nbsp;&nbsp;</strong><br>
                            </h6>


                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-6">
                            <a href="index.php?pg=Vfechamento_editar&id=<?php echo $_GET['id'] ?>" title="Editar entrada" class="btn btn-primary btn-block">EDITAR FECHAMENTO</a>
                        </div>

                        <div class="col-md-6">
                            <a href="index.php?pg=Vfechamento_print1&id=<?php echo $_GET['id']; ?>" target="_blank" class="btn btn-info btn-block">Impressão de comprovante</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

<?php
    try{
        $sql="SELECT * FROM ";
        $sql.="rcafe_caixa_lancamentos ";
        $sql.="WHERE fechamento_id=:id and status=1 order by previsao asc";
        global $pdo;
        $consulta=$pdo->prepare($sql);
        $consulta->bindValue(":id", $_GET['id']);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $lancamentos=$consulta->fetchAll();
    ?>

        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Lançamentos de caixa
                </div>
                <div class="card-body">
                    <table class="table table-sm text-uppercase">
                        <thead class="thead-dark">
                            <tr>
                                <th>TIPO</th>
                                <th class="text-right">VALOR</th>
                                <th>PREVISÃO</th>
                                <th>IDENTIFICAÇÃO</th>
                                <th>AÇÕES</th>
                            </tr>
                        </thead>

                        <?php
                        $valor_acumulado=0;
                        foreach ($lancamentos as $dado){
                            if ($dado['tipo']==1){$tempcor="success";$temptipo="<i class='fas fa-angle-up text-success'></i> Crédito";}
                            if ($dado['tipo']==2){$tempcor="danger";$temptipo="<i class='fas fa-angle-down text-danger'></i> Débito";}

                            if ($dado['finalizado']==1){
                                $cor_back="bg-secondary text-light";
                            }else{
                                $cor_back="";
                            }

                            $detalhes="";

                            $detalhes.=$dado['descricao']." ";






                            echo "<tr class='{$cor_back}'>";

                            echo "<td class=''>";
                            echo $temptipo;
                            echo " - ".dataRetiraHora($dado['data_ts']);
                            echo " <i class='fas fa-info-circle text-info ' title='{$detalhes}'></i>";
                            echo "</td>";

                            echo "<td class='text-right'>R$ ".number_format($dado['valor'],2, ',', ' ')."</td>";

                            if ($dado['finalizado']==1){
                                echo "<td class='info'>";
                            }else {
                                if ($dado['previsao'] <= date('Y-m-d')) {
                                    echo "<td class='text-danger'>";
                                } else {
                                    echo "<td class='text-success'>";
                                }
                            }
                            echo dataBanco2data($dado['previsao']);
                            echo "<br>";
                            echo fncgethistorico($dado['historico'])['historico'];
                            echo "</td>";

                            echo "<td class=''>";
                                echo substr($dado['identificador'], 0, 37);
                            echo ".</td>";

                            echo "<td>";
                            if ($dado['finalizado']==1){
                                echo "";
                            }else{
                                ?>

                                <div class="dropdown show">
                                    <a class="btn btn-sm btn-danger dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        EXCLUIR
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">Não</a>
                                        <a class="dropdown-item bg-danger" href="index.php?pg=Vfechamento2&aca=caixa_lancamentodelete&id=<?php echo $_GET['id']; ?>&tabela_id=<?php echo $dado['id']; ?>">Apagar</a>
                                    </div>
                                </div>

                                <?php
                            }
                            echo "</td>";

                            echo "</tr>";

                            $valor_acumulado+=$dado['valor'];
                        }
                        ?>


                    </table>
                    <hr class="hrgrosso">
                    <form class="form-signin" action="index.php?pg=Vfechamento2&id=<?php echo $_GET['id']; ?>&aca=caixa_lancamentoinsert" method="post" id="formx">
                        <div class="row">
                            <?php
                            if ($fechamento['tipo_fechamento']==1){//compra
                                $f_t="Débito";
                                $f_t_code=2;
                                $f_t_h=1;
                                $identificador=strtoupper(substr(fncgetpessoa($fechamento['comprador'])['nome'], 0, 20)) . " -> " .strtoupper(substr(fncgetpessoa($fechamento['vendedor'])['nome'], 0, 20));
                            }
                            if ($fechamento['tipo_fechamento']==2){//venda
                                $f_t="crédito";
                                $f_t_code=1;
                                $f_t_h=2;
                                $identificador=strtoupper(substr(fncgetpessoa($fechamento['comprador'])['nome'], 0, 20)) . " -> " .strtoupper(substr(fncgetpessoa($fechamento['vendedor'])['nome'], 0, 20));
                            }
                            ?>

                            <input id="fechamento_id" type="hidden" class="" name="fechamento_id" value="<?php echo $fechamento['id']; ?>"/>
                            <input id="historico" type="hidden" class="" name="historico" value="<?php echo $f_t_h; ?>"/>

                            <div class="col-md-12">
                                <label for="identificador">IDENTIFICAÇÃO:</label>
                                <div class="input-group">
                                    <input  id="identificador" type="text" class="form-control" name="identificador" value="<?php echo $identificador;?>" required/>
                                </div>
                            </div>

                            <div class="col-md-6 d-none">
                                <label for="tipo">TIPO:</label>
                                <select name="tipo" id="tipo" class="form-control input-sm" data-live-search="true" required>
                                    <option selected data-tokens="" value="<?php echo $f_t_code;?>">
                                    <?php echo $f_t;?>
                                    </option>
                                    <option data-tokens="" value="1">
                                    Crédito
                                    </option>
                                    <option data-tokens="" value="2">
                                    Débito
                                    </option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="fonte">CONTA:</label>
                                <select name="fonte" id="fonte" class="form-control input-sm" data-live-search="true" required>


                                    <?php $temp_conta=fncgetfonte(1);?>
                                    <option selected data-tokens="" value="<?php echo $temp_conta['id'];?>">
                                        <?php echo $temp_conta['titular'];?>
                                    </option>
                                    <?php
                                    foreach (fncfontelist() as $item) {
                                        ?>
                                        <option data-tokens="" value="<?php echo $item['id'];?>">
                                            <?php echo $item['titular']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="valor">VALOR:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">R$</span>
                                    </div>
                                    <input autocomplete="off" id="valor" placeholder="" type="number" class="form-control" name="valor" value="<?php $tempp = $sum - $valor_acumulado; if ($tempp<=0){$tempp=0;} echo number_format($tempp,2,".","");?>" min="0.05" step="any" required />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="">PREVISÃO</label>
                                <div class="input-group">
                                    <input  id="previsao" type="date" class="form-control" name="previsao" value="<?php echo date('Y-m-d');?>" required/>
                                </div>
                            </div>


                            <div class="col-md-8">
                                <label for="descricao">DESCRIÇÃO:</label>
                                <div class="input-group">
                                    <input  id="descricao" type="text" class="form-control" name="descricao" value="Ref. <?php echo $temp_negociacao?>"/>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <input type="submit" name="enviar" id="enviar" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                            </div>
                            <script>
                                var formID = document.getElementById("formx");
                                var send = $("#enviar");

                                $(formID).submit(function(event){
                                    if (formID.checkValidity()) {
                                        send.attr('disabled', 'disabled');
                                        send.attr('value', 'AGUARDE...');
                                    }
                                });
                            </script>
                        </div>
                    </form>

                    <hr>
                    <?php if ($valor_acumulado<$sum){$tempcor="danger";}else{$tempcor="success";}?>
                    <h6 class="form-cadastro-heading text-<?php echo $tempcor;?>">VALOR PROJETADO NO CAIXA: <BR>
                        R$ <?php echo number_format($valor_acumulado,2, ',', ' ');?>   /   R$ <?php echo number_format($sum,2, ',', ' ');?></h6>

                </div>
            </div>

        </div>


    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>