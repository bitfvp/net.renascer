<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_9"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="ROTEIRO-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
?>
<body>
<main class="container">
    <?php
    $sql = "SELECT * \n"
        . "FROM rcafe_fechamentos \n"
        . "WHERE ((rcafe_fechamentos.imprimir)=1) order by data_ts";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $dados = $consulta->fetchAll();
    $quantrows = $consulta->rowCount();
    $sql=null;
    $consulta=null;
    ?>
    <h4>ROTEIRO DE COLETA DE PRODUTO</h4>
    <?php
    $y=0;$x=0;$fol=1;
    echo date("d/m/Y");
    $roteiro = array();
    foreach($dados as $dado){
        ?>

        <hr class="hrgrosso">
        <?php $vendedor=fncgetpessoa($dado['vendedor']);?>
        <h3>VENDEDOR/PRODUTOR</h3>
        <div class="row ml-1">
            <div class="col-6 border text-uppercase">
                NOME: <strong><?php echo strtoupper($vendedor['nome']); ?></strong>
            </div>
            <div class="col-6 border text-uppercase">
                BAIRRO: <strong><?php echo strtoupper($vendedor['bairro']); ?></strong>
            </div>
            <div class="col-6 border text-uppercase">
                ENDEREÇO: <strong><?php echo strtoupper($vendedor['endereco'])."  ".$vendedor['numero']; ?></strong>
            </div>
            <div class="col-6 border text-uppercase">
                COMPLEMENTO: <strong><?php echo strtoupper($vendedor['complemento'])."  ".$vendedor['numero']; ?></strong>
            </div>

            <div class="col-4 border text-uppercase">
                CIDADE: <strong><?php echo strtoupper($vendedor['cidade']); ?></strong>
            </div>
            <div class="col-8 border text-uppercase">
                INFO.: <strong><?php echo $dado['descricao']; ?></strong>
            </div>

        </div>


        <h3>ESPECIFICAÇÃO DA NEGOCIAÇÃO</h3>
        <div class="row">
            <div class="col-12 text-uppercase">
                <table class="table table-sm table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <td>DESCRIÇÃO</td>
                        <td>SACAS</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $sum=0;

                    try{
                        $sql="SELECT * FROM ";
                        $sql.="rcafe_fechamentos_lotes ";
                        $sql.="WHERE id_fechamento=:id and status=1";
                        global $pdo;
                        $consulta=$pdo->prepare($sql);
                        $consulta->bindValue(":id", $dado['id']);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    }catch ( PDOException $error_msg){
                        echo 'Erroff'. $error_msg->getMessage();
                    }
                    $lotes=$consulta->fetchAll();

                    $sum=0;
                    foreach ($lotes as $dados){
                        ?>
                        <tr>
                            <td><?php echo $dados['descricao']; ?></td>
                            <td><?php echo $dados['sacas']; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

        <label for="data_entrega" class="border-left border-bottom pl-2"><h4>Retirada:____/____/______ Assinatura:____________________________________</h4></label>
        <label for="data_entrega" class="border-left border-bottom pl-2"><h4>Obs.:_____________________________________________________________________</h4></label>


        <?php
    }
    ?>
</main>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>