<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");

$sql_base='SELECT '
    .'ren_vendas.id, '
    .'ren_vendas.`status`, '
    .'ren_vendas.data_ts, '
    .'ren_vendas.usuario, '
    .'ren_vendas.prefixo, '
    .'ren_vendas.contrato, '
    .'ren_vendas.descricao, '
    .'ren_vendas.cliente, '
    .'ren_vendas.transportadora, '
    .'ren_vendas.nota_fiscal, '
    .'ren_vendas.retorno_de_estoque '
    .'FROM ren_vendas '
    .'INNER JOIN ren_pessoas ON ren_vendas.cliente = ren_pessoas.id ';

$sql_where='where ren_vendas.id <> 0 ';

if (isset($_GET['sca']) and $_GET['sca']!='') {
    $sql_where .="AND ren_pessoas.nome LIKE '%".$_GET['sca']."%' ";
}

if (isset($_GET['scb']) and  is_numeric($_GET['scb']) and $_GET['scb']!=0) {
    $sql_where .="AND ren_vendas.contrato LIKE '%".$_GET['scb']."%' ";
}

$tempinicio=" 00:00:01";
$tempfinal=" 23:59:59";
if (isset($_GET['scc']) and $_GET['scc']!=0 and $_GET['scc']!='') {
    $inicial=$_GET['scc'].$tempinicio;
    $final=$_GET['scc'].$tempfinal;
}else{

    if ((isset($_GET['sca']) and $_GET['sca']!='') or (isset($_GET['scb']) and $_GET['scb']!='')){
        //tempo todo
        $inicial="2021-01-01".$tempinicio;
        $final=date("Y-m-d").$tempfinal;
    }else{
        //apenas dia anterior
        $inicial=date("Y-m-d",strtotime("-60 days")).$tempinicio;
        $final=date("Y-m-d").$tempfinal;
    }
}
$sql_where .=" AND ((ren_vendas.data_ts)>=:inicial) And ((ren_vendas.data_ts)<=:final) ";

$sql_ordem ="order by ren_vendas.data_ts DESC LIMIT 0,50 ";


try{
    $sql = $sql_base.$sql_where.$sql_ordem;



    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container-fluid">
    <h3 class="form-cadastro-heading">
        <a href="index.php?pg=Vv_lista" class="text-decoration-none text-dark">Vendas</a>
    </h3>
    <hr>

    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
            </div>
            <input name="pg" value="Vv_lista" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Cliente..." value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <input type="number" name="scb" id="scb" autofocus="true" autocomplete="off" class="form-control" placeholder="Contrato..." value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
            <input type="date" name="scc" id="scc" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
        </div>
    </form>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>


    <table class="table table-sm table-stripe table-hover table-condensed">
        <thead class="thead-dark">
        <tr>
            <th scope="col" class="text-center">#</th>
            <th scope="col"><small>CONTRATO</small></th>
            <th scope="col"><small>CLIENTE</small></th>
            <th scope="col"><small>TRANSPORTADORA</small></th>
            <th scope="col"><small>DATA</small></th>
            <th scope="col" class="text-center"><small>INFO</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="5" >
            </th>
            <td colspan="4"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);
        }
        foreach ($entradas as $dados){
            $ve_id = $dados["id"];
            $prefixo = $dados["prefixo"];
            $contrato = $dados["contrato"];
            $cliente = fncgetpessoa($dados["cliente"])['nome'];
            $transportadora = fncgetpessoa($dados["transportadora"])['nome'];
            $data_pedido = $dados["data_pedido"];
            $data_ts=datahoraBanco2data($dados["data_ts"]);
            $usuario = $dados["usuario"];

            if ($dados["status"]==0 or $dados["status"]==""){
                $status= "<i class='fas fa-lock fa-2x' title='venda finalizada'></i>";
            }else{
                $status= "";
            }


            try{
                $sql = "SELECT * FROM "
                    ."ren_vendas_pesagens "
                    ."WHERE ren_vendas_pesagens.venda=:venda "
                    ."order by ren_vendas_pesagens.data_ts DESC ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":venda", $ve_id);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }
            $pesagens = $consulta->fetchAll();
            $pesagens_quant = $consulta->rowCount();
            $sql = null;
            $consulta = null;


            ?>

            <tr id="<?php echo $ve_id;?>" class="bg-info">
                <th scope="row" id="">
                    <small style='white-space: nowrap;'>
                        <?php echo utf8_encode(strftime('%Y', strtotime("{$dados['data_ts']}")))."-".$dados['id']; ?>
                    </small>
                </th>
                <th scope="row" id="">
                        <?php echo $prefixo."-".$contrato; ?>
                </th>
                <td><?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $ccc = strtoupper($cliente);
                        $cc = explode(CSA, $ccc);
                        $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                        echo $c;
                    }else{
                        echo strtoupper($cliente);
                    }
                    ?></td>
                <td><?php echo strtoupper($transportadora); ?></td>
                <td><?php echo $data_ts; ?></td>
                <td><?php echo $status; ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">


                        <?php
                        if ($pesagens_quant>0){?>
                            <div class="dropdown show">
                                <a class="btn btn-light btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-balance-scale"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <?php
                                    $temp_p=0;
                                    foreach ($pesagens as $dados){
                                        $pe_id = $dados["id"];
                                        $temp_p++;
                                        ?>
                                        <?php if ($dados["datahora_saida"]!="" or $dados["datahora_saida"]!=0){?>
                                            <a href="index.php?pg=Vv_print1&id_v=<?php echo $ve_id ?>&id_p=<?php echo $pe_id; ?>" target="_blank"  class="dropdown-item bg-success">
                                                Pesagem <?php echo $temp_p;?>
                                            </a>
                                        <?php } ?>
                                        <?php
                                    } ?>
                                </div>
                            </div>
                            <?php
                        }?>


                        <a href="index.php?pg=Vvvv_print1&id_v=<?php echo $ve_id; ?>" target="_blank" title="comprovante de carregamento" class="btn btn-sm btn-dark fas fa-print">
                            COM VALORES
                        </a>
                        <a href="index.php?pg=Vvvv_print2&id_v=<?php echo $ve_id; ?>" target="_blank" title="comprovante de carregamento" class="btn btn-sm btn-secondary fas fa-print">
                            NORMAL
                        </a>

                    </div>

                </td>

            </tr>

        <?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>