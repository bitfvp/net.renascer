<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar historico-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="historicoupdate";
    $historico=fncgethistorico($_GET['id']);
}else{
    $a="historicoinsert";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vhistorico_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de histórico</h3>
        <hr>
        <div class="row">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $historico['id']; ?>"/>
            <div class="col-md-12">
                <label for="historico">Histórico:</label>
                <input type="text" name="historico" id="historico" class="form-control" autocomplete="off" placeholder="nome completo" value="<?php echo $historico['historico']; ?>"/>
            </div>

            <div class="col-md-6">
                <label for="despesa">Despesa:</label>
                <select name="despesa" id="despesa" class="form-control" >
                    <option selected="" value="<?php if($historico['despesa']==""){$z=0; echo $z;}else{ echo $historico['despesa'];} ?>">
                        <?php
                        if($historico['despesa']==0){echo"NÃO";}
                        if($historico['despesa']==1){echo"SIM";} ?>
                    </option>
                    <option value="0">NÃO</option>
                    <option value="1">SIM</option>
                </select>
            </div>

            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>