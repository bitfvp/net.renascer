<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        //validação das permissoes
//        if ($allow["allow_9"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $fechamento=fncgetfechamento($_GET['id']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vfechamento_lista");
    exit();
}
?>

<div class="container">
    <?php
    if ($_GET['cab']==0 or $_GET['cab']==''){
        include_once("../includes/renascer_cab.php");
    }else{
        $cab=fncgetcab($_GET['cab']);
        echo "<div class='row'>";
            echo "<div class='col-10 offset-1 text-center'>";
                echo "<h3 class='mt-1'>".$cab['empresa']."</h3>";
                echo "<h6>".$cab['linha1']."</h6>";
                echo "<h6>".$cab['linha2']."</h6>";
            echo "</div>";
        echo "</div>";

    }

    ?>










    <div class="row">
        <div class="col-8">
            <h3 class="text-uppercase">CONFIRMAÇÃO DE NEGOCIAÇÃO</h3>
            <?php
            if (strlen($fechamento['ordem_compra'])>0){
                $ordem_compra = strtoupper($fechamento['ordem_compra']);
                echo "<h5 class=''>ORDEM DE COMPRA EXTERNA: <strong>{$ordem_compra}</strong></h5>";
            }
            ?>
        </div>
        <div class="col-4 text-right">
            <h5>DATA: <strong><?php echo datahoraBanco2data($fechamento['data_ts']); ?></strong></h5>
            <h5 class="">NR: <strong><?php echo utf8_encode(strftime('%Y', strtotime("{$fechamento['data_ts']}")))."-".$fechamento['nr']; ?></strong></h5>
        </div>
    </div>

        <hr class="hrgrosso">
    <?php $vendedor=fncgetpessoa($fechamento['vendedor']);?>
        <h3>VENDEDOR/PRODUTOR</h3>
            <div class="row">
                <div class="col-4 border text-uppercase">
                    NOME: <strong><?php echo strtoupper($vendedor['nome']); ?></strong>
                </div>
                <div class="col-4 border text-uppercase">
                    CONTA: <strong><?php echo strtoupper($vendedor['bancario']); ?></strong>
                </div>
                <div class="col-4 border text-uppercase">
                    ENDEREÇO: <strong><?php echo strtoupper($vendedor['endereco'])."  ".$vendedor['numero']; ?></strong>
                </div>
                <div class="col-4 border text-uppercase">
                    BAIRRO: <strong><?php echo strtoupper($vendedor['bairro']); ?></strong>
                </div>
                <div class="col-4 border text-uppercase">
                    CIDADE: <strong><?php echo strtoupper($vendedor['cidade']); ?></strong>
                </div>
                <div class="col-4 border text-uppercase">
                    CEP: <strong><?php echo strtoupper($vendedor['cep']); ?></strong>
                </div>
                <div class="col-3 border text-uppercase">
                    CPF: <strong>
                        <?php
                        if($vendedor['cpf']!="" and $vendedor['cpf']!=0) {
                            echo "<span class=''>";
                            echo mask($vendedor['cpf'],'###.###.###-##');
                            echo "</span>";
                        }else{
                            echo "<span class=''>";
                            echo "[---]";
                            echo "</span>";
                        }
                        ?>
                    </strong>
                </div>
                <div class="col-3 border text-uppercase">
                    RG: <strong><?php echo strtoupper($vendedor['rg']); ?></strong>
                </div>
                <div class="col-3 border text-uppercase">
                    CNPJ: <strong>
                        <?php
                        if($vendedor['cnpj']!="" and $vendedor['cnpj']!=0) {
                            echo "<span class=''>";
                            echo mask($vendedor['cnpj'],'##.###.###/####-##');
                            echo "</span>";
                        }else{
                            echo "<span class=''>";
                            echo "[---]";
                            echo "</span>";
                        }
                        ?>
                    </strong>
                </div>
                <div class="col-3 border text-uppercase">
                    I.E.: <strong>
                        <?php
                        if($vendedor['insc_est']!="" and $vendedor['insc_est']!=0) {
                            echo "<span class=''>";
                            echo mask($vendedor['insc_est'],'###############');
                            echo "</span>";
                        }else{
                            echo "<span class=''>";
                            echo "[---]";
                            echo "</span>";
                        }
                        ?>
                    </strong>
                </div>
            </div>





    <hr class="hrgrosso">
    <?php $comprador=fncgetpessoa($fechamento['comprador']);?>
    <h3>COMPRADOR</h3>
    <div class="row">
        <div class="col-6 border text-uppercase">
            NOME: <strong><?php echo strtoupper($comprador['nome']); ?></strong>
        </div>
        <div class="col-6 border text-uppercase">
            ENDEREÇO: <strong><?php echo strtoupper($comprador['endereco'])."  ".$comprador['numero']; ?></strong>
        </div>
        <div class="col-4 border text-uppercase">
            BAIRRO: <strong><?php echo strtoupper($comprador['bairro']); ?></strong>
        </div>
        <div class="col-4 border text-uppercase">
            CIDADE: <strong><?php echo strtoupper($comprador['cidade']); ?></strong>
        </div>
        <div class="col-4 border text-uppercase">
            CEP: <strong><?php echo strtoupper($comprador['cep']); ?></strong>
        </div>
        <div class="col-3 border text-uppercase">
            CPF: <strong>
                <?php
                if($comprador['cpf']!="" and $comprador['cpf']!=0) {
                    echo "<span class=''>";
                    echo mask($comprador['cpf'],'###.###.###-##');
                    echo "</span>";
                }else{
                    echo "<span class=''>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>
            </strong>
        </div>
        <div class="col-3 border text-uppercase">
            RG: <strong><?php echo strtoupper($comprador['rg']); ?></strong>
        </div>
        <div class="col-3 border text-uppercase">
            CNPJ: <strong>
                <?php
                if($comprador['cnpj']!="" and $comprador['cnpj']!=0) {
                    echo "<span class=''>";
                    echo mask($comprador['cnpj'],'##.###.###/####-##');
                    echo "</span>";
                }else{
                    echo "<span class=''>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>
            </strong>
        </div>
        <div class="col-3 border text-uppercase">
            I.E.: <strong>
                <?php
                if($comprador['insc_est']!="" and $comprador['insc_est']!=0) {
                    echo "<span class=''>";
                    echo mask($comprador['insc_est'],'###############');
                    echo "</span>";
                }else{
                    echo "<span class=''>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>
            </strong>
        </div>
    </div>




    <hr class="hrgrosso">
    <?php $descarga=fncgetpessoa($fechamento['descarga']);?>
    <h3>DESCARGA</h3>
    <div class="row">
        <div class="col-6 border text-uppercase">
            NOME: <strong><?php echo strtoupper($descarga['nome']); ?></strong>
        </div>
        <div class="col-6 border text-uppercase">
            ENDEREÇO: <strong><?php echo strtoupper($descarga['endereco'])."  ".$descarga['numero']; ?></strong>
        </div>
        <div class="col-4 border text-uppercase">
            BAIRRO: <strong><?php echo strtoupper($descarga['bairro']); ?></strong>
        </div>
        <div class="col-4 border text-uppercase">
            CIDADE: <strong><?php echo strtoupper($descarga['cidade']); ?></strong>
        </div>
        <div class="col-4 border text-uppercase">
            CEP: <strong><?php echo strtoupper($descarga['cep']); ?></strong>
        </div>
        <div class="col-3 border text-uppercase">
            CPF: <strong>
                <?php
                if($descarga['cpf']!="" and $descarga['cpf']!=0) {
                    echo "<span class=''>";
                    echo mask($descarga['cpf'],'###.###.###-##');
                    echo "</span>";
                }else{
                    echo "<span class=''>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>
            </strong>
        </div>
        <div class="col-3 border text-uppercase">
            RG: <strong><?php echo strtoupper($descarga['rg']); ?></strong>
        </div>
        <div class="col-3 border text-uppercase">
            CNPJ: <strong>
                <?php
                if($descarga['cnpj']!="" and $descarga['cnpj']!=0) {
                    echo "<span class=''>";
                    echo mask($descarga['cnpj'],'##.###.###/####-##');
                    echo "</span>";
                }else{
                    echo "<span class=''>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>
            </strong>
        </div>
        <div class="col-3 border text-uppercase">
            I.E.: <strong>
                <?php
                if($descarga['insc_est']!="" and $descarga['insc_est']!=0) {
                    echo "<span class=''>";
                    echo mask($descarga['insc_est'],'###############');
                    echo "</span>";
                }else{
                    echo "<span class=''>";
                    echo "[---]";
                    echo "</span>";
                }
                ?>
            </strong>
        </div>
    </div>


    <hr class="hrgrosso">
    <h3>ESPECIFICAÇÃO DA NEGOCIAÇÃO</h3>
    <div class="row">
        <div class="col-12 text-uppercase">
            <table class="table table-sm table-bordered">
                <thead class="thead-dark">
                <tr>
                    <td>DESCRIÇÃO</td>
                    <td>SACAS</td>
                    <td>PESO</td>
                    <td>PREÇO</td>
                    <td>TOTAL</td>
                </tr>
                </thead>
                <tbody>
                <?php $sum=0;

                try{
                    $sql="SELECT * FROM ";
                    $sql.="rcafe_fechamentos_lotes ";
                    $sql.="WHERE id_fechamento=:id and status=1";
                    global $pdo;
                    $consulta=$pdo->prepare($sql);
                    $consulta->bindValue(":id", $_GET['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                    echo 'Erroff'. $error_msg->getMessage();
                }
                $lotes=$consulta->fetchAll();

                $sum=0;
                foreach ($lotes as $dados){
                    ?>
                    <tr>
                        <td><?php echo $dados['descricao']; ?></td>
                        <td><?php echo $dados['sacas']; ?></td>
                        <td><?php echo $dados['peso']; ?> KG</td>
                        <td>R$ <?php echo number_format($dados['preco'],2,',',' '); ?></td>
                        <td>R$ <?php echo number_format($dados['sacas']*$dados['preco'],2,',',' '); ?></td>
                        <?php $sum=$sum+($dados['sacas']*$dados['preco']);?>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td colspan="4" ></td>
                    <td>R$ <?php echo number_format($sum,2,',',' '); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>


    <div class="row">
        <div class="col-12 border text-uppercase">
            DESCRIÇÃO: <strong><?php echo strtoupper($fechamento['descricao']);; ?></strong>
        </div>
        <div class="col-12 border text-uppercase">
            CONDICAO DE PAGAMENTO: <strong><?php echo strtoupper($fechamento['condicao_pag']); ?></strong>
        </div>
        <div class="col-12 border text-uppercase">
            FORMA DE PAGAMENTO: <strong><?php echo strtoupper($fechamento['forma_pag']); ?></strong>
        </div>
        <div class="col-4 border text-uppercase">
            CONDIÇÃO DE ENTREGA: <strong><?php echo strtoupper($fechamento['condicao_entrega']); ?></strong>
        </div>
        <div class="col-4 border text-uppercase">
            PRAZO DE ENTREGA: <strong><?php echo strtoupper($fechamento['prazo_entrega']); ?></strong>
        </div>

        <div class="col-4 border text-uppercase">
            CORRETAGEM DE COMPRA:
            <strong class="text-info text-uppercase"><?php if ($fechamento['corretagem_c']>0){echo $fechamento['corretagem_c']."%";}else{echo 0;} ; ?>&nbsp;&nbsp;</strong>
        </div>
        <div class="col-4 border text-uppercase">
            CORRETAGEM DE VENDA:
            <strong class="text-info text-uppercase"><?php if ($fechamento['corretagem_v']>0){echo $fechamento['corretagem_v']."%";}else{echo 0;} ; ?>&nbsp;&nbsp;</strong>
        </div>
        <div class="col-4 border text-uppercase">
            CORRETOR:
            <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetcorretor($fechamento['corretor'])['corretor']); ?>&nbsp;&nbsp;</strong>
        </div>
        <div class="col-12 border text-uppercase">
            OBSERVAÇÃO:
            <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['observacao']); ?>&nbsp;&nbsp;</strong>
        </div>

    </div>



    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
        <div class="col-12">
            <strong> Concordo com os dados prescritos acima e dou fé.</strong>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura
            </h4>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>