<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $historico=fncgethistorico($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
$saldo_total=0;
$sql = "SELECT sum(`valor`) FROM rcafe_caixa_lancamentos WHERE status=1 and finalizado=1 and tipo=1 and historico=? ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindParam(1,$historico['id']);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$caixaconta1 = $consulta->fetch();

$sql = "SELECT sum(`valor`) FROM rcafe_caixa_lancamentos WHERE status=1 and finalizado=1 and tipo=2 and historico=? ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindParam(1,$historico['id']);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$caixaconta2 = $consulta->fetch();

$saldo=$caixaconta1[0]-$caixaconta2[0];
$saldo_total=$saldo_total+($saldo);

if ($saldo<0){
    $cor_temp="text-danger";
}else{
    $cor_temp="text-success";
}



?>
<main class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="container-fluid">
                <h3 class="ml-3">HISTÓRICO DE GASTOS E RECEITAS</h3>
                <blockquote class="blockquote blockquote-info">
                    <header>
                        DESCRIÇÃO:
                        <strong class="text-info"><?php echo strtoupper($historico['historico']); ?>&nbsp;&nbsp;</strong>
                    </header>
                    <h6>

                        <br>
                        <br>
                        <strong class="<?php echo $cor_temp;?>"><?php echo "R$".number_format($saldo,2, ',', ' ');?>&nbsp;&nbsp;</strong>
                    </h6>

                </blockquote>
            </div>

        </div>



        <?php
        try{
            $sql = "SELECT "
                ."* "
                ."FROM "
                ."rcafe_caixa_lancamentos "
                ."WHERE "
                ."rcafe_caixa_lancamentos.historico =:historico ";
            $sql .="order by finalizado, previsao asc LIMIT 0,50 ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindValue(":historico", $_GET['id']);
            $consulta->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        $lancamentos = $consulta->fetchAll();
        $entradas_quant = $consulta->rowCount();
        $sql = null;
        $consulta = null;
        ?>




        <div class="col-md-12">
            <table class="table table-sm table-stripe table-hover table-bordered text-uppercase">
                <thead class="thead-dark">
                <tr>
                    <th>TIPO</th>
                    <th>VALOR</th>
                    <th>PREVISÃO/HISTÓRICO</th>
                    <th>QUEM PAGA</th>
                    <th>QUEM RECEBE</th>
                    <th>CONTA</th>
                    <th>COMPROVANTE</th>
                </tr>
                </thead>
                <tfoot>
                <tr class="bg-warning">
                    <th colspan="4" class="bg-info text-right"></th>
                    <th colspan="3" class="bg-info text-right"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</th>
                </tr>
                </tfoot>



                <tbody>
                <?php
                if($_GET['sca']!="" and isset($_GET['sca'])) {
                    $sta = strtoupper($_GET['sca']);
                    define('CSA', $sta);
                }
                if($_GET['scb']!="" and isset($_GET['scb'])) {
                    $stb = strtoupper($_GET['scb']);
                    define('CSB', $stb);
                }
                foreach ($lancamentos as $dado){
                    if ($dado['tipo']==1){$tempcor="success";$temptipo="<i class='fas fa-angle-up text-success'></i> Crédito";}
                    if ($dado['tipo']==2){$tempcor="danger";$temptipo="<i class='fas fa-angle-down text-danger'></i> Débito";}

                    if ($dado['finalizado']==1){
                        $cor_back="bg-secondary text-light";
                    }else{
                        $cor_back="";
                    }

                    $detalhes="";

                    $detalhes.=$dado['descricao']." ";

                    if ($dado['fechamento_id']>0) {
                        $detalhes.="REFERENTE AO FECHAMENTO: ";
                        $fechamento = fncgetfechamento($dado['fechamento_id']);
                        if ($fechamento['tipo_fechamento'] == 1) {
                            $detalhes.= "COMPRA";
                        }
                        if ($fechamento['tipo_fechamento'] == 2) {
                            $detalhes.= "VENDA";
                        }
                        $detalhes.= " -- ";
                        $detalhes.= utf8_encode(strftime('%Y', strtotime("{$fechamento['data_ts']}"))) . "-" . $fechamento['id'] . " --- " . dataRetiraHora($fechamento['data_ts']);
                    }




                    echo "<tr class='{$cor_back}'>";

                    echo "<td class=''>";
                    echo $temptipo;
                    echo " - ".dataRetiraHora($dado['data_ts']);
                    echo " <i class='fas fa-info-circle text-info ' title='{$detalhes}'></i>";
                    echo "</td>";

                    echo "<td class=''>R$ ".number_format($dado['valor'],2, ',', ' ')."</td>";

                    if ($dado['finalizado']==1){
                        echo "<td class='info'>";
                    }else {
                        if ($dado['previsao'] <= date('Y-m-d')) {
                            echo "<td class='text-danger'>";
                        } else {
                            echo "<td class='text-success'>";
                        }
                    }
                    echo dataBanco2data($dado['previsao']);
                    echo "<br>";
                    echo fncgethistorico($dado['historico'])['historico'];
                    echo "</td>";

                    echo "<td class=''>";
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $aaa = $dado['quem_paga'];
                        $aa = explode(CSA, $aaa);
                        $a = implode("<span class='text-danger'>{$sta}</span>", $aa);
                        echo $a;
                    }else{
                        echo $dado['quem_paga'];
                    }
                    echo "</td>";

                    echo "<td class=''>";
                    if($_GET['scb']!="" and isset($_GET['scb'])) {
                        $stb = CSB;
                        $aaa = $dado['quem_recebe'];
                        $aa = explode(CSB, $aaa);
                        $a = implode("<span class='text-danger'>{$stb}</span>", $aa);
                        echo $a;
                    }else{
                        echo $dado['quem_recebe'];
                    }
                    echo "</td>";

                    echo "<td class=''>";
                    echo fncgetfonte($dado['fonte'])['titular'];
                    echo "</td>";


                    echo "<td class=''>";
                    if ($dado['finalizado']==1){
                        echo "<i class='fas fa-info-circle' title='{$dado['obs']}'></i> Finalizado em ".dataRetiraHora($dado['finalizado_data']);

                        echo "<br><a href='index.php?pg=Vcaixa_lista_comprovante&id={$dado['id']}' class='btn btn-sm btn-light' target='_blank'>Comprovante</a>";
                    }else{
                        echo "em aberto";
                    }
                    echo "</td>";

                    echo "</tr>";

                }
                ?>
                </tbody>
            </table>

        </div>

    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>