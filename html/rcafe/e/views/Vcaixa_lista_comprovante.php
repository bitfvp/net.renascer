<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_9"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}


$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
// Recebe

try{
    $sql = "SELECT "
        ."* "
        ."FROM "
        ."rcafe_caixa_lancamentos "
        ."WHERE "
        ."id=:idd ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":idd", $_GET['id']);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$lancamento = $consulta->fetch();
$sql = null;
$consulta = null;

?>
<br>
<main class="container">
<?php include_once("../includes/renascer_cab.php");?>
    <div class="row">
        <div class="col-12 px-5">
            <table class="table table-sm text-uppercase">
                <thead>
                    <tr>
                        <th>VALOR</th>
                        <th>IDENTIFICAÇÃO</th>
                        <th>DESCRIÇÃO</th>
                    </tr>
                </thead>
                <tbody>

                <tr>
                    <td>
                        <?php
                        echo "R$".number_format($lancamento['valor'],2, ',', ' ');
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $lancamento['identificador'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $lancamento['descricao'];
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <?php
                        echo "Pagamento de ".fncgethistorico($lancamento['historico'])['historico'];
                        ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <?php
                        echo "Finalizado em ".dataRetiraHora($lancamento['finalizado_data']). "--- {$lancamento['obs']}";
                        ?>
                    </td>
                </tr>


                <tr>
                    <td colspan="3"><br></td>
                </tr>


                </tbody>
                <tfoot>
                <tr>
                    <th colspan="3" class="text-right pr-3"><?php
                        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_set('America/Sao_Paulo');
                        echo strftime('%A, %d de %B de %Y', strtotime('today'));
                        ?>
                    </th>
                </tr>
                </tfoot>
        </table>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <h3 class="text-center">__________________</h3>
            <h4 class="text-center">ASSINATURA</h4>
        </div>
        <div class="col-6">
            <h3 class="text-center">__________________</h3>
            <h4 class="text-center">ASSINATURA</h4>
        </div>
    </div>
</main>
<br><br><br><br>
<main class="container">
    <?php include("../includes/renascer_cab.php");?>
    <div class="row">
        <div class="col-12 px-5">
            <table class="table table-sm text-uppercase">
                <thead>
                <tr>
                    <th>VALOR</th>
                    <th>IDENTIFICAÇÃO</th>
                    <th>DESCRIÇÃO</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td>
                        <?php
                        echo "R$".number_format($lancamento['valor'],2, ',', ' ');
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $lancamento['identificador'];
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $lancamento['descricao'];
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <?php
                        echo "Pagamento de ".fncgethistorico($lancamento['historico'])['historico'];
                        ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <?php
                        echo "Finalizado em ".dataRetiraHora($lancamento['finalizado_data']). "--- {$lancamento['obs']}";
                        ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="3"><br></td>
                </tr>


                </tbody>
                <tfoot>
                <tr>
                    <th colspan="3" class="text-right pr-3"><?php
                        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_set('America/Sao_Paulo');
                        echo strftime('%A, %d de %B de %Y', strtotime('today'));
                        ?>
                    </th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <h3 class="text-center">__________________</h3>
            <h4 class="text-center">ASSINATURA</h4>
        </div>
        <div class="col-6">
            <h3 class="text-center">__________________</h3>
            <h4 class="text-center">ASSINATURA</h4>
        </div>
    </div>
</main>
</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>