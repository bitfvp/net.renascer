<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vf_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $fechamento=fncgetfechamento($_GET['id']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vfechamento_lista");
    exit();
}

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<main class="container-fluid">
    <div class="row">

        <?php
        if ($fechamento['tipo_fechamento']==1 or $fechamento['tipo_fechamento']==2){
            $dys1="col-md-8";
            $dys2="col-md-4";
        }
        if ($fechamento['tipo_fechamento']==3){
            $dys1="col-md-12";
            $dys2="col-md-4 d-none";
        }
        ?>

        <div class="<?php echo $dys1;?>">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Fechamento
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 border">
                            <header>
                                NEGOCIAÇÃO DO TIPO:
                                <strong class="text-info text-uppercase">
                                    <?php
                                    if ($fechamento['tipo_fechamento']==1){
                                        echo "COMPRA";
                                    }
                                    if ($fechamento['tipo_fechamento']==2){
                                        echo "VENDA";
                                    }
                                    if ($fechamento['tipo_fechamento']==3){
                                        echo "CORRETAGEM";
                                    }
                                    echo " -- NR:";
                                    echo utf8_encode(strftime('%Y', strtotime("{$fechamento['data_ts']}")))."-".$fechamento['nr']."<br>";
                                    if (strlen($fechamento['ordem_compra'])>0){
                                        echo "<p class='text-dark mb-1'>ORDEM DE COMPRA EXTERNA: ".strtoupper($fechamento['ordem_compra'])."</p>";
                                    }

                                    if ($fechamento['id_venda']!=0){
                                        $venda=fncgetvenda($fechamento['id_venda']);
                                        echo "<small class='text-dark'>".utf8_encode(strftime('%Y', strtotime("{$venda['data_ts']}")))."-".$venda['id']." / ".fncgetpessoa($venda['cliente'])['nome']. " / CONTRATO:".$venda['prefixo']."-".$venda['contrato']." ".$venda['descricao']."</small><br>";
                                    }else{
                                        echo "";
                                    }
                                    echo datahoraBanco2data($fechamento['data_ts']);
                                    ?>
                                </strong>
                            </header>

                            <h6>
                                VENDEDOR:
                                <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['vendedor'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                                COMPRADOR:
                                <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['comprador'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                                LOCAL DE DESCARGA:
                                <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['descarga'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                                CORRETAGEM DE COMPRA:
                                <strong class="text-info text-uppercase"><?php if ($fechamento['corretagem_c']>0){echo $fechamento['corretagem_c']."%";}else{echo 0;} ?>&nbsp;&nbsp;</strong><br>
                                CORRETAGEM DE VENDA:
                                <strong class="text-info text-uppercase"><?php if ($fechamento['corretagem_v']>0){echo $fechamento['corretagem_v']."%";}else{echo 0;} ?>&nbsp;&nbsp;</strong><br>
                                MEDIDA:
                                <strong class="text-info text-uppercase"><?php echo $fechamento['medida']." Kg"; ?>&nbsp;&nbsp;</strong><br>
                            </h6>
                        </div>
                        <div class="col-md-6 ">
                            <form action="index.php?pg=Vfechamento&aca=arquivoinsert&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <div class="custom-file">
                                            <input id="arquivos" type="file" class="custom-file-input" name="arquivos[]" value="" multiple/>
                                            <label class="custom-file-label" for="arquivos">Arquivos referente ao fechamento...</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-success btn-block" value="ENVIAR ARQUIVOS"/>
                                    </div>
                                </div>
                            </form>


                            <div class="row">
                                <?php
                                $sql = "SELECT * FROM `rcafe_fechamentos_dados` where fechamento_id='{$_GET['id']}' ";
                                global $pdo;
                                $consulta = $pdo->prepare($sql);
                                $consulta->execute();
                                $dados = $consulta->fetchAll();//$total[0]
                                $sql = null;
                                $consulta = null;

                                foreach ($dados as $dado){
                                    $status="";
                                    if ($dado['status']==0){
                                        $status="bg-danger-light d-none";
                                    }

                                    $link="../../dados/renascer/fechamentos/" . $dado['fechamento_id'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                    switch ($dado['extensao']) {
                                        case "docx":
                                            $caminho=$env->env_estatico . "img/docx.png";
                                            break;
                                        case "doc":
                                            $caminho=$env->env_estatico . "img/doc.png";
                                            break;
                                        case "xls":
                                            $caminho=$env->env_estatico . "img/xls.png";
                                            break;
                                        case "xlsx":
                                            $caminho=$env->env_estatico . "img/xls.png"."";
                                            break;
                                        case "pdf":
                                            $caminho=$env->env_estatico . "img/pdf.png";
                                            break;
                                        default:
                                            $caminho="../../dados/renascer/fechamentos/" . $dado['fechamento_id'] . "/" .  $dado['arquivo'] . "." . $dado['extensao'] ;
                                            $link=$caminho;
                                            break;
                                    }
                                    echo "<div class='col-md-3 text-center {$status}'>";
                                    echo "<a href=" . $link . " target='_blank'>";
                                    echo "<img src=" . $caminho . " alt='...'  class='img-fluid img-thumbnail'>";
                                    echo "</a>";
                                    echo "<a href=" . $link . " target='_blank' title='View larger' class='fas fa-search-plus btn btn-sm'></a>";
                                    if ($dado['status']==0){
                                        $us=fncgetusuario($dado['delete_prof']);
                                        echo "<h6 class='fas fa-dizzy text-dark'>Desativado por ".$us['nome']."</h6>";
                                    }else{
                                        $act="index.php?pg=Vfechamento&id=".$_GET['id'] ."&aca=apagaarquivo&arq=". $dado['arquivo'];
                                        echo "<a href='{$act}' target='_self' title='Delete image' class='far fa-trash-alt btn btn-sm'></a>";
                                    }
                                    echo "</div>";
                                }
                                ?>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <hr>

                            <?php include_once("includes/fechamento_form.php");?>

                        </div>
                        <div class="col-md-6">
                            <h6>

                                DESCRIÇÃO:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['descricao']); ?>&nbsp;&nbsp;</strong><br>
                                CONDIÇÃO DE PAGAMENTO:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['condicao_pag']); ?>&nbsp;&nbsp;</strong><br>
                                FORMA DE PAGAMENTO:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['forma_pag']); ?>&nbsp;&nbsp;</strong><br>
                                CONDIÇÃO DE ENTREGA:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['condicao_entrega']); ?>&nbsp;&nbsp;</strong><br>
                                PRAZO DE ENTREGA:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['prazo_entrega']); ?>&nbsp;&nbsp;</strong><br>

                                CORRETOR:
                                <strong class="text-info text-uppercase"><?php $temp_corretor=strtoupper(fncgetcorretor($fechamento['corretor'])['corretor']); echo $temp_corretor; ?>&nbsp;&nbsp;</strong><br>
                                OBSERVAÇÃO:
                                <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['observacao']); ?>&nbsp;&nbsp;</strong><br>
                            </h6>
                        </div>

                        <div class="col-md-6">
                            <div class="text-center">


                                <?php
                                if ($fechamento['corretagem_c']>0 or $fechamento['corretagem_c']>0){
                                    if ($fechamento['comissao_lancada']==1){
                                        echo "<i class='text-info'>Comissão já lançada</i>";
                                    }else{
                                        if ($fechamento['corretagem_c']>0){
                                            $tempvval=($sum/100)*$fechamento['corretagem_c'];
                                        }
                                        if ($fechamento['corretagem_v']>0){
                                            $tempvval=($sum/100)*$fechamento['corretagem_v'];
                                        }


                                        echo "<strong>Há uma comissão de R$".number_format($tempvval,2,',',' ')." para ".$temp_corretor.", Deseja fazer o lançamento em caixa?</strong><br>";
                                        echo "<a href='index.php?pg=Vfechamento&id=1&valor={$tempvval}&cor={$temp_corretor}&aca=comissaoauto' class='btn btn-danger'>Lançar no caixa</a>";
                                    }

                                }else{
                                    //não existe comissão
                                }
                                ?>
                            </div>
                        </div>

<!--                FIM DO ROW-->
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <a href="index.php?pg=Vfechamento_editar&id=<?php echo $_GET['id'] ?>" title="Editar entrada" class="btn btn-primary btn-block">EDITAR FECHAMENTO</a>
                        </div>

                        <div class="col-md-6">
                            <a href="index.php?pg=Vfechamento_print1&id=<?php echo $_GET['id']; ?>" target="_blank" class="btn btn-info btn-block">Impressão de comprovante</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <div class="<?php echo $dys2;?>">

            <div class="row">
                <div class="col-md-12">

                    <?php
                    try{
                        $sql="SELECT * FROM ";
                        $sql.="rcafe_caixa_lancamentos ";
                        $sql.="WHERE fechamento_id=:id and status=1";
                        global $pdo;
                        $consulta=$pdo->prepare($sql);
                        $consulta->bindValue(":id", $_GET['id']);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    }catch ( PDOException $error_msg){
                        echo 'Erroff'. $error_msg->getMessage();
                    }
                    $lancamentos=$consulta->fetchAll();
                    ?>

                    <div class="card">
                        <div class="card-header bg-info text-light">
                            Lançamentos de caixa
                        </div>
                        <div class="card-body">
                            <a href="index.php?pg=Vfechamento2&id=<?php echo $_GET['id'];?>" class="btn btn-warning btn-block">IR PARA LANÇAMENTOS NO CAIXA <i class="fas fa-arrow-right"></i></a>

                            <?php
                            $valor_acumulado=0;
                            foreach ($lancamentos as $dado){
                                $valor_acumulado+=$dado['valor'];
                            }
                            ?>

                            <hr>
                            <?php if ($valor_acumulado<$sum){$tempcor="danger";}else{$tempcor="success";}?>
                            <h6 class="form-cadastro-heading text-<?php echo $tempcor;?>">VALOR PROJETADO NO CAIXA: <BR>
                                R$ <?php echo number_format($valor_acumulado,2, ',', ' ');?>   /   R$ <?php echo number_format($sum,2, ',', ' ');?></h6>

                        </div>
                    </div>

                </div>


                <div class="col-md-12">
                    <?php
                    //1 é compra
                    if ($fechamento['tipo_fechamento']==1){
                        include_once("includes/fechamento_rm.php");
                    }

                    //2 é venda
                    if ($fechamento['tipo_fechamento']==2 and $fechamento['id_venda']>0){
                        include_once("includes/fechamento_venda.php");
                    }
                    ?>

                </div>

            </div>

        </div>

<!--FIM DE ROW-->
    </div>



</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>