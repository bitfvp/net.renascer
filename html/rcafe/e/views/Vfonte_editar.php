<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar conta-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="fonteupdate";
    $fonte=fncgetfonte($_GET['id']);
}else{
    $a="fonteinsert";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vfonte_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de contas</h3>
        <hr>
        <div class="row">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $fonte['id']; ?>"/>
            <div class="col-md-12">
                <label for="titular">Titular:</label>
                <input type="text" name="titular" id="titular" class="form-control" autocomplete="off" placeholder="" value="<?php echo $fonte['titular']; ?>"/>
            </div>

            <div class="col-md-12">
                <label for="banco">Banco:</label>
                <input type="text" name="banco" id="banco" class="form-control" autocomplete="off" placeholder="" value="<?php echo $fonte['banco']; ?>"/>
            </div>


            <div class="col-md-12">
                <label for="agencia">Agencia:</label>
                <input type="text" name="agencia" id="agencia" class="form-control" autocomplete="off" placeholder="" value="<?php echo $fonte['agencia']; ?>"/>
            </div>
            <script>
                $(document).ready(function(){
                    $('#agencia').mask('000000', {reverse: false});
                });
            </script>

            <div class="col-md-12">
                <label for="conta">conta:</label>
                <input type="text" name="conta" id="conta" class="form-control" autocomplete="off" placeholder="" value="<?php echo $fonte['conta']; ?>"/>
            </div>
            <script>
                $(document).ready(function(){
                    $('#conta').mask('00000-0', {reverse: true});
                });
            </script>

            <div class="col-md-12">
                <label for="cidade">cidade:</label>
                <input type="text" name="cidade" id="cidade" class="form-control" autocomplete="off" placeholder="" value="<?php echo $fonte['cidade']; ?>"/>
            </div>


            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>