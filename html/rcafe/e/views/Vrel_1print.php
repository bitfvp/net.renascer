<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatório de estoque-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe

    $sql = "SELECT * FROM rcafe_entradas_lotes WHERE rcafe_entradas_lotes.peso_atual > 0 ORDER BY ";
    switch ($_GET['order']){
        case "local":
            $sql .= "rcafe_entradas_lotes.localizacao ASC, rcafe_entradas_lotes.data_ts ASC";
            break;
        case "tipo":
            $sql .= "rcafe_entradas_lotes.tipo_cafe ASC, rcafe_entradas_lotes.data_ts ASC";
            break;
        case "data":
            $sql .= "rcafe_entradas_lotes.data_ts ASC";
            break;
    }
    global $pdo;
    $consulta = $pdo->prepare($sql);

    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $lotes = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>
<div class="container-fluid">
    <?php
    switch ($_GET['order']){
        case "local":
            echo "<h3>ESTOQUE ATUAL ORDENADO POR LOCALIZAÇÃO, DATA DE ENTRADA</h3>";
            break;
        case "tipo":
            echo "<h3>ESTOQUE ATUAL ORDENADO POR TIPO DE PRODUTO, DATA DE ENTRADA</h3>";
            break;
        case "data":
            echo "<h3>ESTOQUE ATUAL ORDENADO POR DATA DE ENTRADA</h3>";
            break;
    }
    ?>

<!--    <h5>--><?php //echo fncgetsetor($setor)['setor']?><!--</h5>-->
    <h5><?php echo datahoraBanco2data(dataNow());?></h5>

    <table class="table table-bordered table-hover table-sm">
        <thead>
        <th>PRODUTO</th>
        <th class="text-center">FORNECEDOR</th>
        <th class="text-center">LOTE</th>
        <th>PESO ENTRADA</th>
        <th>PESO ATUAL</th>
        <th>VALOR SACA</th>
        <th class="text-center">LOCAL</th>

        </thead>
        <tbody>
        <?php
        foreach ($lotes as $lote){
            $entrada=fncgetentrada($lote['romaneio']);
            $cordalinha = "  ";
                if ($lote['p_bo']==1){
                    $cordalinha = " text-dark bg-warning ";
                }
            $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

            $letra=fncgetletra($lote["letra"]);

            $peso_entrada = $lote["peso_entrada"];
            $sacas_entrada=$peso_entrada/60.5;
            $sacas_entrada=number_format($sacas_entrada, 2, '.', ',');
            $peso_entrada.=" Kg";
            $bags_entrada = $lote["bags_entrada"];

            $peso_atual = $lote["peso_atual"];
            $sacas_atual=$peso_atual/60.5;
            $sacas_atual=number_format($sacas_atual, 2, '.', ',');
            $peso_atual.=" Kg";
            $bags_atual = $lote["bags_atual"];
            $valor_saca = number_format($lote["valor_saca"],2);

            echo "<tr class='{$cordalinha} text-center'>";
            echo "<td><small>".fncgetprodutos($lote['tipo_cafe'])['abrev']."</small></td>";
            echo "<td><small>".strtoupper(fncgetpessoa($entrada['fornecedor'])['nome'])."</small></td>";
            echo "<td style='white-space: nowrap;'><small>".$romaneio_tipo.$entrada['romaneio']."-".$letra." ".dataRetiraHora($lote['data_ts'])."</small></td>";
            echo "<td><small>".$sacas_entrada."v ".$bags_entrada." bag(s)</small></td>";
            echo "<td><small>".$sacas_atual."v ".$bags_atual." bags</small></td>";
            echo "<td><small>".$valor_saca."R$</small></td>";
            echo "<td><small>".fncgetlocal($lote["localizacao"])['nome']." ".$lote["localizacao_obs"]." ".$lote["bo"]."</small></td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>

</div>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>