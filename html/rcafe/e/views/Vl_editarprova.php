<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_3"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id_l']) and is_numeric($_GET['id_l'])){
    $a="loteprovasave";
    $lote=fncgetlote($_GET['id_l']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vl_lista");
    exit();
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">

        <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header bg-info text-light">
                        Edição de lote
                    </div>
                    <div class="card-body">
                        <form action="<?php echo "index.php?pg=Vl_editarprova&id_l={$_GET['id_l']}&aca={$a}"; ?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                 <div class="col-md-12">
                                     <input id="id_l" type="hidden" class="txt bradius" name="id_l" value="<?php echo $lote['id']; ?>"/>
                                     <input id="pg" type="hidden" class="txt bradius" name="pg" value="<?php echo $_GET['pg']; ?>"/>

                                     <label for="tipo_cafe">PRODUTO:</label>
                                    <select name="tipo_cafe" id="tipo_cafe" class="form-control input-sm" data-live-search="true" required>
                                        <?php
                                        $gettipo_cafe=fncgetprodutos($lote['tipo_cafe']);
                                        ?>
                                        <option selected="" data-tokens="<?php echo $gettipo_cafe['nome'];?>" value="<?php echo $lote['tipo_cafe']; ?>">
                                            <?php echo $gettipo_cafe['nome'];?>
                                        </option>
                                        <?php
                                        foreach (fncprodutoslist() as $item) {
                                            ?>
                                            <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                                <?php echo $item['nome']; ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="cata">CATA:</label>
                                    <input autocomplete="off" id="cata" placeholder="" type="number" class="form-control" name="cata" value="<?php echo $lote['cata']; ?>" required min="0" max="98" step="1" />
                                </div>

                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>


    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>