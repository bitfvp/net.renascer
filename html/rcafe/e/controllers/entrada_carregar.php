<?php

function fncgetentrada($id){
    $sql = "SELECT * FROM ren_entradas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getentrada = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getentrada;
}

function fncgetultimascatas(){
    $sql = "SELECT * FROM ren_entradas where status=1 and romaneio_tipo=3 order by id desc limit 0,25";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cataslista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $cataslista;
}

function fncgetultimasdevolucoes(){
    $sql = "SELECT * FROM ren_entradas where status=1 and romaneio_tipo=4 order by id desc limit 0,10";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $devslista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $devslista;
}

function fncgetultimasligasinternas(){
    $sql = "SELECT * FROM ren_entradas where status=1 and romaneio_tipo=5 order by id desc limit 0,25";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $ligasinternaslista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $ligasinternaslista;
}