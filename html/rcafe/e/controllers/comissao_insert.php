<?php

if($startactiona==1 && $aca=="comissaoauto"){
    $fonte=1;
    $fechamento_id=$_GET['id'];
    $tipo=2;
    $valor=$_GET["valor"];
    $previsao=date('Y-m-d');
    $historico=4;
    $identificador=$_GET["cor"];

    $tempfechamento=fncgetfechamento($fechamento_id);
    $descricao='Pagar comissão Ref. NR-';
    $descricao.=utf8_encode(strftime('%Y', strtotime("{$tempfechamento['data_ts']}")))."-".$tempfechamento['nr'];

    $finalizado=0;
    $usuario=$_SESSION["id"];

//começa a validação
//se algum campo vazia retorna a msg
    if(empty($valor) || empty($fonte)){
        $_SESSION["fsh"]=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vcaixa_lista");
        exit();

    }else{
//executa classe cadastro
        $salvar= new Caixa_lancamento();
        $salvar->fnccaixa_lancamentoinsert($fonte, 0, $tipo, $valor, $previsao, $historico, $identificador, $descricao, $finalizado, $usuario);

        $salvar= new Fechamento();
        $salvar->fncfechamentoupdate_comissao( $fechamento_id );


        $_SESSION['fsh']=[
            "flash"=>"Lançamento de comissão realizado com sucesso!!",
            "type"=>"success",
        ];

        header("Location: index.php?pg=Vfechamento&id={$fechamento_id}");
        exit();
    }
}