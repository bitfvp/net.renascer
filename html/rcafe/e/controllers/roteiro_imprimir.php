<?php
//metodo de selecionar cbs pra imprimir
if ($startactiona == 1 && $aca == "imprimirrosim") {
    //verificar se as super variaveis post estao setadas

    if (isset($_GET["id"])) {
        //dados
        $id = $_GET["id"];
        if (isset($_GET["pg"])) {
            $pg = $_GET["pg"];
        }

        try {
            $sql = "UPDATE rcafe_fechamentos ";
            $sql .= "SET imprimir='1'";
            $sql .= " WHERE id=? ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1, $id);
            $atualiza->execute();
            global $LQ;
            $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        $_SESSION['fsh']=[
            "flash"=>"Marcado!!",
            "type"=>"success",
        ];
        header("Location: index.php?pg=Vfechamento_lista&sca={$_GET['sca']}&scb={$_GET['scb']}&scc={$_GET['scc']}");
        exit();

    }
}
//metodo de selecionar cbs pra nao imprimir
if ($startactiona == 1 && $aca == "imprimirronao") {
    //verificar se as super variaveis post estao setadas
    if (isset($_GET["id"])) {
        //dados
        $id = $_GET["id"];
        if (isset($_GET["pg"])) {
            $pg = $_GET["pg"];
        }
        try {
            $sql = "UPDATE rcafe_fechamentos ";
            $sql .= "SET imprimir='0'";
            $sql .= " WHERE id=? ";

            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindParam(1, $id);
            $atualiza->execute();
            global $LQ;
            $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }

        $_SESSION['fsh']=[
            "flash"=>"Desmarcado!!",
            "type"=>"success",
        ];
        header("Location: index.php?pg=Vfechamento_lista&sca={$_GET['sca']}&scb={$_GET['scb']}&scc={$_GET['scc']}");
        exit();

    }
}