<?php
//gerado pelo geracode
function fnccaixa_lancamentolist(){
    $sql = "SELECT * FROM rcafe_caixa_lancamentos ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $caixa_lancamentolista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $caixa_lancamentolista;
}

function fncgetcaixa_lancamento($id){
    $sql = "SELECT * FROM rcafe_caixa_lancamentos WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getrcafe_caixa_lancamentos = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getrcafe_caixa_lancamentos;
}
?>