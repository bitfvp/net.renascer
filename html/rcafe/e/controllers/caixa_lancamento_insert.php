<?php
//gerado pelo geracode
//use ucwords(strtolower()) e limpadocumento()
if($startactiona==1 && $aca=="caixa_lancamentoinsert"){
    $fonte=$_POST["fonte"];
    $fechamento_id=$_POST["fechamento_id"];
    $tipo=$_POST["tipo"];
    $valor=$_POST["valor"];
    $previsao=$_POST["previsao"];
    $historico=$_POST["historico"];
    $identificador=$_POST["identificador"];
    $descricao=$_POST["descricao"];
    $finalizado=0;
    $obs="";
    $usuario=$_SESSION["id"];

//começa a validação
//se algum campo vazia retorna a msg
    if(empty($valor) || empty($fonte)){
        $_SESSION["fsh"]=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vfechamento&id={$fechamento_id}");
        exit();

    }else{
//executa classe cadastro
        $salvar= new Caixa_lancamento();
        $salvar->fnccaixa_lancamentoinsert($fonte, $fechamento_id, $tipo, $valor, $previsao, $historico, $identificador, $descricao, $finalizado, $obs, $usuario);

        header("Location: index.php?pg=Vfechamento2&id={$fechamento_id}");
        exit();
    }
}


if($startactiona==1 && $aca=="caixa_lancamentoinsert2"){
    $fonte=$_POST["fonte"];
    $fechamento_id=$_POST["fechamento_id"];
    $tipo=$_POST["tipo"];
    $valor=$_POST["valor"];
    $previsao=$_POST["previsao"];
    $historico=$_POST["historico"];
    $identificador=$_POST["identificador"];
    $descricao=$_POST["descricao"];
    if ($_POST["finalizado"]==1){
        $finalizado=$_POST["finalizado"];
        $obs="Conclusão Rápida";
    }else{
        $finalizado=0;
        $obs="";
    }
    $usuario=$_SESSION["id"];

//começa a validação
//se algum campo vazia retorna a msg
    if(empty($valor) || empty($fonte)){
        $_SESSION["fsh"]=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vcaixa_lista");
        exit();

    }else{
//executa classe cadastro
        $salvar= new Caixa_lancamento();
        $salvar->fnccaixa_lancamentoinsert($fonte, $fechamento_id, $tipo, $valor, $previsao, $historico, $identificador, $descricao, $finalizado, $obs, $usuario);

        header("Location: index.php?pg=Vcaixa_lista");
        exit();
    }
}