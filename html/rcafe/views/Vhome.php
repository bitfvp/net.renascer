<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        //if ()
    }
}

$page="Sistemas-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
include_once("includes/alertas.php");
$sorte=rand(1,10);
$sorte=0;
?>
<main class="container">
    <div class="jumbotron p-3 text-center mt-0 mb-3 jumb-<?php echo $sorte;?>">
            <h1 class="display-4">
<!--                <img class="col-md-2" src="--><?php //echo $env->env_estatico; ?><!--img/vetor.png" alt="" title="--><?php //echo $env->env__nome; ?><!--"/>-->
                <img class="col-md-8" src="<?php echo $env->env_estatico; ?>img/renascer.png" alt="" title="<?php echo $env->env__nome; ?>"/>
                <p class="lead d-none">
                    {Definição}
                </p>
            </h1>
    </div>

    <div class="col-md-12 px-0">
        <?php
        function bsmenu($bs_type,$bs_btn,$bs_name,$bs_desc,$bs_url){
            echo "<div class='bs-calltoaction bs-calltoaction-{$bs_type}'>
                <div class='row'>
                    <div class='col-md-8 cta-contents mx-auto'>
                        <h1 class='cta-title'>{$bs_name}</h1>
                        <div class='cta-desc'>
                            <p>{$bs_desc}</p>
                        </div>
                    </div>
                    <div class='col-md-4 cta-button mx-auto'>
                    <a href='{$env->env_url_mod}{$bs_url}' class='btn btn-lg btn-block btn-{$bs_btn}'>Acessar <i class='fa fa-sign-in-alt'></i></a>    
                    </div>
                </div>
            </div>";
        }

        //Escritório
        if ($allow["allow_5"] == "1") {
            bsmenu("primary","success","ESCRITÓRIO","Informações do módulo","e");
        }

        //Escritório
        if ($allow["allow_10"] == "1") {
            bsmenu("primary","success","BALCÃO & PROVA","Informações do módulo","p");
        }



        //admin
        if ($allow["admin"] == "1") {
            bsmenu("warning","success","USUÁRIOS","Apenas administradores","_admin");
        }
        ?>
    </div>
    </div>
</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>