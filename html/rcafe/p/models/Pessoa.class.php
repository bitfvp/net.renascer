<?php
class Pessoa{
    public function fncpessoaedit(
        $id,$nome,$cpf,$rg,$cnpj,$insc_est,$tipo_pessoa,$endereco,$numero,$bairro,$cidade,$complemento,$cep,$telefone,$email,$bancario,$p_motorista,$p_cliente,$p_fornecedor,$p_corretor,$p_responsavel,$p_transportadora
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
                $sql.="ren_pessoas";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

            try {
                $sql="UPDATE ren_pessoas";
                $sql.=" SET";
                $sql .= " nome=:nome,
                    cpf=:cpf,
                    rg=:rg,
                    cnpj=:cnpj,
                    insc_est=:insc_est,
                    tipo_pessoa=:tipo_pessoa,
                    endereco=:endereco,
                    numero=:numero,
                    bairro=:bairro,
                    cidade=:cidade,
                    complemento=:complemento,
                    cep=:cep,
                    telefone=:telefone,
                    email=:email,
                    bancario=:bancario,
                    p_motorista=:p_motorista,
                    p_cliente=:p_cliente,
                    p_fornecedor=:p_fornecedor,
                    p_corretor=:p_corretor,
                    p_responsavel=:p_responsavel,
                    p_transportadora=:p_transportadora
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":nome", $nome);
                $atualiza->bindValue(":cpf", $cpf);
                $atualiza->bindValue(":rg", $rg);
                $atualiza->bindValue(":cnpj", $cnpj);
                $atualiza->bindValue(":insc_est", $insc_est);
                $atualiza->bindValue(":tipo_pessoa", $tipo_pessoa);
                $atualiza->bindValue(":endereco", $endereco);
                $atualiza->bindValue(":numero", $numero);
                $atualiza->bindValue(":bairro", $bairro);
                $atualiza->bindValue(":cidade", $cidade);
                $atualiza->bindValue(":complemento", $complemento);
                $atualiza->bindValue(":cep", $cep);
                $atualiza->bindValue(":telefone", $telefone);
                $atualiza->bindValue(":email", $email);
                $atualiza->bindValue(":bancario", $bancario);
                $atualiza->bindValue(":p_motorista", $p_motorista);
                $atualiza->bindValue(":p_cliente", $p_cliente);
                $atualiza->bindValue(":p_fornecedor", $p_fornecedor);
                $atualiza->bindValue(":p_corretor", $p_corretor);
                $atualiza->bindValue(":p_responsavel", $p_responsavel);
                $atualiza->bindValue(":p_transportadora", $p_transportadora);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
//                header("Location: index.php?pg=Vpessoa_editar&id={$id}");
//                exit();
                header("Location: index.php?pg=Vpessoa_lista");
                exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoanew(
        $nome,$cpf,$rg,$cnpj,$insc_est,$tipo_pessoa,$endereco,$numero,$bairro,$cidade,$complemento,$cep,$telefone,$email,$bancario,$p_motorista,$p_cliente,$p_fornecedor,$p_corretor,$p_responsavel,$p_transportadora
    ){
        //tratamento das variaveis
        //não ter

        try{
            $sql="SELECT id FROM ";
                $sql.="ren_pessoas";
            $sql.=" WHERE cpf=:cpf";
            global $pdo;
            $consultacpf=$pdo->prepare($sql);
            $consultacpf->bindValue(":cpf", $cpf);
            $consultacpf->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcpf=$consultacpf->rowCount();
        try{
            $sql="SELECT id FROM ";
                $sql.="ren_pessoas";
            $sql.=" WHERE cnpj=:cnpj";
            global $pdo;
            $consultacnpj=$pdo->prepare($sql);
            $consultacnpj->bindValue(":cnpj", $cnpj);
            $consultacnpj->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcnpj=$consultacnpj->rowCount();

        if(($contarcnpj==0)or ($cnpj=="")){
        if(($contarcpf==0)or ($cpf=="")){


            //inserção no banco
                try {
                    $sql="INSERT INTO ren_pessoas ";
                    $sql .= "(id,
                    nome,
                    cpf,
                    rg,
                    cnpj,
                    insc_est,
                    tipo_pessoa,
                    endereco,
                    numero,
                    bairro,
                    cidade,
                    complemento,
                    cep,
                    telefone,
                    email,
                    bancario,
                    p_motorista,
                    p_cliente,
                    p_fornecedor,
                    p_corretor,
                    p_responsavel,
                    p_transportadora
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :nome,
                    :cpf,
                    :rg,
                    :cnpj,
                    :insc_est,
                    :tipo_pessoa,
                    :endereco,
                    :numero,
                    :bairro,
                    :cidade,
                    :complemento,
                    :cep,
                    :telefone,
                    :email,
                    :bancario,
                    :p_motorista,
                    :p_cliente,
                    :p_fornecedor,
                    :p_corretor,
                    :p_responsavel,
                    :p_transportadora
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":nome", $nome);
                    $insere->bindValue(":cpf", $cpf);
                    $insere->bindValue(":rg", $rg);
                    $insere->bindValue(":cnpj", $cnpj);
                    $insere->bindValue(":insc_est", $insc_est);
                    $insere->bindValue(":tipo_pessoa", $tipo_pessoa);
                    $insere->bindValue(":endereco", $endereco);
                    $insere->bindValue(":numero", $numero);
                    $insere->bindValue(":bairro", $bairro);
                    $insere->bindValue(":cidade", $cidade);
                    $insere->bindValue(":complemento", $complemento);
                    $insere->bindValue(":cep", $cep);
                    $insere->bindValue(":telefone", $telefone);
                    $insere->bindValue(":email", $email);
                    $insere->bindValue(":bancario", $bancario);
                    $insere->bindValue(":p_motorista", $p_motorista);
                    $insere->bindValue(":p_cliente", $p_cliente);
                    $insere->bindValue(":p_fornecedor", $p_fornecedor);
                    $insere->bindValue(":p_corretor", $p_corretor);
                    $insere->bindValue(":p_responsavel", $p_responsavel);
                    $insere->bindValue(":p_transportadora", $p_transportadora);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }


        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse CPF!!",
                "type"=>"warning",
            ];
        }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse CNPJ!!",
                "type"=>"warning",
            ];
        }


        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM ";
                $sql.="ren_pessoas";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

//                    header("Location: index.php?pg=Vpessoa_editar&id={$maid}");
//                    exit();
            header("Location: index.php?pg=Vpessoa_lista");
            exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }




}
