<?php
class Prova{
    public function fncprovainsert( $vendedor, $vendedor2, $descricao, $quantidade, $finalizado, $observacao, $cata, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO rcafe_provas ";
            $sql.="(id, vendedor, vendedor2, descricao, quantidade, finalizado, observacao, cata, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :vendedor, :vendedor2, :descricao, :quantidade, :finalizado, :observacao, :cata, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":vendedor", $vendedor);
            $insere->bindValue(":vendedor2", $vendedor2);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":quantidade", $quantidade);
            $insere->bindValue(":finalizado", $finalizado);
            $insere->bindValue(":observacao", $observacao);
            $insere->bindValue(":cata", $cata);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];


            header("Location: index.php?pg=Vprova_lista");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncprovaupdate( $id, $vendedor, $vendedor2, $descricao, $quantidade, $finalizado, $observacao, $cata ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="rcafe_provas ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE rcafe_provas ";
                $sql.="SET ";
                $sql .= "vendedor=:vendedor,
                vendedor2=:vendedor2,
                descricao=:descricao,
                quantidade=:quantidade,
finalizado=:finalizado,
observacao=:observacao,
cata=:cata
WHERE id=:id ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":vendedor", $vendedor);
                $atualiza->bindValue(":vendedor2", $vendedor2);
                $atualiza->bindValue(":descricao", $descricao);
                $atualiza->bindValue(":quantidade", $quantidade);
                $atualiza->bindValue(":finalizado", $finalizado);
                $atualiza->bindValue(":observacao", $observacao);
                $atualiza->bindValue(":cata", $cata);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse cadastro em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vprova_lista");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao



}//fim da classe