<?php
//gerado pelo geracode
function fncprovalist(){
    $sql = "SELECT * FROM rcafe_provas ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $provalista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $provalista;
}

function fncgetprova($id){
    $sql = "SELECT * FROM rcafe_provas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getrcafe_provas = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getrcafe_provas;
}
?>