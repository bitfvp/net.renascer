<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=2){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["admin"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Editar Allow-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="usuariosave";
    $us_er=fncgetusuario($_GET['id']);
    $al_low=fncgetallow($_GET['id']);
    //se não pertence a matriz não pode
    if ($us_er['matriz']!=2){
        header("Location: index.php?sca=&pg=Vhome");
        exit();
    }
}else{
    $a="usuarionew";
}
?>


<main class="container"><!--todo conteudo-->

        <form class="frmgrid" action="index.php?pg=Valloweditar&id=<?php echo $_GET['id'];?>&aca=<?php echo $a;?>" method="post">


        <div class="row">
            <div class="col">
                <input type="submit" value="SALVAR" class="btn btn-success btn-block" />
            </div>
            <?php if (isset($_GET['id']) and is_numeric($_GET['id'])){?>
                <div class="col">
                    <a href="index.php?pg=Valloweditar&id=<?php echo $_GET['id'];?>&aca=resetsenha" class="btn btn-danger btn-block">REINICIAR SENHA DO USUÁRIO</a>
                </div>
            <?php } ?>
        </div>

            <hr>
            <div class="row">
                <div class="col-md-4">
                    <label  class="large" for="nome">NOME:</label>
                    <input autocomplete="off" autofocus id="nome" type="text" class="form-control" name="nome" value="<?php echo $us_er['nome']; ?>" required/>
                </div>



                <div class="col-md-" id="nick_busca">
                    <label  class="large" for="">NICK:</label>
                    <input autocomplete="off" id="nick" type="text" class="form-control" name="nick" required value="<?php echo $us_er['nick']; ?>" onkeyup="value=value.replace(/[^a-zA-Z]/g,'')" />
                </div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#nick_busca input[type="text"]').on("keyup", function () {
                            /* Get input value on change */
                            var inputVal = $(this).val();
                            var vpess = '<?php echo $_GET['id'];?>';
                            if (inputVal.length) {
                                $.get("includes/nick_verifica.php", {term: inputVal, pessoa: vpess},).done(function (data) {
                                    // Display the returned data in browser
                                    $('#result').html(data);
                                });
                            } else {
                                $('#result').html('<strong class="text-center text-info"><i class="fa fa-keyboard"></i> Volte a digitar...</strong>');
                            }
                        });
                    });
                    $.ajaxSetup({cache: false});
                </script>

                <div class="col-md-2" id="result" name="result"></div>

                <div class="col-md-3">
                    <label  class="large" for="">E-MAIL<span>*:</span></label>
                    <input autocomplete="off" id="email" type="email" class="form-control" name="email" value="<?php echo $us_er['email']; ?>" required/>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label  class="large" for="">NASCIMENTO:</label>
                    <input name="nascimento" id="nascimento" type="date" class="form-control" value="<?php echo $us_er['nascimento']; ?>" required/>
                </div>

                <div class="col-md-4">
                    <label  class="large" for="">CPF:</label>
                    <input name="cpf" id="cpf" type="text" class="form-control" value="<?php echo $us_er['cpf']; ?>" required/>
                    <script>
                        $(document).ready(function(){
                            $('#cpf').mask('000.000.000-00', {reverse: false});
                        });
                    </script>
                </div>

                <div class="col-md-3">
                    <label  class="x-small" for="status">STATUS:</label>
                    <select name="status" id="status" class="form-control" >
                        // vamos criar a visualização de rf
                        <option selected="" value="<?php if($us_er['status']==""){$z=0; echo $z;}else{ echo $us_er['status'];} ?>">
                            <?php
                            if($us_er['status']==0){echo"DESATIVADO";}
                            if($us_er['status']==1){echo"ATIVO";} ?>
                        </option>
                        <option value="0">DESATIVADO</option>
                        <option value="1">ATIVO</option>
                    </select>
                </div>


            </div>

        <?php if (isset($_GET['id']) and is_numeric($_GET['id'])){?>
        <hr>
            <div class="row">
                <?php
                function Setvv($v){
                    if($v==0){echo"Não";}
                    if($v==1){echo"Sim";}
                }
                ?>
                <div class="col-md-3">
                    <label>ADMINISTRADOR:</label>
                    <select name="admin" id="admin" class="form-control">
                        <option selected="" value="<?php echo $al_low['admin']; ?>">
                            <?php
                            Setvv($al_low['admin']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 d-none">
                    <label>allow_1:</label>
                    <select name="allow_1" id="allow_1" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_1']; ?>">
                            <?php
                            Setvv($al_low['allow_1']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_2:</label>
                    <select name="allow_2" id="allow_2" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_2']; ?>">
                            <?php
                            Setvv($al_low['allow_2']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_3:</label>
                    <select name="allow_3" id="allow_3" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_3']; ?>">
                            <?php
                            Setvv($al_low['allow_3']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_4:</label>
                    <select name="allow_4" id="allow_4" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_4']; ?>">
                            <?php
                            Setvv($al_low['allow_4']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>ESCRITÓRIO:</label>
                    <select name="allow_5" id="allow_5" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_5']; ?>">
                            <?php
                            Setvv($al_low['allow_5']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>ESCRITÓRIO EDIÇÕES E EXCLUSÕES:</label>
                    <select name="allow_6" id="allow_6" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_6']; ?>">
                            <?php
                            Setvv($al_low['allow_6']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_7:</label>
                    <select name="allow_7" id="allow_7" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_7']; ?>">
                            <?php
                            Setvv($al_low['allow_7']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_8:</label>
                    <select name="allow_8" id="allow_8" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_8']; ?>">
                            <?php
                            Setvv($al_low['allow_8']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 d-none">
                    <label>allow_9:</label>
                    <select name="allow_9" id="allow_9" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_9']; ?>">
                            <?php
                            Setvv($al_low['allow_9']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>
                <div class="col-md-3 ">
                    <label>BALCÃO $ PROVA:</label>
                    <select name="allow_10" id="allow_10" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_10']; ?>">
                            <?php
                            Setvv($al_low['allow_10']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_11:</label>
                    <select name="allow_11" id="allow_11" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_11']; ?>">
                            <?php
                            Setvv($al_low['allow_11']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_12:</label>
                    <select name="allow_12" id="allow_12" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_12']; ?>">
                            <?php
                            Setvv($al_low['allow_12']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_13:</label>
                    <select name="allow_13" id="allow_13" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_13']; ?>">
                            <?php
                            Setvv($al_low['allow_13']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_14:</label>
                    <select name="allow_14" id="allow_14" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_14']; ?>">
                            <?php
                            Setvv($al_low['allow_14']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_15:</label>
                    <select name="allow_15" id="allow_15" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_15']; ?>">
                            <?php
                            Setvv($al_low['allow_15']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_16:</label>
                    <select name="allow_16" id="allow_16" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_16']; ?>">
                            <?php
                            Setvv($al_low['allow_16']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_17:</label>
                    <select name="allow_17" id="allow_17" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_17']; ?>">
                            <?php
                            Setvv($al_low['allow_17']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_18:</label>
                    <select name="allow_18" id="allow_18" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_18']; ?>">
                            <?php
                            Setvv($al_low['allow_18']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_19:</label>
                    <select name="allow_19" id="allow_19" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_19']; ?>">
                            <?php
                            Setvv($al_low['allow_19']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>

                <div class="col-md-3 d-none">
                    <label>allow_20:</label>
                    <select name="allow_20" id="allow_20" class="form-control">
                        <option selected="" value="<?php echo $al_low['allow_20']; ?>">
                            <?php
                            Setvv($al_low['allow_20']);
                            ?>
                        </option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>




            </div>
    <?php } ?>
  
</form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>