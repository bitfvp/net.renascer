<?php
$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

    if (!isset($_GET['wtela']) or !is_numeric($_GET['wtela'])){
        echo "
            <script>
        var wtela = window.innerWidth;
        window.location.href = 'index.php?pg=Vass&wtela='+wtela;
    </script>
        ";
    }else{
        $_SESSION['wtela']=$_GET['wtela']-30;
        $temphtela=$_SESSION['wtela']/5;
        $_SESSION['htela']=number_format($temphtela,0);
    }
    ?>

<style>
    .wrapper {
        position: relative;
        width: <?php echo $_SESSION['wtela'];?>px;
        height: <?php echo $_SESSION['htela'];?>px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    img {
        position: absolute;
        left: 0;
        top: 0;
    }

    .signature-pad {
        position: absolute;
        left: 0;
        top: 0;
        width:<?php echo $_SESSION['wtela'];?>px;
        height:<?php echo $_SESSION['htela'];?>px;
    }
</style>

<main class="container-fluid">







    <div class="row">
        <div class="col-12 pt-5">
            <div class="wrapper mb-2 bg-secondary">
                <img src="Hatch.jpg" width=<?php echo $_SESSION['wtela'];?> height=<?php echo $_SESSION['htela'];?> />
                <canvas id="signature-pad" class="signature-pad" width=<?php echo $_SESSION['wtela'];?> height=<?php echo $_SESSION['htela'];?>></canvas>
            </div>
        </div>
        <div class="col-6">
            <button id="save" class="btn btn-block btn-success btn-lg"><h1> GRAVAR ASSINATURA</h1></button>
        </div>
        <div class="col-6">
            <button id="clear" class="btn btn-block btn-outline-dark btn-lg"><h1> LIMPAR</h1></button>
        </div>

            <form action="index.php?pg=Vass&aca=newass&wtela=<?php echo $_SESSION['wtela'];?>" id="formlite" method="post" class="d-none">
                <textarea name="imageCheck" id="imageCheck" cols="30" rows="10"></textarea>
            </form>
    </div>




</main>

<?php
$oculta_online=1;
include_once("{$env->env_root}includes/footer.php");
?>
</body>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@3.0.0-beta.3/dist/signature_pad.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
<script>
    function download(dataURL, filename) {
        if (navigator.userAgent.indexOf("Safari") > -1 && navigator.userAgent.indexOf("Chrome") === -1) {
            window.open(dataURL);
        } else {
            var blob = dataURLToBlob(dataURL);
            var url = window.URL.createObjectURL(blob);

            var a = document.createElement("a");
            a.style = "display: none";
            a.href = url;
            a.download = filename;

            document.body.appendChild(a);
            a.click();

            window.URL.revokeObjectURL(url);
        }
    }

    function dataURLToBlob(dataURL) {
        // Code taken from https://github.com/ebidel/filer.js
        var parts = dataURL.split(';base64,');
        var contentType = parts[0].split(":")[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], { type: contentType });
    }


    var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        penColor: 'rgb(25, 78, 146)'
    });


    var saveButton = document.getElementById('save');
    var cancelButton = document.getElementById('clear');



    saveButton.addEventListener("click", function(event) {
        if (signaturePad.isEmpty()) {
            alert("Faça sua assinatura.");
        } else {
            var dataURL = signaturePad.toDataURL();
            //download(dataURL, "signature.png");
            //alert(dataURL);
            $("#imageCheck").val(dataURL);
                // alert("Faça sua assinatura.");
            document.getElementById("formlite").submit();
        }
    });

    cancelButton.addEventListener('click', function(event) {
        signaturePad.clear();
        window.location.href = 'index.php?pg=Vass';
    });

</script>
</html>
