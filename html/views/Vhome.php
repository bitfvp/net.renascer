<?php
$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if(isset($_SESSION['logado'])){
    switch ($_SESSION['matriz']){
        case 1:
            header("Location: {$env->env_url}ren");
            exit();
            break;
        case 2:
            header("Location: {$env->env_url}rcafe");
            exit();
            break;
        case 3:
            header("Location: {$env->env_url}yyy");
            exit();
            break;
        default:
            echo"<META HTTP-EQUIV=REFRESH CONTENT = '10;URL={$env->env_url}'>";
            $_SESSION['fsh']=[
                "flash"=>"Você não está lotado em lugar nenhum! Favor entrar em contato com o administrador",
                "type"=>"danger",
            ];
            killSession();
            break;
    }
}
include_once("{$env->env_root}includes/head.php");
?>
<main class="container-fluid">
    <main class="container  font3">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card text-center">
                    <div class="card-header">
                        <a href="index.php"><img class="img-responsive img-fluid" src="<?php echo $env->env_estatico; ?>img/renascer.png" alt="" title="<?php echo $env->env_nome; ?>"/></a>
                    </div>
                    <div class="card-body">
                        <form action="index.php?pg=Vlogin&aca=logar" method="post">
                            <input autofocus type="text" name="sysemail" id="sysemail"  placeholder="Usuário" minlength="5" maxlength="30" autocomplete="on" class="form-control mb-1" required>
                            <input type="password" name="syssenha" id="syssenha"  placeholder="Entre com sua senha" minlength="5" maxlength="30" autocomplete="off" class="form-control" required>
                            <button class="w-100 btn btn-lg btn-primary mt-3" type="submit">ENTRAR</button>
                        </form>
                    </div>
                    <div class="card-footer text-muted bg-white">
                        <small class="">
                            O Senhor enviará bênçãos aos seus celeiros e a tudo o que as suas mãos fizerem. O Senhor, o seu Deus, os abençoará na terra que lhes dá. Deuteronômio 28:8
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </main>
</main>

<?php
$oculta_online=1;
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>
