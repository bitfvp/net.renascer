<blockquote class="blockquote blockquote-info">
    <header>
        CONTRATO:
        <strong class="text-info"><?php echo $venda['prefixo']."-".$venda['contrato']." ".dataRetiraHora($venda["data_ts"]); ?>&nbsp;&nbsp;</strong>
    </header>
    <h6>
        DESCRIÇÃO:
        <strong class="text-info"><?php echo $venda['descricao']; ?>&nbsp;&nbsp;</strong><br>
        N.F.:
        <strong class="text-info"><?php echo $venda['nota_fiscal']; ?>&nbsp;&nbsp;</strong><br>
        CLIENTE:
        <strong class="text-info"><?php echo strtoupper(fncgetpessoa($venda['cliente'])['nome']); ?></strong><br>
        TRANSPORTADORA:
        <strong class="text-info"><?php echo strtoupper(fncgetpessoa($venda['transportadora'])['nome']); ?></strong><br>

        <footer class="blockquote-footer">
            <?php echo fncgetusuario($venda['usuario'])['nome']?>&nbsp;&nbsp;
        </footer>
</blockquote>