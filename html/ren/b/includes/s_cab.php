<blockquote class="blockquote blockquote-success">
    <div class="row">
        <div class="col-md-8">
            <h6>
                MOTORISTA:
                <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['motorista'])['nome']); ?></strong><br>
                PLACA(S):
                <strong class="text-info"><?php echo $entrada['placa']; ?></strong><br>


                ENTRADA:
                <strong class="text-info"><?php echo $entrada['obs']; ?></strong><br>
                PESO:
                <strong class="text-info"><?php echo number_format($entrada['peso_entrada'],3,',',' '); ?> Kg</strong><br>
                <strong class="text-info"><?php echo datahoraBanco2data($entrada['data_ts']); ?></strong><br>
                <hr>

                SAÍDA:
                <strong class="text-info"><?php echo $entrada['obs2']; ?></strong><br>
                PESO:
                <strong class="text-info"><?php echo number_format($entrada['peso_saida'],3,',',' '); ?> Kg</strong><br>
                <strong class="text-info"><?php echo datahoraBanco2data($entrada['datahora_saida']); ?></strong><br>

            </h6>
        </div>

        <div class="col-md-4">
            <div class="row">
                <div class="col-12">
                    <h6>Ass. motorista</h6>
                </div>

                    <?php
                    $sql = "SELECT * FROM ren_dados WHERE indicador=? and tipo=2 and status=1";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1,$entrada['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $dados = $consulta->fetchAll();
                    $dadoscount = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;

                    foreach ($dados as $dado){
                        echo "<div class='col-12 text-center'>";
                            echo "<img class=' img-thumbnail' id='' src='../../dados/renascer/{$dado['tipo']}/{$dado['arquivo']}.{$dado['extensao']}' />";
                            $act="index.php?pg=Vs&id_s={$_GET['id_s']}&page_bk=Vs&indice_bk=id_s&id_bk={$_GET['id_s']}&aca=apagaassinatura&arq={$dado['arquivo']}";
                            echo "<a href='{$act}' target='_self' title='Delete image' class='far fa-trash-alt btn btn-sm'></a>";
                        echo "</div>";
                    }
                    ?>






            </div>
        </div>

    </div>
        <footer class="blockquote-footer">
            <?php echo fncgetusuario($entrada['usuario'])['nome']?>&nbsp;&nbsp;
        </footer>
</blockquote>