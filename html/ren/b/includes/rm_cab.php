<blockquote class="blockquote blockquote-success">
    <header>
        ROMANEIO:
        <strong class="text-info"><?php echo $romaneio_tipo.$entrada['ref'].$entrada['romaneio']; ?>&nbsp;&nbsp;</strong>
    </header>
    <div class="row">
        <div class="col-md-8">
            <h6>
                NOTA:
                <strong class="text-info"><?php echo $entrada['nota']; ?>&nbsp;&nbsp;</strong>
                OCR:
                <strong class="text-info"><?php echo $entrada['ocr']; ?>&nbsp;&nbsp;</strong>
                <br>
                FORNECEDOR:
                <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?></strong><br>
                EMPRESA/PRODUTOR:
                <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['empresa'])['nome']); ?></strong><br>
                CORRETOR:
                <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['corretor'])['nome']); ?></strong><br>
                MOTORISTA:
                <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['motorista'])['nome']); ?></strong><br>

                PLACA(S):
                <strong class="text-info"><?php echo $entrada['placa']; ?></strong>

                POSSUI CONTROLE INTERNO:
                <strong class="text-info"><?php
                    switch ($entrada['controle_interno']){
                        case 1:
                            echo "POSSUI CONTROLE INTERNO";
                            break;
                        case 2:
                            echo "NÃO POSSUI CONTROLE INTERNO";
                    }?></strong>
            </h6>
        </div>

        <div class="col-md-4">
            <div class="row">
                <div class="col-12">
                    <h6>Ass. motorista</h6>
                </div>

                <div class="col-12">
                    <?php
                    $sql = "SELECT * FROM tbl_assinatura WHERE entrada=?";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1,$entrada['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $assinatura = $consulta->fetch();
                    $assinaturacount = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;
                    if ($assinaturacount>0){
                        echo "<img class=' img-thumbnail' id='' src='{$assinatura['assinatura']}' />";
                    }
                    ?>
                </div>



                    <?php
                    $sql = "SELECT * FROM ren_dados WHERE indicador=? and tipo=1 and status=1";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindParam(1,$entrada['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    $dados = $consulta->fetchAll();
                    $dadoscount = $consulta->rowCount();
                    $sql=null;
                    $consulta=null;

                    foreach ($dados as $dado){
                        echo "<div class='col-12 text-center'>";
                            echo "<img class=' img-thumbnail' id='' src='../../dados/renascer/{$dado['tipo']}/{$dado['arquivo']}.{$dado['extensao']}' />";
                            $act="index.php?pg=Vrm&id_e={$_GET['id_e']}&page_bk=Vrm&indice_bk=id_e&id_bk={$_GET['id_e']}&aca=apagaassinatura&arq={$dado['arquivo']}";
                            echo "<a href='{$act}' target='_self' title='Delete image' class='far fa-trash-alt btn btn-sm'></a>";
                        echo "</div>";
                    }
                    ?>






            </div>
        </div>

    </div>
        <footer class="blockquote-footer">
            <?php echo fncgetusuario($entrada['usuario'])['nome']?>&nbsp;&nbsp;
        </footer>
</blockquote>