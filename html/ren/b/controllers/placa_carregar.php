<?php
function fncplacalist($motorista){
    $sql = "SELECT * FROM ren_motorista_placas where motorista=? ORDER BY data_ts";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$motorista);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $placalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $placalista;
}