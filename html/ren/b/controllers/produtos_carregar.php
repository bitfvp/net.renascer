<?php

function fncgetprodutos($id){
    $sql = "SELECT * FROM ren_produtos WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getproduto = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getproduto;
}

function fncprodutoslist(){
    $sql = "SELECT * FROM ren_produtos ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $produtoslista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $produtoslista;
}