<?php
//metodo
if($startactiona==1 && $aca=="apagarpesagemve"){

    if (isset($_GET['id_p']) and is_numeric($_GET['id_p'])){
        $id_p=$_GET['id_p'];
    }else{
        $_SESSION['fsh']=[
            "flash"=>"Houve um erro!!, não encontrado o id da pesagem",
            "type"=>"warning",
        ];
        header("Location: {$env->env_url_mod}index.php?pg=Vv_lista");
        exit();
    }

    try {
        $sql = "DELETE FROM `ren_vendas_pesagens` WHERE id = :id ";
        global $pdo;
        $exclui = $pdo->prepare($sql);
        $exclui->bindValue(":id", $id_p);
        $exclui->execute();
    } catch (PDOException $error_msg) {
        echo 'Erro:' . $error_msg->getMessage();
    }

    $_SESSION['fsh']=[
        "flash"=>"Pesagem apagada com sucesso!!",
        "type"=>"success",
    ];
    header("Location: index.php?pg=Vv_lista");
    exit();

}

