<?php

function fncgetlocal($id){
    $sql = "SELECT * FROM ren_locais WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getlocal = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getlocal;
}

function fnclocaislist(){
    $sql = "SELECT * FROM ren_locais ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $locaislista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $locaislista;
}