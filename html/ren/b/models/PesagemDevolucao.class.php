<?php
class PesagemDevolucao{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpesagemdevolucaonew(
        $entrada,$motorista
    ){
        //tratamento das variaveis
        //não ter


            //inserção no banco
                try {
                    $sql="INSERT INTO ren_devolucao_pesagens ";
                    $sql .= "(id,
                    usuario,
                    devolucao,
                    motorista
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :usuario,
                    :devolucao,
                    :motorista
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":usuario", $_SESSION['id']);
                    $insere->bindValue(":devolucao", $entrada);
                    $insere->bindValue(":motorista", $motorista);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }


        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            $sql = "SELECT Max(id) FROM ";
            $sql.="ren_devolucao_pesagens";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mid = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $maid=$mid[0];

            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vd_p2&id_e={$entrada}&id_p={$maid}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpesagemdevolucaoedit(
        $id_p,$entrada,$motorista
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_devolucao_pesagens ";
            $sql.="WHERE id=:id_p and devolucao=:devolucao";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id_p", $id_p);
            $consulta->bindValue(":devolucao", $entrada);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pesagem_datahora_saida=$consulta->fetch();
            $pesagem_count=$consulta->rowCount();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if ($pesagem_datahora_saida==null){

        }

        if($pesagem_count!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_devolucao_pesagens ";
                $sql.="SET ";
                $sql .= "motorista=:motorista, ";
                $sql .= " ";
                $sql .= "WHERE id=:id_p and devolucao=:devolucao ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":peso_saida", $motorista);
                $atualiza->bindValue(":id_p", $id_p);
                $atualiza->bindValue(":devolucao", $entrada);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pesagem cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }



        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vd_p2&id_e={$entrada}&id_p={$id_p}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpesagemdevolucaoedit2(
        $id_p,$entrada,$placa,$peso_entrada
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_devolucao_pesagens ";
            $sql.="WHERE id=:id_p and devolucao=:devolucao";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id_p", $id_p);
            $consulta->bindValue(":devolucao", $entrada);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pesagem_datahora_saida=$consulta->fetch();
            $pesagem_count=$consulta->rowCount();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if ($pesagem_datahora_saida==null){

        }

        if($pesagem_count!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_devolucao_pesagens ";
                $sql.="SET ";
                $sql .= "placa=:placa, peso_entrada=:peso_entrada ";
                $sql .= " ";
                $sql .= "WHERE id=:id_p and devolucao=:devolucao ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":placa", $placa);
                $atualiza->bindValue(":peso_entrada", $peso_entrada);
                $atualiza->bindValue(":id_p", $id_p);
                $atualiza->bindValue(":devolucao", $entrada);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pesagem cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }



        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vd&id_e={$entrada}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpesagemdevolucaofinal(
        $id_p,$entrada,$peso_saida
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_devolucao_pesagens ";
            $sql.="WHERE id=:id_p and devolucao=:devolucao";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id_p", $id_p);
            $consulta->bindValue(":devolucao", $entrada);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pesagem_datahora_saida=$consulta->fetch();
            $pesagem_count=$consulta->rowCount();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if ($pesagem_datahora_saida==null){

        }

        if($pesagem_count!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_devolucao_pesagens ";
                $sql.="SET ";
                $sql .= "peso_saida=:peso_saida, ";
                if ($pesagem_datahora_saida['datahora_saida']==null or $pesagem_datahora_saida['datahora_saida']==0 or $pesagem_datahora_saida['datahora_saida']==""){
                    $sql .= "datahora_saida=CURRENT_TIMESTAMP ";
                }
                $sql .= " ";
                $sql .= "WHERE id=:id_p and devolucao=:devolucao ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":peso_saida", $peso_saida);
                $atualiza->bindValue(":id_p", $id_p);
                $atualiza->bindValue(":devolucao", $entrada);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pesagem cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vd&id_e={$entrada}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }




}
