<?php
class Pesagem{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpesagemnew(
        $entrada,$produto,$embalagem,$peso_entrada,$obs_entrada
    ){
        //tratamento das variaveis
        //não ter


            //inserção no banco
                try {
                    $sql="INSERT INTO ren_entradas_pesagens ";
                    $sql .= "(id,
                    usuario,
                    entrada,
                    produto,
                    embalagem,
                    peso_entrada,
                    obs_entrada
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :usuario,
                    :entrada,
                    :produto,
                    :embalagem,
                    :peso_entrada,
                    :obs_entrada
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":usuario", $_SESSION['id']);
                    $insere->bindValue(":entrada", $entrada);
                    $insere->bindValue(":produto", $produto);
                    $insere->bindValue(":embalagem", $embalagem);
                    $insere->bindValue(":peso_entrada", $peso_entrada);
                    $insere->bindValue(":obs_entrada", $obs_entrada);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }




        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];


            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

//                    header("Location: index.php?pg=Vrm&id_e={$entrada}");
//                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncpesagemedit(
        $id_p,$entrada,$produto,$embalagem,$peso_entrada,$obs_entrada
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_entradas_pesagens ";
            $sql.="WHERE id=:id_p and entrada=:entrada";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id_p", $id_p);
            $consulta->bindValue(":entrada", $entrada);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pesagem_count=$consulta->rowCount();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if($pesagem_count!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_entradas_pesagens ";
                $sql.="SET ";
                $sql .= "produto=:produto,
                    embalagem=:embalagem,
                    peso_entrada=:peso_entrada,
                    obs_entrada=:obs_entrada
                WHERE id=:id_p and entrada=:entrada ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":produto", $produto);
                $atualiza->bindValue(":embalagem", $embalagem);
                $atualiza->bindValue(":peso_entrada", $peso_entrada);
                $atualiza->bindValue(":obs_entrada", $obs_entrada);
                $atualiza->bindValue(":id_p", $id_p);
                $atualiza->bindValue(":entrada", $entrada);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pesagem cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vrm&id_e={$entrada}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncpesagemedit2(
        $id_p,$entrada,$peso_saida,$embalagem_devolvida,$obs_saida
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_entradas_pesagens ";
            $sql.="WHERE id=:id_p and entrada=:entrada";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id_p", $id_p);
            $consulta->bindValue(":entrada", $entrada);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pesagem_datahora_saida=$consulta->fetch();
            $pesagem_count=$consulta->rowCount();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if ($pesagem_datahora_saida==null){

        }

        if($pesagem_count!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_entradas_pesagens ";
                $sql.="SET ";
                $sql .= "peso_saida=:peso_saida, embalagem_devolvida=:embalagem_devolvida, ";
                if ($pesagem_datahora_saida['datahora_saida']==null or $pesagem_datahora_saida['datahora_saida']==0 or $pesagem_datahora_saida['datahora_saida']==""){
                    $sql .= "datahora_saida=CURRENT_TIMESTAMP, ";
                }
                $sql .= "obs_saida=:obs_saida ";
                $sql .= "WHERE id=:id_p and entrada=:entrada ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":peso_saida", $peso_saida);
                $atualiza->bindValue(":embalagem_devolvida", $embalagem_devolvida);
                $atualiza->bindValue(":obs_saida", $obs_saida);
                $atualiza->bindValue(":id_p", $id_p);
                $atualiza->bindValue(":entrada", $entrada);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pesagem cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vrm&id_e={$entrada}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }





}
