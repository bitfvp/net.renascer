<?php
class Expedicao{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncexpedicaonew( $id_e,$saca,$bb,$granel,$saca_q,$bb_q,$granel_q,$obs ){
        //tratamento das variaveis
        //não ter



            //inserção no banco
                try {
                    $sql="INSERT INTO ren_entradas_expedicao ";
                    $sql .= "(id,
                    usuario,
                    entrada,
                    saca,
                    bb,
                    granel,
                    saca_q,
                    bb_q,
                    granel_q,
                    obs
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :usuario,
                    :entrada,
                    :saca,
                    :bb,
                    :granel,
                    :saca_q,
                    :bb_q,
                    :granel_q,
                    :obs
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":usuario", $_SESSION['id']);
                    $insere->bindValue(":entrada", $id_e);
                    $insere->bindValue(":saca", $saca);
                    $insere->bindValue(":bb", $bb);
                    $insere->bindValue(":granel", $granel);
                    $insere->bindValue(":saca_q", $saca_q);
                    $insere->bindValue(":bb_q", $bb_q);
                    $insere->bindValue(":granel_q", $granel_q);
                    $insere->bindValue(":obs", $obs);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }



        if(isset($insere)){

//            $_SESSION['fsh']=[
//                "flash"=>"expedição lançada com sucesso!!",
//                "type"=>"success",
//            ];


        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncexpedicaoedit(
        $id_e,$saca,$bb,$granel,$saca_q,$bb_q,$granel_q,$obs
    ){
        //tratamento das variaveis

            //inserção no banco
            try {
                $sql="UPDATE ren_entradas_expedicao";
                $sql.=" SET";
                $sql .= " saca=:saca,
                    bb=:bb,
                    granel=:granel,
                    saca_q=:saca_q,
                    bb_q=:bb_q,
                    granel_q=:granel_q,
                    obs=:obs
                WHERE entrada=:id_e";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":saca", $saca);
                $atualiza->bindValue(":bb", $bb);
                $atualiza->bindValue(":granel", $granel);
                $atualiza->bindValue(":saca_q", $saca_q);
                $atualiza->bindValue(":bb_q", $bb_q);
                $atualiza->bindValue(":granel_q", $granel_q);
                $atualiza->bindValue(":obs", $obs);
                $atualiza->bindValue(":id_e", $id_e);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }

        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
//            $_SESSION['fsh']=[
//                "flash"=>"Atualização realizado com sucesso!!",
//                "type"=>"success",
//            ];

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }



    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



}
