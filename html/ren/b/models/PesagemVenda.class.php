<?php
class PesagemVenda{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpesagemvendanew(
        $venda,$motorista,$peso_entrada
    ){
        //tratamento das variaveis
        //não ter


            //inserção no banco
                try {
                    $sql="INSERT INTO ren_vendas_pesagens ";
                    $sql .= "(id,
                    usuario,
                    venda,
                    motorista,
                    peso_entrada
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :usuario,
                    :venda,
                    :motorista,
                    :peso_entrada
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":usuario", $_SESSION['id']);
                    $insere->bindValue(":venda", $venda);
                    $insere->bindValue(":motorista", $motorista);
                    $insere->bindValue(":peso_entrada", $peso_entrada);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }




        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            $sql = "SELECT Max(id) FROM ";
            $sql.="ren_vendas_pesagens";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mid = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vv_p2&id_v={$venda}&id_p={$maid}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpesagemvendaedit(
        $id_p,$venda,$motorista,$peso_entrada
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_vendas_pesagens ";
            $sql.="WHERE ren_vendas_pesagens.id=:id_p and ren_vendas_pesagens.venda=:venda";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id_p", $id_p);
            $consulta->bindValue(":venda", $venda);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pesagem_datahora_saida=$consulta->fetch();
            $pesagem_count=$consulta->rowCount();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if ($pesagem_datahora_saida==null){

        }

        if($pesagem_count!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_vendas_pesagens ";
                $sql.="SET ";
                $sql .= "motorista=:motorista, peso_entrada=:peso_entrada ";
                $sql .= " ";
                $sql .= "WHERE ren_vendas_pesagens.id=:id_p and ren_vendas_pesagens.venda=:venda ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":motorista", $motorista);
                $atualiza->bindValue(":peso_entrada", $peso_entrada);
                $atualiza->bindValue(":id_p", $id_p);
                $atualiza->bindValue(":venda", $venda);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Errogg' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pesagem cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vv_p2&id_v={$venda}&id_p={$id_p}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }



    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpesagemvendaedit2(
        $id_p,$venda,$placa
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_vendas_pesagens ";
            $sql.="WHERE ren_vendas_pesagens.id=:id_p and ren_vendas_pesagens.venda=:venda";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id_p", $id_p);
            $consulta->bindValue(":venda", $venda);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pesagem_datahora_saida=$consulta->fetch();
            $pesagem_count=$consulta->rowCount();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if ($pesagem_datahora_saida==null){

        }

        if($pesagem_count!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_vendas_pesagens ";
                $sql.="SET ";
                $sql .= "placa=:placa ";
                $sql .= " ";
                $sql .= "WHERE ren_vendas_pesagens.id=:id_p and ren_vendas_pesagens.venda=:venda ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":placa", $placa);
                $atualiza->bindValue(":id_p", $id_p);
                $atualiza->bindValue(":venda", $venda);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Errogg' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pesagem cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vv&id_v={$venda}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }




    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpesagemvendaeditfinal(
        $id_p,$venda,$peso_saida
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_vendas_pesagens ";
            $sql.="WHERE ren_vendas_pesagens.id=:id_p and ren_vendas_pesagens.venda=:venda";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id_p", $id_p);
            $consulta->bindValue(":venda", $venda);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pesagem_datahora_saida=$consulta->fetch();
            $pesagem_count=$consulta->rowCount();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if ($pesagem_datahora_saida==null){

        }

        if($pesagem_count!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_vendas_pesagens ";
                $sql.="SET ";
                if ($pesagem_datahora_saida['datahora_saida']==null or $pesagem_datahora_saida['datahora_saida']==0 or $pesagem_datahora_saida['datahora_saida']==""){
                    $sql .= "datahora_saida=CURRENT_TIMESTAMP, ";
                }
                $sql .= "peso_saida=:peso_saida ";
                $sql .= " ";
                $sql .= "WHERE ren_vendas_pesagens.id=:id_p and ren_vendas_pesagens.venda=:venda ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":peso_saida", $peso_saida);
                $atualiza->bindValue(":id_p", $id_p);
                $atualiza->bindValue(":venda", $venda);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Errogg' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pesagem cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vv&id_v={$venda}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }





}
