<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");

$sql1='SELECT
ren_vendas.id,
ren_vendas.`status`,
ren_vendas.data_ts,
ren_vendas.usuario,
ren_vendas.prefixo,
ren_vendas.contrato,
ren_vendas.descricao,
ren_vendas.cliente,
ren_vendas.transportadora,
ren_vendas.nota_fiscal,
ren_vendas.retorno_de_estoque
FROM
ren_vendas
INNER JOIN ren_pessoas AS tbl_cliente ON ren_vendas.cliente = tbl_cliente.id ';


$sqlwhere = 'WHERE ren_vendas.id <> 0 ';
if (isset($_GET['scb']) and $_GET['scb']!=0 and $_GET['scb']!='') {
    $inicial=$_GET['scb'];
    $inicial.=" 00:00:01";
    $final=$_GET['scb'];
    $final.=" 23:59:59";
    $sqlwhere .=" AND ((ren_vendas.data_ts)>=:inicial) And ((ren_vendas.data_ts)<=:final) ";
}else{

    if ((isset($_GET['sca']) and $_GET['sca']!='') or (isset($_GET['scb']) and $_GET['scb']!='') or (isset($_GET['scc']) and $_GET['scc']!='')){
        //tempo todo
        $inicial="2021-01-01";
        $inicial.=" 00:00:01";
        $final=date("Y-m-d");
        $final.=" 23:59:59";
        $sqlwhere .=" AND ((ren_vendas.data_ts)>=:inicial) And ((ren_vendas.data_ts)<=:final) ";
    }else{
        //apenas dia anterior
        $inicial=date("Y-m-d",strtotime("-7 days"));
        $inicial.=" 00:00:01";
        $final=date("Y-m-d");
        $final.=" 23:59:59";
        $sqlwhere .=" AND ((ren_vendas.data_ts)>=:inicial) And ((ren_vendas.data_ts)<=:final) ";
    }
}
if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
    $sqlwhere .="AND ren_vendas.contrato LIKE '%".$_GET['sca']."%' ";
}

if (isset($_GET['scc']) and  $_GET['scc']!='') {
    $sqlwhere .="AND tbl_cliente.nome  LIKE '%".$_GET['scc']."%' ";
}


$sqlorder ="order by ren_vendas.data_ts DESC LIMIT 0,50 ";


try{
    $sql = $sql1.$sqlwhere.$sqlorder;
    global $pdo;
    $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container">
    <h3 class="form-cadastro-heading"><a href="index.php?pg=Vv_lista" class="text-decoration-none text-dark">Contratos de venda</a></h3>
    <hr>

    <div class="row">
        <div class="col-md-8">
            <form action="index.php" method="get">
                <div class="input-group mb-3 float-left">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
                    </div>
                    <input name="pg" value="Vv_lista" hidden/>
                    <input type="text" name="scc" id="scc" autofocus="true" autocomplete="off" class="form-control" placeholder="Cliente..." value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
                    <input type="text" name="sca" id="sca" autocomplete="off" class="form-control" placeholder="Contrato..." value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                    <input type="date" name="scb" id="scb" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
                </div>
            </form>

            <script type="text/javascript">
                function selecionaTexto() {
                    document.getElementById("scc").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>
    </div>


    <table class="table table-sm table-stripe table-hover table-condensed">
        <thead class="thead-dark">
        <tr>
            <th scope="col" class="text-center">#</th>
            <th scope="col"><small>CONTRATO</small></th>
            <th scope="col"><small>DESCRIÇÃO</small></th>
            <th scope="col"><small>CLIENTE</small></th>
            <th scope="col"><small>TRANSPORTADORA</small></th>
            <th scope="col"><small>N.F.</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="5" >
            </th>
            <td colspan="4"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);
        }
        if($_GET['scc']!="" and isset($_GET['scc'])) {
            $stc = strtoupper($_GET['scc']);
            define('CSC', $stc);
        }
        foreach ($entradas as $dados){
            $ve_id = $dados["id"];
            $prefixo = $dados["prefixo"];
            $contrato = $dados["contrato"];
            $descricao = $dados["descricao"];

            $cliente = fncgetpessoa($dados["cliente"])['nome'];
            $transportadora = fncgetpessoa($dados["transportadora"])['nome'];
            $nota_fiscal = $dados["nota_fiscal"];
            $usuario = $dados["usuario"];


            try{
                $sql = "SELECT * FROM "
                    ."ren_vendas_pesagens "
                    ."WHERE ren_vendas_pesagens.venda=:venda "
                    ."order by ren_vendas_pesagens.data_ts DESC ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":venda", $ve_id);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }
            $pesagens = $consulta->fetchAll();
            $pesagens_quant = $consulta->rowCount();
            $sql = null;
            $consulta = null;

            ?>

            <tr id="<?php echo $ve_id;?>" class="bg-danger-light">
                <th scope="row" id="">
                    <small><?php echo $ve_id; ?></small>
                </th>
                <th scope="row" id="">
                    <?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $ccc = strtoupper($contrato);
                        $cc = explode(CSA, $ccc);
                        $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                        $contrato = $c;
                    }else{
                        $contrato = strtoupper($contrato);
                    }
                    ?>


                        <?php echo $prefixo."-".$contrato." ".dataRetiraHora($dados["data_ts"]); ?>
                </th>
                <td><?php echo $descricao; ?></td>
                <td><?php
                    if($_GET['scc']!="" and isset($_GET['scc'])) {
                        $stc = CSC;
                        $ccc = strtoupper($cliente);
                        $cc = explode(CSC, $ccc);
                        $c = implode("<span class='text-danger'>{$stc}</span>", $cc);
                        echo $c;
                    }else{
                        echo strtoupper($cliente);
                    }
                    ?></td>
                <td><?php echo strtoupper($transportadora); ?></td>
                <td><?php echo $nota_fiscal; ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vv&id_v=<?php echo $ve_id; ?>" target="" title="nova pesagem" class="btn btn-sm btn-warning fas fa-search-plus text-dark">
                            ACESSAR
                        </a>
                        <a href="index.php?pg=Vv_p&id_v=<?php echo $ve_id; ?>" target="" title="nova pesagem" class="btn btn-sm btn-info fas fa-plus text-dark">
                            NOVA PESAGEM
                        </a>
                    </div>
                </td>
            </tr>

            <?php
            if ($pesagens_quant>0){?>

                <tr>
                    <td class="bg-danger-light">
                        <i class="fas fa-balance-scale fa-2x"></i>
                    </td>
                    <td colspan="6">
                        <table class="table table-stripe table-hover table-condensed">
                            <tbody>
                            <?php
                            foreach ($pesagens as $dados){
                                if ($dados['datahora_saida']==null or $dados['datahora_saida']==0 or $dados['datahora_saida']==""){
                                    $cordalinha = "  ";
                                }else{
                                    $cordalinha = " text-warning bg-secondary ";
                                }
                                $pe_id = $dados["id"];
                                $data_inicial = datahoraBanco2data($dados["data_ts"]);
                                $peso_entrada = $dados["peso_entrada"];
                                $motorista = fncgetpessoa($dados["motorista"])['nome'];
                                $placa = $dados["placa"];
                                $peso_saida = $dados["peso_saida"];
                                $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
                                if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                                    $infopeso=$dados["peso_entrada"]."-PB";
                                }else{
                                    $pl=$dados["peso_saida"]-$dados["peso_entrada"];
                                    $ss=$pl/60;
                                    $infopeso=number_format($pl,0,',',' ') . "Kg " . number_format($ss,2,',',' ') . "v";
                                }
                                $usuario = fncgetusuario($dados["usuario"])['nome'];
                                ?>

                                <tr id="" class="<?php echo $cordalinha; ?>">
                                    <td><?php echo strtoupper($motorista); ?></td>
                                    <td><?php echo $placa; ?></td>
                                    <td><?php echo $data_inicial; ?></td>
                                    <td><?php echo $infopeso ?></td>
                                    <td><?php echo $datahora_saida; ?></td>

                                    <td class="text-center">
                                        <div class="btn-group" role="group" aria-label="">
                                            <?php if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0 or $allow["allow_6"]==1){?>
                                                <a href="index.php?pg=Vv_p&id_v=<?php echo $ve_id; ?>&id_p=<?php echo $pe_id; ?>" title="Editar pesagem" class="btn btn-sm btn-primary">
                                                    <i class="fas fa-pen" title="Editar Pesagem"></i>
                                                    <br>EDITA
                                                </a>

                                                <a href="index.php?pg=Vv_pfinal&id_v=<?php echo $ve_id; ?>&id_p=<?php echo $pe_id; ?>" title="Finalizar pesagem" class="btn btn-sm btn-success">
                                                    <i class="fas fa-check-double"></i>
                                                    <br>FINALIZA
                                                </a>

                                            <?php } ?>

                                            <?php if ($dados["datahora_saida"]!="" or $dados["datahora_saida"]!=0){?>
                                                <a href="index.php?pg=Vv_print1&id_v=<?php echo $ve_id ?>&id_p=<?php echo $pe_id; ?>" target="_blank" title="Imprimir comprovante" class="btn btn-sm btn-outline-success">
                                                <i class="fas fa-print"></i>
                                                <br>IMPRIME
                                                </a>
                                            <?php } ?>

                                            <a href="index.php?pg=V_ass&id_pessoa=<?php echo $dados["motorista"]; ?>&tipo=4&indicador=<?php echo $pe_id; ?>&page_bk=Vv&indice_bk=id_v&id_bk=<?php echo $ve_id; ?>" title="Selecionar assinatura" class="btn btn-sm btn-outline-dark">
                                                <i class="fas fa-file-signature"></i>
                                                <br>ASS.
                                            </a>


                                            <?php if ($allow["allow_6"]==1){?>
                                                <div class="dropdown show">
                                                    <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-trash"><br>EXCLUI</i>
                                                    </a>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                        <a class="dropdown-item" href="#">Não</a>
                                                        <a class="dropdown-item bg-danger" href="index.php?pg=Vv_lista&aca=apagarpesagemve&id_p=<?php echo $pe_id; ?>">Apagar</a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }?>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <?php
            } ?>
            <tr>
                <td class="bg-transparent"  colspan="8" height="50px">
                </td>
            </tr>

<?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>