<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
    $entrada=fncgetentrada($_GET['id_e']);

    $expedicao=fncgetexpedicao($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Ve_lista");
    exit();
}
$romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);

?>

<main class="container">
    <div class="row">
        <div class="col-6 offset-3">
            <img src="<?php echo $env->env_estatico; ?>img/renascer.png" class="img-fluid"  alt="">
        </div>
        <div class="col-3">
            <h1 class="mt-5"><?php echo $romaneio_tipo.$entrada['ref'].$entrada['romaneio']; ?></h1>
        </div>
    </div>

    <hr class="hrgrosso mb-0">
    <hr class="hrgrosso my-0">

    <div class="row">
        <div class="col-8 offset-2 text-center">
            <h1 class="my-0 display-4">Expedição de Entradas</h1>
            <h3 class="my-0"><i> <strong style="letter-spacing: -3px;">--------</strong> Cafés em grãos cru <strong style="letter-spacing: -3px;">--------</strong> </i></h3>
        </div>
    </div>

    <hr class="hrgrosso mb-0">

    <div class="row">
        <div class="col-6 offset-3 text-center">
            <h1 class="">Controle Portaria</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-8">
            <h4 class="my-0">Produtor: <strong><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?></strong></h4>
        </div>
        <div class="col-4 text-right">
            <h4 class="my-0">Data: <strong><?php echo dataRetiraHora($entrada['data_ts']); ?></strong></h4>
        </div>

        <div class="col-8">
            <h4 class="my-0">Corretor: <strong><?php echo strtoupper(fncgetpessoa($entrada['corretor'])['nome']); ?></strong></h4>
        </div>
        <div class="col-4 text-right">
            <h4 class="my-0">NOTA: <strong><?php echo $entrada['nota']; ?></strong> OCR: <strong><?php echo $entrada['ocr']; ?></strong></h4>
        </div>

        <div class="col-8">
            <h4 class="my-0">Motorista: <strong><?php echo strtoupper(fncgetpessoa($entrada['motorista'])['nome']); ?></strong></h4>
        </div>
        <div class="col-4 text-right">
            <h5 class="my-0">PLACAS: <strong><?php echo $entrada['placa']; ?></strong></h5>
        </div>
    </div>

    <hr class="hrgrosso">

    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-3">
                    <?php
                    if (isset($expedicao['saca']) and $expedicao['saca']==1){
                        echo "<h2><i class='text-dark border border-dark'>_X_</i> Sacaria</h2>";
                    }else{
                        echo "<h2><i class='text-light border border-dark'>___</i> Sacaria</h2>";
                    }

                    if (isset($expedicao['bb']) and $expedicao['bb']==1){
                        echo "<h2><i class='text-dark border border-dark'>_X_</i> Big Bags</h2>";
                    }else{
                        echo "<h2><i class='text-light border border-dark'>___</i> Big Bags</h2>";
                    }

                    if (isset($expedicao['granel']) and $expedicao['granel']==1){
                        echo "<h2><i class='text-dark border border-dark'>_X_</i> Granel</h2>";
                    }else{
                        echo "<h2><i class='text-light border border-dark'>___</i> Granel</h2>";
                    }
                    ?>

                </div>
                <div class="col-4">
                    <?php
                    if (isset($expedicao['saca_q']) and strlen($expedicao['saca_q'])>1){
                        echo "<h2>Quant.: {$expedicao['saca_q']}</h2>";
                    }else{
                        echo "<h2>Quant.:___________</h2>";
                    }

                    if (isset($expedicao['bb_q']) and strlen($expedicao['bb_q'])>1){
                        echo "<h2>Quant.: {$expedicao['bb_q']}</h2>";
                    }else{
                        echo "<h2>Quant.:___________</h2>";
                    }

                    if (isset($expedicao['granel_q']) and strlen($expedicao['granel_q'])>1){
                        echo "<h2>Quant.: {$expedicao['granel_q']}</h2>";
                    }else{
                        echo "<h2>Quant.:___________</h2>";
                    }
                    ?>
                </div>
                <div class="col-5 border border-dark">
                    <h3>
                        <strong>Obs.:<?php if (isset($expedicao['obs']) and strlen($expedicao['obs'])>1){echo $expedicao['obs'];}?></strong>
                    </h3>
                </div>
            </div>
        </div>
    </div>

    <hr class="hrgrosso my-0">

    <div class="row">
        <div class="col-8">
            <h1 class="float-right mb-0">Controle Interno</h1>
        </div>
        <div class="col-4">
            <h5 class="float-right mt-3 ">Embalagem: <i class="text-light border border-dark">___</i> Fica  <i class="text-light border border-dark">___</i> Leva</h5>
        </div>
    </div>

    <hr class="hrgrosso my-0">

    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-4 text-center">
                    <h2 class="mt-2">CAIXAS</h2>
                    <table class="table table-bordered text-center">
                        <tr>
                            <td class="py-0"><h2><strong>1</strong></h2></td>
                            <td class="py-0"><h2><strong>2</strong></h2></td>
                            <td class="py-0"><h2><strong>3</strong></h2></td>
                            <td class="py-0"><h2><strong>4</strong></h2></td>
                        </tr>
                        <tr>
                            <td class="py-0"><h2><strong>5</strong></h2></td>
                            <td class="py-0"><h2><strong>6</strong></h2></td>
                            <td class="py-0"><h2><strong>7</strong></h2></td>
                            <td class="py-0"><h2><strong>8</strong></h2></td>
                        </tr>
                        <tr>
                            <td class="py-0"><h2><strong>9</strong></h2></td>
                            <td class="py-0"><h2><strong>10</strong></h2></td>
                            <td class="py-0"><h2><strong>11</strong></h2></td>
                            <td class="py-0"><h2><strong>12</strong></h2></td>
                        </tr>
                        <tr>
                            <td class="py-0"><h2><strong>13</strong></h2></td>
                            <td class="py-0"><h2><strong>14</strong></h2></td>
                            <td class="py-0"><h2><strong>15</strong></h2></td>
                            <td class="py-0"><h2><strong>16</strong></h2></td>
                        </tr>
                    </table>
                </div>
                <div class="col-5 text-center">
                    <h2 class="mt-2">Resumo:</h2>
                </div>
                <div class="col-3 border border-dark text-center">
                    <h1 class="mt-2">Início:</h1>
                    <h1 class="mt-2">___:___</h1>
                    <h1 class="mt-2">Término:</h1>
                    <h1 class="mt-2">___:___</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-3 border border-dark">
            <h2 class="my-0">Arm/Cima</h2>
        </div>
        <div class="col-3 border border-dark">
            <h2 class="my-0">Arm/Baixo</h2>
        </div>
        <div class="col-3 border border-dark">
            <h2 class="my-0">Arm/Sec</h2>
        </div>
        <div class="col-3 border border-dark">
            <h2 class="my-0">Moegas</h2>
        </div>

        <div class="col-3 border border-dark">
            <br><br><br><br><br>
        </div>
        <div class="col-3 border border-dark"></div>
        <div class="col-3 border border-dark"></div>
        <div class="col-3 border border-dark"></div>
    </div>

    <div class="row">
        <div class="col-4 text-center">
            <h1 class="display-3 my-0 py-0">B.O.</h1>
            <h2 class="mt-3 mt-0 py-0"><i class="text-light border border-dark">____</i> SIM  <i class="text-light border border-dark">____</i> NÃO</h2>
        </div>
        <div class="col-8 border border-dark">
            <h2 class="my-0">Obs.:</h2>
        </div>
    </div>

    <hr class="hrgrosso my-0">

    <br><br>
    <div class="row text-center">
        <div class="col-6">
            <?php
            if ($entrada['ass_balanca']!=0){
                $sql = "SELECT * FROM tbl_users_assinatura WHERE user_id=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$entrada['ass_balanca']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $assinatura = $consulta->fetch();
                $assinaturacount = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                if ($assinaturacount>0){
                    echo "<img class='mt-0' height='50' id='' src='{$assinatura['assinatura']}' />";
                }else{
                    echo "<h4>_______________________</h4>";
                }
            }else{
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Balanceiro
            </h4>
        </div>
        <div class="col-6">
            <?php
            $okk=0;
            //novo
            $sql = "SELECT * FROM ren_dados WHERE indicador=? and tipo=1 and status=1";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$entrada['id']);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $dados = $consulta->fetchAll();
            $dadoscount = $consulta->rowCount();
            $sql=null;
            $consulta=null;
            if ($dadoscount>0){
                $okk++;
            }
            foreach ($dados as $dado){
                echo "<img class='mt-0' height='50' id='' src='../../dados/renascer/{$dado['tipo']}/{$dado['arquivo']}.{$dado['extensao']}' />";
            }

            if ($okk==0){
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Motorista
            </h4>
        </div>
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Conferente
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Operador: <strong><?php echo fncgetusuario($entrada['usuario'])['nome']; ?></strong>
            </h6>
        </div>
    </div>


</main>

</body>
</html>

    <SCRIPT LANGUAGE="JavaScript">
        window.print();
    </SCRIPT>