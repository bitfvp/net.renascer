<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}'>";
include_once("includes/topo.php");
?>

<main class="container"><!--todo conteudo-->
    <h1 class="form-cadastro-heading display-4">Balança</h1>
    <hr>
    <div class="row">
        <div class="col-md-3 text-center">
            <a href="index.php?pg=Vrm_lista" class="btn btn-block btn-lg btn-outline-success text-center">
                <i class="fas fa-level-down-alt fa-2x"></i>
                <h2 class="">
                    Entrada de Produtos
                </h2>
            </a>
        </div>
        <div class="col-md-3">
            <a href="index.php?pg=Vd_lista" class="btn btn-block btn-lg btn-outline-dark">
                <i class="fas fa-redo fa-2x"></i>
                <h2 class="">
                    Devolução
                </h2>
            </a>
        </div>

        <div class="col-md-3">
            <a href="index.php?pg=Vv_lista" class="btn btn-block btn-lg btn-outline-info">
                <i class="fas fa-level-up-alt fa-2x"></i>
                <h2 class="">
                    Saída para venda
                </h2>
            </a>
        </div>

        <div class="col-md-3">
            <a href="index.php?pg=Vs_lista" class="btn btn-block btn-lg btn-outline-primary">
                <i class="fas fa-question fa-2x"></i>
                <h2 class="">
                    Simples
                </h2>
            </a>
        </div>


    </div>





    <?php
    try{
        $sql = "SELECT * FROM "
            ."ren_portaria "
            ."WHERE "
            ."ren_portaria.status <> 1 and ren_portaria.tipo=1 ";
        $sql .="order by ren_portaria.data_ts DESC LIMIT 0,50 ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }

    $portarias = $consulta->fetchAll();
    $portarias_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>
    <br>
    <br>
    <h3 class="form-cadastro-heading">Caminhões no pátio</h3>
    <hr>
    <table class="table table-stripe table-sm table-hover table-condensed ">
        <thead class="thead-dark">
        <tr>
<!--            <th scope="col" class="text-center">#</th>-->
            <th scope="col"><small>DATA</small></th>
            <th scope="col"><small>FORNECEDOR</small></th>
            <th scope="col"><small>MOTORISTA</small></th>
            <th scope="col"><small>PLACAS</small></th>
            <th scope="col"><small>EMBALAGEM</small></th>
            <th scope="col"><small>OBS</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="11" class="text-right"><?php echo $portarias_quant;?> Encontrado(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        foreach ($portarias as $dados){
            if ($dados['status']==1){
                $cordalinha = " text-warning bg-gradient-dark ";
            }else{
                $cordalinha = " ";
            }
            $por_id = $dados["id"];
            $motorista = fncgetpessoa($dados["motorista"])['nome'];
            $placa = $dados["placa"];
            $fornecedor = fncgetpessoa($dados["fornecedor"])['nome'];
            $data_ts = datahoraBanco2data($dados["data_ts"]);
            switch ($dados["tipo_embalagem"]){
                case 0:
                    $embalagem="";
                    break;
                case 1:
                    $embalagem="SACARIA";
                    break;
                case 2:
                    $embalagem="BIG BAGS";
                    break;
                case 3:
                    $embalagem="GRANEL";
                    break;
                case 4:
                    $embalagem="MISTA";
                    break;
                case 5:
                    $embalagem="VAZIO";
                    break;

                default:
                    $embalagem="Não selecionada!";
                    break;
            }
            $obs = $dados["obs"];
            $usuario = $dados["usuario"];
            ?>

            <tr id="<?php echo $por_id;?>" class="<?php echo $cordalinha; ?>">
<!--                <th scope="row" id="">-->
<!--                    <small>--><?php //echo $por_id; ?><!--</small>-->
<!--                </th>-->
                <td><?php echo $data_ts; ?></td>
                <td><?php echo $fornecedor; ?></td>
                <td><?php echo $motorista; ?></td>
                <td><?php echo $placa; ?></td>
                <td><?php echo $embalagem; ?></td>
                <td><?php echo $obs; ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>