<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
//    $a="entradasave";
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vrm_lista");
    exit();
}
if (isset($_GET['id_p']) and is_numeric($_GET['id_p'])){
    $a="pesagemsave";
    $pesagem=fncgetpesagem($_GET['id_p']);
}else{
    $a="pesagemnew";
}
$romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-12">
            <?php
            include_once("includes/rm_cab.php");
            ?>
            <div class="btn-group" role="group" aria-label="">
                <a href="index.php?pg=Vrm_p&id_e=<?php echo $_GET['id_e']; ?>" target="" title="nova pesagem" class="btn btn-info fas fa-plus text-dark"> NOVA PESAGEM</a>

                <?php if ($allow["allow_6"]==1){?>
                    <a href="index.php?pg=Vrm_editar&id=<?php echo $_GET['id_e']; ?>" title="Editar entrada" class="btn btn-primary fas fa-pen text-dark"> EDITAR ROMANEIO</a>
                <?php } ?>
                <a href="index.php?pg=Vrm_print1&id_e=<?php echo $_GET['id_e']; ?>" target="_blank" title="comprovante" class="btn btn-dark fas fa-print"> IMPRIMIR COMPROVANTE</a>

                <a href="index.php?pg=Vrm_print2&id_e=<?php echo $_GET['id_e']; ?>" target="_blank" title="expedição" class="btn btn-secondary fas fa-print"> EXPEDIÇÃO DE ENTRADA</a>
                <a href="index.php?pg=Vrm_ass&id_e=<?php echo $_GET['id_e']; ?>" title="Selecionar assinatura" class="btn btn-outline-dark fas fa-file-signature"> ASSINATURA</a>

            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>

    <?php
    try{
        $sql = "SELECT * FROM "
            ."tbl_ass "
            ."order by data_ts DESC limit 0,10 ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $assinaturas = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <div class="card mt-2">
                <div class="card-header bg-info text-light">
                    Assinaturas
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>Assinatura</td>
                            <td>Ações</td>
                        </tr>
                        </thead>

                    <?php
                    foreach ($assinaturas as $item){
                        ?>

                        <tr>
                            <td>
                                <img class=' img-thumbnail' id='' src='<?php echo $item['assinatura']; ?>' />
                            </td>

                            <td>
                                <div class="btn-group" role="group" aria-label="">
                                    <a href="index.php?pg=Vrm&id_e=<?php echo $_GET['id_e']; ?>&id_ss=<?php echo $item['id']; ?>&aca=selctass" title="seleciona" class="btn  btn-success">
                                        SELECIONAR ASSINATURA
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>