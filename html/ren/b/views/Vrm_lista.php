<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");


$sql1='SELECT
ren_entradas.id,
ren_entradas.data_ts,
ren_entradas.usuario,
ren_entradas.`status`,
ren_entradas.possui_lote,
ren_entradas.romaneio_tipo,
ren_entradas.ref,
ren_entradas.romaneio,
ren_entradas.nota,
ren_entradas.nota_geral,
ren_entradas.fornecedor,
ren_entradas.empresa,
ren_entradas.corretor,
ren_entradas.motorista,
ren_entradas.placa,
ren_entradas.controle_interno,
ren_entradas.ocr,
ren_entradas.obs,
ren_entradas.ass_balanca,
ren_entradas.ass_lotes
FROM
ren_entradas
INNER JOIN ren_pessoas AS tbl_fornecedor ON ren_entradas.fornecedor = tbl_fornecedor.id
INNER JOIN ren_pessoas AS tbl_motorista ON ren_entradas.motorista = tbl_motorista.id ';


$sqlwhere = 'WHERE ren_entradas.id <> 0 and ren_entradas.romaneio_tipo=1 ';
if (isset($_GET['scb']) and $_GET['scb']!=0 and $_GET['scb']!='') {
    $inicial=$_GET['scb'];
    $inicial.=" 00:00:01";
    $final=$_GET['scb'];
    $final.=" 23:59:59";
    $sqlwhere .=" AND ((ren_entradas.data_ts)>=:inicial) And ((ren_entradas.data_ts)<=:final) ";
}else{

    if ((isset($_GET['sca']) and $_GET['sca']!='') or (isset($_GET['scb']) and $_GET['scb']!='') or (isset($_GET['scc']) and $_GET['scc']!='')){
        //tempo todo
        $inicial="2021-01-01";
        $inicial.=" 00:00:01";
        $final=date("Y-m-d");
        $final.=" 23:59:59";
        $sqlwhere .=" AND ((ren_entradas.data_ts)>=:inicial) And ((ren_entradas.data_ts)<=:final) ";
    }else{
        //apenas dia anterior
        $inicial=date("Y-m-d",strtotime("-1 days"));
        $inicial.=" 00:00:01";
        $final=date("Y-m-d");
        $final.=" 23:59:59";
        $sqlwhere .=" AND ((ren_entradas.data_ts)>=:inicial) And ((ren_entradas.data_ts)<=:final) ";
    }
}
if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
    $sqlwhere .="AND ren_entradas.romaneio LIKE '%".$_GET['sca']."%' ";
}

if (isset($_GET['scc']) and  $_GET['scc']!='') {
    $sqlwhere .="AND (tbl_fornecedor.nome  LIKE '%".$_GET['scc']."%' or tbl_motorista.nome  LIKE '%".$_GET['scc']."%' ) ";
}


$sqlorder ="order by ren_entradas.data_ts DESC LIMIT 0,50 ";

try{
    $sql = $sql1.$sqlwhere.$sqlorder;
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container-fluid">
    <h3 class="form-cadastro-heading"><a href="index.php?pg=Vrm_lista" class="text-decoration-none text-dark">Entrada</a></h3>
    <hr>



    <div class="row">
        <div class="col-md-9">
            <form action="index.php" method="get">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
                    </div>
                    <input name="pg" value="Vrm_lista" hidden/>
                    <input type="text" name="scc" id="scc" autofocus="true" autocomplete="off" class="form-control" placeholder="Motorista ou fornecedor" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
                    <input type="number" name="sca" id="sca" autocomplete="off" class="form-control" placeholder="Romaneio ou nota" aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                    <input type="date" name="scb" id="scb" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
                </div>
            </form>

            <script type="text/javascript">
                function selecionaTexto() {
                    document.getElementById("scc").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>
        <div class="col-md-3">
            <a href="index.php?pg=Vrm_editar" class="btn btn-block btn-success mb-2" ><i class="fas fa-level-down-alt"></i> Nova entrada</a>
        </div>

    </div>

    <table class="table table-sm table-stripe table-hover table-condensed">
        <thead class="thead-dark">
        <tr>
            <th scope="col" class="text-center">#</th>
            <th scope="col"><small>ROM./NOTA</small></th>
            <th scope="col"><small>MOTORISTA</small></th>
            <th scope="col"><small>PLACAS</small></th>
            <th scope="col"><small>FORNECEDOR</small></th>
            <th scope="col"><small>CORRETOR</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="5" >
            </th>
            <td colspan="4"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        if($_GET['scc']!="" and isset($_GET['scc'])) {
            $stc = strtoupper($_GET['scc']);
            define('CSC', $stc);
        }
        foreach ($entradas as $dados){
            $en_id = $dados["id"];
            $romaneio = $dados["romaneio"];
            $romaneio_tipo = fncgetromaneiotipo($dados["romaneio_tipo"]);
            $ref = $dados["ref"];
            $nota = $dados["nota"];

            $temp_romaneio=$romaneio_tipo.$ref.$romaneio;

            $motorista = fncgetpessoa($dados["motorista"])['nome'];
            $placa = $dados["placa"];
            $fornecedor = fncgetpessoa($dados["fornecedor"])['nome'];
            $corretor = fncgetpessoa($dados["corretor"])['nome'];
            $data_pedido = $dados["data_pedido"];
            $descricao = $dados["descricao"];
            $usuario = $dados["usuario"];


            try{
                $sql = "SELECT * FROM "
                    ."ren_entradas_pesagens "
                    ."WHERE ren_entradas_pesagens.entrada=:entrada "
                    ."order by ren_entradas_pesagens.data_ts DESC ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":entrada", $en_id);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }
            $pesagens = $consulta->fetchAll();
            $pesagens_quant = $consulta->rowCount();
            $sql = null;
            $consulta = null;

            ?>

            <tr id="<?php echo $en_id;?>" class="bg-gradient-success">
                <th scope="row" id="">
                    <small><?php echo $en_id; ?></small>
                </th>
                <th scope="row" id="">
                    <a href="index.php?pg=Vrm&id_e=<?php echo $en_id; ?>" class="badge badge-info text-dark" >
                        <?php
                        if($_GET['sca']!="" and isset($_GET['sca'])) {
                            $sta = CSA;
                            $ccc = $temp_romaneio;
                            $cc = explode(CSA, $ccc);
                            $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                            echo $c;
                        }else{
                            echo $temp_romaneio;
                        }

                        if ($nota!=0){
                            echo "<br>";
                            if($_GET['sca']!="" and isset($_GET['sca'])) {
                                $sta = CSA;
                                $ccc = $nota;
                                $cc = explode(CSA, $ccc);
                                $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                                echo $c;
                            }else{
                                echo $nota;
                            }
                        }
                        ?>
                    </a>
                </th>
                <td><?php
                    if($_GET['scc']!="" and isset($_GET['scc'])) {
                        $stc = CSC;
                        $ccc = strtoupper($motorista);
                        $cc = explode(CSC, $ccc);
                        $c = implode("<span class='text-danger'>{$stc}</span>", $cc);
                        echo $c;
                    }else{
                        echo strtoupper($motorista);
                    }
                ?></td>
                <td><?php echo $placa; ?></td>
                <td><?php
                    if($_GET['scc']!="" and isset($_GET['scc'])) {
                        $stc = CSC;
                        $ccc = strtoupper($fornecedor);
                        $cc = explode(CSC, $ccc);
                        $c = implode("<span class='text-danger'>{$stc}</span>", $cc);
                        echo $c;
                    }else{
                        echo strtoupper($fornecedor);
                    }
                    ?></td>
                <td><?php echo strtoupper($corretor); ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vrm_p&id_e=<?php echo $en_id; ?>" target="" title="nova pesagem" class="btn btn-sm btn-info fas fa-plus text-dark px-0"> NOVA PESAGEM</a>
                        <?php if ($allow["allow_6"]==1){?>
                            <a href="index.php?pg=Vrm_editar&id=<?php echo $en_id; ?>" title="Editar entrada" class="btn btn-sm btn-primary fas fa-pen text-dark px-0"> EDITAR ROMANEIO</a>
                        <?php } ?>
                        <a href="index.php?pg=Vrm_print1&id_e=<?php echo $en_id; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-dark fas fa-print px-0"> COMPROVANTE</a>
                        <a href="index.php?pg=Vexp&id_e=<?php echo $en_id; ?>"  title="expedição" class="btn btn-sm btn-secondary fas fa-print px-0"> EXPEDIÇÃO</a>

                        <a href="index.php?pg=V_ass&id_pessoa=<?php echo $dados["motorista"]; ?>&tipo=1&indicador=<?php echo $en_id; ?>&page_bk=Vrm&indice_bk=id_e&id_bk=<?php echo $en_id; ?>" title="Selecionar assinatura" class="btn btn-sm btn-outline-dark fas fa-file-signature">
                            ASS.
                        </a>
                    </div>

                </td>
            </tr>

            <?php
            if ($pesagens_quant>0){?>

                <tr>
                    <td class="bg-gradient-success">
                        <i class="fas fa-balance-scale fa-2x"></i>
                    </td>
                    <td colspan="6">

                            <table class="table table-stripe table-hover table-condensed">
                                <tbody>
                                <?php
                                foreach ($pesagens as $dados){
                                    if ($dados['datahora_saida']==null or $dados['datahora_saida']==0 or $dados['datahora_saida']==""){
                                        $cordalinha = "  ";
                                    }else{
                                        $cordalinha = " text-warning bg-secondary ";
                                    }
                                    $pe_id = $dados["id"];
                                    $data_inicial = datahoraBanco2data($dados["data_ts"]);
                                    $produto = $dados["produto"];
                                    switch ($dados["embalagem"]){
                                        case 0:
                                            $embalagem="Não selecionada";
                                            break;
                                        case 1:
                                            $embalagem="SACARIA";
                                            break;
                                        case 2:
                                            $embalagem="BIG BAGS";
                                            break;
                                        case 3:
                                            $embalagem="GRANEL";
                                            break;
                                        default:
                                            $embalagem="Não selecionada!";
                                            break;
                                    }
                                    $peso_entrada = $dados["peso_entrada"];
                                    $obs_entrada = $dados["obs_entrada"];
                                    $peso_saida = $dados["peso_saida"];
                                    $obs_saida = $dados["obs_saida"];
                                    $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
                                    switch ($dados["embalagem_devolvida"]){
                                        case 0:
                                            $embalagem_devolvida="";
                                            break;
                                        case 1:
                                            $embalagem_devolvida="EMBALAGEM DEVOLVIDA";
                                            break;
                                        case 2:
                                            $embalagem_devolvida="EMBALAGEM NÃO DEVOLVIDA";
                                            break;
                                        default:
                                            $embalagem_devolvida="";
                                            break;
                                    }
                                    if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                                        $infopeso=$peso_entrada."-PB";
                                    }else{
                                        $pl=$peso_entrada-$peso_saida;
                                        $ss=$pl/60;
                                        $infopeso=number_format($pl,0,',',' ') . "Kg " . number_format($ss,2) . "v";
                                    }
                                    $usuario = fncgetusuario($dados["usuario"])['nome'];
                                    ?>

                                    <tr id="" class="<?php echo $cordalinha; ?>">
                                        <td><?php echo $data_inicial; ?></td>
                                        <td><?php echo $produto; ?></td>
                                        <td><?php echo $embalagem; ?></td>
                                        <td><?php echo $obs_entrada; ?></td>
                                        <td><?php echo $infopeso; ?></td>
                                        <td class="text-dark"><?php echo $obs_saida; ?></td>
                                        <td class="text-dark"><?php echo $datahora_saida; ?></td>
                                        <td class="text-dark"><?php echo $embalagem_devolvida; ?></td>

                                        <td class="text-center">
                                            <div class="btn-group" role="group" aria-label="">
                                            <?php if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0 or $allow["allow_6"]==1){?>
                                                    <a href="index.php?pg=Vrm_p&id_e=<?php echo $en_id; ?>&id_p=<?php echo $pe_id; ?>" title="Editar pesagem" class="btn btn-sm btn-primary fas fa-pen"> EDITAR<br>PESAGEM</a>

                                                    <a href="index.php?pg=Vrm_pfinal&id_e=<?php echo $en_id; ?>&id_p=<?php echo $pe_id; ?>" title="Finalizar pesagem" class="btn btn-sm btn-success fas fa-check-double"> FINALIZAR<br>PESAGEM</a>
                                            <?php } ?>

                                            <?php if ($allow["allow_6"]==1){?>
                                                <div class="dropdown show">
                                                    <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-trash"> EXCLUIR<br>PESAGEM</i>
                                                    </a>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                        <a class="dropdown-item" href="#">Não</a>
                                                        <a class="dropdown-item bg-danger" href="index.php?pg=Vrm_lista&aca=apagarpesagemrm&id_p=<?php echo $pe_id; ?>">Apagar</a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                                </div>
                                        </td>
                                    </tr>
                                    <?php
                                }?>
                                </tbody>
                            </table>
                    </td>
                </tr>
                <?php
            } ?>
            <tr>
                <td class="bg-transparent"  colspan="8" height="50px">
                </td>
            </tr>

<?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>