<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");

$sql1='SELECT
ren_entradas.id,
ren_entradas.data_ts,
ren_entradas.usuario,
ren_entradas.`status`,
ren_entradas.possui_lote,
ren_entradas.romaneio_tipo,
ren_entradas.romaneio,
ren_entradas.nota,
ren_entradas.nota_geral,
ren_entradas.fornecedor,
ren_entradas.empresa,
ren_entradas.corretor,
ren_entradas.motorista,
ren_entradas.placa,
ren_entradas.controle_interno,
ren_entradas.ocr,
ren_entradas.obs,
ren_entradas.ass_balanca,
ren_entradas.ass_lotes
FROM
ren_entradas
INNER JOIN ren_pessoas AS tbl_fornecedor ON ren_entradas.fornecedor = tbl_fornecedor.id ';


$sqlwhere = 'WHERE ren_entradas.id <> 0 and ren_entradas.romaneio_tipo=4 ';
if (isset($_GET['scb']) and $_GET['scb']!=0 and $_GET['scb']!='') {
    $inicial=$_GET['scb'];
    $inicial.=" 00:00:01";
    $final=$_GET['scb'];
    $final.=" 23:59:59";
    $sqlwhere .=" AND ((ren_entradas.data_ts)>=:inicial) And ((ren_entradas.data_ts)<=:final) ";
}else{

    if ((isset($_GET['sca']) and $_GET['sca']!='') or (isset($_GET['scb']) and $_GET['scb']!='') or (isset($_GET['scc']) and $_GET['scc']!='')){
        //tempo todo
        $inicial="2021-01-01";
        $inicial.=" 00:00:01";
        $final=date("Y-m-d");
        $final.=" 23:59:59";
        $sqlwhere .=" AND ((ren_entradas.data_ts)>=:inicial) And ((ren_entradas.data_ts)<=:final) ";
    }else{
        //apenas dia anterior
        $inicial=date("Y-m-d",strtotime("-30 days"));
        $inicial.=" 00:00:01";
        $final=date("Y-m-d");
        $final.=" 23:59:59";
        $sqlwhere .=" AND ((ren_entradas.data_ts)>=:inicial) And ((ren_entradas.data_ts)<=:final) ";
    }
}
if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
    $sqlwhere .="AND ren_entradas.romaneio LIKE '%".$_GET['sca']."%' ";
}

if (isset($_GET['scc']) and  $_GET['scc']!='') {
    $sqlwhere .="AND tbl_fornecedor.nome  LIKE '%".$_GET['scc']."%' ";
}


$sqlorder ="order by ren_entradas.data_ts DESC LIMIT 0,50 ";

try{
    $sql = $sql1.$sqlwhere.$sqlorder;
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container">
    <h3 class="form-cadastro-heading"><a href="index.php?pg=Vd_lista" class="text-decoration-none text-dark">Devoluções ao cliente</a></h3>
    <hr>

    <div class="row">
        <div class="col-md-8">
            <form action="index.php" method="get">
                <div class="input-group mb-3 float-left">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
                    </div>
                    <input name="pg" value="Vd_lista" hidden/>
                    <input type="text" name="scc" id="scc" autofocus="true" autocomplete="off" class="form-control" placeholder="Fornecedor..." value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
                    <input type="number" name="sca" id="sca" autocomplete="off" class="form-control" placeholder="Romaneio..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                    <input type="date" name="scb" id="scb" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
                </div>
            </form>

            <script type="text/javascript">
                function selecionaTexto() {
                    document.getElementById("scc").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>
    </div>



    <table class="table table-sm table-stripe table-hover table-condensed">
        <thead class="thead-dark">
        <tr>
            <th scope="col" class="text-center">#</th>
            <th scope="col"><small>ROMANEIO</small></th>
            <th scope="col"><small>FORNECEDOR</small></th>
            <th scope="col"><small>DATA</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="4" >
            </th>
            <td colspan="4"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        if($_GET['scc']!="" and isset($_GET['scc'])) {
            $stc = strtoupper($_GET['scc']);
            define('CSC', $stc);
        }
        foreach ($entradas as $dados){
            $en_id = $dados["id"];
            $romaneio = $dados["romaneio"];
            $romaneio_tipo = fncgetromaneiotipo($dados["romaneio_tipo"]);

            $fornecedor = fncgetpessoa($dados["fornecedor"])['nome'];
            $data_ts = datahoraBanco2data($dados["data_ts"]);
            $obs = $dados["obs"];
            $usuario = $dados["usuario"];


            try{
                $sql = "SELECT * FROM "
                    ."ren_devolucao_pesagens "
                    ."WHERE ren_devolucao_pesagens.devolucao=:devolucao "
                    ."order by ren_devolucao_pesagens.data_ts DESC ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":devolucao", $en_id);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }
            $pesagens = $consulta->fetchAll();
            $pesagens_quant = $consulta->rowCount();
            $sql = null;
            $consulta = null;

            ?>

            <tr id="<?php echo $en_id;?>" class="bg-gradient-warning">
                <th scope="row" id="">
                    <small><?php echo $en_id; ?></small>
                </th>
                <th scope="row" id="">
                    <a href="index.php?pg=Vd&id_e=<?php echo $en_id; ?>" class="badge badge-info text-dark" >
                        <?php echo $romaneio_tipo.$romaneio; ?>
                    </a>
                </th>
                <td><?php
                    if($_GET['scc']!="" and isset($_GET['scc'])) {
                        $stc = CSC;
                        $ccc = strtoupper($fornecedor);
                        $cc = explode(CSC, $ccc);
                        $c = implode("<span class='text-danger'>{$stc}</span>", $cc);
                        echo $c;
                    }else{
                        echo strtoupper($fornecedor);
                    }
                    ?></td>
                <td><?php echo $data_ts; ?></td>

                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vd_p&id_e=<?php echo $en_id; ?>" target="" title="nova pesagem" class="btn btn-sm btn-info fas fa-plus text-dark">
                            <br>NOVA PESAGEM
                        </a>
                    </div>
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vd_print1&id_e=<?php echo $en_id; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-success fas fa-print text-dark">
                            <br>IMPRIMIR COMPROVANTE
                        </a>
                    </div>
                </td>
            </tr>

            <?php
            if ($pesagens_quant>0){?>

                <tr>
                    <td class="bg-gradient-warning">
                        <i class="fas fa-balance-scale fa-2x"></i>
                    </td>
                    <td colspan="4">
                        <table class="table table-stripe table-hover table-condensed">
                            <tbody>
                            <?php
                            foreach ($pesagens as $dados){
                                if ($dados['datahora_saida']==null or $dados['datahora_saida']==0 or $dados['datahora_saida']==""){
                                    $cordalinha = "  ";
                                }else{
                                    $cordalinha = " text-warning bg-dark ";
                                }
                                $pe_id = $dados["id"];
                                $data_inicial = datahoraBanco2data($dados["data_ts"]);
                                $peso_entrada = $dados["peso_entrada"];
                                $motorista = fncgetpessoa($dados["motorista"])['nome'];
                                $placa = $dados["placa"];
                                $peso_saida = $dados["peso_saida"];
                                $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
                                $usuario = fncgetusuario($dados["usuario"])['nome'];
                                ?>

                                <tr id="" class="<?php echo $cordalinha; ?>">
                                    <!--                                    <th scope="row" id="">-->
                                    <!--                                        <small>--><?php //echo $pe_id; ?><!--</small>-->
                                    <!--                                    </th>-->
                                    <td><?php echo strtoupper($motorista); ?></td>
                                    <td><?php echo $placa; ?></td>
                                    <td><?php echo $data_inicial; ?></td>
                                    <td><?php echo number_format($peso_entrada,0,',',' '); ?>Kg </td>
                                    <td class="text-info"><?php echo number_format($peso_saida,0,',',' '); ?>Kg </td>
                                    <td class="text-info"><?php echo $datahora_saida; ?></td>
                                    <td class="text-center">
                                        <div class="btn-group" role="group" aria-label="">
                                            <?php if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0 or $allow["allow_6"]==1){?>
                                                <a href="index.php?pg=Vd_p&id_e=<?php echo $en_id; ?>&id_p=<?php echo $pe_id; ?>" title="Editar pesagem" class="btn btn-sm btn-primary fas fa-pen">
                                                    <br>EDITAR<br>PESAGEM
                                                </a>

                                                <a href="index.php?pg=Vd_pfinal&id_e=<?php echo $en_id; ?>&id_p=<?php echo $pe_id; ?>" title="Finalizar pesagem" class="btn btn-sm btn-success fas fa-check-double">
                                                    <br>FINALIZAR<br>PESAGEM
                                                </a>

                                            <?php } ?>


                                            <?php if ($allow["allow_6"]==1){?>
                                                <div class="dropdown show">
                                                    <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-trash"><br>EXCLUIR<br>PESAGEM</i>
                                                    </a>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                        <a class="dropdown-item" href="#">Não</a>
                                                        <a class="dropdown-item bg-danger" href="index.php?pg=Vd_lista&aca=apagarpesagemde&id_p=<?php echo $pe_id; ?>">Apagar</a>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </td>

                                </tr>
                                <?php
                            }?>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <?php
            } ?>
            <tr>
                <td class="bg-transparent"  colspan="8" height="50px">
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>