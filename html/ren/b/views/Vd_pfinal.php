<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vv_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
//    $a="entradasave";
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vd_lista");
    exit();
}
if (isset($_GET['id_p']) and is_numeric($_GET['id_p'])){
    $a="pesagemdevolucaofinal";
    $pesagem=fncgetpesagemdevolucao($_GET['id_p']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vd_lista");
    exit();
}
$romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">
</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-4">
            <?php
            include_once("includes/de_cab.php");
            ?>
            <div class="btn-group" role="group" aria-label="">
                <a href="index.php?pg=Vd_print1&id_e=<?php echo $_GET['id_e']; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-outline-dark fas fa-print fa-2x">
                    <i class=""></i>
                </a>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-cadastro-heading">Cadastro de pesagens - Finalizar</h3>
                    <hr>
                    <form class="form-signin" action="<?php echo "index.php?pg=Vd&id_e={$_GET['id_e']}&aca={$a}"; ?>" method="post" id="formentrada1">
                        <div class="row">

                            <div class="col-md-12" id="serchmotorista">
                                <label for="motorista">MOTORISTA</label>
                                <input id="id_p" type="hidden" class="txt bradius" name="id_p" value="<?php echo $pesagem['id']; ?>"/>
                                <div class="input-group">
                                    <?php
                                    if (isset($_GET['id_p']) and is_numeric($_GET['id_p'])){
                                        $v_motorista_id=$pesagem['motorista'];
                                        $v_motorista=fncgetpessoa($pesagem['motorista'])['nome'];

                                    }else{
                                        $v_motorista_id="";
                                        $v_motorista="";
                                    }
                                    $c_motorista = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                                    <input  readonly="readonly" autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="m<?php echo $c_motorista;?>" id="m<?php echo $c_motorista;?>" value="<?php echo $v_motorista; ?>" placeholder="Click no resultado ao aparecer"  required />
                                    <input id="motorista" autocomplete="false" type="hidden" class="form-control" name="motorista" value="<?php echo $v_motorista_id; ?>" />
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="r_motorista">
                                            <i class="fa fas fa-backspace "></i>
                                        </span>
                                    </div>
                                    <div class="resultmotorista"></div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $("#r_motorista").click(function(ev){
                                            document.getElementById('motorista').value='';
                                            document.getElementById('m<?php echo $c_motorista;?>').value='';
                                        });
                                    });
                                </script>
                            </div>

                            <div class="col-md-12" id="">
                                <label for="placa">PLACAS <i class="text-info">* placas separadas por espaço</i> </label>
                                <input readonly="readonly" autocomplete="off" id="placa" type="text" class="form-control text-uppercase" name="placa" value="<?php echo $pesagem['placa']; ?>" placeholder="Preencha com as placas"/>
                                <script>
                                    function isererecebeu(valor) {
                                        var str = $("#placa").val();
                                        $('#placa').val(str+" "+valor);
                                        // $('#quem_recebeu').focus();
                                    }
                                </script>
                                <?php
                                foreach (fncplacalist($pesagem['motorista']) as $placas){
                                    echo "<i id='emoji' class='btn btn-sm m-1 border btn-outline-dark' onclick=\"isererecebeu('{$placas['placa']}')\">{$placas['placa']}</i>";
                                }
                                ?>
                            </div>

                            <div class="col-md-6">
                                <label for="peso_entrada">PESO INICIAL</label>

                                <div class="input-group">
                                    <input readonly="readonly" autocomplete="off" id="peso_entrada" placeholder="insira o peso inicial" type="text" class="form-control" name="peso_entrada" value="<?php echo $pesagem['peso_entrada']; ?>" required />
                                    <div class="input-group-append">
                                        <span class="input-group-text">,000 Kg</span>
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $('#peso_entrada').mask('000000', {reverse: true});
                                    });
                                </script>
                            </div>

                            <div class="col-md-6">
                                <label for="peso_saida">PESO FINAL</label>

                                <div class="input-group">
                                    <input autofocus  autocomplete="off" id="peso_saida" placeholder="insira o peso inicial" type="text" class="form-control" name="peso_saida" value="<?php echo $pesagem['peso_saida']; ?>" required />
                                    <div class="input-group-append">
                                        <span class="input-group-text">,000 Kg</span>
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $('#peso_saida').mask('000000', {reverse: true});
                                    });
                                    jQuery(document).ready(function(){
                                        jQuery('input').on('keyup',function(){
                                            if(jQuery(this).attr('name') === 'informacao'){
                                                return false;
                                            }

                                            var soma1 = (jQuery('#peso_entrada').val() == '' ? 0 : jQuery('#peso_entrada').val());
                                            var soma2 = (jQuery('#peso_saida').val() == '' ? 0 : jQuery('#peso_saida').val());
                                            var pesoliquido = (parseInt(soma2) - parseInt(soma1));
                                            var sacas = (parseInt(pesoliquido) / 60);
                                            var textoinfo = pesoliquido + "KG ou " + sacas + " volumes";
                                            jQuery('#informacao').val(textoinfo);
                                        });
                                    });
                                </script>
                            </div>

                            <div class="col-md-6">
                                <label for="">INFORMAÇÃO</label>
                                <div class="input-group">
                                    <input  id="informacao" type="text" class="form-control" name="informacao" value="<?php $pl=$pesagem['peso_saida']-$pesagem['peso_entrada']; $ss=$pl/60; echo $pl . "KG ou " . $ss . " volumes";?>" readonly="readonly"/>
                                </div>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-12 pb-2" id="">
                                <input type="submit" value="SALVAR" id="gogo" class="btn btn-block btn-success mt-4">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>