<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vs_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_s']) and is_numeric($_GET['id_s'])){
    $a="simplessave";
    $simples=fncgetsimples($_GET['id_s']);

}else{
    $a="simplesnew";
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">


</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vs1&aca={$a}"; ?>" method="post" id="formentrada1">
        <h3 class="form-cadastro-heading">Cadastro de movimentação simples</h3>
        <hr>
        <div class="row">

            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $simples['id']; ?>"/>
            <div class="col-md-6" id="serchmotorista">
                <label for="motorista">MOTORISTA</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id_s']) and is_numeric($_GET['id_s'])){
                        $v_motorista_id=$simples['motorista'];
                        $v_motorista=fncgetpessoa($simples['motorista'])['nome'];

                    }else{
                        $v_motorista_id="";
                        $v_motorista="";
                    }
                    $c_motorista = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input readonly autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="m<?php echo $c_motorista;?>" id="m<?php echo $c_motorista;?>" value="<?php echo $v_motorista; ?>" placeholder="Click no resultado ao aparecer"  required />
                    <input readonly id="motorista" autocomplete="false" type="hidden" class="form-control" name="motorista" value="<?php echo $v_motorista_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_motorista">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultmotorista"></div>
                </div>
                <script>
                    //$(document).ready(function(){
                    //    $("#r_motorista").click(function(ev){
                    //        document.getElementById('motorista').value='';
                    //        document.getElementById('m<?php //echo $c_motorista;?>//').value='';
                    //    });
                    //});
                </script>
            </div>

            <div class="col-md-6" id="">
                <label for="placa">PLACAS <i class="text-info">* placas separadas por espaço</i> </label>
                <input autofocus autocomplete="off" id="placa" type="text" class="form-control text-uppercase" name="placa" value="<?php echo $simples['placa']; ?>" placeholder="Preencha com as placas"/>
                <script>
                    function isererecebeu(valor) {
                        var str = $("#placa").val();
                        $('#placa').val(str+" "+valor);
                        // $('#quem_recebeu').focus();
                    }
                </script>
                <?php
                foreach (fncplacalist($simples['motorista']) as $placas){
                    echo "<i id='emoji' class='btn btn-sm m-1 border btn-outline-dark' onclick=\"isererecebeu('{$placas['placa']}')\">{$placas['placa']}</i>";
                }
                ?>
            </div>

            <div class="col-md-6">
                <label for="peso_entrada">PESO INICIAL</label>
                <div class="input-group">
                    <input  autocomplete="off" id="peso_entrada" placeholder="insira o peso inicial" type="text" class="form-control" name="peso_entrada" value="<?php echo $simples['peso_entrada']; ?>" required />
                    <div class="input-group-append">
                        <span class="input-group-text">,000 Kg</span>
                    </div>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#peso_entrada').mask('000.000', {reverse: true});
                    });
                </script>
            </div>
            <div class="col-md-6">
                <label for="obs">OBSERVAÇÃO INICIAL</label>
                <textarea  id="obs" onkeyup="limite_textarea(this.value,250,obs,'cont')" maxlength="250" class="form-control" rows="2" name="obs"><?php echo $simples['obs']; ?></textarea>
                <span id="cont">250</span>/250
            </div>

<!--            <div class="col-md-5">-->
<!--                <label for="peso_saida">PESO FINAL</label>-->
<!--                <div class="input-group">-->
<!--                    <input readonly autocomplete="off" id="peso_saida" placeholder="insira o peso final" type="text" class="form-control" name="peso_saida" value="--><?php //echo $simples['peso_saida']; ?><!--" required />-->
<!--                    <div class="input-group-append">-->
<!--                        <span class="input-group-text">,000 Kg</span>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <script>-->
<!--                    $(document).ready(function(){-->
<!--                        $('#peso_saida').mask('000.000', {reverse: true});-->
<!--                    });-->
<!--                </script>-->
<!--            </div>-->
<!--            <div class="col-md-6">-->
<!--                <label for="obs2">OBSERVAÇÃO FINAL</label>-->
<!--                <textarea readonly id="obs2" onkeyup="limite_textarea(this.value,250,obs2,'cont')" maxlength="250" class="form-control" rows="2" name="obs2">--><?php //echo $simples['obs2']; ?><!--</textarea>-->
<!--                <span id="cont">250</span>/250-->
<!--            </div>-->


            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="AVANÇAR >>" id="gogo" class="btn btn-block btn-success mt-4">
            </div>


        </div>

    </form>
</div>

<?php
//for ($i = 1; $i <= 1000000; $i++) {
//    echo $i."  ".get_criptografa64($i)."<br>";
//}
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>