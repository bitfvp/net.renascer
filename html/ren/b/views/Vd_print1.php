<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vd_lista");
    exit();
}
$romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);
?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
            <div class="col-8">
                <h4 class="text-uppercase">Devolução de estoque ao proprietário</h4>
                <h4>FORNECEDOR: <strong><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?></strong></h4>
                <h4>OBS: <strong><?php echo $entrada['obs']; ?></strong></h4>
            </div>
            <div class="col-4 text-right">
                <h4 class="">ROMANEIO: <strong><?php echo $romaneio_tipo.$entrada['romaneio']; ?></strong></h4>
                <h4>NOTA: <strong><?php echo $entrada['nota']; ?></strong></h4>
            </div>
    </div>







    <?php
    //1 venda
    //2ct
    //3 devolucao
    try{
        $sql = "SELECT * FROM "
            ."ren_entradas_lotes_saidas "
            ."WHERE id_destino=:id_destino and tipo_saida=3 "
            ."order by ren_entradas_lotes_saidas.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":id_destino", $_GET['id_e']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>

    <h3>LOTES SELECIONADOS PARA DEVOLUÇÃO</h3>
    <table class="table table-sm table-hover table-striped">
        <thead class="thead-inverse thead-dark">
        <tr>
            <th>DATA</th>
            <th>PRODUTO</th>
            <th>LOTE</th>
            <th>FORNECEDOR</th>
            <th>PESO</th>
            <th>ORIGEM</th>
            <th>BO</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="4" >
            </th>
            <td colspan="4"><?php echo $pesagens_quant;?> SAÍDA(S) ENCONTRADA(S)</td>
        </tr>
        </tfoot>
        <tbody>
        <?php
        foreach ($pesagens as $dados){
            $peso_entrada = $dados["peso_entrada"];
            $obs_entrada = $dados["obs_entrada"];
            $peso_saida = $dados["peso_saida"];
            $obs_saida = $dados["obs_saida"];

            $data_ts = datahoraBanco2data($dados["data_ts"]);
            $tipo_cafe = fncgetprodutos($dados["tipo_cafe"])['nome'];
            $lote = fncgetlote($dados["lote"]);
            $entradax=fncgetentrada($lote["romaneio"]);
            $romaneio_tipo = fncgetromaneiotipo($entradax["romaneio_tipo"]);

            switch ($lote['letra']){
                case 1:
                    $letra="A";
                    break;
                case 2:
                    $letra="B";
                    break;
                case 3:
                    $letra="C";
                    break;
                case 4:
                    $letra="D";
                    break;
                case 5:
                    $letra="E";
                    break;
                case 6:
                    $letra="F";
                    break;
                case 7:
                    $letra="G";
                    break;
                case 8:
                    $letra="H";
                    break;
                case 9:
                    $letra="I";
                    break;
                case 10:
                    $letra="J";
                    break;
                default:
                    $letra=$dados["letra"];
                    break;
            }
            $loteinfo=$romaneio_tipo.$entradax['romaneio']." ".$letra;
            $fornecedor = fncgetpessoa($entradax['fornecedor'])['nome'];

            $peso = $dados["peso"];
            $sacas=$peso/60;
            $sacas=number_format($sacas, 2, ',', ' ');

            $origem = fncgetlocal($dados["origem"])['nome'];
            $p_bo = $dados["p_bo"];
            $bo = $dados["bo"];

            $usuario = fncgetusuario($dados["usuario"])['nome'];
            ?>

            <tr id="" class="">
                <td><?php echo $data_ts ?></td>
                <td><?php echo $tipo_cafe; ?></td>
                <td><?php echo $loteinfo; ?></td>
                <td><?php echo strtoupper($fornecedor); ?></td>
                <td><?php echo number_format($peso,0,',',' '). " Kg <br> ".$sacas." VOLUMES";?></td>
                <td><?php echo $origem; ?></td>
                <td><?php echo strtoupper($bo); ?></td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>























        <hr class="hrgrosso">
        <h3>PESAGEM</h3>
    <?php
    try{
        $sql = "SELECT * FROM "
            ."ren_devolucao_pesagens "
            ."WHERE ren_devolucao_pesagens.devolucao=:devolucao  "
            ."order by ren_devolucao_pesagens.data_ts ASC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":devolucao", $_GET['id_e']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;



    $peso_total=0;
    foreach ($pesagens as $dados){
    $pe_id = $dados["id"];
    $data_inicial = datahoraBanco2data($dados["data_ts"]);
    $motorista = fncgetpessoa($dados["motorista"])['nome'];
    $placa = $dados["placa"];
    $peso_entrada = $dados["peso_entrada"];
    $peso_saida = $dados["peso_saida"];
        $peso_liquido=$dados["peso_saida"]-$dados["peso_entrada"];
        $peso_total= $peso_total+$peso_liquido;
        $sacas=$peso_liquido/60;
        $sacas=number_format($sacas, 2, ',', ' ');
    $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
    $usuario = fncgetusuario($dados["usuario"])['nome'];
    ?>
        <div class="row">
            <div class="col-6 border">
                MOTORISTA: <strong><?php echo strtoupper($motorista); ?></strong>
            </div>
            <div class="col-6 border">
                PLACAS: <strong><?php echo $placa; ?></strong>
            </div>
        </div>

        <div class="row">
            <div class="col-6 border">
                ENTRADA: <strong><?php echo $data_inicial; ?></strong>
            </div>
            <div class="col-6 border">
                SAÍDA: <strong><?php echo $datahora_saida; ?></strong>
            </div>
        </div>

        <div class="row">
            <div class="col-6 border">
                PESO INICIAL: <strong><?php echo number_format($peso_entrada,3,',',' '); ?> Kg</strong>
            </div>
            <div class="col-6 border" >
                PESO FINAL: <strong><?php echo number_format($peso_saida,3,',',' '); ?> Kg</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-6 border" >
                PESO LÍQUIDO: <strong><?php echo number_format($peso_liquido,3,',',' '); ?> Kg</strong>
            </div>
            <div class="col-6 border" >
                QUANTIDADE EM VOLUMES: <strong><?php echo $sacas; ?></strong>
            </div>
        </div>
        <hr>
        <?php
    }
    ?>

    <div class="row">
        <div class="col-6 border">
            PESO LÍQUIDO TOTAL: <strong><?php echo number_format($peso_total,3,',',' ')." Kg" ?></strong>
        </div>
        <div class="col-6 border">
            QUANTIDADE EM VOLUMES TOTAIS: <strong><?php $peso_total=$peso_total/60; echo number_format($peso_total, 2, ',', ' '); ?></strong>
        </div>
    </div>

    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
        <div class="col-12">
            <strong> Concordo com os dados prescritos acima e dou fé.</strong>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-6">
            <?php
            $okk=0;

            //novo
            $sql = "SELECT * FROM ren_dados WHERE indicador=? and tipo=3 and status=1";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$entrada['id']);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $dados = $consulta->fetchAll();
            $dadoscount = $consulta->rowCount();
            $sql=null;
            $consulta=null;
            if ($dadoscount>0){
                $okk++;
            }
            foreach ($dados as $dado){
                echo "<img class='mt-0' height='100' id='' src='../../dados/renascer/{$dado['tipo']}/{$dado['arquivo']}.{$dado['extensao']}' />";
            }

            if ($okk==0){
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Responsável de retirada da carga
            </h4>
        </div>

        <div class="col-6">
            <?php
            if ($entrada['usuario']!=0){
                $sql = "SELECT * FROM tbl_users_assinatura WHERE user_id=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$entrada['usuario']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $assinatura = $consulta->fetch();
                $assinaturacount = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                if ($assinaturacount>0){
                    echo "<img class='mt-0' height='50' id='' src='{$assinatura['assinatura']}' />";
                }else{
                    echo "<h4>_______________________</h4>";
                }
            }else{
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Responsável do armazém
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Operador: <strong><?php echo fncgetusuario($entrada['usuario'])['nome']; ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    // window.print();
</SCRIPT>