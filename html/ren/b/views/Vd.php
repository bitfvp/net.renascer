<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vv_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
//    $a="entradasave";
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vd_lista");
    exit();
}

$romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);
?>
<!--/////////////////////////////////////////////////////-->
<!--/////////////////////////////////////////////////////-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php
            include_once("includes/de_cab.php");
            ?>

            <div class="btn-group mb-1" role="group" aria-label="">
                <a href="index.php?pg=Vd_p&id_e=<?php echo $_GET['id_e']; ?>" target="" title="nova pesagem" class="btn btn-sm btn-info text-dark fas fa-plus">
                <br>NOVA PESAGEM
                </a>

                <a href="index.php?pg=Vd_print1&id_e=<?php echo $_GET['id_e']; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-success text-dark fas fa-print">
                    <br>IMPRIMIR COMPROVANTE
                </a>
                <a href="index.php?pg=V_ass&id_pessoa=<?php echo $entrada['fornecedor']; ?>&tipo=3&indicador=<?php echo $_GET['id_e']; ?>&page_bk=Vd&indice_bk=id_e&id_bk=<?php echo $_GET['id_e']; ?>" title="Selecionar assinatura" class="btn btn-outline-dark fas fa-file-signature"> ASSINATURA</a>
            </div>


        </div>
    </div>

    <?php
    try{
        $sql = "SELECT * FROM "
            ."ren_devolucao_pesagens "
            ."WHERE ren_devolucao_pesagens.devolucao=:devolucao "
            ."order by ren_devolucao_pesagens.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":devolucao", $_GET['id_e']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-hover table-striped">
                <thead class="thead-inverse thead-dark">
                    <tr>
                        <th>#</th>
                        <th>MOTORISTA</th>
                        <th>PLACA</th>
                        <th>DATA</th>
                        <th>PESO INICIAL</th>
                        <th class="text-info">PESO FINAL</th>
                        <th class="text-info">DATA FINAL</th>
                        <th class="text-center">AÇÕES</th>
                    </tr>
                </thead>
                <tfoot>
                <tr>
                    <th scope="row" colspan="7" >
                    </th>
                    <td colspan="3"><?php echo $pesagens_quant;?> Pesagem(s) encontrada(s)</td>
                </tr>
                </tfoot>
                <tbody>
                <?php
                foreach ($pesagens as $dados){
                    if ($dados['datahora_saida']==null or $dados['datahora_saida']==0 or $dados['datahora_saida']==""){
                        $cordalinha = " ";
                    }else{
                        $cordalinha = " text-warning bg-dark ";
                    }
                    $pe_id = $dados["id"];
                    $data_inicial = datahoraBanco2data($dados["data_ts"]);
                    $peso_entrada = $dados["peso_entrada"];
                    $peso_saida = $dados["peso_saida"];
                    $motorista = fncgetpessoa($dados["motorista"])['nome'];
                    $placa = $dados["placa"];
                    $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
                    $usuario = fncgetusuario($dados["usuario"])['nome'];
                    ?>

                    <tr id="" class="<?php echo $cordalinha; ?>">
                        <th scope="row" id="">
                            <small><?php echo $pe_id; ?></small>
                        </th>
                        <td><?php echo strtoupper($motorista); ?></td>
                        <td><?php echo $placa; ?></td>
                        <td><?php echo $data_inicial; ?></td>
                        <td><?php echo number_format($peso_entrada,0); ?>K</td>
                        <td class="text-info"><?php echo number_format($peso_saida,0); ?>K</td>
                        <td class="text-info"><?php echo $datahora_saida; ?></td>

                        <td class="text-center">
                            <div class="btn-group" role="group" aria-label="">
                                <?php if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0 or $allow["allow_6"]==1){?>
                                    <a href="index.php?pg=Vd_p&id_e=<?php echo $_GET['id_e']; ?>&id_p=<?php echo $pe_id; ?>" title="Editar pesagem" class="btn btn-sm btn-primary fas fa-pen px-0">
                                        EDITAR PESAGEM
                                    </a>

                                    <a href="index.php?pg=Vd_pfinal&id_e=<?php echo $_GET['id_e']; ?>&id_p=<?php echo $pe_id; ?>" title="Finalizar pesagem" class="btn btn-sm btn-success fas fa-check-double px-0">
                                        FINALIZAR PESAGEM
                                    </a>

                                <?php } ?>


                                <?php if ($allow["allow_6"]==1){?>
                                    <a href="index.php?pg=Vd_lista&aca=apagarpesagemde&id_p=<?php echo $pe_id; ?>" title="Finalizar pesagem" class="btn btn-sm btn-danger fas fa-trash px-0">
                                        EXCLUIR PESAGEM
                                    </a>
                                <?php } ?>
                            </div>
                        </td>


                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>