<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");

if (isset($_GET['id_pessoa']) and is_numeric($_GET['id_pessoa']) and $_GET['id_pessoa']>0){
    $id_pessoa=$_GET['id_pessoa'];
}else{
    $id_pessoa=0;
}

if (isset($_GET['tipo']) and is_numeric($_GET['tipo']) and $_GET['tipo']>0){
    $tipo=$_GET['tipo'];
}else{
    $tipo=0;
}

if (isset($_GET['indicador']) and is_numeric($_GET['indicador']) and $_GET['indicador']>0){
    $indicador=$_GET['indicador'];
}else{
    $indicador=0;
}
if (isset($_GET['page_bk'])){
    $page_bk=$_GET['page_bk'];
}else{
    $page_bk="Vhome";
}
if (isset($_GET['indice_bk'])){
    $indice_bk=$_GET['indice_bk'];
}else{
    $indice_bk=0;
}
if (isset($_GET['id_bk'])){
    $id_bk=$_GET['id_bk'];
}else{
    $id_bk="Vhome";
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container">
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>

    <?php
    try{
        $sql = "SELECT * FROM "
            ."tbl_ass "
            ."order by data_ts DESC limit 0,50 ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $assinaturas = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <div class="card mt-2">
                <div class="card-header bg-info text-light">
                    Assinaturas
                    <a href="index.php?aca=resetassinaturas" class="btn btn-sm btn-warning float-right"> Limpar assinaturas</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>Assinatura</td>
                            <td>Ações</td>
                        </tr>
                        </thead>

                    <?php
                    foreach ($assinaturas as $item){
                        ?>

                        <tr>
                            <td>
                                <img class=' img-thumbnail' id='' src='<?php echo $item['assinatura']; ?>' />
                            </td>

                            <td>
                                <div class="btn-group" role="group" aria-label="">
                                    <a href="index.php?pg=V_ass&id_pessoa=<?php echo $id_pessoa; ?>&tipo=<?php echo $tipo; ?>&indicador=<?php echo $indicador; ?>&page_bk=<?php echo $page_bk; ?>&indice_bk=<?php echo $indice_bk; ?>&id_bk=<?php echo $id_bk; ?>&id_ss=<?php echo $item['id']; ?>&aca=selctass" title="seleciona" class="btn  btn-success">
                                        SELECIONAR ASSINATURA
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>