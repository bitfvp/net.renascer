<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Ve_lista");
    exit();
}
$romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);

?>

<script type="text/javascript">
    $(document).ready(function() {
        document.title = '<?php echo $romaneio_tipo.$entrada['ref'].$entrada['romaneio']; echo " "; echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?>';
    });
</script>

<main class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
        <div class="col-8">
            <h3 class="text-uppercase">MOVIMENTAÇÃO DE ENTRADA</h3>
            <h5>FORNECEDOR: <strong><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?></strong></h5>
            <h5>EMPRESA/PRODUTOR: <strong><?php echo strtoupper(fncgetpessoa($entrada['empresa'])['nome']); ?></strong></h5>
            <h5>CORRETOR: <strong><?php echo strtoupper(fncgetpessoa($entrada['corretor'])['nome']); ?></strong></h5>
            <h5>MOTORISTA: <strong><?php echo strtoupper(fncgetpessoa($entrada['motorista'])['nome']); ?></strong></h5>
            <h5>PLACAS: <strong><?php echo $entrada['placa']; ?></strong></h5>
        </div>
        <div class="col-4 text-right">
            <h5 class="">ROMANEIO: <strong><?php echo $romaneio_tipo.$entrada['ref'].$entrada['romaneio']; ?></strong></h5>
            <h5>NOTA: <strong><?php echo $entrada['nota']; ?></strong>  OCR: <strong><?php echo $entrada['ocr']; ?></strong></h5>
        </div>
    </div>
    <hr class="hrgrosso">

    <?php
    try{
        $sql = "SELECT * FROM "
            ."ren_entradas_pesagens "
            ."WHERE ren_entradas_pesagens.entrada=:entrada "
            ."order by ren_entradas_pesagens.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":entrada", $_GET['id_e']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>

    <?php
    $peso_total=0;
    foreach ($pesagens as $dados){
    $pe_id = $dados["id"];
    $data_inicial = datahoraBanco2data($dados["data_ts"]);
    $produto = $dados["produto"];
    switch ($dados["embalagem"]){
        case 0:
            $embalagem="Não selecionada";
            break;
        case 1:
            $embalagem="SACARIA";
            break;
        case 2:
            $embalagem="BIG BAGS";
            break;
        case 3:
            $embalagem="GRANEL";
            break;
        default:
            $embalagem="Não selecionada!";
            break;
    }
    $peso_entrada = $dados["peso_entrada"];
    $obs_entrada = $dados["obs_entrada"];
    $peso_saida = $dados["peso_saida"];
        $peso_liquido=$dados["peso_entrada"]-$dados["peso_saida"];
        $peso_total= $peso_total+$peso_liquido;
        $sacas=$peso_liquido/60;
    $obs_saida = $dados["obs_saida"];
    $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
    $usuario = fncgetusuario($dados["usuario"])['nome'];
    ?>
        <div class="row">
            <div class="col-6 border">
                PRODUTO: <strong><?php echo $produto; ?></strong>
            </div>
            <div class="col-6 border">
                EMBALAGEM: <strong><?php echo $embalagem; ?></strong>
            </div>
        </div>

        <div class="row">
            <div class="col-6 border">
                ENTRADA: <strong><?php echo $data_inicial; ?></strong>
            </div>
            <div class="col-6 border">
                SAÍDA: <strong><?php echo $datahora_saida; ?></strong>
            </div>
        </div>

        <div class="row">
            <div class="col-6 border">
                PESO INICIAL: <strong><?php echo number_format($peso_entrada,3); ?>Kg</strong>
            </div>
            <div class="col-6 border" >
                PESO FINAL: <strong><?php echo number_format($peso_saida,3); ?>Kg</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-6 border" >
                <strong><?php
                    switch ($dados["embalagem_devolvida"]){
                        case 0:
                            $embalagem_devolvida="";
                            break;
                        case 1:
                            $embalagem_devolvida="EMBALAGEM DEVOLVIDA";
                            break;
                        case 2:
                            $embalagem_devolvida="EMBALAGEM NÃO DEVOLVIDA";
                            break;
                        default:
                            $embalagem_devolvida="";
                            break;
                    }
                    echo $embalagem_devolvida; ?>
                </strong>
            </div>
            <div class="col-3 border" >
                PESO LÍQUIDO: <strong><?php echo number_format($peso_liquido,3); ?>Kg</strong>
            </div>
            <div class="col-3 border" >
                EM VOLUMES: <strong><?php
                    echo number_format($sacas, 2, '.', ',');
                    ?>v</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-6 border">
                OBS DE ENTRADA: <strong><?php echo $obs_entrada; ?></strong>
            </div>
            <div class="col-6 border">
                OBS DE SAÍDA: <strong><?php echo $obs_saida; ?></strong>
            </div>
        </div>
        <hr>
        <?php
    }
    ?>

    <div class="row">
        <div class="col-6 border">
            PESO LÍQUIDO TOTAL: <strong><?php echo number_format($peso_total,3)." Kg" ?></strong>
        </div>
        <div class="col-6 border">
            QUANTIDADE EM VOLUMES TOTAIS: <strong><?php $peso_total=$peso_total/60; echo number_format($peso_total, 2, '.', ','); ?></strong>
        </div>
    </div>

    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php
            echo "Impresso por: ";
            echo fncgetusuario($_SESSION['id'])['nome'];
            echo "<br>";
            echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
        <div class="col-12">
            <strong> Concordo com os dados prescritos acima e dou fé.</strong>
        </div>
    </div>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-6">
                <?php
                $okk=0;

                //antigo
                $sql = "SELECT * FROM tbl_assinatura WHERE entrada=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$entrada['id']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $assinatura = $consulta->fetch();
                $assinaturacount = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                if ($assinaturacount>0){
                    echo "<img class='mt-0' height='100' id='' src='{$assinatura['assinatura']}' />";
                    $okk++;
                }

                //novo
                $sql = "SELECT * FROM ren_dados WHERE indicador=? and tipo=1 and status=1";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$entrada['id']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $dados = $consulta->fetchAll();
                $dadoscount = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                if ($dadoscount>0){
                    $okk++;
                }
                foreach ($dados as $dado){
                    echo "<img class='mt-0' height='50' id='' src='../../dados/renascer/{$dado['tipo']}/{$dado['arquivo']}.{$dado['extensao']}' />";
                }

                if ($okk==0){
                    echo "<h4>_______________________</h4>";
                }
                ?>
            <h4>
                Assinatura do motorista
            </h4>
        </div>

        <div class="col-6">
            <?php
            if ($entrada['ass_balanca']!=0){
                $sql = "SELECT * FROM tbl_users_assinatura WHERE user_id=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$entrada['ass_balanca']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $assinatura = $consulta->fetch();
                $assinaturacount = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                if ($assinaturacount>0){
                    echo "<img class='mt-0' height='50' id='' src='{$assinatura['assinatura']}' />";
                }else{
                    echo "<h4>_______________________</h4>";
                }
            }else{
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>


</main>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>