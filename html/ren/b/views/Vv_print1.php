<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id_v']) and is_numeric($_GET['id_v'])){
    $venda=fncgetvenda($_GET['id_v']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vv_lista");
    exit();
}
?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
        <div class="col-8">
            <h3>COMPROVANTE DE PESAGEM PARA VENDA</h3>
            <h4>CLIENTE: <strong><?php echo strtoupper(fncgetpessoa($venda['cliente'])['nome']); ?></strong></h4>
            <h4>TRANSPORTADORA: <strong><?php echo strtoupper(fncgetpessoa($venda['transportadora'])['nome']); ?></strong></h4>
        </div>
        <div class="col-4 text-right">
            <h4 class="">CONTRATO: <strong><?php echo $venda['prefixo']."-".$venda['contrato']."<br>".$venda['descricao']." ".dataRetiraHora($venda["data_ts"]); ?></strong></h4>
            <h4>N.F.: <strong><?php echo $venda['nota_fiscal']; ?></strong></h4>
        </div>
    </div>
    <hr class="hrgrosso">
    <h3>PESAGEM</h3>
    <?php
    try{
        $sql = "SELECT * FROM "
            ."ren_vendas_pesagens "
            ."WHERE ren_vendas_pesagens.venda=:venda and ren_vendas_pesagens.id=:id "
            ."order by ren_vendas_pesagens.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":venda", $_GET['id_v']);
        $consulta->bindValue(":id", $_GET['id_p']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>

    <?php
    foreach ($pesagens as $dados){
    $pe_id = $dados["id"];
    $data_inicial = datahoraBanco2data($dados["data_ts"]);
    $motorista = fncgetpessoa($dados["motorista"])['nome'];
    $placa = $dados["placa"];
    $peso_entrada = $dados["peso_entrada"];
    $peso_saida = $dados["peso_saida"];
        $peso_liquido=$dados["peso_saida"]-$dados["peso_entrada"];
        $sacas=$peso_liquido/60;
        $sacas=number_format($sacas, 2, ',', ' ');
    $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
    $usuario = fncgetusuario($dados["usuario"])['nome'];
    ?>
        <div class="row">
            <div class="col-6 border">
                MOTORISTA: <strong><?php echo strtoupper($motorista); ?></strong>
            </div>
            <div class="col-6 border">
                PLACAS: <strong><?php echo $placa; ?></strong>
            </div>
        </div>

        <div class="row">
            <div class="col-6 border">
                ENTRADA: <strong><?php echo $data_inicial; ?></strong>
            </div>
            <div class="col-6 border">
                SAÍDA: <strong><?php echo $datahora_saida; ?></strong>
            </div>
        </div>

        <div class="row">
            <div class="col-6 border">
                PESO INICIAL: <strong><?php echo number_format($peso_entrada,3,',',' '); ?>Kg</strong>
            </div>
            <div class="col-6 border" >
                PESO FINAL: <strong><?php echo number_format($peso_saida,3,',',' '); ?>Kg</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-6 offset-6 border" >
                PESO LÍQUIDO: <strong><?php echo number_format($peso_liquido,3,',',' '); ?>Kg</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-6 offset-6 border" >
                QUANTIDADE EM VOLUMES: <strong><?php echo $sacas; ?>v</strong>
            </div>
        </div>
        <hr>
        <br>
        <?php
    }
    ?>

    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
        <div class="col-12">
            <strong> Concordo com os dados prescritos acima e dou fé.</strong>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-6">
            <?php
            $okk=0;

            //novo
            $sql = "SELECT * FROM ren_dados WHERE indicador=? and tipo=4 and status=1";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$_GET['id_p']);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $dados = $consulta->fetchAll();
            $dadoscount = $consulta->rowCount();
            $sql=null;
            $consulta=null;
            if ($dadoscount>0){
                $okk++;
            }
            foreach ($dados as $dado){
                echo "<img class='mt-0' height='50' id='' src='../../dados/renascer/{$dado['tipo']}/{$dado['arquivo']}.{$dado['extensao']}' />";
            }

            if ($okk==0){
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Assinatura do motorista
            </h4>
        </div>
        <div class="col-6">
            <?php
            if ($venda['usuario']!=0){
                $sql = "SELECT * FROM tbl_users_assinatura WHERE user_id=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$venda['usuario']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $assinatura = $consulta->fetch();
                $assinaturacount = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                if ($assinaturacount>0){
                    echo "<img class='mt-0' height='50' id='' src='{$assinatura['assinatura']}' />";
                }else{
                    echo "<h4>_______________________</h4>";
                }
            }else{
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Operador: <strong><?php echo fncgetusuario($venda['usuario'])['nome']; ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>