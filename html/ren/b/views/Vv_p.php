<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vv_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_v']) and is_numeric($_GET['id_v'])){
//    $a="entradasave";
    $venda=fncgetvenda($_GET['id_v']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vv_lista");
    exit();
}
if (isset($_GET['id_p']) and is_numeric($_GET['id_p'])){
    $a="pesagemvesave";
    $pesagem=fncgetpesagemvenda($_GET['id_p']);
}else{
    $a="pesagemvenew";
}

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">
    $(document).ready(function () {
        $('#serchmotorista input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultmotorista");
            if (inputValf.length) {
                $.get("includes/motorista.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultmotorista p", function () {
            var motorista = this.id;
            $.when(
                // $(this).parents("#serchmotorista").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchmotorista").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultmotorista").empty()

                $(this).parents("#serchmotorista").find('input[type="text"]').val($(this).text()),
                $('#motorista').val(motorista),
                $(this).parent(".resultmotorista").empty()

            ).then(
                function() {
                    // roda depois de acao1 e acao2 terminarem
                    setTimeout(function() {
                        //do something special
                        $( "#gogo" ).click();
                    }, 500);
                });
        });
    });
</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-4">
            <?php
            include_once("includes/ve_cab.php");
            ?>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-cadastro-heading">Cadastro de pesagens - parte 1</h3>
                    <hr>
                    <form class="form-signin" action="<?php echo "index.php?pg=Vv_p&id_v={$_GET['id_v']}&aca={$a}"; ?>" method="post" id="formx">
                        <div class="row">

                            <div class="col-md-12" id="serchmotorista">
                                <label for="motorista">MOTORISTA</label>
                                <input id="id_p" type="hidden" class="txt bradius" name="id_p" value="<?php echo $pesagem['id']; ?>"/>
                                <div class="input-group">
                                    <?php
                                    if (isset($_GET['id_v']) and is_numeric($_GET['id_v'])){
                                        $v_motorista_id=$pesagem['motorista'];
                                        $v_motorista=fncgetpessoa($pesagem['motorista'])['nome'];

                                    }else{
                                        $v_motorista_id="";
                                        $v_motorista="";
                                    }
                                    $c_motorista = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                                    <input autofocus autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="m<?php echo $c_motorista;?>" id="m<?php echo $c_motorista;?>" value="<?php echo $v_motorista; ?>" placeholder="Click no resultado ao aparecer"  required />
                                    <input id="motorista" autocomplete="false" type="hidden" class="form-control" name="motorista" value="<?php echo $v_motorista_id; ?>" />
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="r_motorista">
                                            <i class="fa fas fa-backspace "></i>
                                        </span>
                                    </div>
                                    <div class="resultmotorista"></div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $("#r_motorista").click(function(ev){
                                            document.getElementById('motorista').value='';
                                            document.getElementById('m<?php echo $c_motorista;?>').value='';
                                        });
                                    });
                                </script>
                            </div>

                            <div class="col-md-6">
                                <label for="peso_entrada">PESO INICIAL</label>

                                <div class="input-group">
                                    <input autocomplete="off" id="peso_entrada" placeholder="insira o peso inicial" type="text" class="form-control" name="peso_entrada" value="<?php echo $pesagem['peso_entrada']; ?>" required />
                                    <div class="input-group-append">
                                        <span class="input-group-text">,000 Kg</span>
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $('#peso_entrada').mask('000000', {reverse: true});
                                    });
                                </script>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-12 pb-2">
                                <input type="submit" name="gogo" id="gogo" class="btn btn-success btn-block my-2" value="AVANÇAR >>"/>
                            </div>
                            <script>
                                var formID = document.getElementById("formx");
                                var send = $("#gogo");

                                $(formID).submit(function(event){
                                    if (formID.checkValidity()) {
                                        send.attr('disabled', 'disabled');
                                        send.attr('value', 'AGUARDE...');
                                    }
                                });
                            </script>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>