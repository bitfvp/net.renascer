<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="rmsave";
    $entrada=fncgetentrada($_GET['id']);
    $romaneiotemp=$entrada['romaneio'];
}else{


    if (date('Y-m-d')>='2023-01-02'){
//        echo "a data é maior ou igual";
        $sql = "SELECT Max(romaneio) FROM ";
        $sql.="ren_entradas where romaneio_tipo=1 and ref='R' ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $mid = $consulta->fetch();
        $sql=null;
        $consulta=null;
    }else{
//        echo "ainda não";
        $sql = "SELECT Max(romaneio) FROM ";
        $sql.="ren_entradas where romaneio_tipo=1 and ref='' ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $mid = $consulta->fetch();
        $sql=null;
        $consulta=null;
    }

    $a="rmnew";
    $romaneiotemp=$mid[0]+1;
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">
    $(document).ready(function () {
        $('#serchfornecedor input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultfornecedor");
            if (inputValf.length) {
                $.get("includes/fornecedor.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });



        // Set search input value on click of result item
        $(document).on("click", ".resultfornecedor p", function () {
            $(this).parents("#serchfornecedor").find('input[type="text"]').val($(this).text());
            var fornecedor = this.id;
            $('#fornecedor').val(fornecedor);
            $(this).parent(".resultfornecedor").empty();
        });
    });

    $(document).ready(function () {
        $('#serchempresa input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultempresa");
            if (inputValf.length) {
                $.get("includes/empresa.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultempresa p", function () {
            $(this).parents("#serchempresa").find('input[type="text"]').val($(this).text());
            var empresa = this.id;
            $('#empresa').val(empresa);
            $(this).parent(".resultempresa").empty();
        });
    });

    $(document).ready(function () {
        $('#serchcorretor input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultcorretor");
            if (inputValf.length) {
                $.get("includes/corretor.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultcorretor p", function () {
            $(this).parents("#serchcorretor").find('input[type="text"]').val($(this).text());
            var corretor = this.id;
            $('#corretor').val(corretor);
            $(this).parent(".resultcorretor").empty();
        });
    });

    $(document).ready(function () {
        $('#serchmotorista input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultmotorista");
            if (inputValf.length) {
                $.get("includes/motorista.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultmotorista p", function () {
            var motorista = this.id;
            $.when(
                // $(this).parents("#serchmotorista").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchmotorista").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultmotorista").empty()

                $(this).parents("#serchmotorista").find('input[type="text"]').val($(this).text()),
                $('#motorista').val(motorista),
                $(this).parent(".resultmotorista").empty()

            ).then(
                function() {
                    // roda depois de acao1 e acao2 terminarem
                    setTimeout(function() {
                        //do something special
                        $( "#gogo" ).click();
                    }, 500);
                });
        });
    });


</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vrm_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">CADASTRO DE ROMANEIO</h3>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $entrada['id']; ?>"/>
                <label for="romaneio">NÚMERO DE ROMANEIO</label>
                <input autocomplete="off" autofocus id="romaneio" type="text" class="form-control" name="romaneio" value="<?php echo $romaneiotemp; ?>" placeholder="Preencha com o número de romaneio"/>
                <script>
                    $(document).ready(function(){
                        $('#romaneio').mask('0000000', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-3">
                <label for="nota">NÚMERO DE NOTA <i class="text-warning">*</i></label>
                <input autocomplete="off" id="nota" type="text" class="form-control" name="nota" value="<?php echo $entrada['nota']; ?>" placeholder="Preencha com o número da NF" />
                <script>
                    $(document).ready(function(){
                        $('#nota').mask('000.000.000', {reverse: false});
                    });
                </script>
            </div>

            <div class="col-md-3">
                <label for="controle_interno">POSSUI CONTROLE INTERNO</label>
                <select name="controle_interno" id="controle_interno" class="form-control" required>
                    <option selected="" value="<?php if ($entrada['controle_interno'] == "") {
                        echo null;
                    } else {
                        echo $entrada['controle_interno'];
                    } ?>">
                        <?php
                        if ($entrada['controle_interno'] == null) {
                            echo "Selecione...";
                        }

                        if ($entrada['controle_interno'] == 1) {
                            echo "SIM";
                        }
                        if ($entrada['controle_interno'] == 2) {
                            echo "NÃO";
                        }
                        ?>
                    </option>
                    <option value="1">SIM</option>
                    <option value="2">NÃO</option>
                </select>
            </div>

            <div class="col-md-3">
                <label for="ocr">OCR </label>
                <input autocomplete="off" id="ocr" type="text" class="form-control" name="ocr" value="<?php echo $entrada['ocr']; ?>" placeholder="Preencha com o número da OCR" />
                <script>
                    $(document).ready(function(){
                        $('#ocr').mask('00000000', {reverse: true});
                    });
                </script>
            </div>


        </div>

        <div class="row">
            <div class="col-md-6" id="serchfornecedor">
                <label for="fornecedor">FORNECEDOR</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_fornecedor_id=$entrada['fornecedor'];
                        $v_fornecedor=fncgetpessoa($entrada['fornecedor'])['nome'];

                    }else{
                        $v_fornecedor_id="";
                        $v_fornecedor="";
                    }
                    $c_fornecedor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="f<?php echo $c_fornecedor;?>" id="f<?php echo $c_fornecedor;?>" value="<?php echo $v_fornecedor; ?>" placeholder="Click no resultado ao aparecer" required/>
                    <input id="fornecedor" autocomplete="false" type="hidden" class="form-control" name="fornecedor" value="<?php echo $v_fornecedor_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_fornecedor">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultfornecedor"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_fornecedor").click(function(ev){
                            document.getElementById('fornecedor').value='';
                            document.getElementById('f<?php echo $c_fornecedor;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-6" id="serchempresa">
                <label for="empresa">EMPRESA/PRODUTOR <i class="text-warning">*uso com notas</i></label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_empresa_id=$entrada['empresa'];
                        $v_empresa=fncgetpessoa($entrada['empresa'])['nome'];

                    }else{
                        $v_empresa_id="";
                        $v_empresa="";
                    }
                    $c_empresa = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="e<?php echo $c_empresa;?>" id="e<?php echo $c_empresa;?>" value="<?php echo $v_empresa; ?>" placeholder="Click no resultado ao aparecer" />
                    <input id="empresa" autocomplete="false" type="hidden" class="form-control" name="empresa" value="<?php echo $v_empresa_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_empresa">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultempresa"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_empresa").click(function(ev){
                            document.getElementById('empresa').value='';
                            document.getElementById('e<?php echo $c_empresa;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-6" id="serchcorretor">
                <label for="corretor">CORRETOR</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_corretor_id=$entrada['corretor'];
                        $v_corretor=fncgetpessoa($entrada['corretor'])['nome'];

                    }else{
                        $v_corretor_id="";
                        $v_corretor="";
                    }
                    $c_corretor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="c<?php echo $c_corretor;?>" id="c<?php echo $c_corretor;?>" value="<?php echo $v_corretor; ?>" placeholder="Click no resultado ao aparecer" />
                    <input id="corretor" autocomplete="false" type="hidden" class="form-control" name="corretor" value="<?php echo $v_corretor_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_corretor">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultcorretor"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_corretor").click(function(ev){
                            document.getElementById('corretor').value='';
                            document.getElementById('c<?php echo $c_corretor;?>').value='';
                        });
                    });
                </script>
            </div>


            <div class="col-md-6" id="serchmotorista">
                <label for="motorista">MOTORISTA</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_motorista_id=$entrada['motorista'];
                        $v_motorista=fncgetpessoa($entrada['motorista'])['nome'];

                    }else{
                        $v_motorista_id="";
                        $v_motorista="";
                    }
                    $c_motorista = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="m<?php echo $c_motorista;?>" id="m<?php echo $c_motorista;?>" value="<?php echo $v_motorista; ?>" placeholder="Click no resultado ao aparecer"  required />
                    <input id="motorista" autocomplete="false" type="hidden" class="form-control" name="motorista" value="<?php echo $v_motorista_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_motorista">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultmotorista"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_motorista").click(function(ev){
                            document.getElementById('motorista').value='';
                            document.getElementById('m<?php echo $c_motorista;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="AVANÇAR >>" name="gogo" id="gogo" class="btn btn-block btn-success mt-4">
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>


        </div>

    </form>
</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>