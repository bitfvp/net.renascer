<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vv_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_v']) and is_numeric($_GET['id_v'])){
//    $a="entradasave";
    $venda=fncgetvenda($_GET['id_v']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vv_lista");
    exit();
}

?>
<!--/////////////////////////////////////////////////////-->
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-12">
            <?php
            include_once("includes/ve_cab.php");
            ?>

            <div class="btn-group" role="group" aria-label="">

                <a href="index.php?pg=Vv_p&id_v=<?php echo $_GET['id_v']; ?>" target="" title="nova pesagem" class="btn  btn-info fas fa-plus text-dark">
                NOVA PESAGEM
                </a>

            </div>

        </div>
    </div>

    <?php
    try{
        $sql = "SELECT * FROM "
            ."ren_vendas_pesagens "
            ."WHERE ren_vendas_pesagens.venda=:venda "
            ."order by ren_vendas_pesagens.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":venda", $_GET['id_v']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row mt-2">
        <div class="col-md-12">
            <div class="card text-white bg-dark mb-3">
                <div class="card-header">
                    Pesagens abaixo <i class="fas fa-arrow-down"></i>
                </div>
            </div>



                <?php
                foreach ($pesagens as $dados){
                    if ($dados['datahora_saida']==null or $dados['datahora_saida']==0 or $dados['datahora_saida']==""){
                        $cordalinha = " ";
                    }else{
                        $cordalinha = " text-warning bg-secondary ";
                    }
                    $pe_id = $dados["id"];
                    $data_inicial = datahoraBanco2data($dados["data_ts"]);
                    $peso_entrada = number_format($dados["peso_entrada"],0,',',' ')." Kg ";
                    $peso_saida = number_format($dados["peso_saida"],0,',',' ')." Kg";
                    $motorista = fncgetpessoa($dados["motorista"])['nome'];
                    $placa = $dados["placa"];
                    $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
                    $usuario = fncgetusuario($dados["usuario"])['nome'];
                    ?>
            <div class="card">
                <div class="card-body <?php echo $cordalinha; ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <i class="badge badge-info float-right" title="id de pesagem de venda">#<?php echo $pe_id; ?></i>
                            <h6><small>MOTORISTA: </small><strong><?php echo strtoupper($motorista); ?></strong></h6>
                            <h6><small>PLACA: </small><strong><?php echo $placa; ?></strong></h6>
                            <h6><small>INÍCIO: </small><strong><?php echo $data_inicial; ?></strong></h6>
                            <h6><small>PESO INICIAL: </small><strong><?php echo $peso_entrada; ?></strong></h6>
                            <h6><small>PESO FINAL: </small><strong><?php echo $peso_saida; ?></strong></h6>
                            <h6><small>FINAL: </small><strong><?php echo $datahora_saida; ?></strong></h6>

                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-12">
                                    <h6>Ass. motorista</h6>
                                </div>

                                <?php
                                $sql = "SELECT * FROM ren_dados WHERE indicador=? and tipo=4 and status=1";
                                global $pdo;
                                $consulta = $pdo->prepare($sql);
                                $consulta->bindParam(1,$pe_id);
                                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                                $arqs = $consulta->fetchAll();
                                $arqscount = $consulta->rowCount();
                                $sql=null;
                                $consulta=null;

                                foreach ($arqs as $arq){
                                    echo "<div class='col-12 text-center'>";
                                    echo "<img class=' img-thumbnail' id='' src='../../dados/renascer/{$arq['tipo']}/{$arq['arquivo']}.{$arq['extensao']}' />";
                                    $act="index.php?pg=Vv&id_v={$_GET['id_v']}&page_bk=Vv&indice_bk=id_v&id_bk={$_GET['id_v']}&aca=apagaassinatura&arq={$arq['arquivo']}";
                                    echo "<a href='{$act}' target='_self' title='Delete image' class='far fa-trash-alt btn btn-sm'></a>";
                                    echo "</div>";
                                }
                                ?>






                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="btn-group" role="group" aria-label="">
                                <?php if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0 or $allow["allow_6"]==1){?>
                                    <a href="index.php?pg=Vv_p&id_v=<?php echo $_GET['id_v']; ?>&id_p=<?php echo $pe_id; ?>" title="Editar pesagem" class="btn btn-sm btn-primary">
                                        <i class="fas fa-pen"></i>
                                        EDITAR PESAGEM
                                    </a>

                                    <a href="index.php?pg=Vv_pfinal&id_v=<?php echo $_GET['id_v']; ?>&id_p=<?php echo $pe_id; ?>" title="Finalizar pesagem" class="btn btn-sm btn-success">
                                        <i class="fas fa-check-double"></i>
                                        FINALIZAR PESAGEM
                                    </a>

                                <?php } ?>

                                <?php if ($dados["datahora_saida"]!="" or $dados["datahora_saida"]!=0){?>
                                    <a href="index.php?pg=Vv_print1&id_v=<?php echo $_GET['id_v'] ?>&id_p=<?php echo $pe_id; ?>" target="_blank" title="Finalizar pesagem" class="btn btn-sm btn-outline-dark">
                                        <i class="fas fa-print"></i>
                                        IMPRIMIR COMPROVANTE
                                    </a>
                                <?php } ?>



                                <?php if ($allow["allow_6"]==1){?>
                                    <a href="index.php?pg=Vv_lista&aca=apagarpesagemve&id_p=<?php echo $pe_id; ?>" title="Finalizar pesagem" class="btn btn-sm btn-danger">
                                        <i class="fas fa-trash"></i>
                                        EXCLUIR PESAGEM
                                    </a>
                                <?php } ?>

                                <a href="index.php?pg=V_ass&id_pessoa=<?php echo $dados["motorista"]; ?>&tipo=4&indicador=<?php echo $pe_id; ?>&page_bk=Vv&indice_bk=id_v&id_bk=<?php echo $_GET['id_v']; ?>" title="Selecionar assinatura" class="btn btn-outline-dark">
                                    <i class="fas fa-file-signature"></i>
                                    ASSINATURA
                                </a>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
                    <?php
                }
                ?>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>