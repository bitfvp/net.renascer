<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vrm_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
//    $a="entradasave";
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vrm_lista");
    exit();
}
if (isset($_GET['id_p']) and is_numeric($_GET['id_p'])){
    $a="pesagemsave_final";
    $pesagem=fncgetpesagem($_GET['id_p']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vrm_lista");
    exit();
}
$romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-4">
            <?php
            include_once("includes/rm_cab.php");
            ?>
            <div class="btn-group" role="group" aria-label="">
                <?php if ($allow["allow_6"]==1){?>
                    <a href="index.php?pg=Vrm_editar&id=<?php echo $_GET['id_e']; ?>" title="Editar entrada" class="btn btn-sm btn-primary fas fa-pen text-dark">
                        EDITAR ROMANEIO
                    </a>
                <?php } ?>

            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-cadastro-heading">Cadastro de pesagens - final</h3>
                    <hr>
                    <form class="form-signin" action="<?php echo "index.php?pg=Vp&id_e={$_GET['id_e']}&aca={$a}"; ?>" method="post" id="formentrada1">
                        <div class="row">
                            <div class="col-md-4">
                                <input id="id_p" type="hidden" class="txt bradius" name="id_p" value="<?php echo $pesagem['id']; ?>"/>
                                <label for="produto">PRODUTO</label>
                                <input autocomplete="off" readonly="readonly" id="produto" type="text" class="form-control text-uppercase" name="produto" value="<?php echo $pesagem['produto']; ?>" placeholder="" required />
                            </div>
                            <div class="col-md-3">
                                <label for="embalagem">EMBALAGEM</label>
                                <select name="embalagem" id="embalagem" readonly="readonly" class="form-control" required>
                                    <option selected="" value="<?php if ($pesagem['embalagem'] == "") {
                                        echo "";
                                    } else {
                                        echo $pesagem['embalagem'];
                                    } ?>">
                                        <?php
                                        if ($pesagem['embalagem'] == 0) {
                                            echo "Selecione...";
                                        }
                                        if ($pesagem['embalagem'] == 1) {
                                            echo "SACARIA";
                                        }
                                        if ($pesagem['embalagem'] == 2) {
                                            echo "BIG BAGS";
                                        }
                                        if ($pesagem['embalagem'] == 3) {
                                            echo "GRANEL";
                                        }
                                        ?>
                                    </option>
                                    <option value="1">SACARIA</option>
                                    <option value="2">BIG BAGS</option>
                                    <option value="3">GRANEL</option>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <label for="peso_entrada">PESO INICIAL</label>

                                <div class="input-group">
                                    <input autocomplete="off" id="peso_entrada" placeholder="insira o peso inicial" type="text" class="form-control" name="peso_entrada" value="<?php echo $pesagem['peso_entrada']; ?>" required readonly="readonly" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">,000 Kg</span>
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $('#peso_entrada').mask('000000', {reverse: true});
                                    });
                                </script>
                            </div>
                            <div class="col-md-12">
                                <label for="obs_entrada">OBSERVAÇÃO DE ENTRADA</label>
                                <textarea id="obs_entrada" readonly="readonly" onkeyup="limite_textarea(this.value,250,obs_entrada,'cont')" maxlength="250" class="form-control" rows="2" name="obs_entrada"><?php echo $pesagem['obs_entrada']; ?></textarea>
                                <span id="cont">250</span>/250
                            </div>

                            <div class="col-md-6">

                                <label for="peso_saida">PESO FINAL</label>
                                <div class="input-group">
                                    <input autocomplete="off" autofocus id="peso_saida" placeholder="insira o peso final" type="text" class="form-control" name="peso_saida" value="<?php if ($pesagem['peso_saida']==0){}else{echo $pesagem['peso_saida'];}  ?>" required />
                                    <div class="input-group-append">
                                        <span class="input-group-text">,000 Kg</span>
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $('#peso_saida').mask('000000', {reverse: true});
                                    });
                                    jQuery(document).ready(function(){
                                        jQuery('input').on('keyup',function(){
                                            if(jQuery(this).attr('name') === 'informacao'){
                                                return false;
                                            }

                                            var soma1 = (jQuery('#peso_entrada').val() == '' ? 0 : jQuery('#peso_entrada').val());
                                            var soma2 = (jQuery('#peso_saida').val() == '' ? 0 : jQuery('#peso_saida').val());
                                            var pesoliquido = (parseInt(soma1) - parseInt(soma2));
                                            var sacas = (parseInt(pesoliquido) / 60);
                                            var textoinfo = pesoliquido + "KG ou " + sacas + " volumes";
                                            jQuery('#informacao').val(textoinfo);
                                        });
                                    });
                                </script>
                            </div>
                            <div class="col-md-6">
                                <label for="">INFORMAÇÃO</label>
                                <div class="input-group">
                                    <input  id="informacao" type="text" class="form-control" name="informacao" value="<?php $pl=$pesagem['peso_entrada']-$pesagem['peso_saida']; $ss=$pl/60; echo $pl . "KG ou " . $ss . " volumes";?>" readonly="readonly"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="embalagem_devolvida">EMBALAGEM FOI DEVOLVIDA</label>
                                <select name="embalagem_devolvida" id="embalagem_devolvida" class="form-control" required>
                                    <option selected="" value="<?php if ($pesagem['embalagem_devolvida'] == "") {
                                        echo null;
                                    } else {
                                        echo $pesagem['embalagem_devolvida'];
                                    } ?>">
                                        <?php
                                        if ($pesagem['embalagem_devolvida'] == null) {
                                            echo "Selecione...";
                                        }

                                        if ($pesagem['embalagem_devolvida'] == 1) {
                                            echo "SIM";
                                        }
                                        if ($pesagem['embalagem_devolvida'] == 2) {
                                            echo "NÃO";
                                        }
                                        ?>
                                    </option>
                                    <option value="1">SIM</option>
                                    <option value="2">NÃO</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="obs_saida">OBSERVAÇÃO DE SAÍDA</label>
                                <textarea id="obs_saida" onkeyup="limite_textarea(this.value,250,obs_saida,'cont')" maxlength="250" class="form-control" rows="2" name="obs_saida"><?php echo $pesagem['obs_saida']; ?></textarea>
                                <span id="cont">250</span>/250
                            </div>

                            <div class="col-md-12">
                                <input type="submit" class="btn btn-success btn-block my-2" value="FINALIZAR PESAGEM">
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>