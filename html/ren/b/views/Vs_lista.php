<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vs_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT "
        ."ren_simples.id, "
        ."ren_simples.data_ts, "
        ."ren_simples.usuario, "
        ."ren_simples.motorista, "
        ."ren_simples.placa, "
        ."ren_simples.obs, "
        ."ren_simples.obs2, "
        ."ren_simples.peso_entrada, "
        ."ren_simples.peso_saida, "
        ."ren_simples.datahora_saida "
        ."FROM "
        ."ren_simples "
        ."INNER JOIN ren_pessoas AS pessoas_motorista ON ren_simples.motorista = pessoas_motorista.id "
        ."WHERE "
        ."ren_simples.id <> 0 ";

    if (isset($_GET['sca']) and $_GET['sca']!='') {
        $sca=$_GET['sca'];
        $sql .=" AND pessoas_motorista.nome LIKE '%$sca%' ";
    }

    if (isset($_GET['scb']) and $_GET['scb']!=0 and $_GET['scb']!='') {
        $inicial=$_GET['scb'];
        $inicial.=" 00:00:01";
        $final=$_GET['scb'];
        $final.=" 23:59:59";
        $sql .=" AND ((ren_simples.data_ts)>=:inicial) And ((ren_simples.data_ts)<=:final) ";
    }else{
        if (((isset($_GET['sca']) and $_GET['sca']!='') or (isset($_GET['scb']) and $_GET['scb']!='')) or ((isset($_GET['scc']) and $_GET['scc']!='') or (isset($_GET['scc']) and $_GET['scc']!=''))){
            //tempo todo
            $inicial="2021-01-01";
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((ren_simples.data_ts)>=:inicial) And ((ren_simples.data_ts)<=:final) ";
        }else{
            //apenas dia anterior
            $inicial=date("Y-m-d",strtotime("-10 days"));
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((ren_simples.data_ts)>=:inicial) And ((ren_simples.data_ts)<=:final) ";
        }

    }

    if (isset($_GET['scc']) and $_GET['scc']!='') {
        $scc=$_GET['scc'];
        $sql .=" AND (ren_simples.obs LIKE '%$scc%' or ";
        $sql .="ren_simples.obs2 LIKE '%$scc%') ";
    }

    $sql .="order by ren_simples.data_ts DESC LIMIT 0,50 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
//    if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
//        $consulta->bindValue(":romaneio", "%".$_GET['sca']."%");
//    }
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container">
    <h3 class="form-cadastro-heading"><a href="index.php?pg=Vs_lista" class="text-decoration-none text-dark">Entrada e saída simples</a></h3>
    <hr>

    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-9 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
            </div>
            <input name="pg" value="Vs_lista" hidden/>
            <input type="text" name="sca" id="sca" placeholder="motorista" autocomplete="off" class="form-control" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <input type="date" name="scb" id="scb" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
            <input type="text" name="scc" id="scc" placeholder="termo/palavra/citação" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
        </div>
    </form>

    <script type="text/javascript">
        function selecionaTexto() {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <div class="row">
        <div class="col-md-12">
            <a href="index.php?pg=Vs1" class="btn btn-block btn-primary mb-2" ><i class="fas fa-level-down-alt"></i> Novo</a>
        </div>
    </div>

    <table class="table table-sm table-stripe table-hover table-bordered">
        <thead class="thead-dark">
        <tr>
<!--            <th scope="col" class="text-center">#</th>-->
            <th scope="col"><small>DATA</small></th>
            <th scope="col"><small>MOTORISTA</small></th>
            <th scope="col"><small>PLACAS</small></th>
            <th scope="col"><small>OBS</small></th>
            <th scope="col"><small>ENTRADA</small></th>
            <th scope="col"><small>SAÍDA</small></th>
            <th scope="col"><small>OBS</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="5" >
            </th>
            <td colspan="4"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);
        }
        if($_GET['scc']!="" and isset($_GET['scc'])) {
            $stc = strtoupper($_GET['scc']);
            define('CSC', $stc);
        }
        foreach ($entradas as $dados){
            $en_id = $dados["id"];
            $motorista = strtoupper(fncgetpessoa($dados["motorista"])['nome']);
            $placa = $dados["placa"];

            $data_ts = datahoraBanco2data($dados["data_ts"]);
            $peso_entrada = $dados["peso_entrada"];
            $obs = strtoupper($dados["obs"]);

            $peso_saida = $dados["peso_saida"];
            $obs2 = strtoupper($dados["obs2"]);
            $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
            $usuario = $dados["usuario"];

        if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
            $corlinha="";
        }else{
            $corlinha=" bg-dark text-warning ";
        }

            ?>

            <tr id="<?php echo $en_id;?>" class="<?php echo $corlinha;?>">
<!--                <th scope="row" id="">-->
<!--                    <small>--><?php //echo $en_id; ?><!--</small>-->
<!--                </th>-->
                <td><?php echo $data_ts; ?></td>
                <td><?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $aaa = $motorista;
                        $aa = explode(CSA, $aaa);
                        $a = implode("<span class='text-danger'>{$sta}</span>", $aa);
                        echo $a;
                    }else{
                        echo $motorista;
                    }
                    ?>
                </td>
                <td><?php echo $placa; ?></td>
                <td><?php
                    if($_GET['scc']!="" and isset($_GET['scc'])) {
                        $stc = CSC;
                        $ccc = $obs;
                        $cc = explode(CSC, $ccc);
                        $c = implode("<span class='text-danger'>{$stc}</span>", $cc);
                        echo $c;
                    }else{
                        echo $obs;
                    }
                    ?>
                </td>
                <td><?php echo $peso_entrada."KG "; ?></td>
                <td><?php echo $peso_saida."KG "; ?></td>
                <td><?php
                    if($_GET['scc']!="" and isset($_GET['scc'])) {
                        $stc = CSC;
                        $ddd = $obs2;
                        $dd = explode(CSC, $ddd);
                        $d = implode("<span class='text-danger'>{$stc}</span>", $dd);
                        echo $d;
                    }else{
                        echo $obs2;
                    }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                    if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0 or $allow["allow_6"]==1){?>
                        <div class="btn-group" role="group" aria-label="">
                            <a href="index.php?pg=Vsfinal&id_s=<?php echo $en_id; ?>" title="Entrada" class="btn btn-sm btn-success fas fa-check-double text-dark">
                                <br>FINALIZAR
                            </a>
                            <a href="index.php?pg=Vs2&id_s=<?php echo $en_id; ?>" title="Editar entrada" class="btn btn-sm btn- btn-primary fas fa-pen text-dark">
                                <br>EDITAR
                            </a>

                            <?php if ($allow["allow_6"]==1){?>
                                <div class="dropdown show">
                                    <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-trash"><br>EXCLUIR</i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">Não</a>
                                        <a class="dropdown-item bg-danger" href="index.php?pg=Vs_lista&aca=apagarsimples&id_s=<?php echo $en_id; ?>">Apagar</a>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    <?php } ?>
                    <br>
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vs&id_s=<?php echo $en_id; ?>" title="Acessar" class="btn btn-sm btn- btn-warning">
                            <i class="fas fa-search-plus"><br>ACESSAR</i>
                        </a>
                        <a href="index.php?pg=Vs_print1&id_s=<?php echo $en_id; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-info">
                            <i class="fas fa-print"><br>COMPROVANTE</i>
                        </a>
                    </div>

                </td>
            </tr>

<?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>