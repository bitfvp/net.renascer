<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id_s']) and is_numeric($_GET['id_s'])){
    $entrada=fncgetsimples($_GET['id_s']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vs_lista");
    exit();
}
?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
            <div class="col-8">
                <h3 class="text-uppercase">MOVIMENTAÇÃO SIMPLIFICADA</h3>
                <h4>MOTORISTA: <strong><?php echo strtoupper(fncgetpessoa($entrada['motorista'])['nome']); ?></strong></h4>
                <h4>PLACA: <strong><?php echo $entrada['placa']; ?></strong></h4>
            </div>
            <div class="col-4 text-right">
            </div>
    </div>

        <hr class="hrgrosso">
        <h3>INFORMAÇÕES</h3>

            <div class="row">
                <div class="col-6 border">
                    DATA INICIAL: <strong><?php echo datahoraBanco2data($entrada['data_ts']); ?></strong>
                </div>
                <div class="col-6 border">
                    DATA FINAL: <strong><?php echo datahoraBanco2data($entrada['datahora_saida']); ?></strong>
                </div>
            </div>

    <div class="row">
        <div class="col-6 border">
            OBS: <strong><?php echo $entrada['obs']; ?></strong>
        </div>
        <div class="col-6 border">
            OBS FINAL: <strong><?php echo $entrada['obs2']; ?></strong>
        </div>
    </div>

    <div class="row">
        <div class="col-6 border">
            PESO DE ENTRADA: <strong><?php echo number_format($entrada['peso_entrada'],3); ?>KG</strong>
        </div>
        <div class="col-6 border">
            PESO DE SAÍDA: <strong><?php echo number_format($entrada['peso_saida'],3); ?>KG</strong>
        </div>
    </div>

    <?php
    if ($entrada['peso_entrada']>=$entrada['peso_saida']){
        $peso_liquido=$entrada['peso_entrada'] - $entrada['peso_saida'];
    }else{
        $peso_liquido=$entrada['peso_saida'] - $entrada['peso_entrada'];
    }
    ?>

            <div class="row">
                <div class="col-6 offset-6 border" >
                    PESO LÍQUIDO: <strong><?php echo number_format($peso_liquido,3); ?>KG</strong>
                </div>
            </div>


    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
        <div class="col-12">
            <strong> Concordo com os dados prescritos acima e dou fé.</strong>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-6">
            <?php
            $okk=0;

            //novo
            $sql = "SELECT * FROM ren_dados WHERE indicador=? and tipo=2 and status=1";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$entrada['id']);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $dados = $consulta->fetchAll();
            $dadoscount = $consulta->rowCount();
            $sql=null;
            $consulta=null;
            if ($dadoscount>0){
                $okk++;
            }
            foreach ($dados as $dado){
                echo "<img class='mt-0' height='50' id='' src='../../dados/renascer/{$dado['tipo']}/{$dado['arquivo']}.{$dado['extensao']}' />";
            }

            if ($okk==0){
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Assinatura do motorista
            </h4>
        </div>

        <div class="col-6">
            <?php
            if ($entrada['usuario']!=0){
                $sql = "SELECT * FROM tbl_users_assinatura WHERE user_id=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$entrada['usuario']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $assinatura = $consulta->fetch();
                $assinaturacount = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                if ($assinaturacount>0){
                    echo "<img class='mt-0' height='50' id='' src='{$assinatura['assinatura']}' />";
                }else{
                    echo "<h4>_______________________</h4>";
                }
            }else{
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Operador: <strong><?php echo fncgetusuario($entrada['usuario'])['nome']; ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>