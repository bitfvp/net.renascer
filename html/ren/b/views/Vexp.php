<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatorio de lotes por fornecedor-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

try{
    $sql = "SELECT * FROM "
        ."ren_entradas_expedicao "
        ."WHERE entrada=:entrada ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":entrada", $_GET['id_e']);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}
$expedicao = $consulta->fetch();
$expedicao_quant = $consulta->rowCount();
$sql = null;
$consulta = null;


if ($expedicao_quant>0){
    //editar
    $a="expsave";
    $expedicao=fncgetexpedicao($_GET['id_e']);

    if ($expedicao['saca']==0){
        $emb_saca="";
        $emb_sacan="checked";
    }else{
        $emb_saca="checked";
        $emb_sacan="";
    }
    $temp_saca=$expedicao['saca_q'];

    if ($expedicao['bb']==0){
        $emb_bb="";
        $emb_bbn="checked";
    }else{
        $emb_bb="checked";
        $emb_bbn="";
    }
    $temp_bb=$expedicao['bb_q'];


    if ($expedicao['granel']==0){
        $emb_gra="";
        $emb_gran="checked";
    }else{
        $emb_gra="checked";
        $emb_gran="";
    }
    $temp_granel=$expedicao['granel_q'];

    $temp_obs=$expedicao['obs'];



}else{
    //novo
    $a="expnew";
    try{
        $sql = "SELECT * FROM "
            ."ren_entradas_pesagens "
            ."WHERE ren_entradas_pesagens.entrada=:entrada "
            ."order by ren_entradas_pesagens.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":entrada", $_GET['id_e']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;

    $emb_saca="";
    $emb_sacan="checked";
    $peso_saca=0;

    $emb_bb="";
    $emb_bbn="checked";
    $peso_bb=0;

    $emb_gra="";
    $emb_gran="checked";
    $peso_gra=0;
    foreach ($pesagens as $dados){

        if ($dados["embalagem"]==1){
            $emb_saca="checked";
            $emb_sacan="";
            if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                //
            }else{
                $pl=$dados["peso_entrada"]-$dados["peso_saida"];
//            $ss=$pl/60;
                $peso_saca+=$pl;
            }
        }

        if ($dados["embalagem"]==2){
            $emb_bb="checked";
            $emb_bbn="";
            if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                //
            }else{
                $pl=$dados["peso_entrada"]-$dados["peso_saida"];
//            $ss=$pl/60;
                $peso_bb+=$pl;
            }
        }
        if ($dados["embalagem"]==3){
            $emb_gra="checked";
            $emb_gran="";
            if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                //
            }else{
                $pl=$dados["peso_entrada"]-$dados["peso_saida"];
//            $ss=$pl/60;
                $peso_gra+=$pl;
            }
        }

    }
    if($peso_saca>0){
        $temp_saca= $peso_saca/60;
        $temp_saca = number_format($temp_saca,1,',',' ')." V";
    }
    if($peso_bb>0){
        $temp_bb= $peso_bb/60;
        $temp_bb = number_format($temp_bb,1,',',' ')." V";
    }
    if($peso_gra>0){
        $temp_granel= $peso_gra/60;
        $temp_granel = number_format($temp_granel,1,',',' ')." V";
    }

    $temp_obs="";


}
?>
<main class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header bg-info text-light">
                Expedição de Entrada
                </div>
                <div class="card-body">
                    <form action="<?php echo "index.php?pg=Vexp_print&id_e={$_GET['id_e']}&aca={$a}"; ?>" method="post" target="_blank" rel="noopener" name="formx" onsubmit="setTimeout(function () { window.location.reload(); }, 1000)">
                        <div class="row">
                            <style type="text/css">
                                input[type=radio]{
                                    transform:scale(2.1);
                                }

                                .form-check label:last-child {
                                    transform:scale(1.5);
                                }
                            </style>

                            <div class="col-md-4">
                                <label for="saca">Sacaria</label>
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="saca" id="saca" value="1" <?php echo $emb_saca; ?> >
                                    <label class="form-check-label ml-3" for="saca">
                                        SIM
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="saca" id="saca" value="0" <?php echo $emb_sacan; ?> >
                                    <label class="form-check-label ml-3" for="saca">
                                        NÃO
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="bb">Big Bags</label>
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="bb" id="bb" value="1" <?php echo $emb_bb; ?> >
                                    <label class="form-check-label ml-3" for="bb">
                                        SIM
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="bb" id="bb" value="0" <?php echo $emb_bbn; ?> >
                                    <label class="form-check-label ml-3" for="bb">
                                        NÃO
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="granel">Granel</label>
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="radio" name="granel" id="granel" value="1" <?php echo $emb_gra; ?> >
                                    <label class="form-check-label ml-3" for="granel">
                                        SIM
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="granel" id="granel" value="0" <?php echo $emb_gran; ?> >
                                    <label class="form-check-label ml-3" for="granel">
                                        NÃO
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="saca_q">Quant.</label>
                                <input autocomplete="off" id="saca_q" type="text" class="form-control" name="saca_q" value="<?php echo $temp_saca;?>"/>
                            </div>
                            <div class="col-md-4">
                                <label for="bb_q">Quant.</label>
                                <input autocomplete="off" id="bb_q" type="text" class="form-control" name="bb_q" value="<?php echo $temp_bb;?>"/>
                            </div>
                            <div class="col-md-4">
                                <label for="granel_q">Quant.</label>
                                <input autocomplete="off" id="granel_q" type="text" class="form-control" name="granel_q" value="<?php echo $temp_granel;?>"/>
                            </div>



                            <div class="col-md-12">
                                <label for="obs">Obs.</label>
                                <input autocomplete="off" id="obs" type="text" class="form-control" name="obs" value="<?php echo $temp_obs;?>"/>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="GERAR EXPEDIÇÃO"/>
                            </div>
                            <script>
                                var formID = document.getElementById("formx");
                                var send = $("#gogo");

                                $(formID).submit(function(event){
                                    if (formID.checkValidity()) {
                                        send.attr('disabled', 'disabled');
                                        send.attr('value', 'AGUARDE...');
                                    }
                                });
                            </script>
                            <div class="col-md-6">
                                <a href="<?php echo "index.php?pg=Vexp&id_e={$_GET['id_e']}&aca=expreset"; ?>" class="btn btn-lg btn-warning btn-block my-2">Limpar</a>
                            </div>

                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>