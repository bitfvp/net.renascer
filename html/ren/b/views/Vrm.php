<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
//    $a="entradasave";
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vrm_lista");
    exit();
}
if (isset($_GET['id_p']) and is_numeric($_GET['id_p'])){
    $a="pesagemsave";
    $pesagem=fncgetpesagem($_GET['id_p']);
}else{
    $a="pesagemnew";
}
$romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php
            include_once("includes/rm_cab.php");
            ?>
            <div class="btn-group" role="group" aria-label="">
                <a href="index.php?pg=Vrm_p&id_e=<?php echo $_GET['id_e']; ?>" target="" title="nova pesagem" class="btn btn-info fas fa-plus text-dark"> NOVA PESAGEM</a>

                <?php if ($allow["allow_6"]==1){?>
                    <a href="index.php?pg=Vrm_editar&id=<?php echo $_GET['id_e']; ?>" title="Editar entrada" class="btn btn-primary fas fa-pen text-dark"> EDITAR ROMANEIO</a>
                <?php } ?>
                <a href="index.php?pg=Vrm_print1&id_e=<?php echo $_GET['id_e']; ?>" target="_blank" title="comprovante" class="btn btn-dark fas fa-print"> IMPRIMIR COMPROVANTE</a>

                <a href="index.php?pg=Vexp&id_e=<?php echo $_GET['id_e']; ?>" title="expedição" class="btn btn-secondary fas fa-print"> EXPEDIÇÃO DE ENTRADA</a>
                <a href="index.php?pg=V_ass&id_pessoa=<?php echo $entrada['motorista']; ?>&tipo=1&indicador=<?php echo $_GET['id_e']; ?>&page_bk=Vrm&indice_bk=id_e&id_bk=<?php echo $_GET['id_e']; ?>" title="Selecionar assinatura" class="btn btn-outline-dark fas fa-file-signature"> ASSINATURA</a>

            </div>
        </div>
    </div>

    <?php
    try{
        $sql = "SELECT * FROM "
            ."ren_entradas_pesagens "
            ."WHERE ren_entradas_pesagens.entrada=:entrada "
            ."order by ren_entradas_pesagens.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":entrada", $_GET['id_e']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <div class="card mt-2">
                <div class="card-header bg-info text-light">
                    Pesagens
                </div>
                <div class="card-body">
                    <?php
                    foreach ($pesagens as $dados){
                        if ($dados['datahora_saida']==null or $dados['datahora_saida']==0 or $dados['datahora_saida']==""){
                            $cordalinha = " ";
                        }else{
                            $cordalinha = " bg-secondary text-warning";
                        }
                        $pe_id = $dados["id"];
                        $data_inicial = datahoraBanco2data($dados["data_ts"]);
                        $produto = $dados["produto"];
                        switch ($dados["embalagem"]){
                            case 0:
                                $embalagem="Não selecionada";
                                break;
                            case 1:
                                $embalagem="SACARIA";
                                break;
                            case 2:
                                $embalagem="BIG BAGS";
                                break;
                            case 3:
                                $embalagem="GRANEL";
                                break;
                            default:
                                $embalagem="Não selecionada!";
                                break;
                        }
                        $peso_entrada = number_format($dados["peso_entrada"],0)."K";
                        $obs_entrada = $dados["obs_entrada"];
                        $peso_saida = number_format($dados["peso_saida"],0)."K";
                        $obs_saida = $dados["obs_saida"];
                        $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
                        switch ($dados["embalagem_devolvida"]){
                            case 0:
                                $embalagem_devolvida="";
                                break;
                            case 1:
                                $embalagem_devolvida="EMBALAGEM DEVOLVIDA";
                                break;
                            case 2:
                                $embalagem_devolvida="EMBALAGEM NÃO DEVOLVIDA";
                                break;
                            default:
                                $embalagem_devolvida="";
                                break;
                        }
                        if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                            $infopeso=" ";
                        }else{
                            $pl=$dados["peso_entrada"]-$dados["peso_saida"];
                            $ss=$pl/60;
                            $infopeso="PESO TOTAL: ".number_format($pl,0) . "K " . number_format($ss,2) . "v";
                        }
                        $usuario = fncgetusuario($dados["usuario"])['nome'];
                        ?>

                        <div id="" class="row <?php echo $cordalinha; ?> mb-2">

<!--                                <small>--><?php //echo $pe_id; ?><!--</small>-->

                            <div class="col-md-6 border">PRODUTO: <strong><?php echo $produto; ?></strong></div>
                            <div class="col-md-6 border">TIPO DE EMBALAGEM: <strong><?php echo $embalagem; ?></strong></div>
                            <div class="col-md-4 border">INICIO: <strong><?php echo $data_inicial; ?></strong></div>
                            <div class="col-md-4 border">PESO DE ENTRADA: <strong><?php echo $peso_entrada; ?></strong></div>
                            <div class="col-md-4 border">OBS INICIAL: <strong><?php echo $obs_entrada; ?></strong></div>
                            <div class="col-md-4 border">FIM: <strong class="text-dark"><?php echo $datahora_saida; ?></strong></div>
                            <div class="col-md-4 border">PESO DE SAÍDA: <strong class="text-dark"><?php echo $peso_saida; ?></strong></div>
                            <div class="col-md-4 border">OBS DE SAÍDA: <strong class="text-dark"><?php echo $obs_saida; ?></strong></div>
                            <div class="col-md-6 border"><strong class="text-dark"><?php echo $infopeso; ?></strong></div>
                            <div class="col-md-6 border"><strong class="text-dark"><?php echo $embalagem_devolvida; ?></strong></div>
                            <div class="col-md-12 border">
                                <strong class="">
                                    <div class="btn-group" role="group" aria-label="">
                                    <?php
                                    if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0 or $allow["allow_6"]==1){?>

                                        <a href="index.php?pg=Vrm_p&id_e=<?php echo $_GET['id_e']; ?>&id_p=<?php echo $pe_id; ?>" title="Editar pesagem" class="btn  btn-primary fas fa-pen">
                                            <br>EDITAR<br>PESAGEM
                                        </a>
                                        <a href="index.php?pg=Vrm_pfinal&id_e=<?php echo $_GET['id_e']; ?>&id_p=<?php echo $pe_id; ?>" title="Finalizar pesagem" class="btn  btn-success fas fa-check-double">
                                            <br>FINALIZAR<br>PESAGEM
                                        </a>
                                    <?php } ?>

                                    <?php if ($allow["allow_6"]==1){?>
                                        <div class="dropdown show">
                                            <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-trash"><br>EXCLUIR<br>PESAGEM</i>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a class="dropdown-item" href="#">Não</a>
                                                <a class="dropdown-item bg-danger" href="index.php?pg=Vrm_lista&aca=apagarpesagemrm&id_p=<?php echo $pe_id; ?>">Apagar</a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </strong>
                            </div>
                        </div>
                        <hr>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>