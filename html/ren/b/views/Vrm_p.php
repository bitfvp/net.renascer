<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_9"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
//    $a="entradasave";
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vrm_lista");
    exit();
}
if (isset($_GET['id_p']) and is_numeric($_GET['id_p'])){
    $a="pesagemsave";
    $pesagem=fncgetpesagem($_GET['id_p']);
}else{
    $a="pesagemnew";
}
$romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-4">
            <?php
            include_once("includes/rm_cab.php");
            ?>
            <div class="btn-group" role="group" aria-label="">
                <?php if ($allow["allow_6"]==1){?>
                    <a href="index.php?pg=Vrm_editar&id=<?php echo $_GET['id_e']; ?>" title="Editar entrada" class="btn btn-sm btn-primary fas fa-pen text-dark">
                        EDITAR ROMANEIO
                    </a>
                <?php } ?>

            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-cadastro-heading">Cadastro de pesagens - parte 1</h3>
                    <hr>
                    <form class="form-signin" action="<?php echo "index.php?pg=Vrm&id_e={$_GET['id_e']}&aca={$a}"; ?>" method="post" id="formx">
                        <div class="row">
                            <div class="col-md-4">
                                <input id="id_p" type="hidden" class="txt bradius" name="id_p" value="<?php echo $pesagem['id']; ?>"/>
                                <label for="produto">PRODUTO</label>
                                <input autocomplete="off" autofocus id="produto" type="text" class="form-control text-uppercase" name="produto" value="<?php echo $pesagem['produto']; ?>" placeholder="" required />
                            </div>
                            <div class="col-md-3">
                                <label for="embalagem">EMBALAGEM</label>
                                <select name="embalagem" id="embalagem" class="form-control" required>
                                    <option selected="" value="<?php if ($pesagem['embalagem'] == "") {
                                        echo "";
                                    } else {
                                        echo $pesagem['embalagem'];
                                    } ?>">
                                        <?php
                                        if ($pesagem['embalagem'] == 0) {
                                            echo "Selecione...";
                                        }
                                        if ($pesagem['embalagem'] == 1) {
                                            echo "SACARIA";
                                        }
                                        if ($pesagem['embalagem'] == 2) {
                                            echo "BIG BAGS";
                                        }
                                        if ($pesagem['embalagem'] == 3) {
                                            echo "GRANEL";
                                        }
                                        ?>
                                    </option>
                                    <option value="1">SACARIA</option>
                                    <option value="2">BIG BAGS</option>
                                    <option value="3">GRANEL</option>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <label for="peso_entrada">PESO INICIAL</label>

                                <div class="input-group">
                                    <input autocomplete="off" id="peso_entrada" placeholder="insira o peso inicial" type="text" class="form-control" name="peso_entrada" value="<?php echo $pesagem['peso_entrada']; ?>" required />
                                    <div class="input-group-append">
                                        <span class="input-group-text">,000 Kg</span>
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $('#peso_entrada').mask('000.000', {reverse: true});
                                    });
                                </script>
                            </div>
                            <div class="col-md-12">
                                <label for="obs_entrada">OBSERVAÇÃO DE ENTRADA</label>
                                <textarea id="obs_entrada" onkeyup="limite_textarea(this.value,250,obs_entrada,'cont')" maxlength="250" class="form-control" rows="2" name="obs_entrada"><?php echo $pesagem['obs_entrada']; ?></textarea>
                                <span id="cont">250</span>/250
                            </div>

                            <div class="col-md-12">
                                <input type="submit" name="gogo" id="gogo" class="btn btn-success btn-block my-2" value="SALVAR">
                            </div>
                            <script>
                                var formID = document.getElementById("formx");
                                var send = $("#gogo");

                                $(formID).submit(function(event){
                                    if (formID.checkValidity()) {
                                        send.attr('disabled', 'disabled');
                                        send.attr('value', 'AGUARDE...');
                                    }
                                });
                            </script>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>