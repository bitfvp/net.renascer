<?php
class Lotesaida{
    public function fnclotesaidainsert($produto_id, $nota_id, $tipo, $armazem_id, $obs, $quantidade, $valor, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO ren_fz_lotes ";
            $sql.="(id, produto_id, nota_id, tipo, armazem_id, obs, quantidade, valor, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :produto_id, :nota_id, :tipo, :armazem_id, :obs, :quantidade, :valor, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":produto_id", $produto_id);
            $insere->bindValue(":nota_id", $nota_id);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":armazem_id", $armazem_id);
            $insere->bindValue(":obs", $obs);
            $insere->bindValue(":quantidade", $quantidade);
            $insere->bindValue(":valor", $valor);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){

/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao


////////////////////////////////////////////////////
    public function fnclotesaidadelete($tabela_id,$usuario_off,$nota_id ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ren_fz_lotes ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $tabela_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ren_fz_lotes ";
                $sql.="SET ";
                $sql .= "status=0,
usuario_off=:usuario_off,
data_off=CURRENT_TIMESTAMP
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":usuario_off", $usuario_off);
                $atualiza->bindValue(":id", $tabela_id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }


            $sql = "UPDATE `ren_fz_contaestoque` SET `need_recount` = '1' WHERE `ren_fz_contaestoque`.`id` = 1;";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
            $sql = null;
            $consulta = null;



        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse registro cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Desativada Com Sucesso!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vsaida&id={$nota_id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

}//fim da classe