<nav id="navbar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-<?php echo($_SESSION['theme']==1)?"dark":"primary";?> mb-2">
    <nav class='container'>
        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" style="height: 50px;" src="<?php echo $env->env_estatico; ?>img/vetor_simples.png" alt="<?php echo $env->env_nome; ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navcad" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        CADASTROS
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navcad">
                        <a class="dropdown-item" href="index.php?pg=Vpessoa_lista" target="">PESSOAS</a>
                        <a class="dropdown-item" href="index.php?pg=Vunidade_lista" target="">UNIDADES</a>
                        <a class="dropdown-item" href="index.php?pg=Vcategoria_lista" target="">CATEGORIAS</a>
                        <a class="dropdown-item" href="index.php?pg=Vproduto_lista" target="">PRODUTOS</a>
                        <a class="dropdown-item" href="index.php?pg=Varmazem_lista" target="">ARMAZENS</a>
                        <a class="dropdown-item" href="index.php?pg=Vdestino_lista" target="">DESTINOS</a>

                        <div class="dropdown-divider"></div>
                    </div>
                </li>


                <a class="nav-item nav-link " href="index.php?pg=Ventrada_lista">ENTRADAS</a>
                <a class="nav-item nav-link " href="index.php?pg=Vsaida_lista">SAÍDAS</a>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navrel" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        RELATÓRIOS
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navrel">
<!--                        <a class="dropdown-item" href="index.php?pg=Ventrada_rel" target="">ENTRADA</a>-->
                        <a class="dropdown-item" href="index.php?pg=Vconsumo_rel" target="">CONSUMO</a>
                        <a class="dropdown-item" href="index.php?pg=Vestoque_rel" target="">ESTOQUE</a>

                        <div class="dropdown-divider"></div>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navuti" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        UTILITÁRIOS
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navuti">
                        <a class="dropdown-item" href="index.php?pg=Vhome&aca=estoque_recontar" target="">Recontagem de estoque</a>

                        <div class="dropdown-divider"></div>
                    </div>
                </li>



            </ul>

            <?php
            include_once("{$env->env_root}includes/ren/modal_msg_pontos.php");
            ?>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="msgoff()" data-toggle="modal" data-target="#modalmsgs">
                        <?php
                        echo $comp_msg;
                        ?>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modaltemas">
                            <i class="fa fa-tint"></i>
                            Alterar tema
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?PHP echo $env->env_url; ?>?pg=Vlogin"><i class="fa fa-undo"></i> Voltar</a>
                        <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a>
                    </div>
                </li>
            </ul>
        </div>


    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    </nav>
</nav>




<?php
if ($need_recount==1){
    echo "<div class='recontagem text-center text-dark bg-warning'>";
    echo "<h4>Foi feito uma ação de exclusão de lançamentos e será necessario fazer uma <a href='index.php?pg=Vhome&aca=estoque_recontar' class='btn btn-danger my-1'>recontagem geral de estoque</a></h4>";
    echo "</div>";
}


include_once("{$env->env_root}includes/sessao_relogio.php");