<div class="card">
    <div class="card-header bg-info text-light">
        Dados da nota
    </div>
    <div class="card-body">
        Identificador:<strong class="text-info"><?php echo $entrada['identificador']; ?>&nbsp;&nbsp;</strong><br>
        Data:<strong class="text-info"><?php echo datahoraBanco2data($entrada['data_ts']); ?>&nbsp;&nbsp;</strong><br>
        Fornecedor dos produtos:
        <strong class="text-info">
            <?php
            if ($entrada['fornecedor'] != "0") {
                echo fncgetpessoa($entrada['fornecedor'])['nome'];
            } else {
                echo "???????";
            }
            ?>
        </strong>&nbsp;&nbsp;<br>
        Armazém destino:
        <strong class="text-info">
            <?php
            if ($entrada['armazem'] != "0") {
                echo fncgetarmazem($entrada['armazem'])['armazem'];
            } else {
                echo "???????";
            }
            ?>
        </strong>&nbsp;&nbsp;<br>
        <hr>
        Responsável pelo cadastro:<br>
        <strong class="text-info"><?php
            if ($entrada['usuario'] != "0") {
                echo fncgetusuario($entrada['usuario'])['nome'];
            } else {
                echo "???????";
            }
            ?>
        </strong>&nbsp;&nbsp;
        <a href="index.php?pg=Ventrada_editar&id=<?php echo $_GET['id']; ?>" title="Edite nota" class="btn btn-success btn-block mt-4">
            EDITAR NOTA
        </a>
        <a href="index.php?pg=Ventrada_print1&id=<?php echo $_GET['id']; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-dark btn-block fas fa-print">
            <br>IMPRIMIR ENTRADA
        </a>
    </div>
</div>