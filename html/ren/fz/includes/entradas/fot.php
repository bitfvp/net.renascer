<?php
            $sql = "SELECT * \n"
                . "FROM ren_fz_lotes \n"
                . "WHERE (((ren_fz_lotes.nota_id)=?) AND ((ren_fz_lotes.tipo)=1) AND ((ren_fz_lotes.status)=1))\n"
                . "ORDER BY ren_fz_lotes.data_ts DESC";
            global $pdo;
            $cons = $pdo->prepare($sql);
            $cons->bindParam(1, $_GET['id']);
            $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pedlista = $cons->fetchAll();
            $sql = null;
            $consulta = null;
?>
<!-- tabela -->
<table id="tabela" class="table table-hover table-sm mt-1">
    <thead class="bg-info">
    <tr>
        <th>PRODUTO</th>
        <th>QUANTIDADE</th>
        <th>ARMAZÉM</th>
        <th>VALOR</th>
        <th>RESPONSÁVEL</th>
        <th>DATA</th>
        <th>AÇÕES</th>
    </tr>
    </thead>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <tbody class="bg-success">
    <?php
    // vamos criar a visualização
    foreach ($pedlista as $dados) {
        $id = $dados["id"];
        $produto = fncgetproduto($dados["produto_id"])['produto'];
        $quantidade = floatval($dados["quantidade"]);
        $destino = fncgetarmazem($dados["armazem_id"])['armazem'];
        $valor = $dados["valor"];
        $responsavel =explode(' ',fncgetusuario($dados['usuario'])['nome']);
        $data_ts = dataRetiraHora($dados['data_ts']);
        ?>

        <tr>
            <td><?php
                echo strtoupper($produto);
                ?>
            </td>
            <td><?php echo $quantidade; ?></td>
            <td><?php echo strtoupper($destino); ?></td>
            <td>R$<?php echo number_format($valor,2,',','.');?></td>
            <td><?php echo $responsavel[0]; ?></td>
            <td><?php echo $data_ts; ?></td>
            <td>
                <?php
                if ($allow["allow_16"]==1){?>
                    <div class="dropdown show">
                        <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-trash"><br>EXCLUIR</i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="#">Não</a>
                            <a class="dropdown-item bg-danger" href="index.php?pg=Ventrada&aca=loteentradadelete&tabela_id=<?php echo $id; ?>&id=<?php echo $_GET['id']; ?>">Apagar</a>
                        </div>
                    </div>
                <?php }else{
                    echo "<i class='fas fa-ban'></i>";
                }
                ?>

            </td>
        </tr>

        <?php
    }
    ?>
    </tbody>
</table>