<div class="card">
    <div class="card-header bg-info text-light">
        Formulário de entrada de itens
    </div>
    <div class="card-body">
        <form class="form-signin" action="index.php?pg=Ventrada&id=<?php echo $_GET['id']; ?>&aca=loteentradainsert" method="post">
            <div class="row">

                <div class="col-md-12">
                    <label for="produto_id">Produtssso:</label>
                    <select name="produto_id" autofocus="yes" id="produto_id" class="form-control input-sm selectpicker" data-live-search="true"  data-style="btn-warning">
                        // vamos criar a visualização
                        <?php
                        foreach (fncprodutolist() as $item) {
                            ?>
                            <option data-tokens="<?php echo strtoupper($item['produto']); ?>" value="<?php echo $item['id']; ?>"><?php echo $item['produto']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-12">
                    <input type="hidden" name="nota_id" id="nota_id" class="form-control" autocomplete="off" placeholder="" value="<?php echo $_GET['id']; ?>"/>
                </div>
                <div class="col-md-12">
                    <input type="hidden" name="armazem_id" id="armazem_id" class="form-control" autocomplete="off" placeholder="" value="<?php echo $entrada['armazem']; ?>"/>
                </div>

                <div class="col-md-12">
                    <label for="obs">Observação:</label>
                    <input type="text" name="obs" id="obs" class="form-control" autocomplete="off" placeholder="" value=""/>
                </div>
                <div class="col-md-12">
                    <label for="quantidade">Quantidade: <i class="text-danger">use vírgula para frações</i></label>
                    <input type="number" name="quantidade" id="quantidade" required class="form-control" autocomplete="off" value="0" step="0.100" min="0.1" />
                </div>
                <div class="col-md-12">
                    <label for="valor">Valor: <i class="text-info"></i></label>
                    <input id="valor" type="text" class="form-control" autocomplete="off" name="valor" value="" required />
                </div>
                <script>
                    $(document).ready(function(){
                        $('#valor').mask('000.000.000,00', {reverse: true});
                    });
                </script>



                <div class="col-md-12 d-grid">
                    <input type="submit" value="SALVAR" class="btn btn-success my-2 btn-block" />
                </div>

            </div>
        </form>
    </div>
</div>