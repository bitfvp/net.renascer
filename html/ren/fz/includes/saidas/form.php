<div class="card">
    <div class="card-header bg-info text-light">
        Formulário de saída de itens
    </div>
    <div class="card-body">
        <form class="form-signin" action="index.php" method="get">
            <div class="row">
                <input name="pg" value="Vsaida2" hidden/>
                <input name="id" value="<?php echo $_GET['id']; ?>" hidden/>

                <div class="col-md-12">
                    <label for="produto_id">Produto:</label>
                    <select name="produto_id" autofocus="yes" id="produto_id" class="form-control input-sm selectpicker" data-live-search="true"  data-style="btn-warning">
                        // vamos criar a visualização
                        <?php
                        foreach (fncprodutolist() as $item) {
                            ?>
                            <option data-tokens="<?php echo $item['produto']; ?>" value="<?php echo $item['id']; ?>"><?php echo $item['produto']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>


                <div class="col-md-12 d-grid">
                    <input type="submit" value="AVANÇAR" class="btn btn-success my-2 btn-block" />
                </div>

            </div>
        </form>
    </div>
</div>