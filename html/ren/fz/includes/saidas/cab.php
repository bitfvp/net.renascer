<div class="card">
    <div class="card-header bg-info text-light">
        Dados da nota
    </div>
    <div class="card-body">
        Identificador:<strong class="text-info"><?php echo $saida['identificador']; ?>&nbsp;&nbsp;</strong><br>
        Data:<strong class="text-info"><?php echo datahoraBanco2data($saida['data_ts']); ?>&nbsp;&nbsp;</strong><br>
        Destino:
        <strong class="text-info">
            <?php
            if ($saida['destino'] != "0") {
                echo fncgetdestino($saida['destino'])['destino'];
            } else {
                echo "???????";
            }
            ?>
        </strong>&nbsp;&nbsp;<br>
        <hr>
        Responsável pelo cadastro:<br>
        <strong class="text-info"><?php
            if ($saida['usuario'] != "0") {
                echo fncgetusuario($saida['usuario'])['nome'];
            } else {
                echo "???????";
            }
            ?>
        </strong>&nbsp;&nbsp;
        <a href="index.php?pg=Vsaida_editar&id=<?php echo $_GET['id']; ?>" title="Edite nota" class="btn btn-success btn-block mt-4">
            EDITAR NOTA
        </a>
        <a href="index.php?pg=Vsaida_print1&id=<?php echo $_GET['id']; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-dark btn-block fas fa-print">
            <br>IMPRIMIR SAÍDA
        </a>
    </div>
</div>