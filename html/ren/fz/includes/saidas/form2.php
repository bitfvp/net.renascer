<div class="card">
    <div class="card-header bg-info text-light">
        Formulário de saída de itens
    </div>
    <div class="card-body">
        <form class="form-signin" action="index.php?pg=Vsaida&id=<?php echo $_GET['id']; ?>&aca=lotesaidainsert" method="post">
            <div class="row">

                <?php
                $prod = fncgetproduto($_GET['produto_id']);
                ?>

                <div class="col-md-12">
                    <h2 for="produto_id">
                        <span class="badge badge-warning"><?php echo $prod['produto'];?></span>
                        <span class="badge badge-warning"><?php echo "  R$" . $prod['valor'];?></span>
                    </h2>

                </div>
                <input type="hidden" name="produto_id" id="produto_id" class="form-control" autocomplete="off"  value="<?php echo $_GET['produto_id']; ?>"/>
                <input type="hidden" name="valor" id="valor" class="form-control" autocomplete="off" placeholder="" value="<?php echo $prod['valor']; ?>"/>
                    <input type="hidden" name="nota_id" id="nota_id" class="form-control" autocomplete="off" placeholder="" value="<?php echo $_GET['id']; ?>"/>


                <div class="col-md-12">
                    <label for="obs">Observação:</label>
                    <input type="text" name="obs" id="obs" class="form-control" autocomplete="off" placeholder="" value=""/>
                </div>


                <?php
                $sql = "SELECT ren_fz_estoque.armazem_id as armazem, ren_fz_estoque.quantidade as quantidade FROM ren_fz_estoque WHERE produto_id=? and ren_fz_estoque.quantidade>0 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $_GET['produto_id']);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $estoque_armazens = $consulta->fetchAll();
                $estoque_armazens_count = $consulta->rowCount();
                $sql = null;
                $consulta = null;

                if ($estoque_armazens_count>0){
                    foreach ($estoque_armazens as $estoque_armazem){?>
                        <div class="col-md-6">
                            <label for="quantidade_<?php echo $estoque_armazem['armazem'];?>">quantidade: <i class="text-danger">use vírgula para frações</i></label>
                            <input type="number" name="quantidade_<?php echo $estoque_armazem['armazem'];?>" id="quantidade_<?php echo $estoque_armazem['armazem'];?>" class="form-control" autocomplete="off" value="0" step="0.100" min="0" max="<?php echo floatval($estoque_armazem['quantidade']);?>"/>
                        </div>

                        <div class="col-md-6">
                            <label for="quantidade">Disponível em: <?php echo fncgetarmazem($estoque_armazem['armazem'])['armazem'];?></label>
                            <input type="number" tabindex="-1" readonly="readonly" name="info" id="info" required class="form-control" autocomplete="off" value="<?php echo floatval($estoque_armazem['quantidade']);?>" step="0.100" />
                        </div>

                    <?php }
                }else{
                    echo "<div class='col-md-12'>";
                    echo "<h3 class='text-danger mt-2'>NÃO HÁ QUANTIDADES DESSE PRODUTO EM NENHUM ARMAZÉM</h3>";
                    echo "</div>";
                } ?>


                <div class="col-md-12 d-grid">
                    <input type="submit" value="SALVAR" class="btn btn-success my-2 btn-block" />
                </div>

                <div class="col-md-12 d-grid">
                    <a href="index.php?pg=Vsaida&id=<?php echo $_GET['id']; ?>" class="btn btn-primary my-2 btn-block">VOLTAR</a>
                </div>

            </div>
        </form>
    </div>
</div>