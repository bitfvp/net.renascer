<?php
            $sql = "SELECT * \n"
                . "FROM ren_fz_lotes \n"
                . "WHERE (((ren_fz_lotes.produto_id)=?) AND ((ren_fz_lotes.status)=1))\n"
                . "ORDER BY ren_fz_lotes.data_ts DESC";
            global $pdo;
            $cons = $pdo->prepare($sql);
            $cons->bindParam(1, $_GET['id']);
            $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
            $pedlista = $cons->fetchAll();
            $sql = null;
            $consulta = null;
?>
<!-- tabela -->
<table id="tabela" class="table table-hover table-sm">
    <thead class="bg-info">
    <tr>
<!--        <th>Produto</th>-->
        <th>QUANTIDADE</th>
        <th>ARMAZÉM</th>
        <th>DESTINO</th>
        <th>VALOR</th>
        <th>RESPONSÁVEL</th>
        <th>DATA</th>
    </tr>
    </thead>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <tbody class="">
    <?php
    // vamos criar a visualização
    foreach ($pedlista as $dados) {
        $id = $dados["id"];
        $produto = fncgetproduto($dados["produto_id"])['produto'];
        $quantidade = floatval($dados["quantidade"]);
        $armazem = fncgetarmazem($dados["armazem_id"])['armazem'];

        $valor = $dados["valor"];
        $responsavel =explode(' ',fncgetusuario($dados['usuario'])['nome']);
        $data_ts = dataRetiraHora($dados['data_ts']);

        if ($dados["tipo"]==1){
            $cor="bg-success";
            $destino = "";
        }
        if ($dados["tipo"]==2){
            $cor="bg-danger";
            $nota_saida = fncgetsaida($dados["nota_id"]);
            $destino = fncgetdestino($nota_saida["destino"])['destino'];
        }
        ?>

        <tr class="<?php echo $cor; ?>">
<!--            <td>--><?php
//                echo strtoupper($produto);
//                ?>
<!--            </td>-->
            <td><?php echo $quantidade; ?></td>
            <td><?php echo strtoupper($armazem); ?></td>
            <td><?php echo strtoupper($destino); ?></td>
            <td>R$<?php echo number_format($valor,2,',','.');?></td>
            <td><?php echo $responsavel[0]; ?></td>
            <td><?php echo $data_ts; ?></td>
        </tr>

        <?php
    }
    ?>
    </tbody>
</table>