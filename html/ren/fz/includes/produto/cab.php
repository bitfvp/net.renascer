<div class="card">
    <div class="card-header bg-info text-light">
        Dados da do produto
    </div>
    <div class="card-body">
        Produto:<strong class="text-info"><?php echo $produto['produto']; ?>&nbsp;&nbsp;</strong><br>
        Data:<strong class="text-info"><?php echo datahoraBanco2data($produto['data_ts']); ?>&nbsp;&nbsp;</strong><br>
        Unidade:
        <strong class="text-info">
            <?php
            if ($produto['unidade'] != "0") {
                echo fncgetunidade($produto['unidade'])['unidade'];
            } else {
                echo "???????";
            }
            ?>
        </strong>&nbsp;&nbsp;<br>
        Categoria:
        <strong class="text-info">
            <?php
            if ($produto['categoria'] != "0") {
                echo fncgetcategoria($produto['categoria'])['categoria'];
            } else {
                echo "???????";
            }
            ?>
        </strong>&nbsp;&nbsp;<br>
        Estoque minimo:<strong class="text-info"><?php echo floatval($produto['estoque_min']); ?></strong><br>
        Custo médio:<strong class="text-info">R$<?php echo $produto['valor']; ?></strong>
        <a href="index.php?pg=Vproduto&id=<?php echo $_GET['id']; ?>&aca=recalcularcusto" title="Edite nota" class="btn btn-sm btn-info">
            Recalcular custo
        </a>
        <br>
        <hr>
        Responsável pelo cadastro:<br>
        <strong class="text-info"><?php
            if ($produto['usuario'] != "0") {
                echo fncgetusuario($produto['usuario'])['nome'];
            } else {
                echo "???????";
            }
            ?>
        </strong>&nbsp;&nbsp;

        <a href="index.php?pg=Vproduto_editar&id=<?php echo $_GET['id']; ?>" title="Edite nota" class="btn btn-success btn-block mt-4">
            Editar Produto
        </a>
    </div>
</div>