<?php
                $sql = "SELECT ren_fz_estoque.armazem_id as armazem, ren_fz_estoque.quantidade as quantidade FROM ren_fz_estoque WHERE produto_id=? and ren_fz_estoque.quantidade>0 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $_GET['id']);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $estoques = $consulta->fetchAll();
                $estoques_count = $consulta->rowCount();
                $sql = null;
                $consulta = null;
?>
<!-- tabela -->
<table id="tabela" class="table table-hover table-sm">
    <thead class="bg-info">
    <tr>
        <th>ARMAZÉM</th>
        <th>QUANTIDADE</th>
    </tr>
    </thead>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <tbody>
    <?php
    // vamos criar a visualização
    $contagem=0;
    foreach ($estoques as $dados) {
        $quantidade = floatval($dados["quantidade"]);
        $armazem = fncgetarmazem($dados["armazem"])['armazem'];

        $contagem+=$dados["quantidade"];
        ?>

        <tr class="bg-success">
            <td><?php echo strtoupper($armazem); ?></td>
            <td><?php echo $quantidade." ".fncgetunidade($produto['unidade'])['unidade']; ?>(s)</td>
        </tr>

        <?php
    }
    ?>

    <tr class="bg-dark text-white">
        <td>TOTAL</td>
        <td><?php echo floatval($contagem)." ".fncgetunidade($produto['unidade'])['unidade']; ?>(s)</td>
    </tr>

    </tbody>
</table>