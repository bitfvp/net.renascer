<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar entrada-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="entradaupdate";
    $entrada=fncgetentrada($_GET['id']);
}else{
    $a="entradainsert";
}
?>
<main class="container">
    <form class="form-signin" action="<?php echo "index.php?pg=Ventrada_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de entrada</h3>
        <hr>
        <div class="row">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $entrada['id']; ?>"/>
            <div class="col-md-12">
                <label for="identificador">identificador:</label>
                <input type="text" name="identificador" id="identificador" class="form-control" autocomplete="off" placeholder="" value="<?php echo $entrada['identificador']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="fornecedor">fornecedor:</label>
                <select name="fornecedor" required="true" id="fornecedor" class="form-control"  required="true">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $entrada['fornecedor'];?>">
                        <?php
                        $fornecedor=fncgetpessoa($entrada['fornecedor']);
                        echo $fornecedor['nome'];
                        ?>
                    </option>
                    <?php
                    foreach(fncpessoalist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-12">
                <label for="armazem">armazem:</label>
                <select name="armazem" required="true" id="armazem" class="form-control"  required="true">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $entrada['armazem'];?>">
                        <?php
                        $armazem=fncgetarmazem($entrada['armazem']);
                        echo $armazem['armazem'];
                        ?>
                    </option>
                    <?php
                    foreach(fncarmazemlist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['armazem']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>