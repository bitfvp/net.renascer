<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar destino-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="destinoupdate";
    $destino=fncgetdestino($_GET['id']);
}else{
    $a="destinoinsert";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vdestino_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de destinos</h3>
        <hr>
        <div class="row">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $destino['id']; ?>"/>
            <div class="col-md-12">
                <label for="destino">Destino:</label>
                <input type="text" name="destino" id="destino" class="form-control" autocomplete="off" placeholder="" value="<?php echo $destino['destino']; ?>"/>
            </div>
            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>