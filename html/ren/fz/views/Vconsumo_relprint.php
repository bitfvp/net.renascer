<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de consumo-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";


if (isset($_POST['destino']) and is_numeric($_POST['destino']) and $_POST['destino']!=0){
    $destino=$_POST['destino'];
    $sql = "SELECT "
        ."ren_fz_lotes.id, "
        ."ren_fz_lotes.data_ts, "
        ."ren_fz_lotes.usuario, "
        ."ren_fz_lotes.produto_id, "
        ."ren_fz_lotes.tipo, "
        ."ren_fz_lotes.armazem_id, "
        ."ren_fz_lotes.obs, "
        ."ren_fz_lotes.quantidade, "
        ."ren_fz_lotes.valor, "
        ."ren_fz_saidas.destino "
        ."FROM "
        ."ren_fz_lotes INNER JOIN ren_fz_saidas ON ren_fz_saidas.id = ren_fz_lotes.nota_id "
        ."WHERE "
        ."ren_fz_lotes.`status` = 1 AND "
        ."ren_fz_lotes.tipo = 2 AND "
        ."ren_fz_saidas.destino = :destino "
        ."AND ren_fz_saidas.`data_ts` >= :inicial "
        ."AND ren_fz_saidas.`data_ts` <= :final "
        ."ORDER BY ren_fz_lotes.data_ts ASC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":destino",$destino);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $consumo = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}else{
    $sql = "SELECT "
        ."ren_fz_lotes.id, "
        ."ren_fz_lotes.data_ts, "
        ."ren_fz_lotes.usuario, "
        ."ren_fz_lotes.produto_id, "
        ."ren_fz_lotes.tipo, "
        ."ren_fz_lotes.armazem_id, "
        ."ren_fz_lotes.obs, "
        ."ren_fz_lotes.quantidade, "
        ."ren_fz_lotes.valor, "
        ."ren_fz_saidas.destino "
        ."FROM "
        ."ren_fz_lotes INNER JOIN ren_fz_saidas ON ren_fz_saidas.id = ren_fz_lotes.nota_id "
        ."WHERE "
        ."ren_fz_lotes.`status` = 1 AND "
        ."ren_fz_lotes.tipo = 2 "
        ."AND ren_fz_saidas.`data_ts` >= :inicial "
        ."AND ren_fz_saidas.`data_ts` <= :final "
        ."ORDER BY ren_fz_lotes.data_ts ASC";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $consumo = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}


?>
<div class="container-fluid">
    <h3>Relatório de consumo</h3>
    <h5>
        <?php
        if (isset($_POST['destino']) and is_numeric($_POST['destino']) and $_POST['destino']!=0){
            echo strtoupper(fncgetdestino($_POST['destino'])['destino']);
        }else{
            echo "Todos";
        }

        ?>
    </h5>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>

    <table class="table table-sm">
        <thead>
        <th>PRODUTO</th>
        <th>ORIGEM</th>
        <th>DESTINO</th>
        <th>DATA</th>
        <th>QUANTIDADE</th>
        <th>CUSTO</th>
        <th>TOTAL</th>
        </thead>
        <tbody>
        <?php
        $valor_total=0;
        foreach ($consumo as $con){
            $produto=fncgetproduto($con['produto_id']);
            $quantidade=floatval($con['quantidade']);
            $custo=$con['valor'];
            $total=$con['valor']*$con['quantidade'];
            $data_ts=datahoraBanco2data($con['data_ts']);
            $origem=fncgetarmazem($con['armazem_id']);
            $destino=fncgetdestino($con['destino']);

            $valor_total=$valor_total+$total;

            echo "<tr>";
            echo "<td>".strtoupper($produto['produto'])."</td>";
            echo "<td>".strtoupper($origem['armazem'])."</td>";
            echo "<td>".strtoupper($destino['destino'])."</td>";
            echo "<td>".$data_ts."</td>";
            echo "<td>".$quantidade."</td>";
            echo "<td>R$".number_format($custo,2,',','.')."</td>";
            echo "<td>R$".number_format($total,2,',','.')."</td>";
            echo "</tr>";
        }
        echo "<tr>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td></td>";
        echo "<td>R$".number_format($valor_total,2,',','.')."</td>";
        echo "</tr>";
        ?>
        </tbody>
    </table>


</div>
</html>