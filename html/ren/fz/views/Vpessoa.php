<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_15"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}

$page="Pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<main class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="container-fluid">
                <h3 class="ml-3">DADOS DO USUÁRIO</h3>
                <blockquote class="blockquote blockquote-info">
                    <header>
                        NOME:
                        <strong class="text-info"><?php echo strtoupper($pessoa['nome']); ?>&nbsp;&nbsp;</strong>
                    </header>
                    <h6>
                        CPF:
                        <strong class="text-info"><?php
                            if($pessoa['cpf']!="" and $pessoa['cpf']!=0) {
                                echo "<span class='text-info'>";
                                echo mask($pessoa['cpf'],'###.###.###-##');
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>
                        CNPJ:
                        <strong class="text-info"><?php
                            if($pessoa['cnpj']!="" and $pessoa['cnpj']!=0) {
                                echo "<span class='text-info'>";
                                echo mask($pessoa['cnpj'],'##.###.###/####-##');
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>
                        <hr>

                        ENDEREÇO:
                        <strong class="text-info"><?php
                            if($pessoa['endereco']!=""){
                                echo "<span class='azul'>";
                                echo $pessoa['endereco'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>&nbsp;&nbsp;

                        NÚMERO:
                        <strong class="text-info"><?php
                            if ($pessoa['numero']==0){
                                echo "<span class='text-info'>";
                                echo "s/n";
                                echo "</span>";
                            }else{
                                echo "<span class='text-info'>";
                                echo $pessoa['numero'];
                                echo "</span>";
                            }
                            ?></strong>&nbsp;&nbsp;

                        BAIRRO:
                        <strong class="text-info"><?php
                            if($pessoa['bairro']!="") {
                                echo "<span class='text-info'>";
                                echo $pessoa['bairro'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?>
                        </strong>&nbsp;

                        CIDADE:
                        <strong class="text-info"><?php
                            if($pessoa['cidade']!="") {
                                echo "<span class='text-info'>";
                                echo strtoupper($pessoa['cidade']);
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?>
                        </strong>&nbsp;

                        COMPLEMENTO:
                        <strong class="text-info"><?php
                            if($pessoa['complemento']!="") {
                                echo "<span class='text-info'>";
                                echo $pessoa['complemento'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?>
                        </strong>&nbsp;

                        TELEFONE:
                        <strong class="text-info"><?php
                            if($pessoa['telefone']!="") {
                                echo "<span class='text-info'>";
                                echo $pessoa['telefone'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>

                        E-MAIL:
                        <strong class="text-info"><?php
                            if($pessoa['email']!="") {
                                echo "<span class='text-info'>";
                                echo $pessoa['email'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>
                        <hr>
<?php
$perfil="";
if ($pessoa["p_motorista"]==1){
    $perfil .= "<i class='badge fas fa-truck text-danger'>Motorista</i>";
}
if ($pessoa["p_cliente"]==1){
    $perfil .= "<i class='badge fas fa-briefcase text-info'>Cliente</i>";
}
if ($pessoa["p_fornecedor"]==1){
    $perfil .= "<i class='badge fas fa-people-carry'>Fornecedor</i>";
}
if ($pessoa["p_corretor"]==1){
    $perfil .= "<i class='badge fas fa-comments-dollar'>Corretor</i>";
}
if ($pessoa["p_responsavel"]==1){
    $perfil .= "<i class='badge fas fa-user-lock text-warning'>Responsável</i>";
}
if ($pessoa["p_transportadora"]==1){
    $perfil .= "<i class='badge fas fa-shipping-fast d-block text-success'>Transportadora</i>";
}
?>&nbsp;&nbsp;

                        <strong>
                            <?php echo $perfil; ?>
                        </strong>
                    </h6>
                    <footer class="blockquote-footer">
                        Mantenha atualizado</strong>&nbsp;&nbsp;
                    </footer>

                </blockquote>
            </div>

        </div>
        <div class="col-md-4">
            <section class="sidebar-offcanvas" id="sidebar">
                <div class="list-group">
                    <a href="?pg=Vpessoa&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fas fa-home"></i>    PESSOA</a>
                    <a href="?pg=Vpessoa_l&id=<?php echo $_GET['id']; ?>" class="list-group-item"><i class="fas fa-boxes"></i>    LOTES</a>
                </div>
            </section>
            <script type="application/javascript">
                var offset = $('#sidebar').offset().top;
                var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance
                $(document).on('scroll', function () {
                    if (offset <= $(window).scrollTop()) {
                        $meuMenu.addClass('fixarmenu');
                    } else {
                        $meuMenu.removeClass('fixarmenu');
                    }
                });
            </script>
        </div>

    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>