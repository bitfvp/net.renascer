<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="produtoupdate";
    $produto=fncgetproduto($_GET['id']);
}else{
    $a="produtoinsert";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vproduto_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de produto</h3>
        <hr>
        <div class="row">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $produto['id']; ?>"/>
            <div class="col-md-12">
                <label for="produto">Produto:</label>
                <input type="text" name="produto" id="produto" class="form-control" autocomplete="off" placeholder="" value="<?php echo $produto['produto']; ?>" required="true" />
            </div>
            <div class="col-md-6">
                <label for="unidade">Unidade:</label>
                <select name="unidade" required="true" id="unidade" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $produto['unidade'];?>">
                        <?php
                        $unidade=fncgetunidade($produto['unidade']);
                        echo $unidade['unidade'];
                        ?>
                    </option>
                    <?php
                    foreach(fncunidadelist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['unidade']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-6">
                <label for="categoria">Categoria:</label>
                <select name="categoria" required="true" id="categoria" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $produto['categoria'];?>">
                        <?php
                        $categoria=fncgetcategoria($produto['categoria']);
                        echo $categoria['categoria'];
                        ?>
                    </option>
                    <?php
                    foreach(fnccategorialist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['categoria']; ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-12">
                <label for="estoque_min">Estoque mínimo:</label>
                <input required id="estoque_min" type="number" class="form-control" name="estoque_min" value="<?php echo $produto['estoque_min']; ?>" step="1"/>
            </div>

            <div class="col-md-12">
                <label for="valor">Custo: <i class="text-info">*Use em caso de correções</i></label>
                <input required id="valor" type="text" class="form-control" name="valor" value="<?php echo $produto['valor']; ?>"/>
            </div>
            <script>
                $(document).ready(function(){
                    $('#valor').mask('000.000.000,00', {reverse: true});
                });
            </script>

            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>