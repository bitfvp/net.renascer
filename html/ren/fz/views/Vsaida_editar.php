<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar saída-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="saidaupdate";
    $saida=fncgetsaida($_GET['id']);
}else{
    $a="saidainsert";
}
?>
<main class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vsaida_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de saída</h3>
        <hr>
        <div class="row">
            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $saida['id']; ?>"/>
            <div class="col-md-12">
                <label for="identificador">identificador:</label>
                <input type="text" name="identificador" id="identificador" class="form-control" autocomplete="off" placeholder="" value="<?php echo $saida['identificador']; ?>"/>
            </div>
            <div class="col-md-12">
                <label for="destino">destino:</label>
                <select name="destino" required="true" id="destino" class="form-control"  required="true">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $saida['destino'];?>">
                        <?php
                        $destino=fncgetdestino($saida['destino']);
                        echo $destino['destino'];
                        ?>
                    </option>
                    <?php
                    foreach(fncdestinolist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['destino']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>