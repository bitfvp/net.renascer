<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes

    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $entrada=fncgetentrada($_GET['id']);
}else{
    header("Location: index.php");
    exit();
}
?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
            <div class="col-8">
                <h4 class="text-uppercase">NOTA DE ENTRADA DE PRODUTOS PARA ARMAZENAMENTO</h4>
                <h5>IDENTIFICADOR: <strong><?php echo strtoupper($entrada['identificador']); ?></strong></h5>
                <h5>FORNECEDOR DOS PRODUTOS: <strong><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?></strong></h5>
                <h5>ARMAZÉM: <strong><?php echo strtoupper(fncgetarmazem($entrada['armazem'])['armazem']); ?></strong></h5>
                <h5>DATA: <strong><?php echo datahoraBanco2data($entrada['data_ts']); ?></strong></h5>
            </div>
            <div class="col-4 text-right">
            </div>
    </div>

    <?php
    $sql = "SELECT * \n"
        . "FROM ren_fz_lotes \n"
        . "WHERE (((ren_fz_lotes.nota_id)=?) AND ((ren_fz_lotes.tipo)=1) AND ((ren_fz_lotes.status)=1))\n"
        . "ORDER BY ren_fz_lotes.data_ts DESC";
    global $pdo;
    $cons = $pdo->prepare($sql);
    $cons->bindParam(1, $_GET['id']);
    $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pedlista = $cons->fetchAll();
    $sql = null;
    $consulta = null;
    ?>


        <hr class="hrgrosso">
        <h3>INFORMAÇÕES</h3>

    <div class="row">
        <div class="col-3 border">
            PRODUTO
        </div>
        <div class="col-3 border">
            ESTOQUE
        </div>
        <div class="col-2 border">
            QUANTIDADE
        </div>
        <div class="col-4 border">
            VALORES
        </div>
    </div>

    <?php
    // vamos criar a visualização
    foreach ($pedlista as $dados) {
        $id = $dados["id"];
        $produto = fncgetproduto($dados["produto_id"]);
        $quantidade = floatval($dados["quantidade"]);
        $destino = fncgetarmazem($dados["armazem_id"])['armazem'];
        $valor = $dados["valor"];
        $responsavel = fncgetusuario($dados['usuario'])['nome'];
        $total=$valor*$quantidade;
        ?>

    <div class="row">
        <div class="col-3 border">
            <?php echo $produto['produto']; ?>
        </div>
        <div class="col-3 border">
            <?php echo $destino; ?>
        </div>
        <div class="col-2 border">
            <?php echo $quantidade; ?>
        </div>
        <div class="col-4 border">
            R$<?php echo number_format($valor,2,',','.');?>
            <strong class="float-right">
                R$<?php echo number_format($total,2,',','.');?>
            </strong>
        </div>
    </div>

        <?php
    }
    ?>



    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
        <div class="col-12">
            <strong> Concordo com os dados prescritos acima e dou fé.</strong>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Operador: <strong><?php echo fncgetusuario($entrada['usuario'])['nome']; ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print()
</SCRIPT>