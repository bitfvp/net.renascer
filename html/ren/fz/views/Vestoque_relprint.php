<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de estoque-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");


$sql = "SELECT * FROM ren_fz_produtos ORDER BY `produto` ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$produtos = $consulta->fetchAll();
$sql = null;
$consulta = null;






if (isset($_POST['armazem']) and is_numeric($_POST['armazem']) and $_POST['armazem']!=0){
}else{
}


?>
<div class="container-fluid">
    <h3>Relatório de estoque</h3>
    <h5>
        <?php
        if (isset($_POST['armazem']) and is_numeric($_POST['armazem']) and $_POST['armazem']!=0){
            echo strtoupper(fncgetarmazem($_POST['armazem'])['armazem']);
        }else{
            echo "Todos";
        }

        ?>
    </h5>

    <table id="tabela" class="table table-hover table-sm">
        <thead class="bg-dark text-white">
        <tr>
            <th>Produto</th>
            <th>Armazém</th>
            <th>Quantidade</th>
            <th>Total</th>
        </tr>
        </thead>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <tbody>
        <?php

        foreach ($produtos as $produto){
            if (isset($_POST['armazem']) and is_numeric($_POST['armazem']) and $_POST['armazem']!=0){
                //se selecionar armazem
                $sql = "SELECT ren_fz_estoque.armazem_id as armazem, ren_fz_estoque.quantidade as quantidade"
                    ." FROM ren_fz_estoque "
                    ."WHERE produto_id=? and ren_fz_estoque.quantidade>0 AND ren_fz_estoque.armazem_id=? ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $produto['id']);
                $consulta->bindParam(2, $_POST['armazem']);
                $consulta->execute();
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $estoques = $consulta->fetchAll();
                $estoques_count = $consulta->rowCount();
                $sql = null;
                $consulta = null;
            }else{
                //se não selecionar armazem
                $sql = "SELECT ren_fz_estoque.armazem_id as armazem, ren_fz_estoque.quantidade as quantidade"
                    ." FROM ren_fz_estoque "
                    ."WHERE produto_id=? and ren_fz_estoque.quantidade>0 ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $produto['id']);
                $consulta->execute();
                global $LQ;
                $LQ->fnclogquery($sql);
                $estoques = $consulta->fetchAll();
                $estoques_count = $consulta->rowCount();
                $sql = null;
                $consulta = null;
            }

            $contagem=0;
            foreach ($estoques as $dados) {
                $quantidade = floatval($dados["quantidade"]);
                $armazem = fncgetarmazem($dados["armazem"])['armazem'];

                $contagem+=$dados["quantidade"];
                echo "<tr class=''>";
                echo "<td>".strtoupper($produto['produto'])."</td>";
                echo "<td>".strtoupper($armazem)."</td>";
                echo "<td>".$quantidade." ".fncgetunidade($produto['unidade'])['unidade']."(s)</td>";
                echo "<td></td>";
                echo "</tr>";
            }
            if ($contagem>0){
                echo "<tr>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td></td>";
                echo "<td>".floatval($contagem)." ".fncgetunidade($produto['unidade'])['unidade']."(S)</td>";
                echo "</tr>";
            }

        }
        ?>



        </tbody>
    </table>


</div>
</html>