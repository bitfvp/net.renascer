<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar entrada-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $saida=fncgetsaida($_GET['id']);
}else{
    header("Location: index.php");
    exit();
}
?>
<main class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-4">
            <?php include_once("includes/saidas/cab.php");?>
        </div>

        <div class="col-md-8">
            <?php include_once("includes/saidas/form2.php");?>
        </div>

        <div class="col-md-12">
            <?php include_once("includes/saidas/fot.php");?>
        </div>
    </div>


</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>