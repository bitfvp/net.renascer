<?php
//gerado pelo geracode
function fncdestinolist(){
    $sql = "SELECT * FROM ren_fz_destinos ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $destinolista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $destinolista;
}

function fncgetdestino($id){
    $sql = "SELECT * FROM ren_fz_destinos WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getren_fz_destinos = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getren_fz_destinos;
}
?>