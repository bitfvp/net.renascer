<?php
//gerado pelo geracode
function fncarmazemlist(){
    $sql = "SELECT * FROM ren_fz_armazens ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $armazemlista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $armazemlista;
}

function fncgetarmazem($id){
    $sql = "SELECT * FROM ren_fz_armazens WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getren_fz_armazens = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getren_fz_armazens;
}
?>