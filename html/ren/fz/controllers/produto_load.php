<?php
//gerado pelo geracode
function fncprodutolist(){
    $sql = "SELECT * FROM ren_fz_produtos ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $produtolista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $produtolista;
}

function fncgetproduto($id){
    $sql = "SELECT * FROM ren_fz_produtos WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getren_fz_produtos = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getren_fz_produtos;
}
?>