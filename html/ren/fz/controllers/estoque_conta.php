<?php
if ($startactiona == 1 && $aca == "estoque_recontar") {
    $sql = "UPDATE `ren_fz_contaestoque` SET `limpa` = '1' WHERE `ren_fz_contaestoque`.`id` = 1 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $consulta = null;

    header("Location: index.php?pg=Vhome");
    exit();
}




$sql = "SELECT * FROM `ren_fz_contaestoque` WHERE id=1";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$contaestoque = $consulta->fetch();
$sql = null;
$consulta = null;


if ($contaestoque['limpa']==1){

    //limpa estoque
    $sql = "TRUNCATE TABLE `ren_fz_estoque`";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $consulta = null;


//    reinicia as variaveis controladoras
    $contaestoqueidunico = uniqid();
    $sql = "UPDATE `ren_fz_contaestoque` SET `codigo` = '{$contaestoqueidunico}', `limpa` = '0', `need_recount` = '0' WHERE `ren_fz_contaestoque`.`id` = 1;";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $consulta = null;

//pega novamente as controladoras
    $sql = "SELECT * FROM `ren_fz_contaestoque` WHERE id=1";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $contaestoque = $consulta->fetch();
    $sql = null;
    $consulta = null;

    $_SESSION['fsh']=[
        "flash"=>"Processo de verificação de estoque",
        "type"=>"success",
    ];
    header("Location: index.php?pg=Vhome");
    exit();

}







//seleciona produto e armazem pra inserir em estoque
$sql = "SELECT ren_fz_lotes.produto_id, ren_fz_lotes.armazem_id FROM ren_fz_lotes WHERE codigo_contagem <> '{$contaestoque['codigo']}' GROUP BY ren_fz_lotes.produto_id, ren_fz_lotes.armazem_id";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$lotes_estoque_insert = $consulta->fetchAll();
$sql = null;
$consulta = null;


//insere em estoque se não tiver
foreach ($lotes_estoque_insert as $lote){
    $sql = "SELECT id FROM ren_fz_estoque WHERE `produto_id` = '{$lote['produto_id']}' and `armazem_id` = '{$lote['armazem_id']}' ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $lotesestoque_cont = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    if ($lotesestoque_cont>0){
        //ja existe
    }else{
        $sql = "INSERT INTO `ren_fz_estoque` (`id`, `produto_id`, `armazem_id`) VALUES ( NULL, '{$lote['produto_id']}', '{$lote['armazem_id']}')";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
        $sql = null;
        $consulta = null;
    }
}



//entrada
foreach ($lotes_estoque_insert as $lote){
$sql = "SELECT Sum(ren_fz_lotes.quantidade) as quantidade FROM `ren_fz_lotes` WHERE codigo_contagem <> '{$contaestoque['codigo']}' and `produto_id` = '{$lote['produto_id']}' and `armazem_id` = '{$lote['armazem_id']}' and `tipo`=1 and `status`=1 ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute();
global $LQ;
$LQ->fnclogquery($sql);
$lotes_ja_somados = $consulta->fetch();
$sql = null;
$consulta = null;
//    echo $lotes_ja_somados['quantidade'];


    $sql = "UPDATE `ren_fz_estoque` SET `quantidade` = `quantidade` + '{$lotes_ja_somados['quantidade']}' WHERE `produto_id` = '{$lote['produto_id']}' and `armazem_id` = '{$lote['armazem_id']}' ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $consulta = null;

    $sql = "UPDATE `ren_fz_lotes` SET `codigo_contagem` = '{$contaestoque['codigo']}' WHERE `produto_id` = '{$lote['produto_id']}' and `armazem_id` = '{$lote['armazem_id']}' and `tipo`=1  ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $consulta = null;


}





//saida
foreach ($lotes_estoque_insert as $lote){
    $sql = "SELECT Sum(ren_fz_lotes.quantidade) as quantidade FROM `ren_fz_lotes` WHERE codigo_contagem <> '{$contaestoque['codigo']}' and `produto_id` = '{$lote['produto_id']}' and `armazem_id` = '{$lote['armazem_id']}' and `tipo`=2 and `status`=1 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $lotes_ja_somados = $consulta->fetch();
    $sql = null;
    $consulta = null;
//    echo $lotes_ja_somados['quantidade'];


    $sql = "UPDATE `ren_fz_estoque` SET `quantidade` = `quantidade` - '{$lotes_ja_somados['quantidade']}' WHERE `produto_id` = '{$lote['produto_id']}' and `armazem_id` = '{$lote['armazem_id']}' ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $consulta = null;

    $sql = "UPDATE `ren_fz_lotes` SET `codigo_contagem` = '{$contaestoque['codigo']}' WHERE `produto_id` = '{$lote['produto_id']}' and `armazem_id` = '{$lote['armazem_id']}' and `tipo`=2 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $sql = null;
    $consulta = null;


}


if ($contaestoque['need_recount']==1){
    $need_recount=1;
}else{
    $need_recount=0;
}
