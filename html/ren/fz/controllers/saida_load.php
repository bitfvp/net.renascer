<?php
//gerado pelo geracode
function fncsaidalist(){
    $sql = "SELECT * FROM ren_fz_saidas ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $saidalista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $saidalista;
}

function fncgetsaida($id){
    $sql = "SELECT * FROM ren_fz_saidas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getren_fz_saidas = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getren_fz_saidas;
}
?>