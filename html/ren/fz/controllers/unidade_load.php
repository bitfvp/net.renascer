<?php
//gerado pelo geracode
function fncunidadelist(){
    $sql = "SELECT * FROM ren_fz_unidades ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $unidadelista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $unidadelista;
}

function fncgetunidade($id){
    $sql = "SELECT * FROM ren_fz_unidades WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getren_fz_unidades = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getren_fz_unidades;
}
?>