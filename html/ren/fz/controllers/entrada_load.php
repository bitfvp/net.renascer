<?php
//gerado pelo geracode
function fncentradalist(){
    $sql = "SELECT * FROM ren_fz_entradas ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $entradalista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $entradalista;
}

function fncgetentrada($id){
    $sql = "SELECT * FROM ren_fz_entradas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getren_fz_entradas = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getren_fz_entradas;
}
?>