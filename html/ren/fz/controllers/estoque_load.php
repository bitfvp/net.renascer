<?php
//gerado pelo geracode

function fncgetestoquetotal($produto_id){
    $sql = "SELECT Sum(ren_fz_estoque.quantidade) as quantidade FROM ren_fz_estoque WHERE produto_id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $produto_id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getquantidade = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getquantidade['quantidade'];
}
?>