<?php
//gerado pelo geracode
function fnccategorialist(){
    $sql = "SELECT * FROM ren_fz_categorias ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $categorialista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $categorialista;
}

function fncgetcategoria($id){
    $sql = "SELECT * FROM ren_fz_categorias WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getren_fz_categorias = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getren_fz_categorias;
}
?>