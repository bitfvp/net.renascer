<?php
class Portaria{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncportariaanew(
        $tipo,$fornecedor,$motorista
    ){
        //tratamento das variaveis
        //não ter


            //inserção no banco
                try {
                    $sql="INSERT INTO ren_portaria ";
                    $sql .= "(id,
                    usuario,
                    tipo,
                    fornecedor,
                    motorista
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :usuario,
                    :tipo,
                    :fornecedor,
                    :motorista
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":usuario", $_SESSION['id']);
                    $insere->bindValue(":tipo", $tipo);
                    $insere->bindValue(":fornecedor", $fornecedor);
                    $insere->bindValue(":motorista", $motorista);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }




        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM ";
                $sql.="ren_portaria";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Va2&id={$maid}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncportariaedit(
        $id,$fornecedor,$motorista
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_portaria ";
            $sql.="WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $entrada_verifica = $consulta->fetch();
//            $entrada_verifica['motorista'] //////motorista antigo

            if ($entrada_verifica['motorista']!=$motorista){
//                echo "é diferente";
                $placa="";
            }else{
//                echo "é igual";
                $placa=$entrada_verifica['placa'];
            }

        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_portaria ";
                $sql.="SET ";
                $sql .= "fornecedor=:fornecedor,
                    motorista=:motorista,
                    placa=:placa
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":fornecedor", $fornecedor);
                $atualiza->bindValue(":motorista", $motorista);
                $atualiza->bindValue(":placa", $placa);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa entrada cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Va2&id={$id}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncportariaedit2(
        $id,$placas,$tipo_embalagem,$obs
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_portaria";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_portaria ";
                $sql.="SET ";
                $sql .= "placa=:placa, tipo_embalagem=:tipo_embalagem, obs=:obs WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":placa", $placas);
                $atualiza->bindValue(":tipo_embalagem", $tipo_embalagem);
                $atualiza->bindValue(":obs", $obs);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa entrada cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }











    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncportariaeditfinal(
        $id,$tipo_embalagem_saida,$obs_saida
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_portaria ";
            $sql.="WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $portaria_datahora_saida=$consulta->fetch();
            $portaria_count=$consulta->rowCount();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }


        if($portaria_count!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_portaria ";
                $sql.="SET ";
                $sql .= "tipo_embalagem_saida=:tipo_embalagem_saida, ";
                if ($portaria_datahora_saida['datahora_saida']==null or $portaria_datahora_saida['datahora_saida']==0 or $portaria_datahora_saida['datahora_saida']==""){
                    $sql .= "datahora_saida=CURRENT_TIMESTAMP, ";
                }
                $sql .= "obs_saida=:obs_saida, status=1 ";
                $sql .= "WHERE id=:id ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":tipo_embalagem_saida", $tipo_embalagem_saida);
                $atualiza->bindValue(":obs_saida", $obs_saida);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pesagem cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


/////////////////////////////////////////////////////////////////
    public function fncportariabnew(
        $tipo,$fornecedor,$motorista,$placa,$tipo_embalagem,$obs,$tipo_embalagem_saida,$obs_saida
    ){
        //tratamento das variaveis
        //não ter


        //inserção no banco
        try {
            $sql="INSERT INTO ren_portaria ";
            $sql .= "(id,
                    usuario,
                    tipo,
                    fornecedor,
                    motorista,
                    placa,
                    tipo_embalagem,
                    obs,
                    tipo_embalagem_saida,
                    obs_saida,
                    datahora_saida,
                    status
                    )";
            $sql .= " VALUES ";
            $sql .= "(NULL,
                    :usuario,
                    :tipo,
                    :fornecedor,
                    :motorista,
                    :placa,
                    :tipo_embalagem,
                    :obs,
                    :tipo_embalagem_saida,
                    :obs_saida,
                    CURRENT_TIMESTAMP,
                    1
                    )";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":usuario", $_SESSION['id']);
            $insere->bindValue(":tipo", $tipo);
            $insere->bindValue(":fornecedor", $fornecedor);
            $insere->bindValue(":motorista", $motorista);
            $insere->bindValue(":placa", $placa);
            $insere->bindValue(":tipo_embalagem", $tipo_embalagem);
            $insere->bindValue(":obs", $obs);
            $insere->bindValue(":tipo_embalagem_saida", $tipo_embalagem_saida);
            $insere->bindValue(":obs_saida", $obs_saida);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }




        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

            header("Location: index.php?pg=Vhome");
            exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




}
