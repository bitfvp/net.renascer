<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatório de portaria-".$env->env_titulo;
$css="print";
include_once("{$env->env_root}includes/head.php");
?>
<!--começa o conteudo-->
<main class="container">
    <?php
    // Recebe
    $inicial = $_POST['data_inicial']." 00:00:01";
    $final = $_POST['data_final']." 23:59:59";


        $sql = "SELECT * FROM "
            ."ren_portaria "
            . "WHERE ((ren_portaria.data_ts>=:inicial) and (ren_portaria.data_ts<:final) ) ";
                if (isset($_POST['fornecedor']) and $_POST['fornecedor']!='') {
                    $fornecedor=$_POST['fornecedor'];
                    $sql .=" AND ren_portaria.fornecedor=:fornecedor ";
                }
                if (isset($_POST['motorista']) and $_POST['motorista']!='') {
                    $motorista=$_POST['motorista'];
                    $sql .=" AND ren_portaria.motorista=:motorista ";
                }
    if (isset($_POST['tipo']) and $_POST['tipo']!='' and $_POST['tipo']!=0) {
        $tipo=$_POST['tipo'];
        $sql .=" AND ren_portaria.tipo=:tipo ";
    }
            $sql . "ORDER BY \n";
            $sql . "ren_portaria.data_ts ASC";

        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
            if (isset($_POST['fornecedor']) and $_POST['fornecedor']!='') {
                $consulta->bindValue(":fornecedor", $fornecedor);
            }
            if (isset($_POST['motorista']) and $_POST['motorista']!='') {
                $consulta->bindValue(":motorista", $motorista);
            }
            if (isset($_POST['tipo']) and $_POST['tipo']!='' and $_POST['tipo']!=0) {
                $consulta->bindValue(":tipo", $tipo);
            }
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $portarias = $consulta->fetchAll();
    $portarias_count = $consulta->rowCount();
        $sql = null;
        $consulta = null;

    ?>
    <div class="row-fluid">

        <!-- ////////////////////////////////////////// -->
        <h3>RELATÓRIO DE FLUXO NA PORTARIA</h3>
        <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
        <table class="table table-sm table-striped">
            <thead>
            <tr>
                <th scope="col"><small>DATA</small></th>
                <th scope="col"><small>FORNECEDOR</small></th>
                <th scope="col"><small>MOTORISTA</small></th>
                <th scope="col"><small>PLACAS</small></th>
                <th scope="col"><small>EMBALAGEM</small></th>
                <th scope="col"><small>OBS</small></th>
                <th scope="col" class="bg-info"><small>SAÍDA</small></th>
                <th scope="col" class="bg-info"><small>EMBALAGEM</small></th>
                <th scope="col" class="bg-info"><small>OBS</small></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <td><?php echo $portarias_count; if ($portarias_count<=1){ echo " passagem";}else{echo " passagens";}?></td>
            </tr>
            </tfoot>
            <tbody>
            <script>
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })
            </script>
            <?php
            foreach ($portarias as $dados){
                $por_id = $dados["id"];
                $motorista = fncgetpessoa($dados["motorista"])['nome'];
                $placa = $dados["placa"];
                $fornecedor = fncgetpessoa($dados["fornecedor"])['nome'];
                $data_ts = datahoraBanco2data($dados["data_ts"]);
                switch ($dados["tipo_embalagem"]){
                    case 0:
                        $embalagem="";
                        break;
                    case 1:
                        $embalagem="SACARIA";
                        break;
                    case 2:
                        $embalagem="BIG BAGS";
                        break;
                    case 3:
                        $embalagem="GRANEL";
                        break;
                    case 4:
                        $embalagem="MISTA";
                        break;
                    case 5:
                        $embalagem="VAZIO";
                        break;

                    default:
                        $embalagem="Não selecionada!";
                        break;
                }
                $obs = $dados["obs"];
                if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                    $datahora_saida = "";
                }else{
                    $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
                }

                $obs_saida = $dados["obs_saida"];
                switch ($dados["tipo_embalagem_saida"]){
                    case 0:
                        $embalagem_saida="";
                        break;
                    case 1:
                        $embalagem_saida="SACARIA";
                        break;
                    case 2:
                        $embalagem_saida="BIG BAGS";
                        break;
                    case 3:
                        $embalagem_saida="GRANEL";
                        break;
                    case 4:
                        $embalagem_saida="MISTA";
                        break;
                    case 5:
                        $embalagem_saida="VAZIO";
                        break;

                    default:
                        $embalagem_saida="!";
                        break;
                }
                $usuario = $dados["usuario"];


                ?>

                <tr id="<?php echo $por_id;?>" class="">
                    <td><?php echo $data_ts; ?></td>
                    <td><?php echo strtoupper($fornecedor); ?></td>
                    <td><?php echo strtoupper($motorista); ?></td>
                    <td><?php echo $placa; ?></td>
                    <td><?php echo $embalagem; ?></td>
                    <td><?php echo $obs; ?></td>
                    <td class="text-info"><?php echo $datahora_saida; ?></td>
                    <td class="text-info"><?php echo $embalagem_saida; ?></td>
                    <td class="text-info"><?php echo $obs_saida; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <br>
        <h6 class="text-center">Relatório retirado em <?php echo date("d/m/Y H:i:s")." por ".fncgetusuario($_SESSION['id'])['nome'];?></h6>

    </div>
</main>
</body>
</html>
