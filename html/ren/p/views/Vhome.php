<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT  "
."ren_portaria.id, "
."ren_portaria.data_ts, "
."ren_portaria.usuario, "
."ren_portaria.tipo, "
."ren_portaria.fornecedor, "
."ren_portaria.motorista, "
."ren_portaria.placa, "
."ren_portaria.tipo_embalagem, "
."ren_portaria.obs, "
."ren_portaria.datahora_saida, "
."ren_portaria.tipo_embalagem_saida, "
."ren_portaria.obs_saida, "
."ren_portaria.`status` "
."FROM "
."ren_portaria "
."INNER JOIN ren_pessoas AS pessoas_fornecedor ON pessoas_fornecedor.id = ren_portaria.fornecedor "
."INNER JOIN ren_pessoas AS pessoas_motorista ON pessoas_motorista.id = ren_portaria.motorista "
        ."WHERE "
        ."ren_portaria.id <> 0 ";

    if (isset($_GET['sca']) and $_GET['sca']!='') {
        $sca=$_GET['sca'];
        $sql .=" AND pessoas_fornecedor.nome LIKE '%$sca%' ";
//        echo 'statA';
    }
    if (isset($_GET['scb']) and $_GET['scb']!='') {
        $scb=$_GET['scb'];
        $sql .=" AND pessoas_motorista.nome LIKE '%$scb%' ";
//        echo 'statB';
    }

    if (isset($_GET['scc']) and $_GET['scc']!='') {
        $inicial=$_GET['scc'];
        $inicial.=" 00:00:01";
        $final=$_GET['scc'];
        $final.=" 23:59:59";
//        echo 'statC';
        $sql .=" AND ((ren_portaria.data_ts)>=:inicial) And ((ren_portaria.data_ts)<=:final) ";
    }else{
        if ((isset($_GET['sca']) and $_GET['sca']!='') or (isset($_GET['scb']) and $_GET['scb']!='')){
            //tempo todo
            $inicial="2021-01-01";
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((ren_portaria.data_ts)>=:inicial) And ((ren_portaria.data_ts)<=:final) ";
        }else{
            //apenas dia anterior
            $inicial=date("Y-m-d",strtotime("-1 days"));
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((ren_portaria.data_ts)>=:inicial) And ((ren_portaria.data_ts)<=:final) ";
        }

    }

    $sql .="order by ren_portaria.data_ts DESC LIMIT 0,50 ";

    global $pdo;
    $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$portarias = $consulta->fetchAll();
$portarias_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>


<main class="container-fluid"><!--todo conteudo-->
            <h3 class="form-cadastro-heading">Fluxo de Portaria</h3>
            <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-7 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
            </div>
            <input name="pg" value="Vhome" hidden/>
            <input type="text" name="sca" id="sca" placeholder="fornecedor" autocomplete="off" class="form-control" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <input type="text" name="scb" id="scb" placeholder="motorista" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
            <input type="date" name="scc" id="scc" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
        </div>
    </form>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <div class="row">
        <div class="col-md-4">
            <a href="index.php?pg=Va" class="btn btn-block btn-primary mb-2" ><i class="fas fa-truck"></i> Novo</a>
        </div>
        <div class="col-md-4">
            <a href="index.php?pg=Vb" class="btn btn-block btn-primary mb-2" ><i class="fas fa-car-side"></i> Novo</a>
        </div>
        <div class="col-md-4">
            <a href="index.php?pg=Vc" class="btn btn-block btn-primary mb-2" ><i class="fas fa-walking"></i> Novo</a>
        </div>
    </div>

    <table class="table table-stripe table-sm table-hover table-condensed">
        <thead class="thead-dark">
        <tr>
            <th scope="col" class="text-center"></th>
            <th scope="col"><small>DATA</small></th>
            <th scope="col"><small>FORNECEDOR</small></th>
            <th scope="col"><small>MOTORISTA</small></th>
            <th scope="col"><small>PLACAS</small></th>
            <th scope="col"><small>EMBALAGEM</small></th>
            <th scope="col"><small>OBS</small></th>
            <th scope="col" class="bg-info"><small>SAÍDA</small></th>
            <th scope="col" class="bg-info"><small>EMBALAGEM</small></th>
            <th scope="col" class="bg-info"><small>OBS</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="11" class="text-right"><?php echo $portarias_quant;?> Encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        foreach ($portarias as $dados){
            if ($dados['status']==1){
                $cordalinha = " text-warning bg-gradient-dark ";
            }else{
                $cordalinha = " ";
            }
            $por_id = $dados["id"];
            $motorista = fncgetpessoa($dados["motorista"])['nome'];
            $placa = $dados["placa"];
            $fornecedor = fncgetpessoa($dados["fornecedor"])['nome'];
            $data_ts = datahoraBanco2data($dados["data_ts"]);
            switch ($dados["tipo_embalagem"]){
                case 0:
                    $embalagem="";
                    break;
                case 1:
                    $embalagem="SACARIA";
                    break;
                case 2:
                    $embalagem="BIG BAGS";
                    break;
                case 3:
                    $embalagem="GRANEL";
                    break;
                case 4:
                    $embalagem="MISTA";
                    break;
                case 5:
                    $embalagem="VAZIO";
                    break;

                default:
                    $embalagem="Não selecionada!";
                    break;
            }
            $obs = $dados["obs"];
            if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0){
                $datahora_saida = "";
            }else{
                $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
            }

            $obs_saida = $dados["obs_saida"];
            switch ($dados["tipo_embalagem_saida"]){
                case 0:
                    $embalagem_saida="";
                    break;
                case 1:
                    $embalagem_saida="SACARIA";
                    break;
                case 2:
                    $embalagem_saida="BIG BAGS";
                    break;
                case 3:
                    $embalagem_saida="GRANEL";
                    break;
                case 4:
                    $embalagem_saida="MISTA";
                    break;
                case 5:
                    $embalagem_saida="VAZIO";
                    break;

                default:
                    $embalagem_saida="!";
                    break;
            }
            $usuario = $dados["usuario"];

            switch ($dados["tipo"]){
                case 1:
                    $tipo="<i class='fas fa-truck fa-2x'></i>";
                    break;
                case 2:
                    $tipo="<i class='fas fa-car-side fa-2x'></i>";
                    break;
                case 3:
                    $tipo="<i class='fas fa-walking fa-2x'></i>";
                    break;
            }

            ?>

            <tr id="<?php echo $por_id;?>" class="<?php echo $cordalinha; ?>">
                <th scope="row" id="">
                    <small><?php echo $tipo; ?></small>
                </th>
                <td><?php echo $data_ts; ?></td>
                <td><?php echo strtoupper($fornecedor); ?></td>
                <td><?php echo strtoupper($motorista); ?></td>
                <td><?php echo $placa; ?></td>
                <td><?php echo $embalagem; ?></td>
                <td><?php echo $obs; ?></td>
                <td class="text-info"><?php echo $datahora_saida; ?></td>
                <td class="text-info"><?php echo $embalagem_saida; ?></td>
                <td class="text-info"><?php echo $obs_saida; ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <?php if ($dados["datahora_saida"]=="" or $dados["datahora_saida"]==0 or $allow["allow_6"]==1){?>
                            <a href="index.php?pg=Va&id=<?php echo $por_id; ?>" title="Editar registro" class="btn btn-sm btn-primary fas fa-pen">
                                EDITAR
                            </a>

                            <a href="index.php?pg=Va_final&id=<?php echo $por_id; ?>" title="Finalizar" class="btn btn-sm btn-success fas fa-check-double">
                                FINALIZAR
                            </a>
                        <?php } ?>

                        <?php if ($allow["allow_6"]==1){?>
                            <div class="dropdown show">
                                <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-trash"><br>EXCLUIR<br>REGISTRO</i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#">Não</a>
                                    <a class="dropdown-item bg-danger" href="index.php?pg=Vhome&aca=apagarportaria&id=<?php echo $por_id; ?>">Apagar</a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </td>


            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>