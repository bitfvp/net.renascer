<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatório de portaria-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#serchfornecedor input[type="text"]').on("keyup input", function () {
                /* Get input value on change */
                var inputValf = $(this).val();
                var resultDropdownf = $(this).siblings(".resultfornecedor");
                if (inputValf.length) {
                    $.get("includes/fornecedor.php", {term: inputValf}).done(function (data) {
                        // Display the returned data in browser
                        resultDropdownf.html(data);
                    });
                } else {
                    resultDropdownf.empty();
                }
            });

            // Set search input value on click of result item
            $(document).on("click", ".resultfornecedor p", function () {
                $(this).parents("#serchfornecedor").find('input[type="text"]').val($(this).text());
                var fornecedor = this.id;
                $('#fornecedor').val(fornecedor);
                $(this).parent(".resultfornecedor").empty();
            });
        });

        $(document).ready(function () {
            $('#serchmotorista input[type="text"]').on("keyup input", function () {
                /* Get input value on change */
                var inputValf = $(this).val();
                var resultDropdownf = $(this).siblings(".resultmotorista");
                if (inputValf.length) {
                    $.get("includes/motorista.php", {term: inputValf}).done(function (data) {
                        // Display the returned data in browser
                        resultDropdownf.html(data);
                    });
                } else {
                    resultDropdownf.empty();
                }
            });

            // Set search input value on click of result item
            $(document).on("click", ".resultmotorista p", function () {
                var motorista = this.id;
                $.when(
                    $(this).parents("#serchmotorista").find('input[type="text"]').val($(this).text()),
                    $('#motorista').val(motorista),
                    $(this).parent(".resultmotorista").empty()

                ).then(
                    function() {
                        // roda depois de acao1 e acao2 terminarem
                        setTimeout(function() {
                            //do something special
                            $( "#gogo" ).click();
                        }, 500);
                    });
            });
        });
    </script>
<div class="row">
<div class="col-md-2"></div>
    <div class="col-md-6">
<!-- =============================começa conteudo======================================= -->
        <div class="card">
            <div class="card-header bg-info text-light">
            Relatório de fluxo na portaria
            </div>
            <div class="card-body">
                <form action="index.php?pg=Vrel1print" method="post" target="_blank">
                    <input type="submit" class="btn btn-lg btn-success btn-block" value="GERAR RELATÓRIO"/>

                    <div class="form-group">
                        <label for="data_inicial">DATA INICIAL:</label>
                        <input id="data_inicial" type="date" class="form-control" name="data_inicial" value="" autofocus required/>
                    </div>

                    <div class="form-group">
                        <label for="data_final">DATA FINAL:</label>
                        <input id="data_final" type="date" class="form-control" name="data_final" value="" required/>
                    </div>

                    <div class="form-group" id="serchfornecedor">
                        <label for="fornecedor">FORNECEDOR:</label>
                        <div class="input-group">
                            <?php
                            $c_fornecedor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                            <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="f<?php echo $c_fornecedor;?>" id="f<?php echo $c_fornecedor;?>" value="" placeholder="Click no resultado ao aparecer"/>
                            <input id="fornecedor" autocomplete="false" type="hidden" class="form-control" name="fornecedor" value="" />
                            <div class="input-group-append">
                            <span class="input-group-text" id="r_fornecedor">
                                <i class="fa fas fa-backspace "></i>
                            </span>
                            </div>
                            <div class="resultfornecedor"></div>
                        </div>
                        <script>
                            $(document).ready(function(){
                                $("#r_fornecedor").click(function(ev){
                                    document.getElementById('fornecedor').value='';
                                    document.getElementById('f<?php echo $c_fornecedor;?>').value='';
                                });
                            });
                        </script>
                    </div>

                    <div class="form-group" id="serchmotorista">
                        <label for="motorista">MOTORISTA:</label>
                        <div class="input-group">
                            <?php
                            $c_motorista = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                            <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="m<?php echo $c_motorista;?>" id="m<?php echo $c_motorista;?>" value="" placeholder="Click no resultado ao aparecer"/>
                            <input id="motorista" autocomplete="false" type="hidden" class="form-control" name="motorista" value="" />
                            <div class="input-group-append">
                            <span class="input-group-text" id="r_motorista">
                                <i class="fa fas fa-backspace "></i>
                            </span>
                            </div>
                            <div class="resultmotorista"></div>
                        </div>
                        <script>
                            $(document).ready(function(){
                                $("#r_motorista").click(function(ev){
                                    document.getElementById('motorista').value='';
                                    document.getElementById('m<?php echo $c_motorista;?>').value='';
                                });
                            });
                        </script>
                    </div>
                    <div class="form-group" id="">
                        <label for="tipo">TIPO:</label>
                        <select name="tipo" id="tipo" class="form-control input-sm" data-live-search="true">
                            <option selected="" data-tokens="" value="0" selected>
                                TODOS
                            </option>
                            <option data-tokens="" value="1">
                                MOVIMENTAÇÃO DE CAMINHÕES
                            </option>
                            <option data-tokens="" value="2">
                                MOVIMENTAÇÃO DE VEÍCULOS COM PLACA
                            </option>
                            <option data-tokens="" value="3">
                                MOVIMENTAÇÃO DE PESSOAS
                            </option>
                        </select>
                    </div>
                </form>
            </div>
        </div>

<!-- =============================fim conteudo======================================= -->       
    </div>
    <div class="col-md-2"></div>
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>