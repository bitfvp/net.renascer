<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vhome'>";
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="portariasave_final";
    $portaria=fncgetportaria($_GET['id']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vhome");
    exit();
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-4">
            <blockquote class="blockquote blockquote-success">
                <header>
                    FORNECEDOR:
                    <strong class="text-info"><?php echo strtoupper(fncgetpessoa($portaria['fornecedor'])['nome']); ?>&nbsp;&nbsp;</strong>
                </header>
                <h6>
                    ENTRADA:
                    <strong class="text-info"><?php echo datahoraBanco2data($portaria['data_ts']); ?></strong>

                    MOTORISTA:
                    <strong class="text-info"><?php echo strtoupper(fncgetpessoa($portaria['motorista'])['nome']); ?></strong>
                    <br>
                    PLACA(S):
                    <strong class="text-info"><?php echo $portaria['placa']; ?></strong>
                    <br>
                    EMBALAGEM:
                    <strong class="text-info">
                        <?php
                        switch ($portaria["tipo_embalagem"]){
                            case 0:
                                echo $embalagem="Não selecionada";
                                break;
                            case 1:
                                echo $embalagem="SACARIA";
                                break;
                            case 2:
                                echo $embalagem="BIG BAGS";
                                break;
                            case 3:
                                echo $embalagem="GRANEL";
                                break;
                            case 4:
                                echo $embalagem="MISTA";
                                break;
                            case 5:
                                echo $embalagem="VAZIO";
                                break;

                            default:
                                echo $embalagem="Não selecionada!";
                                break;
                        }
                        ?>
                    </strong>
                    <br>
                    OBS:
                    <strong class="text-info"><?php echo $portaria['obs']; ?></strong>

                    <footer class="blockquote-footer">
                        <?php echo fncgetusuario($portaria['usuario'])['nome']?>&nbsp;&nbsp;
                    </footer>
            </blockquote>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="form-cadastro-heading">SAÍDA</h3>
                    <hr>
                    <form class="form-signin" action="<?php echo "index.php?pg=Vhome&id={$_GET['id']}&aca={$a}"; ?>" method="post" id="formentrada1">
                        <div class="row">
                            <div class="col-md-5">
                                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $portaria['id']; ?>"/>
                                <label for="tipo_embalagem_saida">EMBALAGEM</label>
                                <select autofocus name="tipo_embalagem_saida" id="tipo_embalagem_saida" class="form-control" required>
                                    <option selected="" value="<?php if ($portaria['tipo_embalagem_saida'] == "") {
                                        $z = 0;
                                        echo $z;
                                    } else {
                                        echo $portaria['tipo_embalagem_saida'];
                                    } ?>">
                                        <?php
                                        if ($portaria['tipo_embalagem_saida'] == 0) {
                                            echo "Selecione...";
                                        }
                                        if ($portaria['tipo_embalagem_saida'] == 1) {
                                            echo "SACARIA";
                                        }
                                        if ($portaria['tipo_embalagem_saida'] == 2) {
                                            echo "BIG BAGS";
                                        }
                                        if ($portaria['tipo_embalagem_saida'] == 3) {
                                            echo "GRANEL";
                                        }
                                        if ($portaria['tipo_embalagem_saida'] == 4) {
                                            echo "MISTA";
                                        }
                                        if ($portaria['tipo_embalagem_saida'] == 5) {
                                            echo "VAZIO";
                                        }
                                        ?>
                                    </option>
                                    <option value="1">SACARIA</option>
                                    <option value="2">BIG BAGS</option>
                                    <option value="3">GRANEL</option>
                                    <option value="4">MISTA</option>
                                    <option value="5">VAZIO</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="obs_saida">OBSERVAÇÃO DE SAÍDA</label>
                                <textarea id="obs_saida" onkeyup="limite_textarea(this.value,250,obs_saida,'cont')" maxlength="250" class="form-control" rows="2" name="obs_saida"><?php echo $portaria['obs_saida']; ?></textarea>
                                <span id="cont">250</span>/250
                            </div>

                            <div class="col-md-12">
                                <input type="submit" class="btn btn-success btn-block my-2" value="SALVAR">
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>