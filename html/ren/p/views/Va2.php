<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="portariasave2";
    $portaria=fncgetportaria($_GET['id']);

}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vhome");
    exit();
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Ve2&id={$_GET['id']}&aca={$a}"; ?>" method="post" id="formentrada1">
        <h3 class="form-cadastro-heading">CADASTRO DE ENTRADA</h3>
        <hr>
        <div class="row">
            <div class="col-md-6" id="serchfornecedor">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $portaria['id']; ?>"/>
                <label for="fornecedor">FORNECEDOR</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_fornecedor_id=$portaria['fornecedor'];
                        $v_fornecedor=fncgetpessoa($portaria['fornecedor'])['nome'];

                    }else{
                        $v_fornecedor_id="";
                        $v_fornecedor="";
                    }
                    $c_fornecedor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input readonly="readonly" autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="f<?php echo $c_fornecedor;?>" id="f<?php echo $c_fornecedor;?>" value="<?php echo $v_fornecedor; ?>" placeholder="Click no resultado ao aparecer" required/>
                    <input readonly="readonly" id="fornecedor" autocomplete="false" type="hidden" class="form-control" name="fornecedor" value="<?php echo $v_fornecedor_id; ?>" />
                    <div class="input-group-append">
                            <span class="input-group-text" id="r_fornecedor">
                                <i class="fa fas fa-backspace "></i>
                            </span>
                    </div>
                    <div class="resultfornecedor"></div>
                </div>
                <script>
                    //$(document).ready(function(){
                    //    $("#r_fornecedor").click(function(ev){
                    //        document.getElementById('fornecedor').value='';
                    //        document.getElementById('f<?php //echo $c_fornecedor;?>//').value='';
                    //    });
                    //});
                </script>
            </div>


            <div class="col-md-6" id="serchmotorista">
                <label for="motorista">MOTORISTA</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_motorista_id=$portaria['motorista'];
                        $v_motorista=fncgetpessoa($portaria['motorista'])['nome'];

                    }else{
                        $v_motorista_id="";
                        $v_motorista="";
                    }
                    $c_motorista = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input readonly="readonly" autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="m<?php echo $c_motorista;?>" id="m<?php echo $c_motorista;?>" value="<?php echo $v_motorista; ?>" placeholder="Click no resultado ao aparecer"  required />
                    <input readonly="readonly" id="motorista" autocomplete="false" type="hidden" class="form-control" name="motorista" value="<?php echo $v_motorista_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_motorista">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultmotorista"></div>
                </div>
                <script>
                    //$(document).ready(function(){
                    //    $("#r_motorista").click(function(ev){
                    //        document.getElementById('motorista').value='';
                    //        document.getElementById('m<?php //echo $c_motorista;?>//').value='';
                    //    });
                    //});
                </script>
            </div>
        </div>


        <div class="row">
                        <div class="col-md-3">
                            <label for="tipo_embalagem">EMBALAGEM</label>
                            <select autofocus name="tipo_embalagem" id="tipo_embalagem" class="form-control" required>
                                <option selected="" value="<?php if ($portaria['tipo_embalagem'] == "") {
                                    $z = 0;
                                    echo $z;
                                } else {
                                    echo $portaria['tipo_embalagem'];
                                } ?>">
                                    <?php
                                    if ($portaria['tipo_embalagem'] == 0) {
                                        echo "Selecione...";
                                    }
                                    if ($portaria['tipo_embalagem'] == 1) {
                                        echo "SACARIA";
                                    }
                                    if ($portaria['tipo_embalagem'] == 2) {
                                        echo "BIG BAGS";
                                    }
                                    if ($portaria['tipo_embalagem'] == 3) {
                                        echo "GRANEL";
                                    }
                                    if ($portaria['tipo_embalagem'] == 4) {
                                        echo "MISTA";
                                    }
                                    if ($portaria['tipo_embalagem'] == 5) {
                                        echo "VAZIO";
                                    }
                                    ?>
                                </option>
                                <option value="1">SACARIA</option>
                                <option value="2">BIG BAGS</option>
                                <option value="3">GRANEL</option>
                                <option value="4">MISTA</option>
                                <option value="5">VAZIO</option>
                            </select>
                        </div>

            <div class="col-md-6" id="">
                <label for="placa">PLACAS <i class="text-info">* placas separadas por espaço</i> </label>
                <input autocomplete="off" id="placa" type="text" class="form-control text-uppercase" name="placa" value="<?php echo $portaria['placa']; ?>" placeholder="Preencha com as placas"/>
                <script>
                    function isererecebeu(valor) {
                        var str = $("#placa").val();
                        $('#placa').val(str+" "+valor);
                        // $('#quem_recebeu').focus();
                    }
                </script>
                <?php
                foreach (fncplacalist($portaria['motorista']) as $placas){
                    echo "<i id='emoji' class='btn btn-sm m-1 border btn-outline-dark' onclick=\"isererecebeu('{$placas['placa']}')\">{$placas['placa']}</i>";
                }
                ?>
            </div>

                        <div class="col-md-12">
                            <label for="obs">OBSERVAÇÃO DE ENTRADA</label>
                            <textarea id="obs" onkeyup="limite_textarea(this.value,250,obs,'cont')" maxlength="250" class="form-control" rows="2" name="obs"><?php echo $portaria['obs']; ?></textarea>
                            <span id="cont">250</span>/250
                        </div>
        </div>



        <div class="row">
            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" id="gogo" class="btn btn-block btn-success mt-4">
            </div>
        </div>

    </form>
</div>

<?php
//for ($i = 1; $i <= 1000000; $i++) {
//    echo $i."  ".get_criptografa64($i)."<br>";
//}
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>