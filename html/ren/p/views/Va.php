<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="portariasave";
    $portaria=fncgetportaria($_GET['id']);

}else{
    $a="portariaanew";
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">
    $(document).ready(function () {
        $('#serchfornecedor input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultfornecedor");
            if (inputValf.length) {
                $.get("includes/fornecedor.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultfornecedor p", function () {
            $(this).parents("#serchfornecedor").find('input[type="text"]').val($(this).text());
            var fornecedor = this.id;
            $('#fornecedor').val(fornecedor);
            $(this).parent(".resultfornecedor").empty();
        });
    });

    $(document).ready(function () {
        $('#serchmotorista input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultmotorista");
            if (inputValf.length) {
                $.get("includes/motorista.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultmotorista p", function () {
            var motorista = this.id;
            $.when(
                $(this).parents("#serchmotorista").find('input[type="text"]').val($(this).text()),
                $('#motorista').val(motorista),
                $(this).parent(".resultmotorista").empty()

            ).then(
                function() {
                    // roda depois de acao1 e acao2 terminarem
                    setTimeout(function() {
                        //do something special
                        $( "#gogo" ).click();
                    }, 500);
                });
        });
    });
</script>
<!------------------------------>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Va&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">CADASTRO DE ENTRADA</h3>
        <hr>
        <div class="row">
            <div class="col-md-6" id="serchfornecedor">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $portaria['id']; ?>"/>
                <label for="fornecedor">FORNECEDOR</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_fornecedor_id=$portaria['fornecedor'];
                        $v_fornecedor=fncgetpessoa($portaria['fornecedor'])['nome'];

                    }else{
                        $v_fornecedor_id="";
                        $v_fornecedor="";
                    }
                    $c_fornecedor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" autofocus type="text" class="form-control" aria-autocomplete="none" name="f<?php echo $c_fornecedor;?>" id="f<?php echo $c_fornecedor;?>" value="<?php echo $v_fornecedor; ?>" placeholder="Click no resultado ao aparecer" required/>
                    <input id="fornecedor" autocomplete="false" type="hidden" class="form-control" name="fornecedor" value="<?php echo $v_fornecedor_id; ?>" />
                    <div class="input-group-append">
                            <span class="input-group-text" id="r_fornecedor">
                                <i class="fa fas fa-backspace "></i>
                            </span>
                    </div>
                    <div class="resultfornecedor"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_fornecedor").click(function(ev){
                            document.getElementById('fornecedor').value='';
                            document.getElementById('f<?php echo $c_fornecedor;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-6" id="serchmotorista">
                <label for="motorista">MOTORISTA</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_motorista_id=$portaria['motorista'];
                        $v_motorista=fncgetpessoa($portaria['motorista'])['nome'];

                    }else{
                        $v_motorista_id="";
                        $v_motorista="";
                    }
                    $c_motorista = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="m<?php echo $c_motorista;?>" id="m<?php echo $c_motorista;?>" value="<?php echo $v_motorista; ?>" placeholder="Click no resultado ao aparecer"  required />
                    <input id="motorista" autocomplete="false" type="hidden" class="form-control" name="motorista" value="<?php echo $v_motorista_id; ?>" />
                    <div class="input-group-append">
                            <span class="input-group-text" id="r_motorista">
                                <i class="fa fas fa-backspace "></i>
                            </span>
                    </div>
                    <div class="resultmotorista"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_motorista").click(function(ev){
                            document.getElementById('motorista').value='';
                            document.getElementById('m<?php echo $c_motorista;?>').value='';
                        });
                    });
                </script>
            </div>




            <div class="col-md-6 pt-2" id="">
                <input type="submit" name="gogo" id="gogo" class="btn btn-success btn-block my-2" value="AVANÇAR >>"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>

        </div>

    </form>
</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>