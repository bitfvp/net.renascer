<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_8"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vhome'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
//    $a="portariasave";
//    $portaria=fncgetportaria($_GET['id']);

}else{
    $a="portariabnew";
}
?>
<!--/////////////////////////////////////////////////////-->

<!------------------------------>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vb&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">CADASTRO DE ENTRADA</h3>
        <hr>
        <div class="row">

            <div class="col-md-6" id="">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $portaria['id']; ?>"/>
                <label for="placa">PLACAS: <i class="text-info">* placas separadas por espaço</i> </label>
                <input autocomplete="off" autofocus id="placa" type="text" class="form-control text-uppercase" name="placa" value="<?php echo $portaria['placa']; ?>" placeholder="Preencha com as placas"/>
            </div>

            <div class="col-md-12">
                <label for="obs">DESCREVA QUEM, COMO E O POR QUÊ:</label>
                <textarea id="obs" onkeyup="limite_textarea(this.value,250,obs,'cont')" maxlength="250" class="form-control" rows="2" name="obs" required><?php echo $portaria['obs']; ?></textarea>
                <span id="cont">250</span>/250
            </div>



            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" name="gogo" id="gogo" class="btn btn-block btn-success mt-4">
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>

        </div>

    </form>
</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>