<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_19"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vcaixa_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $lancamento=fncgetcaixa_lancamento($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <!-- =============================começa conteudo======================================= -->
            <div class="card mt-2">
                <div class="card-header bg-info text-light">
                    Finalizar lançamento
                </div>
                <div class="card-body">
                    <form action="index.php?pg=Vcaixa_lista&aca=caixa_lancamentoupdate" method="post">
                        <div class="row">
                            <div class="form-group col-md-4 d-none">
                                <label>Código do benefício:</label>
                                <input id="id" type="text" readonly="true" class="form-control disabled" name="id" value="<?php echo $lancamento['id']; ?>"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Quem paga:</label>
                                <input id="quem_paga" type="text" readonly="true" class="form-control disabled" name="quem_paga" value="<?php echo $lancamento['quem_paga']; ?>"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Quem recebe:</label>
                                <input id="quem_recebe" type="text" readonly="true" class="form-control disabled" name="quem_recebe" value="<?php echo $lancamento['quem_recebe']; ?>"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Valor:</label>
                                <?php $val_temp="R$ ".number_format($lancamento['valor'],2, ',', ' ');?>
                                <input id="valor" type="text" readonly="true" class="form-control disabled" name="valor" value="<?php echo $val_temp; ?>"/>
                            </div>

                            <div class="form-group col-md-8">
                                <label>descrição:</label>
                                <input id="descricao" type="text" readonly="true" class="form-control disabled" name="descricao" value="<?php echo $lancamento['descricao']; ?>"/>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="finalizado">Finalizado:</label>
                                <select name="finalizado" id="finalizado" class="form-control">
                                    <option selected="" value="<?php if($lancamento['finalizado']==""){$z=0; echo $z;}else{ echo $lancamento['finalizado'];} ?>">
                                        <?php
                                        if($lancamento['finalizado']==0){echo"Aguardando";}
                                        if($lancamento['finalizado']==1){echo"Sim";}
                                        ?>
                                    </option>
                                    <option value="0">Aguardando...</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="obs">Obs:</label>
                                <textarea id="obs" onkeyup="limite_textarea(this.value,255,obs,'cont')" maxlength="3000" class="form-control" rows="3" name="obs"><?php echo $lancamento['obs']; ?></textarea>
                                <span id="cont">255</span>/255
                            </div>

                            <div class="form-group col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="SALVAR REGISTRO"/>
                            </div>
                        </div>
                    </form>

                    </p>
                </div>
            </div>


            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3"></div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>