<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_19"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vcaixa_lista'>";
include_once("includes/topo.php");

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <!-- =============================começa conteudo======================================= -->
            <div class="card mt-2 text-uppercase">
                <div class="card-header bg-info text-light">
                    lançamento
                </div>
                <div class="card-body">
                    <form class="form-signin" action="index.php?pg=Vcaixa_lista&aca=caixa_lancamentoinsert2" method="post" id="formx">
                        <div class="row">
                            <?php
//                            if ($fechamento['tipo_fechamento']==1){//compra
//                                $f_t="Débito";
//                                $f_t_code=2;
//                                $f_t_h=1;
//                                $quem_paga=strtoupper(fncgetpessoa($fechamento['comprador'])['nome']);
//                                $quem_recebe=strtoupper(fncgetpessoa($fechamento['vendedor'])['nome']);
//                            }
//                            if ($fechamento['tipo_fechamento']==2){//venda
//                                $f_t="crédito";
//                                $f_t_code=1;
//                                $f_t_h=2;
//                                $quem_paga=strtoupper(fncgetpessoa($fechamento['comprador'])['nome']);
//                                $quem_recebe=strtoupper(fncgetpessoa($fechamento['vendedor'])['nome']);
//                            }
                            ?>

                            <input id="fechamento_id" type="hidden" class="" name="fechamento_id" value="0"/>

                            <div class="col-md-4">
                                <label for="historico">HISTÓRICO:</label>
                                <select name="historico" id="historico" class="form-control input-sm text-uppercase" data-live-search="true" required>

                                    <option selected data-tokens="" value="">
                                        Nada selecionado
                                    </option>
                                    <?php
                                    foreach (fnchistoricolist() as $item) {
                                        ?>
                                        <option data-tokens="" value="<?php echo $item['id'];?>">
                                            <?php echo $item['historico']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label for="valor">VALOR:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">R$</span>
                                    </div>
                                    <input autocomplete="off" id="valor" placeholder="" type="number" class="form-control" name="valor" value="" min="0.5" step="any" required />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="">PREVISÃO</label>
                                <div class="input-group">
                                    <input  id="previsao" type="date" class="form-control" name="previsao" value="" required/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="tipo">TIPO:</label>
                                <select name="tipo" id="tipo" class="form-control input-sm text-uppercase" data-live-search="true" required>
                                    <option selected data-tokens="" value="<?php echo $f_t_code;?>">
                                        <?php echo $f_t;?>
                                    </option>
                                    <option data-tokens="" value="1">
                                        Crédito
                                    </option>
                                    <option data-tokens="" value="2">
                                        Débito
                                    </option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="fonte">CONTA:</label>
                                <select name="fonte" id="fonte" class="form-control input-sm text-uppercase" data-live-search="true" required>

                                    <option selected data-tokens="" value="">
                                        Nada selecionado
                                    </option>
                                    <?php
                                    foreach (fncfontelist() as $item) {
                                        ?>
                                        <option data-tokens="" value="<?php echo $item['id'];?>">
                                            <?php echo $item['titular']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="quem_paga">QUEM PAGA:</label>
                                <div class="input-group">
                                    <input  id="quem_paga" type="text" class="form-control" name="quem_paga" value="Renascer" required/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="quem_recebe">QUEM RECEBE:</label>
                                <div class="input-group">
                                    <input  id="quem_recebe" type="text" class="form-control" name="quem_recebe" value="" required/>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <label for="descricao">DESCRIÇÃO:</label>
                                <div class="input-group">
                                    <input  id="descricao" type="text" class="form-control" name="descricao" value=""/>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <input type="submit" name="enviar" id="enviar" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                            </div>
                            <script>
                                var formID = document.getElementById("formx");
                                var send = $("#enviar");

                                $(formID).submit(function(event){
                                    if (formID.checkValidity()) {
                                        send.attr('disabled', 'disabled');
                                        send.attr('value', 'AGUARDE...');
                                    }
                                });
                            </script>
                        </div>
                    </form>

                    </p>
                </div>
            </div>


            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3"></div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>