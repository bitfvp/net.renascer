<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de lançamentos-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

if (isset($_POST['historico']) and is_numeric($_POST['historico']) and $_POST['historico']>0){
    $historico=$_POST['historico'];
}else{
    $historico=0;
}


    $sql = "SELECT * FROM "
        ."ren_a_caixa_lancamentos "
        ."WHERE finalizado=1 "
        ."AND ren_a_caixa_lancamentos.`data_ts` >= :inicial "
        ."AND ren_a_caixa_lancamentos.`data_ts` <= :final ";
        if ($historico>0){
            $sql.="AND ren_a_caixa_lancamentos.`historico`=:historico ";
        }
        $sql.="ORDER BY ren_a_caixa_lancamentos.data_ts ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    if ($historico>0) {
        $consulta->bindValue(":historico", $historico);
    }
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $lancamentos = $consulta->fetchAll();
    $sql=null;
    $consulta=null;



?>
<div class="container">

    <?php
        include_once("includes/renascer_cab2.php");
    ?>


    <div class="row">
        <div class="col-12">
            <h3 class="text-uppercase">RELATÓRIO DE LANÇAMENTOS</h3>
            <h5 class="">PERÍODO: <strong><?php echo dataBanco2data($_POST['data_inicial'])." a ".dataBanco2data($_POST['data_final']); ?></strong></h5>
        </div>
    </div>


    <table class="table table-sm text-uppercase">
        <thead class="thead-dark">
            <tr>
                <th class="text-center">TIPO</th>
                <th>VALOR</th>
                <th>QUEM PAGA</th>
                <th>BENEFICIÁRIO</th>
                <th>HISTÓRICO</th>
                <th>CORRESPONDE A</th>
            </tr>
        </thead>

        <tbody>
        <?php
        $total_debito=0;
        $total_credito=0;
        foreach ($lancamentos as $dado){
        if ($dado['tipo']==1){
            $tempcor="success";
            $temptipo="<i class='fas fa-angle-up text-success'></i> Crédito";
            $total_credito+=$dado['valor'];
        }
        if ($dado['tipo']==2){
            $tempcor="danger";
            $temptipo="<i class='fas fa-angle-down text-danger'></i> Débito";
            $total_debito+=$dado['valor'];
        }



        echo "<tr class=''>";

        echo "<td class='text-center'>";
        echo $temptipo;
        echo " - ".dataRetiraHora($dado['finalizado_data']);
        echo "</td>";

        echo "<td style='white-space: nowrap;'>R$ ".number_format($dado['valor'],2, ',', ' ')."</td>";

        echo "<td class=''>";
            echo $dado['quem_paga'];
        echo "</td>";

        echo "<td class=''>";
            echo $dado['quem_recebe'];
        echo "</td>";


        echo "<td class='info'>";
        echo fncgethistorico($dado['historico'])['historico'];
        echo "</td>";

        echo "<td class='info'>";
        echo $dado['descricao'];
        echo "</td>";

        echo "</tr>";

        } ?>

        </tbody>

    </table>

<div class="row">
    <?php
    if ($total_debito>0){
        echo "<div class='col-md-12 border text-right'><strong>R$ ".number_format($total_debito,2, ',', ' ')." em débitos</strong></div>";
    }

    if ($total_credito>0){
        echo "<div class='col-md-12 border text-right'><strong>R$ ".number_format($total_credito,2, ',', ' ')." em créditos</strong></div>";
    }
    ?>

</div>



    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
        <div class="col-12">
            <strong> Concordo com os dados prescritos acima e dou fé.</strong>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura
            </h4>
        </div>
    </div>


</div>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print()
</SCRIPT>