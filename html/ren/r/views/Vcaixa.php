<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_19"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vcaixa'>";
include_once("includes/topo.php");

$contas=fncfontelist();
?>

<main class="container-fluid"><!--todo conteudo-->

    <div class="row">
        <div class="col-md-8">
            <a href="index.php?pg=Vlancamento_editar2" class="btn btn-block btn-primary mb-2" ><i class="fas fa-level-down-alt"></i> Novo</a>
        </div>

        <div class="col-md-4">
            <a href='index.php?pg=Vcaixa_lista' class="btn btn-block btn-info btn-block mb-2"> LANÇAMENTOS</a>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 bg-light text-center">
            <!--            <img class="img-fluid" id="hover1" src="--><?php //echo $env->env_estatico; ?><!--img/silo.png" alt="">-->
<!--            <img class="img-fluid" style="height: 120px;" src="--><?php //echo $env->env_estatico; ?><!--img/jaf2.png" alt="">-->
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm text-left table-striped table-hover">




                        <?php
                        $saldo_total=0;
                        foreach ($contas as $conta){
                            $sql = "SELECT sum(`valor`) FROM ren_a_caixa_lancamentos WHERE status=1 and finalizado=1 and tipo=1 and fonte=? ";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindParam(1,$conta['id']);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $caixaconta1 = $consulta->fetch();

                            $sql = "SELECT sum(`valor`) FROM ren_a_caixa_lancamentos WHERE status=1 and finalizado=1 and tipo=2 and fonte=? ";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindParam(1,$conta['id']);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $caixaconta2 = $consulta->fetch();

                            $saldo=$caixaconta1[0]-$caixaconta2[0];
                            $saldo_total=$saldo_total+($saldo);

                            if ($saldo<0){
                                $cor_temp="text-danger";
                            }else{
                                $cor_temp="text-success";
                            }


                            echo "<tr class='text-uppercase'>";
                            echo "<td><a href='index.php?pg=Vfonte&id={$conta['id']}' class='text-decoration-none'>";
                            echo $conta['titular']. "  ".$conta['banco']. "  ".$conta['agencia']. "  ".$conta['conta']. "  ".$conta['cidade'];
                            echo "</a></td>";
                            echo "<td style='white-space: nowrap;' class='{$cor_temp}'>";
                            echo "R$".number_format($saldo,2, ',', ' ');
                            echo "</td>";
                            echo "</tr>";

                        }
                        ?>


                        <tr class="" id="destacar">
                            <td>SALDO TOTAL</td>
                            <td style="white-space: nowrap;"><?php
                                echo "R$".number_format($saldo_total,2, ',', ' ');
                                ?>
                            </td>
                        </tr>

                        <style type="text/css">
                            #destacar {
                                background: linear-gradient(-45deg, #ee7752, #e73c7e, #23a6d5, #23d5ab);
                                background-size: 400% 400%;
                                -webkit-animation: gradient 15s ease infinite;
                                animation: gradient 15s ease infinite;
                            }

                            @-webkit-keyframes gradient {
                                0% {
                                    background-position: 0% 50%;
                                }
                                50% {
                                    background-position: 100% 50%;
                                }
                                100% {
                                    background-position: 0% 50%;
                                }
                            }

                            @keyframes gradient {
                                0% {
                                    background-position: 0% 50%;
                                }
                                50% {
                                    background-position: 100% 50%;
                                }
                                100% {
                                    background-position: 0% 50%;
                                }
                            }
                        </style>

                    </table>
                </div>
            </div>






            <div class="card">
                <div class="card-body">
                    <table class="table table-sm text-left table-striped table-hover">

                        <?php

                            $sql = "SELECT sum(`valor`) FROM ren_a_caixa_lancamentos WHERE status=1 and finalizado=0 and tipo=1 ";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindParam(1,$conta['id']);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $receber = $consulta->fetch();

                            $sql = "SELECT sum(`valor`) FROM ren_a_caixa_lancamentos WHERE status=1 and finalizado=0 and tipo=2 ";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindParam(1,$conta['id']);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $pagar = $consulta->fetch();

//                            $saldo=$caixaconta1[0]-$caixaconta2[0];


                            $cor_temp="text-success";

                            echo "<tr class='text-uppercase'>";
                            echo "<td>SALDO A RECEBER</td>";
                            echo "<td style='white-space: nowrap;' class='{$cor_temp}'>";
                            echo "R$ ".number_format($receber[0],2, ',', ' ');
                            echo "</td>";
                            echo "</tr>";

                        $cor_temp="text-danger";

                        echo "<tr class='text-uppercase'>";
                        echo "<td>SALDO A PAGAR</td>";
                        echo "<td style='white-space: nowrap;' class='{$cor_temp}'>";
                        echo "R$ ".number_format($pagar[0],2, ',', ' ');
                        echo "</td>";
                        echo "</tr>";

                        ?>

                    </table>
                </div>
            </div>



            <div class="card mt-2">
                <div class="card-header bg-info">
                    EM ABERTO
                </div>
                <div class="card-body">
                    <?php
                    try{
                        $date_temp = date("Y-m-d");
                        $sql="SELECT * FROM ";
                        $sql.="ren_a_caixa_lancamentos ";
                        $sql.="WHERE finalizado=0 and status=1 order by previsao asc";
                        global $pdo;
                        $consulta=$pdo->prepare($sql);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    }catch ( PDOException $error_msg){
                        echo 'Erroff'. $error_msg->getMessage();
                    }
                    $lancamentos=$consulta->fetchAll();
                    ?>

                    <table class="table table-sm table-hover text-uppercase">
                        <thead class="thead-dark">
                        <tr>
                            <th>TIPO</th>
                            <th>VALOR</th>
                            <th>PREVISÃO/HISTÓRICO</th>
                            <th>QUEM PAGA</th>
                            <th>QUEM RECEBE</th>
                            <th>CONTA</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach ($lancamentos as $dado){
                            if ($dado['tipo']==1){$tempcor="success";$temptipo="<i class='fas fa-angle-up text-success'></i> Crédito";}
                            if ($dado['tipo']==2){$tempcor="danger";$temptipo="<i class='fas fa-angle-down text-danger'></i> Débito";}


                            echo "<tr>";

                            echo "<td class=''>";
                            echo $temptipo;
                            echo " - ".dataRetiraHora($dado['data_ts']);
                            echo " <i class='fas fa-info-circle text-info ' title='{$dado['descricao']}'></i>";
                            echo "</td>";

                            echo "<td class=''>R$ ".number_format($dado['valor'],2, ',', ' ')."</td>";

                            if ($dado['finalizado']==1){
                                echo "<td class='info'>";
                            }else {
                                if ($dado['previsao'] <= date('Y-m-d')) {
                                    echo "<td class='text-danger'>";
                                } else {
                                    echo "<td class='text-success'>";
                                }
                            }
                            echo dataBanco2data($dado['previsao']);
                            echo "<br>";
                            echo fncgethistorico($dado['historico'])['historico'];
                            echo "</td>";

                            echo "<td class=''>";
                            echo $dado['quem_paga'];
                            echo "</td>";

                            echo "<td class=''>";
                            echo $dado['quem_recebe'];
                            echo "</td>";

                            echo "<td class=''>";
                            echo fncgetfonte($dado['fonte'])['titular'];
                            echo "</td>";

                            echo "</tr>";

                        }
                        ?>
                        </tbody>

                    </table>
                </div>
            </div>





        </div>



</main>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>