<?php
//gerado pelo geracode
function fncfontelist(){
    $sql = "SELECT * FROM ren_a_fontes ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $fontelista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $fontelista;
}

function fncgetfonte($id){
    $sql = "SELECT * FROM ren_a_fontes WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getren_a_fontes = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getren_a_fontes;
}
?>