<?php
//gerado pelo geracode
//use ucwords(strtolower()) e limpadocumento()
if($startactiona==1 && $aca=="caixa_lancamentodelete"){
    $usuario_off=$_POST["usuario_off"];
    $tabela_id=$_GET["tabela_id"];
    $fechamento_id=$_GET["id"];

    try {
        $sql = "DELETE FROM `ren_a_caixa_lancamentos` WHERE id =:id ";
        global $pdo;
        $exclui = $pdo->prepare($sql);
        $exclui->bindValue(":id", $tabela_id);
        $exclui->execute();
    } catch (PDOException $error_msg) {
        echo 'Erro:' . $error_msg->getMessage();
    }

    $_SESSION['fsh'] = [
        "flash" => "lançamento apagado com sucesso!!",
        "type" => "success",
    ];
    header("Location: index.php?pg=Vfechamento2&id={$fechamento_id}");
    exit();
}


if($startactiona==1 && $aca=="caixa_lancamentodelete2"){
    $usuario_off=$_POST["usuario_off"];
    $tabela_id=$_GET["id"];

    try {
        $sql = "DELETE FROM `ren_a_caixa_lancamentos` WHERE id =:id ";
        global $pdo;
        $exclui = $pdo->prepare($sql);
        $exclui->bindValue(":id", $tabela_id);
        $exclui->execute();
    } catch (PDOException $error_msg) {
        echo 'Erro:' . $error_msg->getMessage();
    }

    $_SESSION['fsh'] = [
        "flash" => "lote apagado com sucesso!!",
        "type" => "success",
    ];
    header("Location: index.php?pg=Vcaixa_lista");
    exit();
}
?>