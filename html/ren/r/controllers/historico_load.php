<?php
//gerado pelo geracode
function fnchistoricolist(){
    $sql = "SELECT * FROM ren_a_historicos ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $historicolista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $historicolista;
}

function fncgethistorico($id){
    $sql = "SELECT * FROM ren_a_historicos WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getren_a_historicos = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getren_a_historicos;
}
?>