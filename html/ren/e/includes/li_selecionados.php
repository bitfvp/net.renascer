<?php
try{
    $sql = "SELECT * from ren_ligas_lotes "
        ."WHERE ren_ligas_lotes.id>0 and ren_ligas_lotes.liga=:liga "
        ."ORDER BY ren_ligas_lotes.data_ts ASC LIMIT 0,1000 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":liga",$_GET['id']);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}
$lotes = $consulta->fetchAll();
$lotes_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<div class="card mt-2">
    <div class="card-header bg-info text-dark text-center py-2">
        LOTES SELECIONADOS
    </div>
    <div class="card-body">

        <table class="table table-sm">
            <thead class="thead-dark">
            <tr>
                <th scope="col">LOTE</th>
                <th scope="col" class="text-center">PRODUTO</th>
                <th scope="col" class="text-center">SACAS</th>
                <th scope="col" class="text-center">VALOR/SACA</th>
            </tr>
            </thead>

            <?php

            // vamos criar a visualização
            $lotes_selecionados = array();
            $infovolumes=0;
            $infovalor=0;
            foreach ($lotes as $dados){

            array_push($lotes_selecionados, $dados["lote"]);

            $id = $dados["id"];
            $data=dataRetiraHora($dados["data_ts"]);
            $usuario=fncgetusuario($dados["usuario"])['nome'];
            $liga=$dados["liga"];
//            $lote=$dados["lote"];
            $sacas=$dados["sacas"];

            $lote=fncgetlote($dados["lote"]);//////////lote x
            $entrada=fncgetentrada($lote["romaneio"]);
            $romaneio = $entrada["romaneio"];
            $romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);
            $letra=fncgetletra($lote["letra"]);
            $tipo_cafe = fncgetprodutos($lote["tipo_cafe"])['abrev'];

            $valor_saca = $lote["valor_saca"];
            $fornecedor = fncgetpessoa($entrada["fornecedor"])['nome'];

            $peso_atual = $lote["peso_atual"];
            $sacas_atual=$peso_atual/60;
            $sacas_atual=number_format($sacas_atual, 2, '.', ',');
            ?>
            <tbody class="">
            <tr class="">
                <th scope="row" id="" style="white-space: nowrap;">
                    <?php
                    echo $romaneio_tipo.$romaneio." ".$letra;
                    ?>
                </th>

                <td class="small text-center">
                    <?php echo $tipo_cafe;?>
                </td>

                <td class="text-center">
                    <?php echo $sacas."v";
                    if ($entradax['status']==1){
                        echo " de ".$sacas_atual."v";
                    }
                        ?>
                </td>

                <td class="text-center">
                    <?php echo $valor_saca."R$";?>
                </td>

            </tr>
            <tr>
                <td colspan="3">
                    <?php echo strtoupper($fornecedor); ?>
                </td>
                <td colspan="3">
                    <?php
                    if ($entradax['status']==1){?>
                        <div class="dropdown hide">
                            <a class="btn btn-outline-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-trash"><br>EXCLUIR</i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Não</a>
                                <a class="dropdown-item bg-danger" href="index.php?pg=Vligas&id=<?php echo $_GET['id']; ?>&aca=apagarligalote&item=<?php echo $id; ?>">Apagar</a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </td>
            </tr>
            <tr class="">
                <td colspan="6" class="py-0 my-0">
                    <hr class="hrgrosso py-0 my-0">
                </td>

            </tr>
            <?php
            $valortemp=$valor_saca*$sacas;

            $infovolumes=$infovolumes+$sacas;
            $infovalor=$infovalor+$valortemp;
            }

            $infovalor1=@($infovalor/$infovolumes);
            $infovalor1=number_format($infovalor1, 2, '.', ',');
            $infovalor2=$infovalor;
            $infovalor2=number_format($infovalor2, 2, '.', ',');
            ?>

            <script>
                $(document).ready(function(){
                    //alert('<?php //echo $infovolumes;?>//');
                    $('#infovolumes').text('<?php echo $infovolumes;?> volumes');
                    $('#infovalor1').text('<?php echo $infovalor1;?> R$');
                    $('#infovalor2').text('<?php echo $infovalor2;?> R$');
                });
            </script>
            </tbody>
        </table>

    </div>
</div>