<blockquote class="blockquote blockquote-success">
    <header>
        CLIENTE:
        <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entradax['cliente'])['nome']); ?>&nbsp;&nbsp;</strong>
    </header>
    <h6>
        CONTRATO:
        <strong class="text-info"><?php echo $entradax['contrato']; ?>&nbsp;&nbsp;</strong><br>

        VALOR ESTIMADO:
        <strong class="text-info"><?php echo number_format($entradax['valor'],2); ?> R$&nbsp;&nbsp;</strong><br>

        DESCRIÇÃO:
        <strong class="text-info"><?php echo $entradax['descricao']; ?>&nbsp;&nbsp;</strong>

        <table class="table table-sm mb-0">
            <thead class="thead-light">
            <tr>
                <th>VOLUMES</th>
                <th>VALOR SACA</th>
                <th>VALOR CALCULADO</th>
            </tr>
            </thead>
            <tbody class="text-danger">
            <tr>
                <td id="infovolumes">Volumes</td>
                <td id="infovalor1"> Valor saca</td>
                <td id="infovalor2"> Valor calculado</td>
            </tr>
            </tbody>
        </table>


        <footer class="blockquote-footer">
            <?php echo fncgetusuario($entradax['usuario'])['nome']?>&nbsp;&nbsp;
        </footer>
</blockquote>