<?php
try{
    $sql = "SELECT * from ren_entradas_lotes "
        ."WHERE ren_entradas_lotes.id>0 and ren_entradas_lotes.peso_atual>0 "
        ."ORDER BY ren_entradas_lotes.tipo_cafe ASC, ren_entradas_lotes.data_ts ASC LIMIT 0,1000 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}
$lotes = $consulta->fetchAll();
$lotes_quant = $consulta->rowCount();
$sql = null;
$consulta = null;



try{
    $sql = "SELECT ren_entradas_lotes.tipo_cafe from ren_entradas_lotes "
        ."WHERE ren_entradas_lotes.id>0 and ren_entradas_lotes.peso_atual>0 "
        ."GROUP BY ren_entradas_lotes.tipo_cafe ORDER BY ren_entradas_lotes.tipo_cafe ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}
$tipos = $consulta->fetchAll();
$sql = null;
$consulta = null;


?>
<div class="card mt-2">
    <div class="card-header bg-light text-dark text-center" style="">
        LOTES PARA LIGA
        <br>
        <?php
        if (isset($_GET['t']) and is_numeric($_GET['t']) and $_GET['t']==1){
            echo "<a href='index.php?pg=Vligas&id={$_GET['id']}' class='btn btn-sm btn-outline-info text-dark' >NENHUM</a>";
            $stilomostrar="display: none;";
            $stilobtn="btn btn-sm btn-outline-info text-dark";
        }else{
            echo "<a href='index.php?pg=Vligas&id={$_GET['id']}&t=1' class='btn btn-sm btn-info text-light' >TODOS</a>";
            $stilomostrar=" ";
            $stilobtn="btn btn-sm btn-info text-light";
        }
        ?>





        <?php
        foreach ($tipos as $tipo){
            $t=fncgetprodutos($tipo[0]);

            echo "<button id='btn_{$tipo[0]}' onclick='mostra_{$tipo[0]}()' class='{$stilobtn}' title='{$t['nome']}'>{$t['abrev']}</button>";
//            echo "<h6 id='bloco_{$tipo[0]}'></h6>";
            echo "
            <script type='text/javascript'>
        function mostra_{$tipo[0]}() {
            var btn_{$tipo[0]} = document.getElementById('btn_{$tipo[0]}');
            if (document.getElementById('lll_{$tipo[0]}').style.display === 'none') {
                $('tr#lll_{$tipo[0]}').toggle(1000);
                btn_{$tipo[0]}.className='btn btn-sm btn-info text-light';
            } else {
                $('tr#lll_{$tipo[0]}').toggle(1000);
                btn_{$tipo[0]}.className='btn btn-sm btn-outline-info text-dark';
            }
        }
    </script>
            ";
        }
        ?>
    </div>

    <div class="card-body">

        <table class="table table-sm">
            <thead class="thead-dark">
            <tr>
                <th scope="col">LOTE</th>
                <th scope="col" class="text-center">PRODUTO</th>
                <th scope="col" class="text-center">SACAS</th>
                <th scope="col" class="text-center">VALOR/SACA</th>
                <th scope="col" class="text-center">DATA</th>
            </tr>
            </thead>

            <?php

//            var_dump($lotes_selecionados);

            // vamos criar a visualização
            foreach ($lotes as $dados){
                $id_l = $dados["id"];
                $entrada=fncgetentrada($dados["romaneio"]);
                $romaneio = $entrada["romaneio"];
                $romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);
                $letra=fncgetletra($dados["letra"]);
                $data_l = $dados["data_ts"];

                $tipo_cafe = fncgetprodutos($dados["tipo_cafe"])['nome'];

                $peso_entrada = $dados["peso_entrada"];
                $sacas_entrada=$peso_entrada/60;
                $sacas_entrada=number_format($sacas_entrada, 1, '.', ',');
                $peso_entrada.=" Kg";
                $bags_entrada = $dados["bags_entrada"];
                $localizacao = $dados["localizacao"];
                $localizacao_obs = $dados["localizacao_obs"];
                $p_bo = $dados["p_bo"];
                $bo = $dados["bo"];
                $peso_atual = $dados["peso_atual"];
                $sacas_atual=$peso_atual/60;
                $sacas_atual=number_format($sacas_atual, 2, '.', ',');
                $peso_atual.=" Kg";
                $bags_atual = $dados["bags_atual"];
                $obs = $dados["obs"];
                $valor_saca = $dados["valor_saca"];
                $fornecedor = fncgetpessoa($entrada["fornecedor"])['nome'];

            $cordalinha = "  ";
            if (!in_array($id_l,$lotes_selecionados)){
                if ($dados['p_bo']==1){
                    $cordalinha = " text-dark bg-warning ";
                }
            }else{
                $cordalinha = " text-light bg-dark ";
            }

            ?>
            <tbody>
            <tr class="<?php echo $cordalinha; ?>" id='lll_<?php echo $dados["tipo_cafe"]; ?>' style="<?php echo $stilomostrar; ?>">
                <th scope="row" id="<?php echo $id_l;  ?>" style="white-space: nowrap;" title="<?php echo $bo;?>">
                    <?php
                    echo $romaneio_tipo.$romaneio." ".$letra;
                    ?>
                </th>

                <td class="small text-center">
                    <?php echo $tipo_cafe;?>
                </td>

                <td class="text-center">
                    <strong class="text-info">
                        <?php echo $sacas_atual."v";?>
                    </strong>
                </td>

                <td class="text-center">
                    <?php echo $valor_saca."R$";?>
                </td>

                <td class="text-center">
                    <?php
                    echo dataRetiraHora($dados["data_ts"]);
                    ?>
                </th>

            </tr>
            <tr class="<?php echo $cordalinha; ?>" id='lll_<?php echo $dados["tipo_cafe"]; ?>' style="<?php echo $stilomostrar; ?>">
                <td>
                    <small>
                        <?php echo strtoupper($fornecedor); ?>
                    </small>
                </td>
                <td colspan="4" style="white-space: nowrap; width: 60%">
                    <?php

                    if (!in_array($id_l,$lotes_selecionados)){?>

                    <form class="form-inline" action="index.php?pg=Vligas&id=<?php echo $_GET['id']; ?>&aca=newligalotea" method="post">
                        <div class="input-group">
                            <input id="lote" type="hidden" class="txt bradius" name="lote" value="<?php echo $id_l; ?>"/>
                            <input autocomplete="off" id="quant" placeholder="" type="number" class="form-control" name="quant" value="" step="0.01" max="<?php echo $sacas_atual;?>" min="1" required />
                            <div class="input-group-append">
                                <span class="input-group-text"> volumes</span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mx-1">SELECIONAR</button>
                        <a class="btn btn-dark mx-1" tabindex="-1" href="index.php?pg=Vligas&id=<?php echo $_GET['id']; ?>&aca=newligaloteb&lote=<?php echo $id_l; ?>&quant=<?php echo $sacas_atual;?>">TUDO</a>
                    </form>

                        <?php
                    }else{
                        echo "<strong class='text-light'>LOTE SELECIONADO</strong>";
                    }
                    ?>

                </td>
            </tr>
            <tr class="" id='lll_<?php echo $dados["tipo_cafe"]; ?>' style="<?php echo $stilomostrar; ?>">
                <td colspan="5">
                    <hr class="hrgrosso">
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>

    </div>
</div>