<?php
$entrada=fncgetentrada($lote['romaneio']);

$romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

if ($lote['peso_atual']==null or $lote['peso_atual']==0 or $lote['peso_atual']==""){
    $cordalinha = "dark ";
}else{
    if ($lote['p_bo']==1){
        $cordalinha = "warning ";
    }
}
$letra=fncgetletra($lote["letra"]);

$id_e = $lote["id"];
$peso_entrada = $lote["peso_entrada"];
$sacas_entrada=$peso_entrada/60;
$sacas_entrada=number_format($sacas_entrada, 2, '.', ',');
$bags_entrada = $lote["bags_entrada"];
$localizacao = $lote["localizacao"];
$localizacao_obs = $lote["localizacao_obs"];
$peso_atual = $lote["peso_atual"];
$sacas_atual=$peso_atual/60;
$sacas_atual=number_format($sacas_atual, 2, '.', ',');
$bags_atual = $lote["bags_atual"];
$p_bo = $lote["p_bo"];
$bo = $lote["bo"];
$obs = $lote["obs"];
$valor_saca = $lote["valor_saca"];
$responsavel = fncgetpessoa($lote["responsavel"])['nome'];
?>

<div class="col-md-5">
    <div class="card">
        <div class="card-header bg-info text-light">
            Dados do lote
        </div>
        <div class="card-body">
            <h6>
                <blockquote class="blockquote blockquote-<?php echo $cordalinha; ?>">
                    <a href="#">
                        <strong class='text-success'><?php echo $romaneio_tipo.$entrada['romaneio']. " ".$letra;?></strong>
                    </a>
                    FORNECEDOR: <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?>&nbsp&nbsp</strong><br>
                    PRODUTO: <strong class="text-info"><?php echo fncgetprodutos($lote['tipo_cafe'])['nome']; ?>&nbsp&nbsp</strong><br>
                    PROVA: <strong class="text-info">
                        <?php
                        if ($lote["verificado"]==0){
                            echo " <i class='text-danger'>NÃO VERIFICADA</i>";
                        }else{
                            echo " <i class='text-success'>VERIFICADA</i>";
                        }
                        ?>
                    </strong><br>
                    <small><small>ENTROU COMO: </small><small class="text-info"><?php echo fncgetprodutos($lote['tipo_cafe_entrada'])['nome']; ?></small></small><br>
                    PESO INICIAL: <strong class="text-success"><?php echo $peso_entrada. "KG ou ".$sacas_entrada." volumes"; ?></strong><br>
                    BIG BAGS INICIAL: <strong class="text-success"><?php echo $bags_entrada." BAGS"; ?></strong><br>
                    LOCALIZAÇÃO: <strong class="text-info"><?php echo fncgetlocal($localizacao)['nome']." ". $localizacao_obs; ?></strong><br>
                    RESPONSÁVEL: <strong class="text-info"><?php echo strtoupper($responsavel); ?></strong><br>

                    PESO ATUAL: <strong class="text-danger"><?php echo $peso_atual. "KG ou ".$sacas_atual." volumes"; ?></strong><br>
                    BIG BAGS ATUAL: <strong class="text-danger"><?php echo $bags_atual." BAGS"; ?></strong><br>
                    <?php
                    if ($p_bo==1){
                        echo "B.O.: <strong class='text-info'>{$bo}</strong><br>";
                    }
                    ?>
                    OBS GERAIS: <strong class="text-info"><?php echo $obs; ?></strong><br>

                    <?php if ($allow["allow_4"]==1){?>
                    VALOR POR SACA: <strong class="text-info"><?php echo $valor_saca; ?> R$</strong><br>
                    <?php } ?>

                    <strong class="text-info" title=""><?php echo datahoraBanco2data($lote['data_ts']); ?>&nbsp;&nbsp;</strong>

                    <?php
                    if ($lote["alterado"]!=0){
                        echo "<br><i class='text-danger'>Esse lote já sofreu alteração e não pode ser excluido ou editado!!</i>";
                    }
                    ?>

                    <footer class="blockquote-footer">
                        <?php
                        $us=fncgetusuario($lote['usuario']);
                        echo $us['nome'];
                        ?>
                    </footer>
                </blockquote>
            </h6>
        </div>
    </div>
</div>