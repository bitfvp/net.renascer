<nav id="navbar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-<?php echo($_SESSION['theme']==1)?"dark":"primary";?> mb-2">
        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" style="height: 50px;" src="<?php echo $env->env_estatico; ?>img/vetor_simples.png" alt="<?php echo $env->env_nome; ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <a class="nav-item nav-link" href="index.php?pg=Vpessoa_lista">CADASTRO GERAL</a>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navpro" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        LOTES
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navpro">
                        <a class="dropdown-item" href="index.php?pg=Vl_lista">Lotes no armazém</a>
                        <?php if ($allow["allow_3"]==1){?>
                        <a class="dropdown-item" href="index.php?pg=Vlp_lista">Aguardando prova</a>
                        <?php } ?>
                        <?php if ($allow["allow_4"]==1){?>
                        <a class="dropdown-item" href="index.php?pg=Vlv_lista">Aguardando valor</a>
                        <?php } ?>
                    </div>
                </li>

                <?php if ($allow["allow_2"]==1){?>
                <a class="nav-item nav-link" href="index.php?pg=Vligas_lista">BLEND'S</a>
                <?php } ?>
                <?php if ($allow["allow_4"]==1){?>
                <a class="nav-item nav-link" href="index.php?pg=Vv_lista">VENDAS</a>
                <?php } ?>
            </ul>

            <?php
            include_once("{$env->env_root}includes/ren/modal_msg_pontos.php");
            ?>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="msgoff()" data-toggle="modal" data-target="#modalmsgs">
                        <?php
                        echo $comp_msg;
                        ?>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modaltemas">
                            <i class="fa fa-tint"></i>
                            Alterar tema
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?PHP echo $env->env_url; ?>?pg=Vlogin"><i class="fa fa-undo"></i> Voltar</a>
                        <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a>
                    </div>
                </li>
            </ul>
        </div>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</nav>
<?php
include_once("{$env->env_root}includes/sessao_relogio.php");