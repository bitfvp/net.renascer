<?php
try{
    $sql = "SELECT "
        ."ren_entradas_lotes.id, "
."ren_entradas_lotes.`status`, "
."ren_entradas_lotes.data_ts, "
."ren_entradas_lotes.usuario, "
."ren_entradas_lotes.alterado, "
."ren_entradas_lotes.romaneio, "
."ren_entradas_lotes.letra, "
."ren_entradas_lotes.tipo_cafe, "
."ren_entradas_lotes.tipo_cafe_entrada, "
."ren_entradas_lotes.peso_entrada, "
."ren_entradas_lotes.bags_entrada, "
."ren_entradas_lotes.localizacao, "
."ren_entradas_lotes.localizacao_obs, "
."ren_entradas_lotes.p_bo, "
."ren_entradas_lotes.bo, "
."ren_entradas_lotes.peso_atual, "
."ren_entradas_lotes.bags_atual, "
."ren_entradas_lotes.obs, "
."ren_entradas_lotes.responsavel, "
."ren_entradas_lotes.verificado, "
."ren_entradas_lotes.valor_saca "
        ."from "
        ."ren_entradas_lotes "
        ."INNER JOIN ren_entradas ON ren_entradas_lotes.romaneio = ren_entradas.id "
        ."INNER JOIN ren_pessoas ON ren_entradas.fornecedor = ren_pessoas.id "
        ."WHERE ren_entradas_lotes.id>0 and ";
        if (isset($_GET['tipo'])and is_numeric($_GET['tipo']) and $_GET['tipo']>0) {
            $sql .="ren_entradas_lotes.tipo_cafe = :tipo AND ";
        }
    if (isset($_GET['fornecedor'])and $_GET['fornecedor']<>"") {
        $sql .="ren_pessoas.nome LIKE :fornecedor AND ";
    }
    if (isset($_GET['romaneio'])and is_numeric($_GET['romaneio']) and $_GET['romaneio']>0) {
        $sql .="ren_entradas.romaneio LIKE :romaneio AND ";
    }
        $sql .="ren_entradas_lotes.peso_atual>0 "
        ."ORDER BY ren_entradas_lotes.tipo_cafe ASC, ren_entradas_lotes.data_ts ASC ";
    if (isset($_GET['tipo']) or isset($_GET['fornecedor']) or isset($_GET['romaneio'])){
        $sql .="LIMIT 0,1000 ";
    }else{
        $sql .="LIMIT 0,0 ";
    }

    global $pdo;
    $consulta = $pdo->prepare($sql);
            if (isset($_GET['tipo'])and is_numeric($_GET['tipo']) and $_GET['tipo']>0) {
                $consulta->bindValue(":tipo", $_GET['tipo']);
            }
    if (isset($_GET['fornecedor'])and $_GET['fornecedor']<>"") {
        $consulta->bindValue(":fornecedor", "%".$_GET['fornecedor']."%");
    }
    if (isset($_GET['romaneio'])and is_numeric($_GET['romaneio']) and $_GET['romaneio']>0) {
        $consulta->bindValue(":romaneio", "%".$_GET['romaneio']."%");
    }
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}
$lotes = $consulta->fetchAll();
$lotes_quant = $consulta->rowCount();
$sql = null;
$consulta = null;



try{
    $sql = "SELECT ren_entradas_lotes.tipo_cafe from ren_entradas_lotes "
        ."WHERE ren_entradas_lotes.id>0 and ren_entradas_lotes.peso_atual>0 "
        ."GROUP BY ren_entradas_lotes.tipo_cafe ORDER BY ren_entradas_lotes.tipo_cafe ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}
$tipos = $consulta->fetchAll();
$sql = null;
$consulta = null;


?>

<div class="card mt-2">
    <div class="card-header bg-light text-dark text-center" style="">
        LOTES PARA LIGA
        <br>

        <form action="index.php" >
            <div class="form-row">

                <input type="hidden" name="pg" value="<?php echo $_GET['pg'];?>">
                <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">

                <div class="form-group col-md-4 mb-0">
                    <input type="text" id="romaneio" name="romaneio" placeholder="Romaneio" class="form-control form-control-sm" value="<?php echo $_GET['romaneio'];?>" >
                </div>
                <script>
                    $(document).ready(function(){
                        $('#romaneio').mask('00000000', {reverse: true});
                    });
                </script>
                <div class="form-group col-md-4 mb-0">
                    <input type="text" id="fornecedor" name="fornecedor" placeholder="Fornecedor" class="form-control form-control-sm" value="<?php echo $_GET['fornecedor'];?>" >
                </div>
                <div class="form-group col-md-4 mb-0">
                    <select class="form-control form-control-sm" id="tipo" name="tipo">
                        <?php
                        if (isset($_GET['tipo']) and is_numeric($_GET['tipo']) and $_GET['tipo']>0){
                            $t1=fncgetprodutos($_GET['tipo']);
                            echo "<option selected value='{$_GET['tipo']}'>{$t1['abrev']}</option>";
                        }else{
                            echo "<option selected value='0'>Todos os tipos</option>";
                        }
                        foreach ($tipos as $tipo){
                            $t2=fncgetprodutos($tipo[0]);
                            echo "<option value='{$tipo[0]}'>{$t2['abrev']}</option>";//{$t2['nome']}
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-6 offset-md-3 mt-1">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search">Buscar</i></button>
                    <a href="index.php?pg=<?php echo $_GET['pg'];?>&id=<?php echo $_GET['id'];?>" class="btn btn-warning"><i class="fas fa-eraser">Limpar</i></a>
                </div>
            </div>

        </form>
    </div>

    <div class="card-body">

        <table class="table table-sm">
            <thead class="thead-dark">
            <tr>
                <th scope="col">LOTE</th>
                <th scope="col" class="text-center">PRODUTO</th>
                <th scope="col" class="text-center">SACAS</th>
                <th scope="col" class="text-center">VALOR/SACA</th>
                <th scope="col" class="text-center">DATA</th>
            </tr>
            </thead>

            <?php

//            var_dump($lotes_selecionados);

            // vamos criar a visualização
            foreach ($lotes as $dados){
                $id_l = $dados["id"];
                $entrada=fncgetentrada($dados["romaneio"]);
                $romaneio = $entrada["romaneio"];
                $romaneio_tipo = fncgetromaneiotipo($entrada["romaneio_tipo"]);
                $letra=fncgetletra($dados["letra"]);
                $data_l = $dados["data_ts"];

                $tipo_cafe = fncgetprodutos($dados["tipo_cafe"])['nome'];

                $peso_entrada = $dados["peso_entrada"];
                $sacas_entrada=$peso_entrada/60;
                $sacas_entrada=number_format($sacas_entrada, 1, '.', ',');
                $peso_entrada.=" Kg";
                $bags_entrada = $dados["bags_entrada"];
                $localizacao = $dados["localizacao"];
                $localizacao_obs = $dados["localizacao_obs"];
                $p_bo = $dados["p_bo"];
                $bo = $dados["bo"];
                $peso_atual = $dados["peso_atual"];
                $sacas_atual=$peso_atual/60;
                $sacas_atual=number_format($sacas_atual, 2, '.', ',');
                $peso_atual.=" Kg";
                $bags_atual = $dados["bags_atual"];
                $obs = $dados["obs"];
                $valor_saca = $dados["valor_saca"];
                $fornecedor = fncgetpessoa($entrada["fornecedor"])['nome'];

            $cordalinha = "  ";
            if (!in_array($id_l,$lotes_selecionados)){
                if ($dados['p_bo']==1){
                    $cordalinha = " text-dark bg-warning ";
                }
            }else{
                $cordalinha = " text-light bg-secondary ";
            }

            ?>
            <tbody>
            <tr class="<?php echo $cordalinha; ?>" id=''>
                <th scope="row" id="<?php echo $id_l;  ?>" style="white-space: nowrap;" title="<?php echo $bo;?>">
                    <?php
                    echo $romaneio_tipo.$romaneio." ".$letra;
                    ?>
                </th>

                <td class="small text-center">
                    <?php echo $tipo_cafe;?>
                </td>

                <td class="text-center">
                    <strong class="text-info">
                        <?php echo $sacas_atual."v";?>
                    </strong>
                </td>

                <td class="text-center">
                    <?php echo $valor_saca."R$";?>
                </td>

                <td class="text-center">
                    <?php
                    echo dataRetiraHora($dados["data_ts"]);
                    ?>
                </th>

            </tr>
            <tr class="<?php echo $cordalinha; ?>" id=''>
                <td>
                    <small>
                        <?php echo strtoupper($fornecedor); ?>
                    </small>
                </td>
                <td colspan="4" style="white-space: nowrap; width: 60%">
                    <?php

                    if (!in_array($id_l,$lotes_selecionados)){?>

                    <form class="form-inline" action="index.php?pg=Vligas&id=<?php echo $_GET['id']; ?>&aca=newligalotea" method="post">
                        <div class="input-group">
                            <input id="lote" type="hidden" class="txt bradius" name="lote" value="<?php echo $id_l; ?>"/>
                            <input autocomplete="off" id="quant" placeholder="" type="number" class="form-control" name="quant" value="" step="0.01" max="<?php echo $sacas_atual;?>" min="1" required />
                            <div class="input-group-append">
                                <span class="input-group-text"> volumes</span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mx-1">SELECIONAR</button>
                        <a class="btn btn-dark mx-1" tabindex="-1" href="index.php?pg=Vligas&id=<?php echo $_GET['id']; ?>&aca=newligaloteb&lote=<?php echo $id_l; ?>&quant=<?php echo $sacas_atual;?>">TUDO</a>
                    </form>

                        <?php
                    }else{
                        echo "<strong class='text-light blink'>LOTE SELECIONADO</strong>";
                    }
                    ?>

                </td>
            </tr>
            <tr class="" id='' >
                <td colspan="5">
                    <hr class="hrgrosso my-0">
                </td>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>

    </div>
</div>