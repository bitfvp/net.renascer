<?php

function fncgetromaneiotipo($id){
    switch ($id){
        case 1:
            $tipo="RM-";
            break;
        case 2:
            $tipo="RE-";
            break;
        case 3:
            $tipo="CT-";
            break;
        case 4:
            $tipo="DE-";
            break;
        case 5:
            $tipo="LI-";
            break;
    }
    return $tipo;
}
