<?php
//metodo
if ($startactiona == 1 && $aca == "apagarligalote") {

    if (isset($_GET['item']) and is_numeric($_GET['item'])) {
        $item = $_GET['item'];
    } else {
        $_SESSION['fsh'] = [
            "flash" => "Houve um erro!!, não encontrado o id da pessoa",
            "type" => "warning",
        ];
        header("Location: {$env->env_url_mod}index.php?pg=Vligas_lista");
        exit();
    }

    try {
        $sql = "DELETE FROM `ren_ligas_lotes` WHERE id = :id ";
        global $pdo;
        $exclui = $pdo->prepare($sql);
        $exclui->bindValue(":id", $item);
        $exclui->execute();
    } catch (PDOException $error_msg) {
        echo 'Erro:' . $error_msg->getMessage();
    }

    $_SESSION['fsh'] = [
        "flash" => "item apagado com sucesso!!",
        "type" => "success",
    ];
    header("Location: index.php?pg=Vligas&id={$_GET['id']}");
    exit();

}

