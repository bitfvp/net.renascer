<?php

function fncgetletra($id){
    switch ($id){
        case 1:
            $letra="A";
            break;
        case 2:
            $letra="B";
            break;
        case 3:
            $letra="C";
            break;
        case 4:
            $letra="D";
            break;
        case 5:
            $letra="E";
            break;
        case 6:
            $letra="F";
            break;
        case 7:
            $letra="G";
            break;
        case 8:
            $letra="H";
            break;
        case 9:
            $letra="I";
            break;
        case 10:
            $letra="J";
            break;
        default:
            $letra="null";
            break;
    }
    return $letra;
}




function fncletralist(){
    $letralista = [
        array(
            "id" => "1",
            "letra" => "A",
            ),
        array(
            "id" => "2",
            "letra" => "B",
        ),
        array(
            "id" => "3",
            "letra" => "C",
        ),
        array(
            "id" => "4",
            "letra" => "D",
        ),
        array(
            "id" => "5",
            "letra" => "E",
        ),
        array(
            "id" => "6",
            "letra" => "F",
        ),
        array(
            "id" => "7",
            "letra" => "G",
        ),
        array(
            "id" => "8",
            "letra" => "H",
        ),
        array(
            "id" => "9",
            "letra" => "I",
        ),
        array(
            "id" => "10",
            "letra" => "J",
        ),
    ];
    return $letralista;
}