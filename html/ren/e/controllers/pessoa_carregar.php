<?php
function fncpessoalist(){
    $sql = "SELECT * FROM ren_pessoas ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $pessoalista;
}

function fncgetpessoa($id){
    $sql = "SELECT * FROM ren_pessoas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getpessoa = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getpessoa;
}

function fncpessoaresponsavellist(){
    $sql = "SELECT * FROM ren_pessoas where p_responsavel=1 ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $pessoalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $pessoalista;
}
?>