<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_2"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $entradax=fncgetligas($_GET['id']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vligas_lista");
    exit();
}
?>
<!--/////////////////////////////////////////////////////-->
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    include_once("includes/li_cab.php");
                    ?>
                    <div class="btn-group" role="group" aria-label="">
                        <?php if ($allow["allow_6"]==1){?>
                            <a href="index.php?pg=Vligas_editar&id=<?php echo $_GET['id']; ?>" title="Editar liga" class="btn btn-sm btn-primary fas fa-pen text-dark">
                                <br>EDITAR
                            </a>
                        <?php } ?>
                        <a href="index.php?pg=Vligas_print1&id=<?php echo $_GET['id']; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-dark fas fa-print">
                            <br>IMPRIMIR
                        </a>

                    </div>
                </div>
                <div class="col-md-12">
                    <?php
                    include_once("includes/li_selecionados.php");
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <?php
            if ($entradax['status']==1){
                include_once("includes/li_lotes.php");
            }

            ?>
        </div>
    </div>

</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>