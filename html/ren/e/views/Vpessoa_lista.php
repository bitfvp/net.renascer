<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Lista de pessoas-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from ren_pessoas WHERE nome LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from ren_pessoas ";
}
// total de registros a serem exibidos por página
$total_reg = "50"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container-fluid"><!--todo conteudo-->
    <h2>Listagem de cadastros</h2>
    <hr>
    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vpessoa_lista" hidden/>
            <input type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Buscar por nome..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
        </div>
    </form>
    <a href="index.php?pg=Vpessoa_editar" class="btn btn btn-success btn-block col-md-6 float-right">
        NOVO CADASTRO
    </a>
    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <table class="table table-striped table-hover table-sm">
        <thead class="thead-dark">
            <tr>
                <th>NOME</th>
                <th>BAIRRO</th>
                <th>CIDADE</th>
                <th class="text-center">PERFIL</th>
                <th class="text-center">AÇÕES</th>
            </tr>
        </thead>
        <tbody>
        <?php
        // vamos criar a visualização
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            $bairro = $dados["bairro"];
            $cidade = $dados["cidade"];
            $perfil="";
            if ($dados["p_motorista"]==1){
                $perfil .= "<i class='badge fas fa-truck text-danger'>Motorista</i>";
            }
            if ($dados["p_cliente"]==1){
                $perfil .= "<i class='badge fas fa-briefcase text-info'>Cliente</i>";
            }
            if ($dados["p_fornecedor"]==1){
                $perfil .= "<i class='badge fas fa-people-carry'>Fornecedor</i>";
            }
            if ($dados["p_corretor"]==1){
                $perfil .= "<i class='badge fas fa-comments-dollar'>Corretor</i>";
            }
            if ($dados["p_responsavel"]==1){
                $perfil .= "<i class='badge fas fa-user-lock text-warning'>Responsável</i>";
            }
            if ($dados["p_transportadora"]==1){
                $perfil .= "<i class='badge fas fa-shipping-fast text-success'>Transportadora</i>";
            }
            ?>
            <tr>
                <td>
                    <a href="index.php?pg=Vpessoa&id=<?php echo $id;?>" class="text-uppercase">
                    <?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $ccc = $nome;
                        $cc = explode(CSA, $ccc);
                        $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                        echo $c;
                    }else{
                        echo $nome;
                    }
                    ?>
                    </a>
                </td>
                <td class="text-uppercase"><?php echo $bairro; ?></td>
                <td class="text-uppercase"><?php echo $cidade; ?></td>
                <td class=""><?php echo $perfil; ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vpessoa_editar&id=<?php echo $id; ?>" title="Editar registro" class="btn btn-sm btn-primary">
                            <i class="fas fa-pen"> EDITAR</i>
                        </a>

                        <?php if ($allow["allow_6"]==1 and $dados["count_rank"]==0){?>
                            <div class="dropdown show">
                                <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-trash"> EXCLUIR</i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="#">Não</a>
                                    <a class="dropdown-item bg-danger" href="index.php?pg=Vpessoa_lista&aca=apagarpessoa&id=<?php echo $id; ?>">Apagar</a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
