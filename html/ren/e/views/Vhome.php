<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_1"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vhome'>";
include_once("includes/topo.php");

$sql = "SELECT sum(`peso_atual`) FROM ren_entradas_lotes WHERE status=1 and peso_atual<>0 ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
$estoquetudo = $consulta->fetch();
?>

<main class="container-fluid"><!--todo conteudo-->


    <div class="row">
        <div class="col-md-4 bg-light text-center">
            <!--            <img class="img-fluid" id="hover1" src="--><?php //echo $env->env_estatico; ?><!--img/silo.png" alt="">-->
            <img class="img-fluid" style="height: 120px;" src="<?php echo $env->env_estatico; ?>img/renascer.png" alt="">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm text-left table-striped table-hover">
                        <tr class="" id="destacar">
                            <td>CAFÉ EM ESTOQUE</td>
                            <td style="white-space: nowrap;"><?php
                                $sacas=$estoquetudo[0]/60;
                                $peso=$estoquetudo[0];
                                echo number_format($sacas,2)."v<br>".number_format($peso,2)."Kg";
                                ?>
                            </td>
                        </tr>

                        <style type="text/css">
                            #destacar {
                                background: linear-gradient(-45deg, #ee7752, #e73c7e, #23a6d5, #23d5ab);
                                background-size: 400% 400%;
                                -webkit-animation: gradient 15s ease infinite;
                                animation: gradient 15s ease infinite;
                            }

                            @-webkit-keyframes gradient {
                                0% {
                                    background-position: 0% 50%;
                                }
                                50% {
                                    background-position: 100% 50%;
                                }
                                100% {
                                    background-position: 0% 50%;
                                }
                            }

                            @keyframes gradient {
                                0% {
                                    background-position: 0% 50%;
                                }
                                50% {
                                    background-position: 100% 50%;
                                }
                                100% {
                                    background-position: 0% 50%;
                                }
                            }
                        </style>



                        <?php
                        foreach (fncprodutoslist() as $produtos){
                            $sql = "SELECT sum(`peso_atual`) FROM ren_entradas_lotes WHERE status=1 and peso_atual<>0 and tipo_cafe=? ";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindParam(1,$produtos['id']);
                            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                            $estoqueportipo = $consulta->fetch();

                            $sacas=$estoqueportipo[0]/60;
                            $peso=$estoqueportipo[0];

                            if ($estoqueportipo[0]==0 or $estoqueportipo[0]==null or $estoqueportipo[0]=="" or !is_numeric($estoqueportipo[0])){
                                $exib= " d-none";
                            }else{
                                $exib= " ";
                            }

                            echo "<tr class='{$exib}'>";
                            echo "<td>";
                            echo $produtos['nome'];
                            echo "</td>";
                            echo "<td style='white-space: nowrap;'>";
                            echo number_format($sacas,2)."v<br>".number_format($peso,2)."Kg";
                            echo "</td>";
                            echo "</tr>";

                        }
                        ?>

                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card mt-2">
                <div class="card-header bg-dark text-light">
                    Caminhões no pátio
                </div>
                <div class="card-body">
                    <?php
                    try{
                        $sql = "SELECT * FROM "
                            ."ren_portaria "
                            ."WHERE "
                            ."ren_portaria.status <> 1 and ren_portaria.tipo=1 ";
                        $sql .="order by ren_portaria.data_ts DESC LIMIT 0,50 ";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->execute();
                        global $LQ;
                        $LQ->fnclogquery($sql);
                    }catch ( PDOException $error_msg){
                        echo 'Erro'. $error_msg->getMessage();
                    }

                    $portarias = $consulta->fetchAll();
                    $portarias_quant = $consulta->rowCount();
                    $sql = null;
                    $consulta = null;
                    ?>

                    <table class="table table-stripe table-sm table-hover table-condensed">
                        <thead class="">
                        <tr>
                            <!--                        <th scope="col" class="text-center">#</th>-->
                            <th scope="col"><small>DATA</small></th>
                            <th scope="col"><small>FORNECEDOR</small></th>
                            <th scope="col"><small>MOTORISTA</small></th>
                            <!--                        <th scope="col"><small>PLACAS</small></th>-->
                            <th scope="col"><small>EMBALAGEM</small></th>
                            <th scope="col"><small>OBS</small></th>
                        </tr>
                        </thead>
                        <tfoot>
                        </tfoot>

                        <tbody>
                        <script>
                            $(function () {
                                $('[data-toggle="tooltip"]').tooltip()
                            })
                        </script>
                        <?php
                        foreach ($portarias as $dados){
                            if ($dados['status']==1){
                                $cordalinha = " text-warning bg-gradient-dark ";
                            }else{
                                $cordalinha = " ";
                            }
                            $por_id = $dados["id"];
                            $motorista = fncgetpessoa($dados["motorista"])['nome'];
                            $placa = $dados["placa"];
                            $fornecedor = fncgetpessoa($dados["fornecedor"])['nome'];
                            $data_ts = datahoraBanco2data($dados["data_ts"]);
                            switch ($dados["tipo_embalagem"]){
                                case 0:
                                    $embalagem="";
                                    break;
                                case 1:
                                    $embalagem="SACARIA";
                                    break;
                                case 2:
                                    $embalagem="BIG BAGS";
                                    break;
                                case 3:
                                    $embalagem="GRANEL";
                                    break;
                                case 4:
                                    $embalagem="MISTA";
                                    break;
                                case 5:
                                    $embalagem="VAZIO";
                                    break;

                                default:
                                    $embalagem="Não selecionada!";
                                    break;
                            }
                            $obs = $dados["obs"];
                            $usuario = $dados["usuario"];
                            ?>

                            <tr id="<?php echo $por_id;?>" class="<?php echo $cordalinha; ?>">
                                <!--                            <th scope="row" id="">-->
                                <!--                                <small>--><?php //echo $por_id; ?><!--</small>-->
                                <!--                            </th>-->
                                <td><?php echo $data_ts; ?></td>
                                <td><?php echo strtoupper($fornecedor); ?></td>
                                <td><?php echo strtoupper($motorista); ?></td>
                                <!--                            <td>--><?php //echo $placa; ?><!--</td>-->
                                <td><?php echo $embalagem; ?></td>
                                <td><?php echo $obs; ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>



</main>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>