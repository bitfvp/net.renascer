<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_3"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vlp_lista'>";
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    $sca = $_GET['sca'];
    $scfornecedor = $_GET['fornecedor'];
    $sctipo_cafe = $_GET['tipo_cafe'];
    $sclocalizacao = $_GET['localizacao'];

    //consulta se ha busca
    $sql_where = "WHERE "
                . "ren_entradas_lotes.verificado=0 AND "
                    . "ren_entradas.romaneio LIKE '%$sca%' AND "
                    . "ren_pessoas.nome LIKE '%$scfornecedor%' AND "
                    . "ren_entradas_lotes.tipo_cafe LIKE '%$sctipo_cafe%' AND "
                    . "ren_entradas_lotes.localizacao LIKE '%$sclocalizacao%' ";
    $sql = "SELECT "
                    . "ren_entradas_lotes.id, "
                    . "ren_entradas.id as id_e, "
                    . "ren_entradas.romaneio, "
                    . "ren_entradas_lotes.data_ts, "
                    . "ren_entradas_lotes.letra, "
                    . "ren_entradas_lotes.tipo_cafe, "
                    . "ren_entradas_lotes.verificado, "
                    . "ren_entradas_lotes.peso_entrada, "
                    . "ren_entradas_lotes.bags_entrada, "
                    . "ren_entradas_lotes.localizacao, "
                    . "ren_entradas_lotes.localizacao_obs, "
                    . "ren_entradas_lotes.p_bo, "
                    . "ren_entradas_lotes.bo, "
                    . "ren_entradas_lotes.peso_atual, "
                    . "ren_entradas_lotes.bags_atual, "
                    . "ren_entradas_lotes.obs, "
                    . "ren_entradas.romaneio_tipo, "
                    . "ren_pessoas.nome AS fornecedor "
                    . "FROM "
                    . "ren_entradas_lotes "
                    . "INNER JOIN ren_entradas ON ren_entradas_lotes.romaneio = ren_entradas.id "
                    . "INNER JOIN ren_pessoas ON ren_entradas.fornecedor = ren_pessoas.id ";

}else {
//consulta se nao ha busca
    $sql_where = "WHERE ren_entradas_lotes.verificado=0 AND ren_entradas_lotes.id>0 and ren_entradas_lotes.peso_atual>0 ";
    $sql = "SELECT "
        . "ren_entradas_lotes.id, "
        . "ren_entradas.id as id_e, "
        . "ren_entradas.romaneio, "
        . "ren_entradas_lotes.data_ts, "
        . "ren_entradas_lotes.letra, "
        . "ren_entradas_lotes.tipo_cafe, "
        . "ren_entradas_lotes.verificado, "
        . "ren_entradas_lotes.peso_entrada, "
        . "ren_entradas_lotes.bags_entrada, "
        . "ren_entradas_lotes.localizacao, "
        . "ren_entradas_lotes.localizacao_obs, "
        . "ren_entradas_lotes.p_bo, "
        . "ren_entradas_lotes.bo, "
        . "ren_entradas_lotes.peso_atual, "
        . "ren_entradas_lotes.bags_atual, "
        . "ren_entradas_lotes.obs, "
        . "ren_entradas.romaneio_tipo, "
        . "ren_pessoas.nome AS fornecedor "
        . "FROM "
        . "ren_entradas_lotes "
        . "INNER JOIN ren_entradas ON ren_entradas_lotes.romaneio = ren_entradas.id "
        . "INNER JOIN ren_pessoas ON ren_entradas.fornecedor = ren_pessoas.id ";
}
// total de registros a serem exibidos por página
$total_reg = "500"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
//    $sql_orderby= "ORDER BY ren_entradas_lotes.peso_atual DESC,ren_entradas_lotes.romaneio DESC LIMIT $inicio,$total_reg";
    $sql_orderby= "ORDER BY ren_entradas_lotes.data_ts DESC LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql_where.$sql_orderby);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    global $pdo;
    $todos=$pdo->prepare($sql.$sql_where);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>

<div class="container-fluid">
    <form action="index.php" method="get" class="col-md-10" >
        <div class="input-group input-group-lg mb-3 float-left" >
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search"></i></button>
            </div>
            <input name="pg" value="Vlp_lista" hidden/>
            <input style="text-transform:lowercase;" type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control " placeholder="Buscar por romaneio..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <input type="search" autocomplete="off" class="form-control" id="fornecedor" name="fornecedor" placeholder="Fornecedor..." value="<?php if (isset($_GET['fornecedor'])) {echo $_GET['fornecedor'];} ?>">
            <select name="tipo_cafe" id="tipo_cafe" class="form-control">
                <?php
                if (isset($_GET['tipo_cafe']) and is_numeric($_GET['tipo_cafe'])){
                    $gettipo_cafe=fncgetprodutos($_GET['tipo_cafe']);
                    echo "<option value='{$gettipo_cafe['id']}'>{$gettipo_cafe['nome']}</option>";
                }else{
                    echo "<option value=''>Produto...</option>\n";
                }

                foreach (fncprodutoslist() as $item) {
                    echo "<option value='{$item['id']}'>{$item['nome']}</option>\n";
                }
                ?>
            </select>
            <select name="localizacao" id="localizacao" class="form-control">
                <?php
                if (isset($_GET['localizacao']) and is_numeric($_GET['localizacao'])){
                    $getlocalizacao=fncgetlocal($_GET['localizacao']);
                    echo "<option value='{$getlocalizacao['id']}'>{$getlocalizacao['nome']}</option>";
                }else{
                    echo "<option value=''>Localização...</option>\n";
                }

                foreach (fnclocaislist() as $item) {
                    echo "<option value='{$item['id']}'>{$item['nome']}</option>\n";
                }
                ?>
            </select>
            <div class="input-group-append">
                <a href="index.php?pg=Vlp_lista" class="btn btn-outline-dark"><i class="fas fa-eraser"></i></a>
            </div>

        </div>
    </form>
</div>

<script type="text/javascript">
    function selecionaTexto()
    {
        document.getElementById("sca").select();
    }
    window.onload = selecionaTexto();
</script>


<div class="container-fluid">
    <table class="table table-striped table-hover table-sm table-responsive-md">
        <thead>
        <tr>

            <th colspan="4">
                <script type="text/javascript">
                    function mostra_rm() {
                        var btn_rm = document.getElementById("btn_rm");
                        if (document.getElementById('RM-').style.display === "none") {
                            // document.getElementById('bloco_pb').style.display = "";
                            $('tr#RM-').toggle(1000);
                            btn_rm.className='btn btn-sm btn-success mx-1';
                        } else {
                            // document.getElementById('bloco_pb').style.display = "none";
                            $('tr#RM-').toggle(1000);
                            btn_rm.className='btn btn-sm btn-outline-success mx-1';
                        }
                    }
                    function mostra_re() {
                        var btn_re = document.getElementById("btn_re");
                        if (document.getElementById('RE-').style.display === "none") {
                            // document.getElementById('RE-').style.display = "";
                            $('tr#RE-').toggle(1000);
                            btn_re.className='btn btn-sm btn-success mx-1';
                        } else {
                            // document.getElementById('RE-').style.display = "none";
                            $('tr#RE-').toggle(1000);
                            btn_re.className='btn btn-sm btn-outline-success mx-1';
                        }
                    }
                    function mostra_ct() {
                        var btn_ct = document.getElementById("btn_ct");
                        if (document.getElementById('CT-').style.display === "none") {
                            // document.getElementById('bloco_ct').style.display = "";
                            $('tr#CT-').toggle(1000);
                            btn_ct.className='btn btn-sm btn-success mx-1';
                        } else {
                            // document.getElementById('bloco_ct').style.display = "none";
                            $('tr#CT-').toggle(1000);
                            btn_ct.className='btn btn-sm btn-outline-success mx-1';
                        }
                    }
                    function mostra_lg() {
                        var btn_lg = document.getElementById("btn_lg");
                        if (document.getElementById('LG-').style.display === "none") {
                            // document.getElementById('bloco_lg').style.display = "";
                            $('tr#LG-').toggle(1000);
                            btn_lg.className='btn btn-sm btn-success mx-1';
                        } else {
                            // document.getElementById('bloco_li').style.display = "none";
                            $('tr#LG-').toggle(1000);
                            btn_lg.className='btn btn-sm btn-outline-success mx-1';
                        }
                    }
                </script>
                <button id="btn_rm" onclick="mostra_rm()" class="btn btn-sm btn-success mx-1">RM</button>
                <button id="btn_re" onclick="mostra_re()" class="btn btn-sm btn-success mx-1">RE</button>
                <button id="btn_ct" onclick="mostra_ct()" class="btn btn-sm btn-success mx-1">CT</button>
                <button id="btn_lg" onclick="mostra_lg()" class="btn btn-sm btn-success mx-1">LG</button>

                <!--apenas pra funcao funcionar e o botao ficar translucido-->
                <h6 id="bloco_rm"></h6>
                <h6 id="bloco_re"></h6>
                <h6 id="bloco_ct"></h6>
                <h6 id="bloco_lg"></h6>
                <!--apenas pra funcao funcionar e o botao ficar translucido-->
            </th>

            <th scope="row" colspan="2">
                <nav aria-label="Page navigation example">
                    <ul class="pagination pagination-sm mb-0">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vlp_lista&sca={$_GET['sca']}&fornecedor={$_GET['fornecedor']}&tipo_cafe={$_GET['tipo_cafe']}&localizacao={$_GET['localizacao']}&pgn={$anterior}'><span aria-hidden='true'>← Anterior</a></li> ";
                        }
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vlp_lista&sca={$_GET['sca']}&fornecedor={$_GET['fornecedor']}&tipo_cafe={$_GET['tipo_cafe']}&localizacao={$_GET['localizacao']}&pgn={$proximo}'>Próximo →</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>

            <th colspan="3" class="text-info text-right"><?php echo $tr;?> lote(s)</th>
        </tr>
        </thead>
        <thead class="thead-dark">
        <tr>
            <th scope="col">LOTE</th>
            <th scope="col">DATA</th>
            <th scope="col">FORNECEDOR</th>
            <th scope="col">PRODUTO</th>
            <th scope="col">PROVA</th>
            <th class="text-center">VOLUMES</th>
            <th scope="col">LOCALIZAÇÃO</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="6">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        // agora vamos criar os botões "Anterior e próximo"
                        $anterior = $pc -1;
                        $proximo = $pc +1;
                        if ($pc>1) {
                            echo " <li class='page-item mr-2'><a class='page-link' href='index.php?pg=Vlp_lista&sca={$_GET['sca']}&fornecedor={$_GET['fornecedor']}&tipo_cafe={$_GET['tipo_cafe']}&localizacao={$_GET['localizacao']}&pgn={$anterior}'><span aria-hidden='true'>← Anterior</a></li> ";
                        }
                        if ($pc<$tp) {
                            echo " <li class='page-item'><a class='page-link' href='index.php?pg=Vlp_lista&sca={$_GET['sca']}&fornecedor={$_GET['fornecedor']}&tipo_cafe={$_GET['tipo_cafe']}&localizacao={$_GET['localizacao']}&pgn={$proximo}'>Próximo →</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
            </th>
            <th colspan="3" class="text-info text-right"><?php echo $tr;?> lote(s)</th>
        </tr>
        </tfoot>

        <?php
        if(isset($_GET['sca']) and $_GET['sca']!="") {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        if(isset($_GET['fornecedor']) and $_GET['fornecedor']!="") {
            $st2 = strtoupper($_GET['fornecedor']);
            define('CSB', $st2);//TESTE
        }

        // vamos criar a visualização
        while ($dados =$limite->fetch()){
        $cordalinha = "  ";
        if ($dados['peso_atual']==null or $dados['peso_atual']==0 or $dados['peso_atual']==""){
            $cordalinha = " text-warning bg-dark ";
        }else{
            if ($dados['p_bo']==1){
                $cordalinha = " text-dark bg-warning ";
            }
        }

        $id_l = $dados["id"];
        $romaneio = $dados["romaneio"];
        $romaneio_tipo=fncgetromaneiotipo($dados["romaneio_tipo"]);

        $letra=fncgetletra($dados["letra"]);

        $entrada_id = $dados["id_e"];
        $data_l = $dados["data_ts"];

        $tipo_cafe = $dados["tipo_cafe"];
        $tipo_cafe_entrada = $dados["tipo_cafe_entrada"];
        if ($dados["verificado"]==0){
            $bebida="<small class='text-danger'>NÃO VERIFICADA</small>";
        }else{
            $bebida="<small class='text-success'>VERIFICADA</small>";
        }
        $peso_entrada = $dados["peso_entrada"];
        $sacas_entrada=$peso_entrada/60;
        $sacas_entrada=number_format($sacas_entrada, 1, '.', ',');
        $peso_entrada.=" Kg";
        $bags_entrada = $dados["bags_entrada"];
        $localizacao = $dados["localizacao"];
        $localizacao_obs = $dados["localizacao_obs"];
        $p_bo = $dados["p_bo"];
        $bo = $dados["bo"];
        $peso_atual = $dados["peso_atual"];
        $sacas_atual=$peso_atual/60;
        $sacas_atual=number_format($sacas_atual, 2, '.', ',');
        $peso_atual.=" Kg";
        $bags_atual = $dados["bags_atual"];
        $obs = $dados["obs"];
        $fornecedor = strtoupper($dados["fornecedor"]);

        ?>
        <tbody>
        <tr class="small <?php echo $cordalinha; ?>" id='<?php echo $romaneio_tipo; ?>'>
            <th scope="row" id="<?php echo $id_l;  ?>" style="white-space: nowrap;">
                <a href="?pg=Vl&id_l=<?php echo $id_l;?>" title="Ver">
                    <?php

                    if(isset($_GET['sca']) and $_GET['sca']!="") {
                        $sta = CSA;
                        $nnn = $romaneio;
                        $nn = explode(CSA, $nnn);
                        $n = implode("<span class='text-danger'>{$sta}</span>", $nn);
                        echo $romaneio_tipo.$n." ".$letra;
                    }else{
                        echo $romaneio_tipo.$romaneio." ".$letra;
                    }
                    ?>
                </a>
            </th>
            <td>
                    <?php
                    echo datahoraBanco2data($dados["data_ts"]);
                    ?>
            </th>
            <td>
                <?php
                    if (isset($_GET['fornecedor']) and $_GET['fornecedor']!="") {
                        $stb = CSB;
                        $ccc = $fornecedor;
                        $cc = explode(CSB, $ccc);
                        $c = implode("<span class='text-danger'>{$stb}</span>", $cc);
                        echo $c;
                    } else {
                        echo $fornecedor;
                    }
                ?>
            </td>
            <td>
                <?php echo fncgetprodutos($tipo_cafe)['abrev'];?>
            </td>

            <td>
                <?php echo $bebida;?>
            </td>

            <td class="text-center" style="white-space: nowrap;">
                <?php echo $peso_entrada. ", ".$sacas_entrada."v <i class='fas fa-angle-double-right'></i> ".$peso_atual. ", ".$sacas_atual."v";?>
            </td>


            <td>
                <?php echo fncgetlocal($localizacao)['nome']. " ". $localizacao_obs;?>
            </td>

        </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
</div>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>