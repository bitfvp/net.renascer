<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_4"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT * FROM "
        ."ren_vendas "
        ."WHERE "
        ."ren_vendas.id <> 0 ";
    if (isset($_GET['scb']) and $_GET['scb']!=0 and $_GET['scb']!='') {
        $inicial=$_GET['scb'];
        $inicial.=" 00:00:01";
        $final=$_GET['scb'];
        $final.=" 23:59:59";

        $sql .=" AND ((ren_vendas.data_ts)>=:inicial) And ((ren_vendas.data_ts)<=:final) ";
    }else{
        $inicial=date("Y-m-d",strtotime("-30 days"));
        $inicial.=" 00:00:01";
        $final=date("Y-m-d");;
        $final.=" 23:59:59";
        $sql .=" AND ((ren_vendas.data_ts)>=:inicial) And ((ren_vendas.data_ts)<=:final) ";
    }
    if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
        $sql .="AND ren_vendas.contrato LIKE :contrato ";
    }
    $sql .="order by ren_vendas.data_ts DESC LIMIT 0,50 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    if (isset($_GET['scb']) and $_GET['scb']!=0 and $_GET['scb']!='') {
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
    }else{
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
    }
    if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
        $consulta->bindValue(":contrato", "%".$_GET['sca']."%");
    }
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container-fluid"><!--todo conteudo-->
    <h3 class="form-cadastro-heading">Vendas</h3>
    <hr>

    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-6 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
            </div>
            <input name="pg" value="Vv_lista" hidden/>
            <input type="number" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Contrato..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <input type="date" name="scb" id="scb" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
        </div>
    </form>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>


    <table class="table table-sm table-stripe table-hover table-condensed">
        <thead class="thead-dark">
        <tr>
            <th scope="col"><small>CONTRATO</small></th>
            <th scope="col"><small>CLIENTE</small></th>
            <th scope="col"><small>TRANSPORTADORA</small></th>
            <th scope="col"><small>DATA</small></th>
            <th scope="col" class="text-center"><small>INFO</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="5" >
            </th>
            <td colspan="4"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        foreach ($entradas as $dados){
            $ve_id = $dados["id"];
            $prefixo = $dados["prefixo"];
            $contrato = $dados["contrato"];
            $cliente = fncgetpessoa($dados["cliente"])['nome'];
            $transportadora = fncgetpessoa($dados["transportadora"])['nome'];
            $data_pedido = $dados["data_pedido"];
            $data_ts=datahoraBanco2data($dados["data_ts"]);
            $usuario = $dados["usuario"];

            if ($dados["status"]==0 or $dados["status"]==""){
                $status= "<i class='fas fa-lock fa-2x' title='venda finalizada'></i>";
            }else{
                $status= "";
            }
            ?>

            <tr id="<?php echo $ve_id;?>" class="bg-info">
                <th scope="row" id="">
                        <?php echo $prefixo."-".$contrato; ?>
                </th>
                <td><?php echo strtoupper($cliente); ?></td>
                <td><?php echo strtoupper($transportadora); ?></td>
                <td><?php echo $data_ts; ?></td>
                <td><?php echo $status; ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vvvv_print1&id_v=<?php echo $ve_id; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-dark fas fa-print">
                            <br>IMPRIMIR<br>COMPROVANTE
                        </a>
                    </div>

                </td>

            </tr>

<?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>