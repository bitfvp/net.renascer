<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_2"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vligas_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT * FROM "
        ."ren_ligas "
        ."WHERE "
        ."ren_ligas.id <> 0 "
        ."order by ren_ligas.data_ts DESC LIMIT 0,500 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container-fluid"><!--todo conteudo-->
    <h3 class="form-cadastro-heading">Blend's</h3>
    <hr>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <div class="row">
        <div class="col-md-5">
            <a href="index.php?pg=Vligas_editar" class="btn btn-block btn-success mb-2" ><i class="fas fa-tasks"></i> Novo blend</a>
        </div>
        <div class="col-md-5">
            <a href="index.php?pg=Vrel_1print&order=tipo" class="btn btn-block btn-info mb-2" target="_blank">Estoque atual para composição de blend</a>
        </div>
    </div>

    <table class="table table-sm table-stripe table-hover table-condensed">
        <thead class="thead-dark">
        <tr>
            <th scope="col"><small>CLIENTE</small></th>
            <th scope="col"><small>CONTRATO</small></th>
            <th scope="col"><small>DESC</small></th>
            <th scope="col"><small>VALOR</small></th>
            <th scope="col"><small>INFO</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="4" >
            </th>
            <td colspan="4"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        foreach ($entradas as $dados){
            $id_li = $dados["id"];
            $cliente = fncgetpessoa($dados["cliente"])['nome'];
            $data_ts = $dados["data_ts"];
            $contrato = $dados["contrato"];
            $descricao = $dados["descricao"];
            $valor = $dados["valor"];
            $usuario = $dados["usuario"];
            if ($dados["status"]==0 or $dados["status"]==""){
                $status= "<i class='fas fa-lock fa-2x' title='blend finalizada'></i>";
            }else{
                $status= "";
            }

            ?>

            <tr id="<?php echo $id_li;?>" class="bg-gradient-success">
<!--                <th scope="row" id="">-->
<!--                    <small>--><?php //echo $id_li; ?><!--</small>-->
<!--                </th>-->
                <td><?php echo strtoupper($cliente); ?></td>
                <td><?php echo $contrato; ?></td>
                <td><?php echo $descricao; ?></td>
                <td style='white-space: nowrap;'><?php echo number_format($valor,2); ?> R$</td>
                <td><?php echo $status; ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vligas&id=<?php echo $id_li; ?>" title="Abrir liga" class="btn btn-sm btn- btn-warning fab fa-searchengin text-dark">
                            <br>ABRIR
                        </a>
                        <?php if ($allow["allow_2"]==1){?>
                            <a href="index.php?pg=Vligas_editar&id=<?php echo $id_li; ?>" title="Editar entrada" class="btn btn-sm btn-primary fas fa-pen text-dark">
                                <br>EDITAR
                            </a>
                        <?php } ?>
                        <a href="index.php?pg=Vligas_print1&id=<?php echo $id_li; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-dark fas fa-print">
                            <br>IMPRIMIR
                        </a>
                    </div>

                </td>
            </tr>

<?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>