<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_2"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="ligassave";
    $entrada=fncgetligas($_GET['id']);
}else{
    $a="ligasnew";
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

    $(document).ready(function () {
        $('#serchcliente input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultcliente");
            if (inputValf.length) {
                $.get("includes/cliente.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultcliente p", function () {
            $(this).parents("#serchcliente").find('input[type="text"]').val($(this).text());
            var cliente = this.id;
            $('#cliente').val(cliente);
            $(this).parent(".resultcliente").empty();
        });
    });


</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vligas_editar&aca={$a}"; ?>" method="post" id="form1">
        <h3 class="form-cadastro-heading">CADASTRO DE BLEND</h3>
        <hr>

        <div class="row">
            <div class="col-md-12" id="serchcliente">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $entrada['id']; ?>"/>
                <label for="cliente">CLIENTE</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_cliente_id=$entrada['cliente'];
                        $v_cliente=fncgetpessoa($entrada['cliente'])['nome'];

                    }else{
                        $v_cliente_id="";
                        $v_cliente="";
                    }
                    $c_cliente = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="c<?php echo $c_cliente;?>" id="c<?php echo $c_cliente;?>" value="<?php echo $v_cliente; ?>" placeholder="Click no resultado ao aparecer" required/>
                    <input id="cliente" autocomplete="false" type="hidden" class="form-control" name="cliente" value="<?php echo $v_cliente_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_cliente">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultcliente"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_cliente").click(function(ev){
                            document.getElementById('cliente').value='';
                            document.getElementById('c<?php echo $c_cliente;?>').value='';
                        });
                    });
                </script>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6">
                <label for="contrato">CONTRATO</label>
                <input autocomplete="off" id="contrato" type="text" class="form-control" name="contrato" value="<?php echo $entrada['contrato']; ?>" placeholder="informações referente ao contrato"/>
            </div>

            <div class="col-md-4">
                <label for="valor">VALOR ESTIMADO:</label>
                <div class="input-group">
                    <input autocomplete="off" id="valor" placeholder="" type="text" class="form-control" name="valor" value="<?php echo $entrada['valor']; ?>"  />
                    <div class="input-group-append">
                        <span class="input-group-text">,00 R$</span>
                    </div>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#valor').mask('00.000.000', {reverse: true});
                    });
                </script>
            </div>

            <div class="col-md-12">
                <label for="descricao">DESCRIÇÃO</label>
                <input autocomplete="off" id="descricao" type="text" class="form-control" name="descricao" value="<?php echo $entrada['descricao']; ?>" placeholder=""/>
            </div>


            <div class="col-md-12">
                <label for="status">STATUS:</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="r_corretor">
                            <i class="fa fas fa-unlock-alt text-success"></i>
                        </span>
                    </div>
                    <select name="status" id="status" class="form-control" required>
                        <option selected="" value="<?php
                        if(is_null($entrada['status'])){
                            echo 1;
                        }
                        if($entrada['status']=="0"){
                            echo 0;
                        }
                        if($entrada['status']=="1"){
                            echo 1;
                        }
                        ?>">
                            <?php
                            if(is_null($entrada['status'])){echo"ATIVO";}
                            if($entrada['status']=="0"){echo"FECHADO";}
                            if($entrada['status']=="1"){echo"ATIVO";} ?>
                        </option>
                        <option value="0">FECHADO</option>
                        <option value="1">ATIVO</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" name="gogo" id="gogo" class="btn btn-block btn-success mt-4">
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>

    </form>
</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>