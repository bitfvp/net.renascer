<?php
class Ligas{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncligasnew(
        $status,$cliente,$contrato,$descricao,$valor
    ){
        //tratamento das variaveis
        //não ter


            //inserção no banco
                try {
                    $sql="INSERT INTO ren_ligas ";
                    $sql .= "(id,
                    usuario,
                    status,
                    cliente,
                    contrato,
                    descricao,
                    valor
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :usuario,
                    :status,
                    :cliente,
                    :contrato,
                    :descricao,
                    :valor
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":usuario", $_SESSION['id']);
                    $insere->bindValue(":status", $status);
                    $insere->bindValue(":cliente", $cliente);
                    $insere->bindValue(":contrato", $contrato);
                    $insere->bindValue(":descricao", $descricao);
                    $insere->bindValue(":valor", $valor);
                    $insere->execute();
                } catch (PDOException $error_msg) {
                    echo 'Error' . $error_msg->getMessage();
                }



        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM ";
                $sql.="ren_ligas";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vligas&id={$maid}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncligasedit(
        $id,$status,$cliente,$contrato,$descricao,$valor
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_ligas ";
            $sql.="WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);


        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_ligas ";
                $sql.="SET ";
                $sql .= "status=:status, cliente=:cliente,
                    contrato=:contrato,
                    descricao=:descricao, valor=:valor
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":status", $status);
                $atualiza->bindValue(":cliente", $cliente);
                $atualiza->bindValue(":contrato", $contrato);
                $atualiza->bindValue(":descricao", $descricao);
                $atualiza->bindValue(":valor", $valor);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa entrada cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vligas&id={$id}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }

}
