<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_1"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Editar local-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="localupdate";
    $local=fncgetlocal($_GET['id']);
}else{
    $a="localinsert";
}
?>
<main class="container">
    <form class="form-signin" action="<?php echo "index.php?pg=Vlocal_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de local de armazenamento</h3>
        <hr>
        <div class="row">
            <input id="id" type="hidden" class="" name="id" value="<?php echo $local['id']; ?>"/>
            <div class="col-md-12">
                <label for="nome">Local:</label>
                <input type="text" name="nome" id="nome" class="form-control" autocomplete="off" placeholder="" value="<?php echo $local['nome']; ?>"/>
            </div>


            <div class="col-md-12">
                <div class="d-grid gap-2">
                    <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success my-2" value="SALVAR"/>
                </div>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>