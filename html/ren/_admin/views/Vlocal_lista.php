<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes

    }
}

$page="Lista de locais-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['sca'])){
    //consulta se ha busca
    $sql = "select * from ren_locais WHERE nome LIKE '%$sca%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from ren_locais ";
}
// total de registros a serem exibidos por página
$total_reg = "50"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
$pgn=$_GET['pgn'];
if (!$pgn) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute(); global $LQ; $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas
?>
<main class="container">
    <h2>Locais de armazenamento</h2>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <form action="index.php" method="get" >
                <div class="input-group input-group-lg mb-3" >
                    <button class="btn btn-outline-success" type="submit"><i class="fas fa-search"></i></button>
                    <input name="pg" value="Vlocal_lista" hidden/>
                    <input style="text-transform:lowercase;" type="text" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control " placeholder="Buscar por local..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <div class="d-grid gap-2">
                <a href="index.php?pg=Vlocal_editar" class="btn btn-success btn-lg">
                    NOVO LOCAL
                </a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function selecionaTexto() {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <table class="table table-striped table-hover table-sm">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>LOCAL</th>
                <th class="text-center">AÇÕES</th>
            </tr>
        </thead>
        <tbody>
        <?php
        // vamos criar a visualização
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        while ($dados =$limite->fetch()){
            $id = $dados["id"];
            $nome = strtoupper($dados["nome"]);
            ?>
            <tr>
                <td class="text-center">
                    <?php echo $id; ?>
                </td>
                <td>
                    <a href="#" class="text-uppercase">
                    <?php
                    if($_GET['sca']!="" and isset($_GET['sca'])) {
                        $sta = CSA;
                        $ccc = $nome;
                        $cc = explode(CSA, $ccc);
                        $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                        echo $c;
                    }else{
                        echo $nome;
                    }
                    ?>
                    </a>
                </td>

                <td class="text-center">
                    <?php if ($dados['bloqueado']!=1){?>

                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vlocal_editar&id=<?php echo $id; ?>" title="Editar registro" class="btn btn-sm btn-primary">
                            <i class="fas fa-pen"><br>EDITAR</i>
                        </a>
                    </div>
                    <?php }?>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

</main>
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>
