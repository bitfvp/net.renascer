<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
//        if ($allow["admin"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }
    }
}


$page="Acessos ao sistema-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <div class="col-md-12">
            <!-- =============================começa conteudo======================================= -->
            <?php
            // Prepare a select statement
            $sql = "SELECT * FROM tbl_users where matriz=1 AND status=1 ORDER BY nome ASC ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $frase = $consulta->fetchall();
            $cont = $consulta->rowCount();
            $sql=null;
            $consulta=null;
            ?>
            <div class="card">
                <div class="card-header bg-info text-light">
                    Dados
                </div>
                <div class="card-body">
                    <table class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th scope="col">NOME</th>
                            <th scope="col">NICK</th>
                            <th scope="col">E-MAIL</th>
                            <th scope="col">NASCIMENTO</th>
                            <th scope="col">CPF</th>

                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach ($frase as $tx){

                            echo "<tr class='text-dark'>";
                            echo "<td>{$tx['nome']}</td>";
                            echo "<th>{$tx['nick']}</th>";
                            echo "<th>{$tx['email']}</th>";

                            echo "<th>";
                            if($tx['nascimento']!="1900-01-01" and $tx['nascimento']!="" and $tx['nascimento']!="1000-01-01") {
                                echo "<span class='text-info'>";
                                echo dataBanco2data ($tx['nascimento']);
                                echo " <i class='text-success'>".Calculo_Idade($tx['nascimento'])." anos</i>";
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            echo "</th>";

                            echo "<th>";
                            if($tx['cpf']!="") {
                                echo "<span class='text-info'>";
                                echo mask($tx['cpf'],'###.###.###-##');
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            echo "</th>";

                            echo "</tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- =============================fim conteudo======================================= -->
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>