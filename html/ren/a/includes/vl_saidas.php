<div class="col-md-12">
    <?php
    $sql = "SELECT * \n"
        . "FROM ren_entradas_lotes_saidas \n"
        . "WHERE ((ren_entradas_lotes_saidas.lote)=?)\n"
        . "ORDER BY ren_entradas_lotes_saidas.data_ts DESC";
    global $pdo;
    $cons = $pdo->prepare($sql);
    $cons->bindParam(1, $_GET['id_l']);
    $cons->execute(); global $LQ; $LQ->fnclogquery($sql);
    $saidas = $cons->fetchAll();
    $sql = null;
    $consulta = null;
    ?>
    <!-- tabela -->
    <table id="tabela" class="table table-hover table-sm">
        <thead class="bg-info">
        <tr>
            <th>TIPO DE SAÍDA</th>
            <th class="text-center">DATA</th>
            <th class="text-center">PESO</th>
            <th class="text-center">DESTINO</th>
            <th class="text-center">USUÁRIO</th>
            <th class="text-center">AÇÕES</th>
        </tr>
        </thead>
        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <tbody class="bg-success">
        <?php
        // vamos criar a visualização
        //1 venda
        //2ct
        //3 devolucao
        foreach ($saidas as $dados) {
            $id = $dados["id"];
            $lote = $dados["lote"];
            switch ($dados["tipo_saida"]){
                case 1:
                    $tipo_saida="Venda";
                    break;
                case 2:
                    $tipo_saida="Maquinação";
                    break;
                case 3:
                    $tipo_saida="Devolução";
                    break;
                case 4:
                    $tipo_saida="Liga interna";
                    break;
            }
            $sacas=$dados["peso"]/60;
            $sacas=number_format($sacas, 1, '.', ',');
            $peso=$dados["peso"]." Kg";
            $destino = $dados["id_destino"];
            $responsavel = fncgetusuario($dados['usuario'])['nome'];

            switch ($dados["tipo_saida"]){
                case 1:
                    $venda=fncgetvenda($dados["id_destino"]);
                    $destino="CONTRATO ".$venda['prefixo']."-".$venda['contrato'];
                    $link="Vvv&id_v";
                    break;
                case 2:
                    $romaneio=fncgetentrada($dados["id_destino"]);
                    $destino="CT ".$romaneio['romaneio'];
                    $link="Vct_s&id_e";
                    break;
                case 3:
                    $romaneio=fncgetentrada($dados["id_destino"]);
                    $destino="DE ".$romaneio['romaneio'];
                    $link="Vde&id_e";
                    break;
                case 4:
                    $romaneio=fncgetentrada($dados["id_destino"]);
                    $destino="LG ".$romaneio['romaneio'];
                    $link="Vlg&id_e";
                    break;
            }
            ?>

            <tr>
                <td><?php
                    echo $tipo_saida;
                    ?>
                </td>
                <td class="text-center">
                    <?php
                    echo datahoraBanco2data($dados["data_ts"]);
                    ?>
                </td>
                <td class="text-center"><?php echo $peso. " ou ".$sacas." volumes"; ?></td>
                <td class="text-center"><a href="index.php?pg=<?php echo $link; ?>=<?php echo $dados["id_destino"]; ?>"><?php echo $destino; ?></a></td>
                <td class="text-center"><?php echo $responsavel; ?></td>
                <td class="text-center">
                    <?php if ($allow["allow_6"]==1 and $dados["count_rank"]==0){?>
                        <div class="dropdown show">
                            <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-trash"><br>EXCLUIR</i>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="#">Não</a>
                                <a class="dropdown-item bg-danger" href="index.php?pg=Vl&id_l=<?php echo $lote; ?>&peso=<?php echo $dados["peso"]; ?>&id=<?php echo $id; ?>&aca=apagarsaida">Apagar</a>
                            </div>
                        </div>
                    <?php } ?>
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>

</div>