<div class="col-md-3">
    <blockquote class="blockquote blockquote-success">
        <header>
            ROMANEIO:
            <strong class="text-info"><?php echo $romaneio_tipo.$ref.$entrada['romaneio']."  ".$entrada['obs']; ?>&nbsp;&nbsp;</strong>
        </header>
        <h6>
            NOTA:
            <strong class="text-info"><?php echo $entrada['nota']; ?>&nbsp;&nbsp;</strong><br>
            FORNECEDOR:
            <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?></strong>
            CORRETOR:
            <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['corretor'])['nome']); ?></strong><br>
            MOTORISTA:
            <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['motorista'])['nome']); ?></strong><br>

            PLACA(S):
            <strong class="text-info"><?php echo $entrada['placa']; ?></strong>

            <footer class="blockquote-footer">
                <?php echo fncgetusuario($entrada['usuario'])['nome']?>&nbsp;&nbsp;
            </footer>
    </blockquote>
<!--    <div class="btn-group" role="group" aria-label="">-->
<!--        <a href="index.php?pg=Vrm_editar&id=--><?php //echo $_GET['id_e'] ?><!--" title="Editar entrada" class="btn btn-sm btn- btn-outline-primary fas fa-pen fa-2x text-dark"></a>-->
<!--    </div>-->
</div>