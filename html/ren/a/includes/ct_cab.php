<div class="col-md-3">
    <blockquote class="blockquote blockquote-success">
        <header>
            ROMANEIO:
            <strong class="text-info"><?php echo $romaneio_tipo.$entrada['ref'].$entrada['romaneio']."  ".$entrada['obs']; ?>&nbsp;&nbsp;</strong>
        </header>
        <h6>
            NOTA:
            <strong class="text-info"><?php echo $entrada['nota']; ?>&nbsp;&nbsp;</strong><br>
            FORNECEDOR:
            <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?></strong>

            <footer class="blockquote-footer">
                <?php echo fncgetusuario($entrada['usuario'])['nome']?>&nbsp;&nbsp;
            </footer>
    </blockquote>


    <div class="btn-group" role="group" aria-label="">
        <a href="index.php?pg=Vct_editar&id=<?php echo $_GET['id_e'] ?>" title="Editar entrada" class="btn btn-sm btn-primary fas fa-pen text-dark">
            <br>EDITAR ROMANEIO
        </a>
        <a href="index.php?pg=Vct_print1&id_e=<?php echo $_GET['id_e']; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-dark fas fa-print">
            <br>IMPRIMIR CONTROLE DE MAQUINAÇÃO
        </a>

    </div>


</div>