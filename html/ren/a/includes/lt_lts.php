<?php
// Recebe
if (isset($_GET['id_e'])) {
    $rm_id = $_GET['id_e'];
    //existe um id e se ele é numérico
    if (!empty($rm_id) && is_numeric($rm_id)) {
        // Captura os dados do cliente solicitado
        $sql = "SELECT * \n"
            . "FROM ren_entradas_lotes \n"
            . "WHERE (((ren_entradas_lotes.romaneio)=?) and status=1)\n"
            . "ORDER BY ren_entradas_lotes.data_ts ASC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $rm_id);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $lotes = $consulta->fetchAll();
        $sql = null;
        $consulta = null;
    }
}
?>

<div class="card mt-2">
    <div id="pointofview" class="card-header bg-info text-light">
        Lotes desse romaneio
    </div>
    <div class="card-body">
        <a href="index.php?pg=Vlt_print1&id_e=<?php echo $_GET['id_e'];?>" target="_blank">
            <span class="fa fa-print text-warning" aria-hidden="true"> Impressão de lotes</span>
        </a>
        <?php
        foreach ($lotes as $lt) {
            $entrada=fncgetentrada($lt['romaneio']);
            $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);
            $ref=$entrada["ref"];

            $cordalinha = "info ";
            if ($lt['peso_atual']==null or $lt['peso_atual']==0 or $lt['peso_atual']==""){
                $cordalinha = "dark ";
            }else{
                if ($lt['p_bo']==1){
                    $cordalinha = "warning ";
                }
            }
            $letra=fncgetletra($lt["letra"]);
            $id_l = $lt["id"];
            $peso_entrada = $lt["peso_entrada"];
            $sacas_entrada=$peso_entrada/60;
            $sacas_entrada=number_format($sacas_entrada, 2, '.', ',');
            $bags_entrada = $lt["bags_entrada"];
            $localizacao = $lt["localizacao"];
            $localizacao_obs = $lt["localizacao_obs"];
            $peso_atual = $lt["peso_atual"];
            $sacas_atual=$peso_atual/60;
            $sacas_atual=number_format($sacas_atual, 2, '.', ',');
            $bags_atual = $lt["bags_atual"];
            $p_bo = $lt["p_bo"];
            $bo = $lt["bo"];
            $obs = $lt["obs"];
            $responsavel = fncgetpessoa($lt["responsavel"])['nome'];
            ?>
            <hr>
            <blockquote class="blockquote blockquote-<?php echo $cordalinha; ?>">
                <a href="?pg=Vl&id_l=<?php echo $lt['id'];?>" class="float-right">
                    <strong class='badge badge-success'><h3><?php echo $romaneio_tipo.$ref.$entrada['romaneio']. " ".$letra;?></h3></strong>
                </a>
                Fornecedor: <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?>&nbsp&nbsp</strong><br>
                Produto: <strong class="text-info">
                    <?php echo fncgetprodutos($lt['tipo_cafe'])['nome']; ?>
                </strong><br>
                Prova: <strong class="text-info">
                    <?php
                    if ($lt["verificado"]==0){
                        echo " <i class='text-danger'>NÃO VERIFICADA</i>";
                    }else{
                        echo " <i class='text-success'>VERIFICADA</i>";
                    }
                    ?>
                </strong><br>
                <small><small>Entrou como: </small><small class="text-info"><?php echo fncgetprodutos($lt['tipo_cafe_entrada'])['nome']; ?>&nbsp&nbsp</small></small><br>
                Peso inicial: <strong class="text-success"><?php echo $peso_entrada. "KG ou ".$sacas_entrada." volumes"; ?>&nbsp&nbsp</strong><br>
                Big Bags inicial: <strong class="text-success"><?php echo $bags_entrada." bags"; ?>&nbsp&nbsp</strong><br>
                Localização: <strong class="text-info"><?php echo fncgetlocal($localizacao)['nome']." ". $localizacao_obs; ?>&nbsp&nbsp</strong><br>

                Peso atual: <strong class="text-danger"><?php echo $peso_atual. "KG ou ".$sacas_atual." volumes"; ?>&nbsp&nbsp</strong><br>
                Big Bags atual: <strong class="text-danger"><?php echo $bags_atual." bags"; ?>&nbsp&nbsp</strong><br>
                <?php
                if ($p_bo==1){
                    echo "B.O.: <strong class='text-info'>{$bo}&nbsp&nbsp</strong><br>";
                }
                ?>
                Obs gerais: <strong class="text-info"><?php echo $obs; ?>&nbsp&nbsp</strong><br>
                RESPONSÁVEL: <strong class="text-info"><?php echo strtoupper($responsavel); ?>&nbsp&nbsp</strong><br>
                <strong class="text-info" title=""><?php echo datahoraBanco2data($lt['data_ts']); ?>&nbsp;&nbsp;</strong>

                <?php
                if ($lt["alterado"]!=0){
                    echo "<br><i class='text-danger'>Esse lote já sofreu alteração e não pode ser excluido ou editado!!</i>";
                }else{
                    echo "<a href='index.php?pg=Vl_editar&id_e={$entrada['id']}&id_l={$id_l}' class='btn btn-block btn-info mb-1'>EDIÇÃO</a>";?>
                    <div class="dropdown show">
                        <a class="btn btn-danger btn-block dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-trash">EXCLUSÃO DE LOTE SEM MOVIMENTAÇÃO</i>
                        </a>
                        <div class="dropdown-menu btn-block" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="#">NÃO</a>
                            <a class="dropdown-item bg-danger" href="index.php?pg=Vl&id_l=<?php echo $id_l;?>&aca=excluilote">APAGAR LOTE</a>
                        </div>
                    </div>

                    <?php
                }
                ?>

                <footer class="blockquote-footer">
                    <?php
                    $us=fncgetusuario($lt['usuario']);
                    echo $us['nome'];
                    ?>
                </footer>
            </blockquote>
            <?php
            echo "</h6>";
        }
        ?>
    </div>
</div>