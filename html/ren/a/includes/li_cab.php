<blockquote class="blockquote blockquote-success">
    <header>
        CLIENTE:
        <strong class="text-info"><?php echo strtoupper(fncgetpessoa($entrada['cliente'])['nome']); ?>&nbsp;&nbsp;</strong>
    </header>
    <h6>
        CONTRATO:
        <strong class="text-info"><?php echo $entrada['contrato']; ?>&nbsp;&nbsp;</strong><br>

        DESCRIÇÃO:
        <strong class="text-info"><?php echo $entrada['descricao']; ?>&nbsp;&nbsp;</strong><br>


        <footer class="blockquote-footer">
            <?php echo fncgetusuario($entrada['usuario'])['nome']?>&nbsp;&nbsp;
        </footer>
</blockquote>