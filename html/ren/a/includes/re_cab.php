<div class="col-md-3">
    <blockquote class="blockquote blockquote-success">
        <header>
            ROMANEIO:
            <strong class="text-info"><?php echo $romaneio_tipo.$entrada['romaneio']."  ".$entrada['obs']; ?>&nbsp;&nbsp;</strong>
        </header>
        <h6>
            NOTA:
            <strong class="text-info"><?php echo $entrada['nota']; ?>&nbsp;&nbsp;</strong><br>
            FORNECEDOR:
            <strong class="text-info"><?php echo fncgetpessoa($entrada['fornecedor'])['nome']; ?></strong>

            <footer class="blockquote-footer">
                <?php echo fncgetusuario($entrada['usuario'])['nome']?>&nbsp;&nbsp;
            </footer>
    </blockquote>
    <div class="btn-group" role="group" aria-label="">
        <a href="index.php?pg=Vre_editar&id=<?php echo $_GET['id_e'] ?>" title="Editar entrada" class="btn btn-sm btn- btn-outline-primary fas fa-pen fa-2x text-dark"></a>
    </div>
</div>