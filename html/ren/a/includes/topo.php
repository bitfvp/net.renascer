<nav id="navbar" class="navbar sticky-top navbar-expand-lg navbar-dark bg-<?php echo($_SESSION['theme']==1)?"dark":"primary";?> mb-2">

        <a class="navbar-brand" href="<?php echo $env->env_url_mod;?>">
            <img class="d-inline-block align-top m-0 p-0" style="height: 50px;" src="<?php echo $env->env_estatico; ?>img/vetor_simples.png" alt="<?php echo $env->env_nome; ?>">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <?php
                if (isset($_GET['comp']) and $_GET['comp']==1){
                    echo "<a class='nav-item nav-link fas fa-expand-alt' href='index.php?pg=Vhome'></a>";
                }else{
                    echo "<a class='nav-item nav-link fas fa-compress-alt' href='index.php?pg=Vhome&comp=1'></a>";
                }
                ?>
                <a class="nav-item nav-link" href="index.php?pg=Vpessoa_lista">CADASTRO GERAL</a>
                <a class="nav-item nav-link " href="index.php?pg=Vl_lista">LOTES</a>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navrel" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ROMANEIOS
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navrel">
                        <a class="dropdown-item" href="index.php?pg=Vrm_lista" target="">RM-ENTRADA</a>
                        <a class="dropdown-item" href="index.php?pg=Vre_lista" target="">RE-RETORNO DE ESTOQUE</a>
                        <a class="dropdown-item" href="index.php?pg=Vct_lista" target="">CT-MAQUINAÇÃO</a>
                        <a class="dropdown-item" href="index.php?pg=Vde_lista" target="">DE-DEVOLUÇÃO AO FORNECEDOR</a>
                        <a class="dropdown-item" href="index.php?pg=Vlg_lista" target="">LG-LIGA INTERNA</a>
                        <div class="dropdown-divider"></div>
                    </div>
                </li>



<!--                <a class="nav-item nav-link" href="index.php?pg=Vv_lista">VENDAS</a>-->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navven" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        VENDAS
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navven">
                        <a class="dropdown-item" href="index.php?pg=Vv_lista" target="">VENDAS</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="index.php?pg=Vrel_vendas" target="">RELATÓRIO DE VENDAS</a>
                        <a class="dropdown-item" href="index.php?pg=Vrel_vendastransporte" target="">RELATÓRIO DE TRANSPORTE EM VENDAS </a>
                    </div>
                </li>



                <a class="nav-item nav-link" href="index.php?pg=Vligas_lista">BLENDS</a>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navrel" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            RELATÓRIOS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navrel">
                            <a class="dropdown-item" href="index.php?pg=Vrel_1print&order=local" target="_blank">ESTOQUE ATUAL POR LOCALIZAÇÃO</a>
                            <a class="dropdown-item" href="index.php?pg=Vrel_1print&order=data" target="_blank">ESTOQUE ATUAL POR DATA</a>
                            <a class="dropdown-item" href="index.php?pg=Vrel_1print&order=tipo" target="_blank">ESTOQUE ATUAL POR TIPO DE PRODUTO, DATA</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="index.php?pg=Vrel_2print&order=local" target="_blank">RELATÓRIO DE CONFERÊNCIA</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="index.php?pg=Vrel_lotes" target="">TODOS OS LOTES DE ENTRADA</a>
                            <a class="dropdown-item" href="index.php?pg=Vrel_fl" target="">LOTES POR FORNECEDOR</a>
                        </div>
                    </li>





            </ul>

            <?php
            include_once("{$env->env_root}includes/ren/modal_msg_pontos.php");
            ?>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="msgoff()" data-toggle="modal" data-target="#modalmsgs">
                        <?php
                        echo $comp_msg;
                        ?>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                        <a href="#" class="dropdown-item" data-toggle="modal" data-target="#modaltemas">
                            <i class="fa fa-tint"></i>
                            Alterar tema
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?PHP echo $env->env_url; ?>?pg=Vlogin"><i class="fa fa-undo"></i> Voltar</a>
                        <a class="dropdown-item" href="?aca=logout"><i class="fa fa-sign-out-alt"></i>&nbsp;Sair</a>
                    </div>
                </li>
            </ul>
        </div>


    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</nav>
<?php
include_once("{$env->env_root}includes/sessao_relogio.php");