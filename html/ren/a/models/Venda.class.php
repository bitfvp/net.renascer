<?php
class Venda{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncvendanew(
        $status,$prefixo,$contrato,$descricao,$cliente,$transportadora,$nota_fiscal,$retorno_de_estoque
    ){
        //tratamento das variaveis
        //não ter

        try{
            $sql="SELECT id FROM ";
                $sql.="ren_vendas ";
            $sql.="WHERE contrato<>0 and contrato=:contrato ";
            global $pdo;
            $consultaromaneio=$pdo->prepare($sql);
            $consultaromaneio->bindValue(":contrato", $contrato);
            $consultaromaneio->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarromaneio=$consultaromaneio->rowCount();

        if(($contarromaneio==0)or ($contarromaneio=="")){


            //inserção no banco
                try {
                    $sql="INSERT INTO ren_vendas ";
                    $sql .= "(id,
                    status,
                    usuario,
                    prefixo,
                    contrato,
                    descricao,
                    cliente,
                    transportadora,
                    nota_fiscal,
                    retorno_de_estoque
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :status,
                    :usuario,
                    :prefixo,
                    :contrato,
                    :descricao,
                    :cliente,
                    :transportadora,
                    :nota_fiscal,
                    :retorno_de_estoque
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":status", $status);
                    $insere->bindValue(":usuario", $_SESSION['id']);
                    $insere->bindValue(":prefixo", $prefixo);
                    $insere->bindValue(":contrato", $contrato);
                    $insere->bindValue(":descricao", $descricao);
                    $insere->bindValue(":cliente", $cliente);
                    $insere->bindValue(":transportadora", $transportadora);
                    $insere->bindValue(":nota_fiscal", $nota_fiscal);
                    $insere->bindValue(":retorno_de_estoque", $retorno_de_estoque);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }


        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha um contrato com esse número!!",
                "type"=>"warning",
            ];
        }



        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM ";
                $sql.="ren_vendas ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vvv&id_v={$maid}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncvendaedit(
        $id,$status,$prefixo,$contrato,$descricao,$cliente,$transportadora,$nota_fiscal,$retorno_de_estoque
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_vendas";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $entrada_verifica = $consulta->fetch();
//            $entrada_verifica['motorista'] //////motorista antigo


        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_vendas ";
                $sql.="SET ";
                $sql .= "status=:status, 
                prefixo=:prefixo,
                    contrato=:contrato,
                    descricao=:descricao,
                    cliente=:cliente,
                    transportadora=:transportadora,
                    nota_fiscal=:nota_fiscal,
                    retorno_de_estoque=:retorno_de_estoque
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":status", $status);
                $atualiza->bindValue(":prefixo", $prefixo);
                $atualiza->bindValue(":contrato", $contrato);
                $atualiza->bindValue(":descricao", $descricao);
                $atualiza->bindValue(":cliente", $cliente);
                $atualiza->bindValue(":transportadora", $transportadora);
                $atualiza->bindValue(":nota_fiscal", $nota_fiscal);
                $atualiza->bindValue(":retorno_de_estoque", $retorno_de_estoque);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa entrada cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vvv&id_v={$id}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




}
