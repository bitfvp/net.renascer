<?php
class Saidas{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncsaidanew(
        $id_destino,$peso,$bags_atual,$lote_id,$tipo_saida,$origem,$tipo_cafe,$p_bo,$bo,$peso_atual,$caixa
    ){
        //tratamento das variaveis
        //não ter


            //inserção no banco
                try {
                    $sql="INSERT INTO ren_entradas_lotes_saidas ";
                    $sql .= "(id,
                    usuario,
                    lote,
                    tipo_saida,
                    origem,
                    peso,
                    tipo_cafe,
                    p_bo,
                    bo,
                    id_destino
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :usuario,
                    :lote,
                    :tipo_saida,
                    :origem,
                    :peso,
                    :tipo_cafe,
                    :p_bo,
                    :bo,
                    :id_destino
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":usuario", $_SESSION['id']);
                    $insere->bindValue(":lote", $lote_id);
                    $insere->bindValue(":tipo_saida", $tipo_saida);
                    $insere->bindValue(":origem", $origem);
                    $insere->bindValue(":peso", $peso);
                    $insere->bindValue(":tipo_cafe", $tipo_cafe);
                    $insere->bindValue(":p_bo", $p_bo);
                    $insere->bindValue(":bo", $bo);
                    $insere->bindValue(":id_destino", $id_destino);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }


                $novopeso=$peso_atual-$peso;
        try {
            $sql="UPDATE ren_entradas_lotes";
            $sql.=" SET";
            $sql .= " peso_atual=:peso_atual,
                    bags_atual=:bags_atual,
                    alterado=1
                WHERE id=:id";
            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindValue(":peso_atual", $novopeso);
            $atualiza->bindValue(":bags_atual", $bags_atual);
            $atualiza->bindValue(":id", $lote_id);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if ($caixa<=16 and $caixa>0){
            $sql = "SELECT * FROM ren_entradas_lotes WHERE id=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$lote_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $getlote = $consulta->fetch();
            $sql=null;
            $consulta=null;
            //$getlote['romaneio']

            $sql = "SELECT * FROM ren_entradas WHERE id=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$getlote['romaneio']);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $getentrada = $consulta->fetch();
            $sql=null;


            $fornecedor=$getentrada['fornecedor'];
            try {
                $sql="INSERT INTO ren_caixas_volumes ";
                $sql .= "(id,
                    usuario,
                    status,
                    caixa,
                    peso,
                    fornecedor,
                    tipo_cafe,
                    lote
                    )";
                $sql .= " VALUES ";
                $sql .= "(NULL,
                    :usuario,
                    :status,
                    :caixa,
                    :peso,
                    :fornecedor,
                    :tipo_cafe,
                    :lote
                    )";
                global $pdo;
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":usuario", $_SESSION['id']);
                $insere->bindValue(":status", 1);
                $insere->bindValue(":caixa", $caixa);
                $insere->bindValue(":peso", $peso);
                $insere->bindValue(":fornecedor", $fornecedor);
                $insere->bindValue(":tipo_cafe", $tipo_cafe);
                $insere->bindValue(":lote", $getlote['id']);

                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }






        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vl&id_l={$lote_id}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
