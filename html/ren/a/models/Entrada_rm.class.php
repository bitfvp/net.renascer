<?php
class Entrada_rm{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncrmnew(
        $romaneio_tipo,$romaneio,$nota,$fornecedor,$empresa,$corretor,$motorista,$controle_interno,$ocr,$ref
    ){
        //tratamento das variaveis
        //não ter

        try{
            $sql="SELECT id FROM ";
            $sql.="ren_entradas";
            $sql.=" WHERE romaneio<>0 and romaneio=:romaneio and romaneio_tipo=:romaneio_tipo and ref=:ref";
            global $pdo;
            $consultaromaneio=$pdo->prepare($sql);
            $consultaromaneio->bindValue(":romaneio", $romaneio);
            $consultaromaneio->bindValue(":romaneio_tipo", $romaneio_tipo);
            $consultaromaneio->bindValue(":ref", $ref);
            $consultaromaneio->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarromaneio=$consultaromaneio->rowCount();

        if(($contarromaneio==0)or ($contarromaneio=="")){


            //inserção no banco
            try {
                $sql="INSERT INTO ren_entradas ";
                $sql .= "(id,
                    usuario,
                    romaneio_tipo,
                    romaneio,
                    nota,
                    fornecedor,
                    empresa,
                    corretor,
                    motorista,
                    controle_interno,
                    ocr,
                    ref
                    )";
                $sql .= " VALUES ";
                $sql .= "(NULL,
                    :usuario,
                    :romaneio_tipo,
                    :romaneio,
                    :nota,
                    :fornecedor,
                    :empresa,
                    :corretor,
                    :motorista,
                    :controle_interno,
                    :ocr,
                    :ref
                    )";
                global $pdo;
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":usuario", $_SESSION['id']);
                $insere->bindValue(":romaneio_tipo", $romaneio_tipo);
                $insere->bindValue(":romaneio", $romaneio);
                $insere->bindValue(":nota", $nota);
                $insere->bindValue(":fornecedor", $fornecedor);
                $insere->bindValue(":empresa", $empresa);
                $insere->bindValue(":corretor", $corretor);
                $insere->bindValue(":motorista", $motorista);
                $insere->bindValue(":controle_interno", $controle_interno);
                $insere->bindValue(":ocr", $ocr);
                $insere->bindValue(":ref", $ref);
                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }


        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma romaneio com esse número!!",
                "type"=>"warning",
            ];
        }



        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            $sql = "SELECT Max(id) FROM ";
            $sql.="ren_entradas";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mid = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

            header("Location: index.php?pg=Vrm_editar2&id={$maid}");
            exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }

    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////










//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncrmedit(
        $id,$romaneio,$nota,$fornecedor,$empresa,$corretor,$motorista,$controle_interno,$ocr
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_entradas";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $entrada_verifica = $consulta->fetch();
//            $entrada_verifica['motorista'] //////motorista antigo

            if ($entrada_verifica['motorista']!=$motorista){
//                echo "é diferente";
                $placa="";
            }else{
//                echo "é igual";
                $placa=$entrada_verifica['placa'];
            }

        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_entradas";
                $sql.=" SET";
                $sql .= " romaneio=:romaneio,
                    nota=:nota,
                    fornecedor=:fornecedor,
                    empresa=:empresa,
                    corretor=:corretor,
                    motorista=:motorista,
                    controle_interno=:controle_interno,
                    ocr=:ocr,
                    placa=:placa
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":romaneio", $romaneio);
                $atualiza->bindValue(":nota", $nota);
                $atualiza->bindValue(":fornecedor", $fornecedor);
                $atualiza->bindValue(":empresa", $empresa);
                $atualiza->bindValue(":corretor", $corretor);
                $atualiza->bindValue(":motorista", $motorista);
                $atualiza->bindValue(":controle_interno", $controle_interno);
                $atualiza->bindValue(":ocr", $ocr);
                $atualiza->bindValue(":placa", $placa);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa entrada cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vrm_editar2&id={$id}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncrmedit2(
        $id,$placas
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_entradas";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_entradas ";
                $sql.="SET ";
                $sql .= "placa=:placa WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":placa", $placas);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa entrada cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vrm&id_e={$id}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }

}
