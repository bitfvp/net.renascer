<?php
class Lotes{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnclotenew(
        $id_e,$letra,$tipo_cafe,$peso_entrada,$bags_entrada,$localizacao,$localizacao_obs,$p_bo,$bo,$obs,$responsavel,$pg
    ){
        //tratamento das variaveis
        //não ter


            //inserção no banco
                try {
                    $sql="INSERT INTO ren_entradas_lotes ";
                    $sql .= "(id,
                    usuario,
                    romaneio,
                    letra,
                    tipo_cafe,
                    tipo_cafe_entrada,
                    peso_entrada,
                    bags_entrada,
                    localizacao,
                    localizacao_obs,
                    p_bo,
                    bo,
                    peso_atual,
                    bags_atual,
                    obs,
                    responsavel
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :usuario,
                    :romaneio,
                    :letra,
                    :tipo_cafe,
                    :tipo_cafe_entrada,
                    :peso_entrada,
                    :bags_entrada,
                    :localizacao,
                    :localizacao_obs,
                    :p_bo,
                    :bo,
                    :peso_atual,
                    :bags_atual,
                    :obs,
                    :responsavel
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":usuario", $_SESSION['id']);
                    $insere->bindValue(":romaneio", $id_e);
                    $insere->bindValue(":letra", $letra);
                    $insere->bindValue(":tipo_cafe", $tipo_cafe);
                    $insere->bindValue(":tipo_cafe_entrada", $tipo_cafe);
                    $insere->bindValue(":peso_entrada", $peso_entrada);
                    $insere->bindValue(":bags_entrada", $bags_entrada);
                    $insere->bindValue(":localizacao", $localizacao);
                    $insere->bindValue(":localizacao_obs", $localizacao_obs);
                    $insere->bindValue(":p_bo", $p_bo);
                    $insere->bindValue(":bo", $bo);
                    $insere->bindValue(":peso_atual", $peso_entrada);
                    $insere->bindValue(":bags_atual", $bags_entrada);
                    $insere->bindValue(":obs", $obs);
                    $insere->bindValue(":responsavel", $responsavel);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }

        try {
            $sql="UPDATE ren_entradas";
            $sql.=" SET";
            $sql .= " possui_lote=1, ass_lotes=:ass_lotes
                WHERE id=:id";
            global $pdo;
            $atualiza = $pdo->prepare($sql);
            $atualiza->bindValue(":ass_lotes", $_SESSION['id']);
            $atualiza->bindValue(":id", $id_e);
            $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

        } catch (PDOException $error_msg) {
            echo 'Erro' . $error_msg->getMessage();
        }


        if ($localizacao<=16){
                $sql = "SELECT Max(id) FROM ";
                $sql.="ren_entradas_lotes";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];



            $sql = "SELECT * FROM ren_entradas WHERE id=?";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1,$id_e);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $getentrada = $consulta->fetch();
            $sql=null;


            $fornecedor=$getentrada['fornecedor'];
            try {
                $sql="INSERT INTO ren_caixas_volumes ";
                $sql .= "(id,
                    usuario,
                    status,
                    caixa,
                    peso,
                    fornecedor,
                    tipo_cafe,
                    lote
                    )";
                $sql .= " VALUES ";
                $sql .= "(NULL,
                    :usuario,
                    :status,
                    :caixa,
                    :peso,
                    :fornecedor,
                    :tipo_cafe,
                    :lote
                    )";
                global $pdo;
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":usuario", $_SESSION['id']);
                $insere->bindValue(":status", 1);
                $insere->bindValue(":caixa", $localizacao);
                $insere->bindValue(":peso", $peso_entrada);
                $insere->bindValue(":fornecedor", $fornecedor);
                $insere->bindValue(":tipo_cafe", $tipo_cafe);
                $insere->bindValue(":lote", $maid);

                $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }



        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Lote criado com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];

            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg={$pg}&id_e={$id_e}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncloteedit(
        $id_l,$id_e,$letra,$tipo_cafe,$peso_entrada,$bags_entrada,$peso_atual,$bags_atual,$localizacao,$localizacao_obs,$p_bo,$bo,$obs,$responsavel
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_entradas_lotes";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id_l);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);


        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_entradas_lotes";
                $sql.=" SET";
                $sql .= " letra=:letra,
                    tipo_cafe=:tipo_cafe,
                    peso_entrada=:peso_entrada,
                    bags_entrada=:bags_entrada,
                    peso_atual=:peso_atual,
                    bags_atual=:bags_atual,
                    localizacao=:localizacao,
                    localizacao_obs=:localizacao_obs,
                    p_bo=:p_bo,
                    bo=:bo,
                    obs=:obs,
                    responsavel=:responsavel
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":letra", $letra);
                $atualiza->bindValue(":tipo_cafe", $tipo_cafe);
                $atualiza->bindValue(":peso_entrada", $peso_entrada);
                $atualiza->bindValue(":bags_entrada", $bags_entrada);
                $atualiza->bindValue(":peso_atual", $peso_atual);
                $atualiza->bindValue(":bags_atual", $bags_atual);
                $atualiza->bindValue(":localizacao", $localizacao);
                $atualiza->bindValue(":localizacao_obs", $localizacao_obs);
                $atualiza->bindValue(":p_bo", $p_bo);
                $atualiza->bindValue(":bo", $bo);
                $atualiza->bindValue(":obs", $obs);
                $atualiza->bindValue(":responsavel", $responsavel);
                $atualiza->bindValue(":id", $id_l);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse lote cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vl&id_l={$id_l}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fnclotemudalocalizacao(
        $id,$localizacao,$localizacao_obs,$bags_atual,$responsavel
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_entradas_lotes";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);


        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_entradas_lotes";
                $sql.=" SET";
                $sql .= " localizacao=:localizacao,
                    localizacao_obs=:localizacao_obs,
                    bags_atual=:bags_atual,
                    responsavel=:responsavel
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":localizacao", $localizacao);
                $atualiza->bindValue(":localizacao_obs", $localizacao_obs);
                $atualiza->bindValue(":bags_atual", $bags_atual);
                $atualiza->bindValue(":responsavel", $responsavel);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse lote cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vl&id_l={$id}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function fncloteeditprova(
        $id_l,$tipo_cafe
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
            $sql.="ren_entradas_lotes";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id_l);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);


        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco
            try {
                $sql="UPDATE ren_entradas_lotes";
                $sql.=" SET";
                $sql .= "
                    tipo_cafe=:tipo_cafe,
                    verificado=1
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":tipo_cafe", $tipo_cafe);
                $atualiza->bindValue(":id", $id_l);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse lote cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vl&id_l={$id_l}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }

}
