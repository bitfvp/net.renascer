<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
//    $a="entradasave";
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Ve_lista");
    exit();
}

$romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">
        <?php
        include_once("includes/rm_cab.php");
        ?>

        <?php
        try{
            $sql = "SELECT * FROM "
                ."ren_entradas_pesagens "
                ."WHERE ren_entradas_pesagens.entrada=:entrada "
                ."order by ren_entradas_pesagens.data_ts DESC ";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindValue(":entrada", $_GET['id_e']);
            $consulta->execute();
            global $LQ;
            $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }
        $pesagens = $consulta->fetchAll();
        $pesagens_quant = $consulta->rowCount();
        $sql = null;
        $consulta = null;
        ?>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Pesagens
                </div>
                <div class="card-body">

                    <?php
                    foreach ($pesagens as $dados){
                        if ($dados['datahora_saida']==null or $dados['datahora_saida']==0 or $dados['datahora_saida']==""){
                            $cordalinha = "  ";
                        }else{
                            $cordalinha = "text-dark bg-success ";
                        }
                        $pe_id = $dados["id"];
                        $data_inicial = datahoraBanco2data($dados["data_ts"]);
                        $produto = $dados["produto"];
                        switch ($dados["embalagem"]){
                            case 0:
                                $embalagem="Não selecionada";
                                break;
                            case 1:
                                $embalagem="SACARIA";
                                break;
                            case 2:
                                $embalagem="BIG BAGS";
                                break;
                            case 3:
                                $embalagem="GRANEL";
                                break;
                            default:
                                $embalagem="Não selecionada!";
                                break;
                        }
                        $peso_entrada =$dados["peso_entrada"];
                        $obs_entrada = $dados["obs_entrada"];

                        if ($dados["peso_saida"]<=0 or $dados["peso_saida"]==""){
                            $peso_saida = "AGUARDE";
                            $obs_saida = "AGUARDE";
                            $datahora_saida = "AGUARDE";
                        }else{
                            $peso_saida = $dados["peso_saida"];
                            $peso_saida = number_format($peso_saida,2,',',' ')."Kg";
                            $obs_saida = $dados["obs_saida"];
                            $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
                        }





                        $usuario = fncgetusuario($dados["usuario"])['nome'];
                        ?>

                            <table class="table table-sm">
                                <tr class="<?php echo $cordalinha; ?>">

                                    <td>PRODUTO: <strong><?php echo $produto; ?></strong></td>
                                    <td>EMBALAGEM: <strong><?php echo $embalagem; ?></strong></td>
                                </tr>
                                <tr class="<?php echo $cordalinha; ?>">
                                    <td>INÍCIO: <strong><?php echo $data_inicial; ?></strong></td>
                                    <td>SAÍDA: <strong><?php echo $datahora_saida; ?></strong></td>
                                </tr>
                                <tr class="<?php echo $cordalinha; ?>">

                                    <td>OBS DE ESTRADA: <strong><?php echo $obs_entrada; ?></strong></td>
                                    <td>OBS DE SAÍDA: <strong><?php echo $obs_saida; ?></strong></td>
                                </tr>
                                <tr class="<?php echo $cordalinha; ?>">
                                    <td>PESO DE ENTRADA: <strong><?php echo number_format($peso_entrada,2,',',' '); ?>Kg</strong></td>
                                    <td>PESO DE SAÍDA: <strong><?php echo $peso_saida; ?></strong></td>
                                </tr>

                            </table>
                        <?php
                    }
                    ?>
                </div>
            </div><!-- fim de card-->

        </div>

        <div class="col-md-3">
            <section class="sidebar-offcanvas" id="sidebar">
                <div class="list-group">
                    <?php
                    switch ($entrada["romaneio_tipo"]){
                        case 1:
//                            $romaneio_tipo="RM-";
                            echo "<a href='?pg=Vrm&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            echo "<a href='?pg=Vrm_p&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-balance-scale'></i>    PESSAGENS</a>";
                            break;
                        case 2:
//                            $romaneio_tipo="RE-";
                            echo "<a href='?pg=Vrm&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            break;
                        case 3:
//                            $romaneio_tipo="CT-";
                            echo "<a href='?pg=Vrm&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            echo "<a href='?pg=Vrm_s&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-list'></i>    SELEÇÃO DE LOTES</a>";
                            break;
                        case 4:
//                            $romaneio_tipo="DE-";
                            echo "<a href='?pg=Vrm_d&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-undo-alt'></i>    PARA DEVOLUÇÃO</a>";
                            break;
                    }
                    ?>
                </div>
            </section>
            <script type="application/javascript">
                var offset = $('#sidebar').offset().top;
                var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance
                $(document).on('scroll', function () {
                    if (offset <= $(window).scrollTop()) {
                        $meuMenu.addClass('fixarmenu');
                    } else {
                        $meuMenu.removeClass('fixarmenu');
                    }
                });
            </script>

        </div>


    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>