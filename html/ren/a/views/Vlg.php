<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vlg_lista");
    exit();
}
if (isset($_GET['id_l']) and is_numeric($_GET['id_l'])){
//    $a="lotesave";
//    $lote=fncgetlote($_GET['id_l']);
//    if ($lote['p_bo']==1){
//        $p_bo="checked";
//        $p_bon="";
//    }else{
//        $p_bo="";
//        $p_bon="checked";
//    }
}else{
    $a="lotenew";
    $p_bo="";
    $p_bon="checked";
}
$romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">

        <?php
        include_once("includes/lg_cab.php");
        ?>

        <div class="col-md-6">
            <?php
            if ($entrada["romaneio_tipo"]!=4){?>
                <div class="card">
                    <div class="card-header bg-info text-light">
                        Lançamento de lotes
                    </div>
                    <div class="card-body">
                        <form action="<?php echo "index.php?pg=Vlg&id_e={$_GET['id_e']}&aca={$a}"; ?>" method="post" enctype="multipart/form-data" id="formx">
                            <div class="row">
                                <div class="col-md-12">
                                    <input id="id_l" type="hidden" class="txt bradius" name="id_l" value="<?php echo $lote['id']; ?>"/>
                                    <input id="pg" type="hidden" class="txt bradius" name="pg" value="<?php echo $_GET['pg']; ?>"/>
                                    <label for="tipo_cafe">PRODUTO:</label>
                                    <select name="tipo_cafe" id="tipo_cafe" class="form-control input-sm" data-live-search="true" required>
                                        <?php
                                        $gettipo_cafe=fncgetprodutos($lote['tipo_cafe']);
                                        ?>
                                        <option selected="" data-tokens="<?php echo $gettipo_cafe['nome'];?>" value="<?php echo $lote['tipo_cafe']; ?>">
                                            <?php echo $gettipo_cafe['nome'];?>
                                        </option>
                                        <?php
                                        foreach (fncprodutoslist() as $item) {
                                            ?>
                                            <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                                <?php echo $item['nome']; ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="peso_entrada">PESO DE ENTRADA:</label>
                                    <div class="input-group">
                                        <input autocomplete="off" id="peso_entrada" placeholder="" type="text" class="form-control" name="peso_entrada" value="<?php echo $lote['peso_entrada']; ?>" required />
                                        <div class="input-group-append">
                                            <span class="input-group-text">,000 Kg</span>
                                        </div>
                                    </div>
                                    <script>
                                        $(document).ready(function(){
                                            $('#peso_entrada').mask('000000', {reverse: true});
                                        });
                                        jQuery(document).ready(function(){
                                            jQuery('input').on('keyup',function(){
                                                if(jQuery(this).attr('name') === 'informacao'){
                                                    return false;
                                                }

                                                var soma1 = (jQuery('#peso_entrada').val() == '' ? 0 : jQuery('#peso_entrada').val());
                                                var pesoliquido = (parseInt(soma1));
                                                var sacas = (parseInt(pesoliquido) / 60);
                                                // var textoinfo = pesoliquido + "KG ou " + sacas + " volumes";
                                                var textoinfo = sacas + " volumes";
                                                jQuery('#informacao').val(textoinfo);
                                            });
                                        });
                                    </script>
                                </div>

                                <div class="col-md-6">
                                    <label for="">INFORMAÇÃO</label>
                                    <div class="input-group">
                                        <input  id="informacao" type="text" class="form-control" name="informacao" value="<?php $pl=$lote['peso_entrada']; $ss=$pl/60; echo $ss . " volumes";?>" readonly="readonly"/>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label for="localizacao">LOCALIZAÇÃO:</label>
                                    <select name="localizacao" id="localizacao" class="form-control input-sm" data-live-search="true" required>
                                        <?php
                                        $getlocalizacao=fncgetlocal($lote['localizacao']);
                                        ?>
                                        <option selected="" data-tokens="<?php echo $getlocalizacao['nome'];?>" value="<?php echo $lote['localizacao']; ?>">
                                            <?php echo $getlocalizacao['nome'];?>
                                        </option>
                                        <?php
                                        foreach (fnclocaislist() as $item) {
                                            ?>
                                            <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                                <?php echo $item['nome']; ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label for="bags_entrada">BIG BAGS:</label>
                                    <div class="input-group">
                                        <input autocomplete="off" id="bags_entrada" placeholder="" type="text" class="form-control" name="bags_entrada" value="<?php echo $lote['bags_entrada']; ?>" required />
                                    </div>
                                    <script>
                                        $(document).ready(function(){
                                            $('#bags_entrada').mask('00', {reverse: true});
                                        });
                                    </script>
                                </div>

                                <div class="col-md-4">
                                    <label for="localizacao_obs">OBS LOCALIZAÇÃO:</label>
                                    <div class="input-group">
                                        <input autocomplete="off" id="localizacao_obs" placeholder="" type="text" class="form-control" name="localizacao_obs" value="<?php echo $lote['localizacao_obs']; ?>" />
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label for="p_bo">B.O.</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="p_bo" id="p_bo" value="1" <?php echo $p_bo ?> >
                                        <label class="form-check-label" for="p_bo">
                                            SIM
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="p_bo" id="p_bo" value="0" <?php echo $p_bon; ?> >
                                        <label class="form-check-label" for="p_bo">
                                            NÃO
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <label for="bo">B.O. OBS</label>
                                    <textarea id="bo" onkeyup="limite_textarea(this.value,250,bo,'contbo')" maxlength="250" class="form-control" rows="1" name="bo"><?php echo $lote['bo']; ?></textarea>
                                    <span id="contbo">250</span>/250
                                </div>

                                <div class="col-md-12">
                                    <label for="obs">OBS GERAIS</label>
                                    <textarea id="obs" onkeyup="limite_textarea(this.value,250,obs,'cont')" maxlength="250" class="form-control" rows="1" name="obs"><?php echo $lote['obs']; ?></textarea>
                                    <span id="cont">250</span>/250
                                </div>

                                <div class="col-md-12">
                                    <label for="responsavel">RESPONSÁVEL:</label>
                                    <select name="responsavel" id="responsavel" class="form-control input-sm" data-live-search="true" required>
                                        <?php
                                        $getpessoa=fncgetpessoa($lote['responsavel']);
                                        ?>
                                        <option selected="" data-tokens="<?php echo $getpessoa['nome'];?>" value="<?php echo $lote['responsavel']; ?>">
                                            <?php echo $getpessoa['nome'];?>
                                        </option>
                                        <?php
                                        foreach (fncpessoaresponsavellist() as $item) {
                                            ?>
                                            <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                                <?php echo strtoupper($item['nome']); ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                                </div>
                                <script>
                                    var formID = document.getElementById("formx");
                                    var send = $("#gogo");

                                    $(formID).submit(function(event){
                                        if (formID.checkValidity()) {
                                            send.attr('disabled', 'disabled');
                                            send.attr('value', 'AGUARDE...');
                                        }
                                    });
                                </script>
                            </div>
                        </form>
                    </div>
                </div>
                <?php }else{
                echo"<META HTTP-EQUIV=REFRESH CONTENT = '0.2;URL={$env->env_url_mod}index.php?pg=Vde&id_e={$_GET['id_e']}'>";
            }
            ?>



            <?php
            include_once("includes/lt_lts.php");
            ?>



        </div>

        <div class="col-md-3">
            <section class="sidebar-offcanvas" id="sidebar">
                <div class="list-group">
                    <?php
                    switch ($entrada["romaneio_tipo"]){
                        case 1:
//                            $romaneio_tipo="RM-";
                            echo "<a href='?pg=Vrm&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            echo "<a href='?pg=Vrm_p&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-balance-scale'></i>    PESSAGENS</a>";
                            break;
                        case 2:
//                            $romaneio_tipo="RE-";
                            echo "<a href='?pg=Vrm&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            break;
                        case 3:
//                            $romaneio_tipo="CT-";
                            echo "<a href='?pg=Vct&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            echo "<a href='?pg=Vct_s&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-list'></i>    SELEÇÃO DE LOTES</a>";
                            break;
                        case 4:
//                            $romaneio_tipo="DE-";
                            echo "<a href='?pg=Vrm_d&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-undo-alt'></i>    PARA DEVOLUÇÃO</a>";
                            break;
                        case 5:
//                            $romaneio_tipo="LG-";
                            echo "<a href='?pg=Vlg&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            echo "<a href='?pg=Vlg_s&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-list'></i>    SELEÇÃO DE LOTES</a>";
                            break;

                    }
                    ?>
                </div>
            </section>
            <script type="application/javascript">
                var offset = $('#sidebar').offset().top;
                var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance
                $(document).on('scroll', function () {
                    if (offset <= $(window).scrollTop()) {
                        $meuMenu.addClass('fixarmenu');
                    } else {
                        $meuMenu.removeClass('fixarmenu');
                    }
                });
            </script>


            <?php
            if ($entrada["romaneio_tipo"]==1){

            ?>
                <hr>
                <h4>Resumo das pesagens</h4>
                <?php
                try{
                    $sql = "SELECT * FROM "
                        ."ren_entradas_pesagens "
                        ."WHERE ren_entradas_pesagens.entrada=:entrada "
                        ."order by ren_entradas_pesagens.data_ts DESC ";
                    global $pdo;
                    $consulta = $pdo->prepare($sql);
                    $consulta->bindValue(":entrada", $_GET['id_e']);
                    $consulta->execute();
                    global $LQ;
                    $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                    echo 'Erro'. $error_msg->getMessage();
                }
                $pesagens = $consulta->fetchAll();
                $pesagens_quant = $consulta->rowCount();
                $sql = null;
                $consulta = null;


                foreach ($pesagens as $dados){
                if ($dados['datahora_saida']==null or $dados['datahora_saida']==0 or $dados['datahora_saida']==""){
                $cordalinha = " ";
                }else{
                $cordalinha = " text-warning bg-dark ";
                }
                $pe_id = $dados["id"];
                $produto = $dados["produto"];
                $obs_entrada = $dados["obs_entrada"];
                $peso_liquido=$dados["peso_entrada"]-$dados["peso_saida"];
                    $sacas=$peso_liquido/60;
                    $peso_liquido.=" Kg";
                $obs_saida = $dados["obs_saida"];
                $datahora_saida = datahoraBanco2data($dados["datahora_saida"]);
                $usuario = fncgetusuario($dados["usuario"])['nome'];
                ?>

                <div id="" class="<?php echo $cordalinha; ?> p-1">
                    <i class="d-block fas fa-minus"><?php echo $produto; ?></i>
                    <i class="d-block fas fa-minus"><?php echo $peso_liquido; ?></i>
                    <i class="d-block fas fa-minus"><?php echo $sacas; ?>  volumes</i>
                    <i class="d-block fas fa-minus"><?php echo $obs_entrada; ?></i>
                    <i class="d-block fas fa-minus"><?php echo $obs_saida; ?></i>
                    <i class="d-block fas fa-minus"><?php echo $datahora_saida; ?></i>
                </div>
                    <br>
                <?php
                    }
            }//fim do if de pesagens
                ?>


        </div>


    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>