<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_l']) and is_numeric($_GET['id_l'])){
    $lote=fncgetlote($_GET['id_l']);
}else{

}
$cordalinha = "info ";

?>
<!--/////////////////////////////////////////////////////-->

<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">

        <?php
        include_once("includes/vl_cab.php");
        ?>

        <!-- esquerdo -->
        <div class="col-md-7">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Mover lote
                </div>
                <div class="card-body">
                    <form class="form-signin" action="index.php?pg=Vl&id_l=<?php echo $_GET['id_l']; ?>&aca=mudalocalizacao" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="localizacao">LOCALIZAÇÃO:</label>
                                <input id="id_l" type="hidden" class="txt bradius" name="id_l" value="<?php echo $lote['id']; ?>"/>
                                <select name="localizacao" id="localizacao" class="form-control input-sm" data-live-search="true">
                                    <?php
                                    $getlocalizacao=fncgetlocal($lote['localizacao']);
                                    ?>
                                    <option selected="" data-tokens="<?php echo $getlocalizacao['nome'];?>" value="<?php echo $lote['localizacao']; ?>">
                                        <?php echo $getlocalizacao['nome'];?>
                                    </option>
                                    <?php
                                    foreach (fnclocaislist() as $item) {
                                        ?>
                                        <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                            <?php echo $item['nome']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="localizacao_obs">OBS LOCALIZAÇÃO:</label>
                                <div class="input-group">
                                    <input autocomplete="off" id="localizacao_obs" placeholder="" type="text" class="form-control" name="localizacao_obs" value="<?php echo $lote['localizacao_obs']; ?>" />
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="bags_atual">NOVA CONTAGEM DE BIG BAGS:</label>
                                <div class="input-group">
                                    <input autocomplete="off" id="bags_atual" placeholder="" type="text" class="form-control" name="bags_atual" value="<?php echo $lote['bags_atual']; ?>" required />
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $('#bags_entrada').mask('00', {reverse: true});
                                    });
                                </script>
                            </div>
                            <div class="col-md-6">
                                <label for="responsavel">RESPONSÁVEL:</label>
                                <select name="responsavel" id="responsavel" class="form-control input-sm" data-live-search="true" required>
                                    <?php
                                    $getpessoa=fncgetpessoa($lote['responsavel']);
                                    ?>
                                    <option selected="" data-tokens="<?php echo $getpessoa['nome'];?>" value="<?php echo $lote['responsavel']; ?>">
                                        <?php echo $getpessoa['nome'];?>
                                    </option>
                                    <?php
                                    foreach (fncpessoaresponsavellist() as $item) {
                                        ?>
                                        <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                            <?php echo strtoupper($item['nome']); ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>


                            <div class="col-md-12">
                                <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php
        include_once("includes/vl_saidas.php");
        ?>



    </div>
</div>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>