<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_l']) and is_numeric($_GET['id_l'])){
    $lote=fncgetlote($_GET['id_l']);
}else{

}

$cordalinha = "info ";
?>
<!--/////////////////////////////////////////////////////-->

<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">

        <?php
        include_once("includes/vl_cab.php");
        ?>


        <!-- esquerdo -->
        <div class="col-md-7">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Saída para venda
                </div>
                <div class="card-body">
                    <form class="form-signin" action="index.php?pg=Vl&id_l=<?php echo $_GET['id_l']; ?>&aca=novasv" method="post" id="formx">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="id_destino">VENDA:</label>
                                <input id="id_l" type="hidden" class="txt bradius" name="id_l" value="<?php echo $lote['id']; ?>"/>
                                <input id="tipo_saida" type="hidden" class="txt bradius" name="tipo_saida" value="1"/>
                                <select name="id_destino" id="id_destino" class="form-control input-sm" data-live-search="true" required>

                                    <option selected data-tokens="" value="">
                                        Nada selecionado
                                    </option>
                                    <?php
                                    foreach (fncgetultimasvendas() as $item) {
                                        ?>
                                        <option data-tokens="" value="<?php echo $item['id'];?>">
                                            CONTRATO:<?php echo $item['prefixo']."-".$item['contrato']." ".$item['descricao']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="peso">PESO PARA SAÍDA:</label>
                                <div class="input-group">
                                    <input autocomplete="off" id="peso" placeholder="" type="number" class="form-control" name="peso" value="0" required max="<?php echo $lote['peso_atual']; ?>" min="0" step="10" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">,000 Kg</span>
                                    </div>
                                </div>
                                <script>
                                    jQuery(document).ready(function(){
                                        jQuery('input').on('keyup',function(){
                                            if(jQuery(this).attr('name') === 'informacao'){
                                                return false;
                                            }

                                            var soma1 = (jQuery('#peso').val() == '' ? 0 : jQuery('#peso').val());
                                            var pesoliquido = (parseInt(soma1));
                                            var sacas = (parseInt(pesoliquido) / 60);
                                            // var textoinfo = pesoliquido + "KG ou " + sacas + " volumes";
                                            var textoinfo = sacas + " volumes";
                                            jQuery('#informacao').val(textoinfo);
                                        });
                                    });
                                </script>
                            </div>
                            <div class="col-md-6">
                                <label for="">INFORMAÇÃO</label>
                                <div class="input-group">
                                    <input  id="informacao" type="text" class="form-control" name="informacao" value="0" readonly="readonly"/>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <label for="bags_atual">NOVA CONTAGEM DE BIG BAGS:</label>
                                <div class="input-group">
                                    <input autocomplete="off" id="bags_atual" placeholder="" type="text" class="form-control" name="bags_atual" value="<?php echo $lote['bags_atual']; ?>" required />
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        $('#bags_entrada').mask('00', {reverse: true});
                                    });
                                </script>
                            </div>

                            <div class="col-md-6">
                                <label for="caixa">ACRESCENTAR A CAIXA:</label>
                                <select name="caixa" id="caixa" class="form-control input-sm" data-live-search="true">
                                    <?php
                                    foreach (fnccaixaslista() as $item) {
                                        ?>
                                        <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                            <?php echo $item['nome']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                    <option data-tokens="" value="0" selected>
                                        NÃO É NECESSÁRIO
                                    </option>
                                </select>
                            </div>


                            <div class="col-md-12">
                                <input type="submit" name="enviar" id="enviar" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                            </div>
                            <script>
                                var formID = document.getElementById("formx");
                                var send = $("#enviar");

                                $(formID).submit(function(event){
                                    if (formID.checkValidity()) {
                                        send.attr('disabled', 'disabled');
                                        send.attr('value', 'AGUARDE...');
                                    }
                                });
                            </script>
                        </div>
                    </form>
                </div>
            </div>
        </div>

<?php
include_once("includes/vl_saidas.php");
?>



    </div>
</div>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>