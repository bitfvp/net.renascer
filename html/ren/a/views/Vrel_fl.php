<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatorio de lotes por fornecedor-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<div class="row">
<div class="col-md-2"></div>
    <div class="col-md-6">
<!-- =============================começa conteudo======================================= -->
        <div class="card">
            <div class="card-header bg-info text-light">
            Relatório de lotes por fornecedor
            </div>
            <div class="card-body">
                <form action="index.php?pg=Vrel_flprint" method="post" target="_blank" name="form1">
                    <input type="submit" id="btnsub" class="btn btn-lg btn-success btn-block mb-2" value="GERAR RELATÓRIO"/>

                    <div class="form-group">
                        <label for="data_inicial">DATA INICIAL:</label>
                        <input id="data_inicial" type="date" class="form-control" name="data_inicial" value="<?php echo date('Y-m')."-01";?>" autofocus required/>
                    </div>

                    <div class="form-group">
                        <label for="data_final">DATA FINAL:</label>
                        <input id="data_final" type="date" class="form-control" name="data_final" value="<?php echo date('Y-m-t');?>" required/>
                    </div>

                    <div class="form-group">
                        <label for="fornecedor">FORNECEDOR:</label>
                        <select name="fornecedor" id="fornecedor" class="form-control" required>
                            <option selected="" value="">
                                Selecione...
                            </option>
                            <?php
                            foreach(fncpessoafornecedorlist() as $item){
                                ?>
                                <option value="<?php echo $item['id']; ?>"><?php echo $item['nome']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                </form>
            </div>
        </div>

<!-- =============================fim conteudo======================================= -->       
    </div>
    <div class="col-md-2"></div>
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>