<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_6"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $liga=fncgetligas($_GET['id']);
    $cliente=fncgetpessoa($liga['cliente']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vligas_lista");
    exit();
}


?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
        <div class="col-8">
            <h2>BLEND</h2>
            <h4>CLIENTE: <strong><?php echo strtoupper($cliente['nome']); ?></strong></h4>
            <h4>DATA: <strong><?php echo dataRetiraHora($liga['data_ts']); ?></strong></h4>
        </div>
        <div class="col-4 text-right">
            <h4 class="">CONTRATO: <strong><?php echo $liga['contrato']; ?></strong></h4>
            <h4>DESCRIÇÃO: <strong><?php echo $liga['descricao']; ?></strong></h4>
        </div>
    </div>

    <hr class="hrgrosso">
    <h3>Lotes</h3>
    <?php
    try{
        $sql = "SELECT "
            ."ren_ligas_lotes.id, "
            ."ren_ligas_lotes.data_ts, "
            ."ren_ligas_lotes.usuario, "
            ."ren_ligas_lotes.liga, "
            ."ren_ligas_lotes.lote, "
            ."ren_ligas_lotes.sacas "
            ."FROM ren_ligas_lotes INNER JOIN ren_entradas_lotes ON ren_entradas_lotes.id = ren_ligas_lotes.lote "
            ."WHERE ren_ligas_lotes.id>0 and ren_ligas_lotes.liga=:liga "
            ."ORDER BY ren_entradas_lotes.localizacao ASC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":liga", $_GET['id']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $liga_lotes = $consulta->fetchAll();
    $liga_lotes_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-hover table-striped">
                <thead class="thead-inverse thead-dark">
                <tr>
                    <th>DATA</th>
                    <th>PRODUTO</th>
                    <th>LOTE</th>
                    <th>FORNECEDOR</th>
                    <th>ATUAL</th>
                    <th>LOCAL</th>
                    <th>MARCADO</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sacastotais=0;
                foreach ($liga_lotes as $dados){
                    $lote = fncgetlote($dados["lote"]);
                    $localizacao=fncgetlocal($lote['localizacao'])['nome'];
                    $localizacao_obs = $lote["localizacao_obs"];
                    $entrada=fncgetentrada($lote["romaneio"]);
                    $fornecedor = fncgetpessoa($entrada['fornecedor'])['nome'];
                    $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);
                    $letra=fncgetletra($lote["letra"]);
                    $loteinfo=$romaneio_tipo.$entrada['romaneio']." ".$letra;
                    $data_ts = dataRetiraHora($lote["data_ts"]);
                    $tipo_cafe = fncgetprodutos($lote["tipo_cafe"])['abrev'];


                    $peso_entrada = mask($lote["peso_entrada"],'######')." Kg";

                    $peso_atual = $lote["peso_atual"];
                    $sacas=$peso_atual/60;
                    $sacas=number_format($sacas, 2, '.', ',');
                    $bags_atual = $lote["bags_atual"];




                    $origem = fncgetlocal($dados["origem"])['nome'];
                    $p_bo = $dados["p_bo"];
                    $bo = $dados["bo"];
                    ?>

                    <tr id="" class="">
                        <td><?php echo $data_ts ?></td>
                        <td><?php echo $tipo_cafe; ?></td>
                        <td><?php echo $loteinfo; ?></td>
                        <td><?php echo strtoupper($fornecedor); ?></td>
                        <td><?php
                            echo $peso_atual
                                ."KG "
                                .$sacas
                                ." volumes <br>"
                                .$bags_atual
                                ." bags";?></td>
                        <td><?php echo $localizacao. " ". $localizacao_obs;?></td>
                        <td><?php echo $dados['sacas'];?>v</td>
                    </tr>
                    <?php
                    $sacastotais=$sacastotais+$dados['sacas'];
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-6 border">
            QUANTIDADE TOTAL EM VOLUMES: <strong><?php echo $sacastotais; ?></strong>
        </div>
    </div>

    <br>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Usuário: <strong><?php echo fncgetusuario($_SESSION['id'])['nome']; ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print()
</SCRIPT>