<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
//    $a="entradasave";
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vl_lista");
    exit();
}
if (isset($_GET['id_l']) and is_numeric($_GET['id_l'])){
    $a="lotesave";
    $lote=fncgetlote($_GET['id_l']);
    if ($lote['p_bo']==1){
        $p_bo="checked";
        $p_bon="";
    }else{
        $p_bo="";
        $p_bon="checked";
    }
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vl_lista");
    exit();
}

$romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);
$ref=$entrada["ref"];
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">

        <?php
        include_once("includes/lt_cab.php");
        ?>

        <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-info text-light">
                        Edição de lote
                    </div>
                    <div class="card-body">
                        <form action="<?php echo "index.php?pg=Vl_editar&id_e={$_GET['id_e']}&id_l={$_GET['id_l']}&aca={$a}"; ?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-4">
                                    <input id="id_l" type="hidden" class="txt bradius" name="id_l" value="<?php echo $lote['id']; ?>"/>
                                    <input id="pg" type="hidden" class="txt bradius" name="pg" value="<?php echo $_GET['pg']; ?>"/>
                                    <label for="letra">LETRA:</label>
                                    <select name="letra" id="letra" class="form-control input-sm" data-live-search="true" required>
                                        <?php
                                        $letra=fncgetletra($lote["letra"]);
                                        ?>
                                        <option selected="" value="<?php echo $lote['letra']; ?>">
                                            <?php echo $letra;?>
                                        </option>
                                        <?php
                                        foreach (fncletralist() as $item) {
                                            ?>
                                            <option value="<?php echo $item['id'];?>">
                                                <?php echo $item['letra']; ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-8">
                                    <label for="tipo_cafe">PRODUTO:</label>
                                    <select name="tipo_cafe" id="tipo_cafe" class="form-control input-sm" data-live-search="true" required>
                                        <?php
                                        $gettipo_cafe=fncgetprodutos($lote['tipo_cafe']);
                                        ?>
                                        <option selected="" data-tokens="<?php echo $gettipo_cafe['nome'];?>" value="<?php echo $lote['tipo_cafe']; ?>">
                                            <?php echo $gettipo_cafe['nome'];?>
                                        </option>
                                        <?php
                                        foreach (fncprodutoslist() as $item) {
                                            ?>
                                            <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                                <?php echo $item['nome']; ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="peso_entrada">PESO DE ENTRADA:</label>
                                    <div class="input-group">
                                        <input autocomplete="off" id="peso_entrada" placeholder="" type="text" class="form-control" name="peso_entrada" value="<?php echo $lote['peso_entrada']; ?>" required />
                                        <div class="input-group-append">
                                            <span class="input-group-text">,000 Kg</span>
                                        </div>
                                    </div>
                                    <script>
                                        $(document).ready(function(){
                                            $('#peso_entrada').mask('000000', {reverse: true});
                                        });
                                    </script>
                                </div>
                                <div class="col-md-6">
                                    <label for="bags_entrada">BIG BAGS ENTRADA:</label>
                                    <div class="input-group">
                                        <input autocomplete="off" id="bags_entrada" placeholder="" type="text" class="form-control" name="bags_entrada" value="<?php echo $lote['bags_entrada']; ?>" required />
                                    </div>
                                    <script>
                                        $(document).ready(function(){
                                            $('#bags_entrada').mask('00', {reverse: true});
                                        });
                                    </script>
                                </div>



                                <div class="col-md-6">
                                    <label for="peso_atual">PESO ATUAL:</label>
                                    <div class="input-group">
                                        <input autocomplete="off" id="peso_atual" placeholder="" type="text" class="form-control" name="peso_atual" value="<?php echo $lote['peso_atual']; ?>" required />
                                        <div class="input-group-append">
                                            <span class="input-group-text">,000 Kg</span>
                                        </div>
                                    </div>
                                    <script>
                                        $(document).ready(function(){
                                            $('#peso_atual').mask('000000', {reverse: true});
                                        });
                                    </script>
                                </div>
                                <div class="col-md-6">
                                    <label for="bags_atual">BIG BAGS ATUAL:</label>
                                    <div class="input-group">
                                        <input autocomplete="off" id="bags_atual" placeholder="" type="text" class="form-control" name="bags_atual" value="<?php echo $lote['bags_atual']; ?>" required />
                                    </div>
                                    <script>
                                        $(document).ready(function(){
                                            $('#bags_atual').mask('00', {reverse: true});
                                        });
                                    </script>
                                </div>




                                <div class="col-md-4">
                                    <label for="localizacao">LOCALIZAÇÃO:</label>
                                    <select name="localizacao" id="localizacao" class="form-control input-sm" data-live-search="true" required>
                                        <?php
                                        $getlocalizacao=fncgetlocal($lote['localizacao']);
                                        ?>
                                        <option selected="" data-tokens="<?php echo $getlocalizacao['nome'];?>" value="<?php echo $lote['localizacao']; ?>">
                                            <?php echo $getlocalizacao['nome'];?>
                                        </option>
                                        <?php
                                        foreach (fnclocaislist() as $item) {
                                            ?>
                                            <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                                <?php echo $item['nome']; ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="localizacao_obs">OBS LOCALIZAÇÃO:</label>
                                    <div class="input-group">
                                        <input autocomplete="off" id="localizacao_obs" placeholder="" type="text" class="form-control" name="localizacao_obs" value="<?php echo $lote['localizacao_obs']; ?>" />
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <label for="p_bo">B.O.</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="p_bo" id="p_bo" value="1" <?php echo $p_bo ?> >
                                        <label class="form-check-label" for="p_bo">
                                            SIM
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="p_bo" id="p_bo" value="0" <?php echo $p_bon; ?> >
                                        <label class="form-check-label" for="p_bo">
                                            NÃO
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <label for="bo">B.O. OBS</label>
                                    <textarea id="bo" onkeyup="limite_textarea(this.value,250,bo,'contbo')" maxlength="250" class="form-control" rows="1" name="bo"><?php echo $lote['bo']; ?></textarea>
                                    <span id="contbo">250</span>/250
                                </div>

                                <div class="col-md-12">
                                    <label for="obs">OBS GERAIS</label>
                                    <textarea id="obs" onkeyup="limite_textarea(this.value,250,obs,'cont')" maxlength="250" class="form-control" rows="1" name="obs"><?php echo $lote['obs']; ?></textarea>
                                    <span id="cont">250</span>/250
                                </div>

                                <div class="col-md-12">
                                    <label for="responsavel">RESPONSÁVEL:</label>
                                    <select name="responsavel" id="responsavel" class="form-control input-sm" data-live-search="true" required>
                                        <?php
                                        $getpessoa=fncgetpessoa($lote['responsavel']);
                                        ?>
                                        <option selected="" data-tokens="<?php echo $getpessoa['nome'];?>" value="<?php echo $lote['responsavel']; ?>">
                                            <?php echo $getpessoa['nome'];?>
                                        </option>
                                        <?php
                                        foreach (fncpessoaresponsavellist() as $item) {
                                            ?>
                                            <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                                                <?php echo strtoupper($item['nome']); ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-lg btn-success btn-block mt-2" value="SALVAR"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>


    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>