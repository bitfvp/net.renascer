<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatorio de vendas-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->
<div class="row">
<div class="col-md-2"></div>
    <div class="col-md-6">
<!-- =============================começa conteudo======================================= -->
        <div class="card">
            <div class="card-header bg-info text-light">
            Relatório de vendas
            </div>
            <div class="card-body">

                <script type="text/javascript">
                    function ch_f() {
                        var butf = document.getElementById("btnf");
                        var butl = document.getElementById("btnl");
                        butf.className='btn d-none';
                        butl.className='btn btn-info float-right my-1';
                        document.getElementById("btnsub").value= "GERAR RELATÓRIO COMPLETO"
                        document.form1.action = "index.php?pg=Vrel_vendasprint";
                    }

                    function ch_l() {
                        var butf = document.getElementById("btnf");
                        var butl = document.getElementById("btnl");
                        butf.className='btn btn-info float-right my-1';
                        butl.className='btn d-none';
                        document.getElementById("btnsub").value= "GERAR RELATÓRIO BÁSICO"
                        document.form1.action = "index.php?pg=Vrel_vendasprint_basico";
                    }
                </script>


                <form action="index.php?pg=Vrel_vendasprint_basico" method="post" target="_blank" name="form1">
                    <input type="submit"id="btnsub" class="btn btn-lg btn-success btn-block" value="GERAR RELATÓRIO BÁSICO"/>

                    <div class="form-group">
                        <label for="data_inicial">DATA INICIAL:</label>
                        <input id="data_inicial" type="date" class="form-control" name="data_inicial" value="<?php echo date('Y-m')."-01";?>" autofocus required/>
                    </div>

                    <div class="form-group">
                        <label for="data_final">DATA FINAL:</label>
                        <input id="data_final" type="date" class="form-control" name="data_final" value="<?php echo date('Y-m-t');?>" required/>
                    </div>
                </form>
                <?php
                echo "<button class='btn d-none' id='btnl' onclick='ch_l()' >TROCAR TIPO DE RELATÓRIO</button>\n";
                echo "<button class='btn btn-info float-right my-1' id='btnf' onclick='ch_f()' >TROCAR TIPO DE RELATÓRIO</button>\n";
                ?>
            </div>
        </div>

<!-- =============================fim conteudo======================================= -->       
    </div>
    <div class="col-md-2"></div>
</div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>