<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['caixa']) and is_numeric($_GET['caixa'])){
    $a="caixanew";
    $caixa=fncgetcaixa($_GET['caixa']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vhome");
    exit();
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

    $(document).ready(function () {
        $('#serchfornecedor input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultfornecedor");
            if (inputValf.length) {
                $.get("includes/fornecedor.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultfornecedor p", function () {
            var fornecedor = this.id;
            $.when(
                // $(this).parents("#serchfornecedor").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchfornecedor").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultfornecedor").empty()

                $(this).parents("#serchfornecedor").find('input[type="text"]').val($(this).text()),
                $('#fornecedor').val(fornecedor),
                $(this).parent(".resultfornecedor").empty()

            ).then(
                function() {
                    // roda depois de acao1 e acao2 terminarem
                    // setTimeout(function() {
                    //     //do something special
                    //     $( "#gogo" ).click();
                    // }, 500);
                });
        });
    });

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vcaixa&aca={$a}"; ?>" method="post" id="formvenda1">
        <h3 class="form-cadastro-heading">Cadastro de visualização de caixa- <?php echo $caixa['nome']; ?></h3>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <input id="caixa" type="hidden" class="txt bradius" name="caixa" value="<?php echo $caixa['id']; ?>"/>
                    <label for="peso">PESO:</label>
                    <div class="input-group">
                        <input autocomplete="off" id="peso" placeholder="" type="text" class="form-control" name="peso" value="0" required />
                        <div class="input-group-append">
                            <span class="input-group-text">,000 Kg</span>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function(){
                            $('#peso').mask('000.000', {reverse: true});
                        });
                    </script>
            </div>

            <div class="col-md-6" id="serchfornecedor">
                <label for="fornecedor">FORNECEDOR</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="r_corretor">
                            <a href="index.php?pg=Vpessoa_lista" target="_blank" class="fa fas fa-plus text-success "></a>
                        </span>
                    </div>
                    <?php
                    $c_fornecedor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="f<?php echo $c_fornecedor;?>" id="f<?php echo $c_fornecedor;?>" value="" placeholder="Click no resultado ao aparecer" required/>
                    <input id="fornecedor" autocomplete="false" type="hidden" class="form-control" name="fornecedor" value="" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_fornecedor">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultfornecedor"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_fornecedor").click(function(ev){
                            document.getElementById('fornecedor').value='';
                            document.getElementById('f<?php echo $c_fornecedor;?>').value='';
                        });
                    });
                </script>
            </div>



            <div class="col-md-6">
                <label for="lote">LOTE:</label>
                <select name="lote" id="lote" class="form-control input-sm" data-live-search="true" required>
                    <?php
                    foreach (fncgetultimoslotes() as $item) {
                        $entrada=fncgetentrada($item['romaneio']);
                        $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

                        $letra=fncgetletra($item["letra"]);

                        ?>
                        <option data-tokens="" value="<?php echo $item['id'];?>">
                            ROMANEIO:<?php echo $romaneio_tipo.$entrada['romaneio']." ".$letra; ?>
                        </option>
                        <?php
                    }
                    ?>
                    <option data-tokens="" value="0" selected>
                        ROMANEIO NÃO DEFINIDO
                    </option>
                </select>
            </div>

            <div class="col-md-6">
                <label for="tipo_cafe">PRODUTO:</label>
                <select name="tipo_cafe" id="tipo_cafe" class="form-control input-sm" data-live-search="true">
                    <?php
                    foreach (fncprodutoslist() as $item) {
                        ?>
                        <option data-tokens="<?php echo $item['nome'];?>" value="<?php echo $item['id'];?>">
                            <?php echo $item['nome']; ?>
                        </option>
                        <?php
                    }
                    ?>
                    <option data-tokens="" value="0" selected>
                        TIPO DE CAFÉ NÃO DEFINIDO
                    </option>
                </select>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" id="gogo" class="btn btn-block btn-outline-success mt-4">
            </div>
        </div>


    </form>
</div>

<?php
//for ($i = 1; $i <= 1000000; $i++) {
//    echo $i."  ".get_criptografa64($i)."<br>";
//}
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>