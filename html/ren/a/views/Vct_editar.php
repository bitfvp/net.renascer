<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vct_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="ct_save";
    $entrada=fncgetentrada($_GET['id']);
    $romaneiotemp=$entrada['romaneio'];
}else{

    if (date('Y-m-d')>='2023-03-04'){
//        echo "a data é maior ou igual";
        $sql = "SELECT Max(romaneio) FROM ";
        $sql.="ren_entradas where romaneio_tipo=3 and ref='M' ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $mid = $consulta->fetch();
        $sql=null;
        $consulta=null;
    }else{
//        echo "ainda não";
        $sql = "SELECT Max(romaneio) FROM ";
        $sql.="ren_entradas where romaneio_tipo=3 and ref='' ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        $mid = $consulta->fetch();
        $sql=null;
        $consulta=null;
    }

    $a="ct_new";
    $romaneiotemp=$mid[0]+1;
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">
    $(document).ready(function () {
        $('#serchfornecedor input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultfornecedor");
            if (inputValf.length) {
                $.get("includes/fornecedor.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });



        // Set search input value on click of result item
        $(document).on("click", ".resultfornecedor p", function () {
            var fornecedor = this.id;
            $.when(
                // $(this).parents("#serchfornecedor").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchfornecedor").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultfornecedor").empty()

                $(this).parents("#serchfornecedor").find('input[type="text"]').val($(this).text()),
                $('#fornecedor').val(fornecedor),
                $(this).parent(".resultfornecedor").empty()

            ).then(
                function() {
                    // roda depois de acao1 e acao2 terminarem
                    setTimeout(function() {
                        //do something special
                        $( "#gogo" ).click();
                    }, 500);
                });
        });
    });



</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vct_lista&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro de maquinação</h3>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $entrada['id']; ?>"/>
                <label for="romaneio">NÚMERO DE ROMANEIO</label>
                <input autocomplete="off" id="romaneio" type="text" class="form-control" name="romaneio" value="<?php echo $romaneiotemp; ?>" placeholder="Preencha com o número de romaneio"/>
                <script>
                    $(document).ready(function(){
                        $('#romaneio').mask('0000000', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-4">
                <label for="nota">NÚMERO DE NOTA <i class="text-warning">*</i></label>
                <input autocomplete="off" id="nota" type="text" class="form-control" name="nota" value="<?php echo $entrada['nota']; ?>" placeholder="Preencha com o número da NF" />
                <script>
                    $(document).ready(function(){
                        $('#nota').mask('000.000.000', {reverse: true});
                    });
                </script>
            </div>

            <div class="col-md-3">
                <label for="controle_interno">POSSUI CONTROLE INTERNO</label>
                <select name="controle_interno" id="controle_interno" class="form-control" required>
                    <option selected="" value="<?php if ($entrada['controle_interno'] == "") {
                        echo null;
                    } else {
                        echo $entrada['controle_interno'];
                    } ?>">
                        <?php
                        if ($entrada['controle_interno'] == null) {
                            echo "Selecione...";
                        }

                        if ($entrada['controle_interno'] == 1) {
                            echo "SIM";
                        }
                        if ($entrada['controle_interno'] == 2) {
                            echo "NÃO";
                        }
                        ?>
                    </option>
                    <option value="1">SIM</option>
                    <option value="2">NÃO</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6" id="serchfornecedor">
                <label for="fornecedor">FORNECEDOR</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="r_corretor">
                            <a href="index.php?pg=Vpessoa_lista" target="_blank" class="fa fas fa-plus text-success "></a>
                        </span>
                    </div>
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_fornecedor_id=$entrada['fornecedor'];
                        $v_fornecedor=fncgetpessoa($entrada['fornecedor'])['nome'];

                    }else{
                        $v_fornecedor_id="";
                        $v_fornecedor="";
                    }
                    $c_fornecedor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="f<?php echo $c_fornecedor;?>" id="f<?php echo $c_fornecedor;?>" value="<?php echo $v_fornecedor; ?>" placeholder="Click no resultado ao aparecer" required/>
                    <input id="fornecedor" autocomplete="false" type="hidden" class="form-control" name="fornecedor" value="<?php echo $v_fornecedor_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_fornecedor">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultfornecedor"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_fornecedor").click(function(ev){
                            document.getElementById('fornecedor').value='';
                            document.getElementById('f<?php echo $c_fornecedor;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-6">
                <label for="status">STATUS:</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="r_corretor">
                            <i class="fa fas fa-unlock-alt text-success"></i>
                        </span>
                    </div>
                    <select name="status" id="status" class="form-control" required>
                        <option selected="" value="<?php
                        if(is_null($entrada['status'])){
                            echo 1;
                        }
                        if($entrada['status']=="0"){
                            echo 0;
                        }
                        if($entrada['status']=="1"){
                            echo 1;
                        }
                        ?>">
                            <?php
                            if(is_null($entrada['status'])){echo"ATIVO";}
                            if($entrada['status']=="0"){echo"FECHADO";}
                            if($entrada['status']=="1"){echo"ATIVO";} ?>
                        </option>
                        <option value="0">FECHADO</option>
                        <option value="1">ATIVO</option>
                    </select>
                </div>
            </div>

            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" name="gogo" id="gogo" class="btn btn-block btn-success mt-4">
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>


        </div>

    </form>
</div>

<?php
//for ($i = 1; $i <= 1000000; $i++) {
//    echo $i."  ".get_criptografa64($i)."<br>";
//}
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>