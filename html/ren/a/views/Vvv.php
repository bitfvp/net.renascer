<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_v']) and is_numeric($_GET['id_v'])){
//    $a="entradasave";
    $venda=fncgetvenda($_GET['id_v']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vv_lista");
    exit();
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">
        <div class="col-md-5">
            <blockquote class="blockquote blockquote-success">
                <header>
                    CONTRATO:
                    <strong class="text-info"><?php echo $venda['prefixo']."-".$venda['contrato']; ?>&nbsp;&nbsp;</strong>
                </header>
                <h6>
                    DESCRIÇÃO:
                    <strong class="text-info"><?php echo $venda['descricao']; ?>&nbsp;&nbsp;</strong><br>
                    CLIENTE:
                    <strong class="text-info"><?php echo strtoupper(fncgetpessoa($venda['cliente'])['nome']); ?></strong><br>
                    TRANSPORTADORA:
                    <strong class="text-info"><?php echo strtoupper(fncgetpessoa($venda['transportadora'])['nome']); ?></strong><br>
                    NOTA FISCAL:
                    <strong class="text-info"><?php echo $venda['nota_fiscal']; ?></strong><br>
                    RETORNO DE ESTOQUE:
                    <strong class="text-info"><?php echo $venda['retorno_de_estoque']; ?></strong><br>

                    <?php
                    if ($venda["status"]==0 or $venda["status"]==""){
                        echo "VENDA FINALIZADA <i class='fas fa-lock fa-2x' title='venda finalizada'></i><br>";
                    }
                    ?>

                    <footer class="blockquote-footer">
                        <?php echo fncgetusuario($venda['usuario'])['nome']?>&nbsp;&nbsp;
                    </footer>
            </blockquote>
            <div class="btn-group" role="group" aria-label="">
                    <a href="index.php?pg=Vv&id_v=<?php echo $_GET['id_v'] ?>" title="Editar entrada" class="btn btn-sm btn-primary fas fa-pen text-dark">
                        <br>EDITAR ROMANEIO
                    </a>
                <a href="index.php?pg=Vvvv_print1&id_v=<?php echo $_GET['id_v']; ?>" target="_blank" title="comprovante" class="btn btn-sm btn-dark fas fa-print">
                    <br>IMPRIMIR COMPROVANTE
                </a>

            </div>
        </div>

        <div class="col-md-7">
            <h3>PESAGENS</h3>
                <?php
                $sql = "SELECT * FROM "
                    ."ren_vendas_pesagens "
                    ."WHERE venda=:venda "
                    ."order by ren_vendas_pesagens.data_ts DESC ";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":venda", $_GET['id_v']);
                $consulta->execute();
                $motoristas = $consulta->fetchAll();
                $motoristas_quant = $consulta->rowCount();
                ?>

            <table class="table table-sm table-hover table-striped">
            <thead class="thead-inverse thead-dark">
            <tr>
                <th>#</th>
                <th>MOTORISTA</th>
                <th>PLACA</th>
                <th>PESO INICIAL</th>
                <th>PESO FINAL</th>
                <th>PESO LIQUIDO</th>
            </tr>
            </thead>
                <?PHP
                foreach ($motoristas as $dados){
                    $motorista = fncgetpessoa($dados["motorista"])['nome'];
                    $placa = $dados["placa"];
                    $peso_entrada = mask($dados["peso_entrada"],'######')." Kg";
                    $peso_saida = mask($dados["peso_saida"],'######')." Kg";
                    $peso_liquido=$dados["peso_saida"]-$dados["peso_entrada"];
                    $sacas=$peso_liquido/60;
                    $sacas=number_format($sacas, 1, '.', ',');
                    $peso_liquido.=" Kg";
                    ?>

                    <tr id="" class="">
                        <th scope="row" id="">
                            <small></small>
                        </th>
                        <td><?php echo strtoupper($motorista); ?></td>
                        <td><?php echo $placa; ?></td>
                        <td><?php echo $peso_entrada; ?></td>
                        <td><?php echo $peso_saida;?></td>
                        <td><?php echo $peso_liquido. " ou ".$sacas." volumes";?></td>
                    </tr>
                    <?php
                }
                ?>

            </table>

        </div>

    </div>
    <br>
    <?php
    //1 venda
    //2ct
    //3 devolucao
    try{
        $sql = "SELECT * FROM "
            ."ren_entradas_lotes_saidas "
            ."WHERE id_destino=:id_destino and tipo_saida=1 "
            ."order by ren_entradas_lotes_saidas.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":id_destino", $_GET['id_v']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <H3>SAÍDAS PARA VENDA</H3>
            <table class="table table-sm table-hover table-striped">
                <thead class="thead-inverse thead-dark">
                    <tr>
                        <th>#</th>
                        <th>DATA</th>
                        <th>PRODUTO</th>
                        <th>LOTE</th>
                        <th>FORNECEDOR</th>
                        <th>PESO</th>
                        <th>ORIGEM</th>
                        <th>BO</th>
                    </tr>
                </thead>
                <tfoot>
                <tr>
                    <th scope="row" colspan="4" >
                    </th>
                    <td colspan="4"><?php echo $pesagens_quant;?> Saída(s) encontrada(s)</td>
                </tr>
                </tfoot>
                <tbody>
                <?php
                foreach ($pesagens as $dados){
                    $peso_entrada = mask($dados["peso_entrada"],'######')." Kg";
                    $obs_entrada = $dados["obs_entrada"];
                    $peso_saida = mask($dados["peso_saida"],'######')." Kg";
                    $obs_saida = $dados["obs_saida"];

                    $data_ts = datahoraBanco2data($dados["data_ts"]);
                    $tipo_cafe = fncgetprodutos($dados["tipo_cafe"])['nome'];
                    $lote = fncgetlote($dados["lote"]);
                    $entrada=fncgetentrada($lote["romaneio"]);
                    $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

                    $letra=fncgetletra($lote["letra"]);

                    $loteinfo=$romaneio_tipo.$entrada['ref'].$entrada['romaneio']." ".$letra;
                    $fornecedor = fncgetpessoa($entrada['fornecedor'])['nome'];

                    $peso = $dados["peso"];
                    $sacas=$peso/60;
                    $sacas=number_format($sacas, 1, '.', ',');

                    $origem = fncgetlocal($dados["origem"])['nome'];
                    $p_bo = $dados["p_bo"];
                    $bo = $dados["bo"];

                    $usuario = fncgetusuario($dados["usuario"])['nome'];
                    ?>

                    <tr id="" class="<?php echo $cordalinha; ?>">
                        <th scope="row" id="">
                            <small><?php echo $pe_id; ?></small>
                        </th>
                        <td><?php echo $data_ts ?></td>
                        <td><?php echo $tipo_cafe; ?></td>
                        <td><?php echo $loteinfo; ?></td>
                        <td><?php echo strtoupper($fornecedor); ?></td>
                        <td><?php echo $peso. "KG OU ".$sacas." VOLUMES";?></td>
                        <td><?php echo $origem; ?></td>
                        <td><?php echo $bo; ?></td>
                    </tr>
                    <?php
                }
                ?>




                </tbody>
            </table>
        </div>
    </div>


</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>