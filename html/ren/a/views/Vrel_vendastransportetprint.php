<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
        <div class="col-12">
            <h3>TRANSPORTE EM VENDAS POR TRANSPORTADORA</h3>
            <h6>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h6>
        </div>
    </div>
    <hr class="hrgrosso">

    <?php
    if (isset($_POST['transportadora']) and $_POST['transportadora']>0){
        $sql = "SELECT "
            ."ren_pessoas.nome AS transportadora, "
            ."ren_vendas_pesagens.motorista, "
            ."ren_vendas_pesagens.placa, "
            ."ren_vendas_pesagens.peso_entrada, "
            ."ren_vendas_pesagens.peso_saida, "
            ."ren_vendas_pesagens.data_ts, "
            ."ren_vendas.cliente "
            ."FROM "
            ."ren_vendas_pesagens "
            ."INNER JOIN ren_vendas ON ren_vendas_pesagens.venda = ren_vendas.id "
            ."INNER JOIN ren_pessoas ON ren_vendas.transportadora = ren_pessoas.id "
            ."WHERE "
            ."ren_vendas_pesagens.id <> 0 "
            ." AND ((ren_vendas_pesagens.data_ts)>=:inicial) And ((ren_vendas_pesagens.data_ts)<=:final) "
            ." AND ren_vendas.transportadora = :transportadora "
            ."order by transportadora ASC, ren_vendas.id ASC, ren_vendas_pesagens.motorista ASC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->bindValue(":transportadora", $_POST['transportadora']);
        $consulta->execute();

    }else{
        $sql = "SELECT "
            ."ren_pessoas.nome AS transportadora, "
            ."ren_vendas_pesagens.motorista, "
            ."ren_vendas_pesagens.placa, "
            ."ren_vendas_pesagens.peso_entrada, "
            ."ren_vendas_pesagens.peso_saida, "
            ."ren_vendas_pesagens.data_ts, "
            ."ren_vendas.cliente "
            ."FROM "
            ."ren_vendas_pesagens "
            ."INNER JOIN ren_vendas ON ren_vendas_pesagens.venda = ren_vendas.id "
            ."INNER JOIN ren_pessoas ON ren_vendas.transportadora = ren_pessoas.id "
            ."WHERE "
            ."ren_vendas_pesagens.id <> 0 "
            ." AND ((ren_vendas_pesagens.data_ts)>=:inicial) And ((ren_vendas_pesagens.data_ts)<=:final) "
            ."order by transportadora ASC, ren_vendas.id ASC, ren_vendas_pesagens.motorista ASC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->execute();
    }

    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    ?>
    <table class="table table-sm table-stripe table-hover table-condensed">
        <thead class="thead-dark">
        <tr>
            <th scope="col"><small>TRANSPORTADORA</small></th>
            <th scope="col"><small>MOTORISTA</small></th>
            <th scope="col"><small>PLACA</small></th>
            <th scope="col"><small>VOL.</small></th>
            <th scope="col"><small>CLIENTE/DESTINO</small></th>
            <th scope="col"><small>DATA</small></th>
        </tr>
        </thead>
        <tbody>

    <?php
        foreach ($pesagens as $dados){
            $transportadora = strtoupper($dados["transportadora"]);

            $motorista = strtoupper(fncgetpessoa($dados["motorista"])['nome']);
            $placa = $dados["placa"];
            $peso_liquido=$dados["peso_saida"]-$dados["peso_entrada"];
            $sacas=$peso_liquido/60;
            $cliente = strtoupper(fncgetpessoa($dados["cliente"])['nome']);
            $data_ts=dataRetiraHora($dados["data_ts"]);
            ?>

                <tr id="" class="">
                    <td><?php echo $transportadora; ?></td>
                    <td><?php echo $motorista; ?></td>
                    <td><?php echo $placa; ?></td>
                    <td><?php echo number_format($sacas,1,',',' '); ?>sc</td>
                    <td><?php echo $cliente; ?></td>

                    <td><?php echo $data_ts; ?></td>
                </tr>
            <?php
        }
        ?>

        </tbody>
    </table>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>



</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>