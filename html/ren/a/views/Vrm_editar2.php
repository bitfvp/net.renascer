<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vrm_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="rm_save2";
    $entrada=fncgetentrada($_GET['id']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Ve_lista");
    exit();
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vrm_editar2&id={$_GET['id']}&aca={$a}"; ?>" method="post" id="formentrada1">
        <h3 class="form-cadastro-heading">Cadastro de Entrada</h3>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $entrada['id']; ?>"/>
                <label for="romaneio">NÚMERO DE ROMANEIO</label>
                <input readonly="readonly" autocomplete="off" id="romaneio" type="text" class="form-control" name="romaneio" value="<?php echo $entrada['romaneio']; ?>" placeholder="Preencha com o número de romaneio"/>
                <script>
                    $(document).ready(function(){
                        $('#romaneio').mask('0000000', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-3">
                <label for="nota">NÚMERO DE NOTA <i class="text-warning">*</i></label>
                <input readonly="readonly" autocomplete="off" id="nota" placeholder="" type="text" class="form-control" name="nota" value="<?php echo $entrada['nota']; ?>" placeholder="Preencha com o número da NF" />
                <script>
                    $(document).ready(function(){
                        $('#nota').mask('000.000.000', {reverse: true});
                    });
                </script>
            </div>

            <div class="col-md-3">
                    <label for="controle_interno">POSSUI CONTROLE INTERNO</label>
                    <select readonly="readonly" name="controle_interno" id="controle_interno" class="form-control">
                        <option selected="" value="<?php if ($entrada['controle_interno'] == "") {
                            echo null;
                        } else {
                            echo $entrada['controle_interno'];
                        } ?>">
                            <?php
                            if ($entrada['controle_interno'] == null) {
                                echo "Selecione...";
                            }

                            if ($entrada['controle_interno'] == 1) {
                                echo "SIM";
                            }
                            if ($entrada['controle_interno'] == 2) {
                                echo "NÃO";
                            }
                            ?>
                        </option>
                        <option value="1">SIM</option>
                        <option value="2">NÃO</option>
                    </select>
            </div>

            <div class="col-md-3">
                <label for="ocr">OCR </label>
                <input readonly="readonly" autocomplete="off" id="ocr" type="text" class="form-control" name="ocr" value="<?php echo $entrada['ocr']; ?>" placeholder="Preencha com o número da OCR" />
                <script>
                    $(document).ready(function(){
                        $('#ocr').mask('00000000', {reverse: true});
                    });
                </script>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6" id="serchfornecedor">
                <label for="fornecedor">FORNECEDOR</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_fornecedor_id=$entrada['fornecedor'];
                        $v_fornecedor=fncgetpessoa($entrada['fornecedor'])['nome'];

                    }else{
                        $v_fornecedor_id="";
                        $v_fornecedor="";
                    }
                    $c_fornecedor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input readonly="readonly" autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="f<?php echo $c_fornecedor;?>" id="f<?php echo $c_fornecedor;?>" value="<?php echo $v_fornecedor; ?>" placeholder="Click no resultado ao aparecer" required/>
                    <input readonly="readonly" id="fornecedor" autocomplete="false" type="hidden" class="form-control" name="fornecedor" value="<?php echo $v_fornecedor_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_fornecedor">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultfornecedor"></div>
                </div>
            </div>

            <div class="col-md-6" id="serchempresa">
                <label for="empresa">EMPRESA/PRODUTOR <i class="text-warning">*uso com notas</i></label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_empresa_id=$entrada['empresa'];
                        $v_empresa=fncgetpessoa($entrada['empresa'])['nome'];

                    }else{
                        $v_empresa_id="";
                        $v_empresa="";
                    }
                    $c_empresa = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input readonly="readonly" autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="e<?php echo $c_empresa;?>" id="e<?php echo $c_empresa;?>" value="<?php echo $v_empresa; ?>" placeholder="Click no resultado ao aparecer" />
                    <input readonly="readonly" id="empresa" autocomplete="false" type="hidden" class="form-control" name="empresa" value="<?php echo $v_empresa_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_empresa">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultempresa"></div>
                </div>
            </div>

            <div class="col-md-6" id="serchcorretor">
                <label for="corretor">CORRETOR</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_corretor_id=$entrada['corretor'];
                        $v_corretor=fncgetpessoa($entrada['corretor'])['nome'];

                    }else{
                        $v_corretor_id="";
                        $v_corretor="";
                    }
                    $c_corretor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input readonly="readonly" autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="c<?php echo $c_corretor;?>" id="c<?php echo $c_corretor;?>" value="<?php echo $v_corretor; ?>" placeholder="Click no resultado ao aparecer" />
                    <input readonly="readonly" id="corretor" autocomplete="false" type="hidden" class="form-control" name="corretor" value="<?php echo $v_corretor_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_corretor">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultcorretor"></div>
                </div>
            </div>


            <div class="col-md-6" id="serchmotorista">
                <label for="motorista">MOTORISTA</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_motorista_id=$entrada['motorista'];
                        $v_motorista=fncgetpessoa($entrada['motorista'])['nome'];

                    }else{
                        $v_motorista_id="";
                        $v_motorista="";
                    }
                    $c_motorista = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input readonly="readonly" autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="m<?php echo $c_motorista;?>" id="m<?php echo $c_motorista;?>" value="<?php echo $v_motorista; ?>" placeholder="Click no resultado ao aparecer"  required />
                    <input readonly="readonly" id="motorista" autocomplete="false" type="hidden" class="form-control" name="motorista" value="<?php echo $v_motorista_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_motorista">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultmotorista"></div>
                </div>
            </div>

            <div class="col-md-12" id="">
                <label for="placa">PLACAS <i class="text-info">* placas separadas por espaço</i> </label>
                <input autocomplete="off" autofocus id="placa" type="text" class="form-control text-uppercase" name="placa" value="<?php echo $entrada['placa']; ?>" placeholder="Preencha com as placas"/>
                <script>
                    function isererecebeu(valor) {
                        var str = $("#placa").val();
                        $('#placa').val(str+" "+valor);
                        // $('#quem_recebeu').focus();
                    }
                </script>
                <?php
                foreach (fncplacalist($entrada['motorista']) as $placas){
                    echo "<i id='emoji' class='btn btn-sm m-1 border btn-outline-dark' onclick=\"isererecebeu('{$placas['placa']}')\">{$placas['placa']}</i>";
                }
                ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" id="gogo" class="btn btn-block btn-success mt-4">
            </div>
        </div>


    </form>
</div>

<?php
//for ($i = 1; $i <= 1000000; $i++) {
//    echo $i."  ".get_criptografa64($i)."<br>";
//}
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>