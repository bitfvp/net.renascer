<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoasave";
    $pessoa=fncgetpessoa($_GET['id']);

    if ($pessoa['p_motorista']==1){
        $p_motorista="checked";
        $p_motoristan="";
    }else{
        $p_motorista="";
        $p_motoristan="checked";
    }
    if ($pessoa['p_cliente']==1){
        $p_cliente="checked";
        $p_clienten="";
    }else{
        $p_cliente="";
        $p_clienten="checked";
    }
    if ($pessoa['p_fornecedor']==1){
        $p_fornecedor="checked";
        $p_fornecedorn="";
    }else{
        $p_fornecedor="";
        $p_fornecedorn="checked";
    }
    if ($pessoa['p_corretor']==1){
        $p_corretor="checked";
        $p_corretorn="";
    }else{
        $p_corretor="";
        $p_corretorn="checked";
    }
    if ($pessoa['p_responsavel']==1){
        $p_responsavel="checked";
        $p_responsaveln="";
    }else{
        $p_responsavel="";
        $p_responsaveln="checked";
    }
    if ($pessoa['p_transportadora']==1){
        $p_transportadora="checked";
        $p_transportadoran="";
    }else{
        $p_transportadora="";
        $p_transportadoran="checked";
    }

}else{
    $a="pessoanew";

        $p_motorista="";
        $p_motoristan="checked";
        $p_cliente="";
        $p_clienten="checked";
        $p_fornecedor="";
        $p_fornecedorn="checked";
    $p_corretor="";
    $p_corretorn="checked";
        $p_responsavel="";
        $p_responsaveln="checked";
        $p_transportadora="";
        $p_transportadoran="checked";

}
?>
<div class="container-fluid"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vpessoa_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro</h3>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $pessoa['id']; ?>"/>
                <label for="nome">NOME</label>
                <input autocomplete="off" autofocus id="nome" placeholder="Nome completo" type="text" class="form-control" name="nome" value="<?php echo $pessoa['nome']; ?>" required/>
            </div>
            <div class="col-md-3">
                <label for="apelido">APELIDO</label>
                <input autocomplete="off" id="apelido" placeholder="" type="text" class="form-control" name="apelido" value="<?php echo $pessoa['apelido']; ?>" />
            </div>
            <div class="col-md-3">
                <label for="cpf">CPF</label>
                <input autocomplete="off" id="cpf" type="text" class="form-control" name="cpf" value="<?php echo $pessoa['cpf']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cpf').mask('000.000.000-00', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-2">
                <label for="rg">RG</label>
                <input autocomplete="off" id="rg" type="text" class="form-control" name="rg" value="<?php echo $pessoa['rg']; ?>" maxlength="19"/>
            </div>

            <div class="col-md-3">
                <label for="cpf">CNPJ</label>
                <input autocomplete="off" id="cnpj" type="text" class="form-control" name="cnpj" value="<?php echo $pessoa['cnpj']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cnpj').mask('00.000.000/0000-00', {reverse: true});
                    });
                </script>
            </div>

            <div class="col-md-3">
                <label for="insc_est">INSCRIÇÃO ESTADUAL</label>
                <input autocomplete="off" id="insc_est" type="text" class="form-control" name="insc_est" value="<?php echo $pessoa['insc_est']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#insc_est').mask('000000000000000', {reverse: true});
                    });
                </script>
            </div>

        </div>
        <div class="row">
            <div class="col-md-5">
                <label for="endereco">ENDEREÇO</label>
                <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" value="<?php echo $pessoa['endereco']; ?>"/>
            </div>
            <div class="col-md-3">
                <label for="numero">NÚMERO</label>
                <input id="numero" type="number" autocomplete="off" class="form-control" name="numero" value="<?php echo $pessoa['numero']; ?>"/>
            </div>
            <div class="col-md-4">
                <label for="bairro">BAIRRO</label>
                <input autocomplete="off" id="bairro" type="text" class="form-control" name="bairro" value="<?php echo $pessoa['bairro']; ?>"/>
            </div>
            <div class="col-md-3">
                <label for="cidade">CIDADE</label>
                <input autocomplete="off" id="cidade" type="text" class="form-control" name="cidade" value="<?php echo $pessoa['cidade']; ?>"/>
            </div>
            <div class="col-md-5">
                <label for="complemento">COMPLEMENTO</label>
                <input autocomplete="off" id="complemento" type="text" class="form-control" name="complemento" value="<?php echo $pessoa['complemento']; ?>"/>
            </div>
            <div class="col-md-4">
                <label for="cep">CEP</label>
                <input autocomplete="off" id="cep" type="text" class="form-control" name="cep" value="<?php echo $pessoa['cep']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cep').mask('00000-000', {reverse: false});
                    });
                </script>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="telefone">TELEFONE</label>
                <input autocomplete="off" id="telefone" type="tel" class="form-control" name="telefone" value="<?php echo $pessoa['telefone']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-6">
                <label for="email">E-MAIL</label>
                <input autocomplete="off" id="email" type="text" class="form-control" name="email" value="<?php echo $pessoa['email']; ?>"/>
            </div>
        </div>

        <style type="text/css">
            input[type=radio]{
                transform:scale(2.1);
            }

            .form-check label:last-child {
                transform:scale(1.5);
            }
        </style>

        <div class="row py-2">
            <div class="col-md-2">
                <label for="p_motorista">MOTORISTA</label>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="radio" name="p_motorista" id="p_motorista" value="1" <?php echo $p_motorista; ?> >
                    <label class="form-check-label ml-3" for="p_motorista">
                        SIM
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="p_motorista" id="p_motorista" value="0" <?php echo $p_motoristan; ?> >
                    <label class="form-check-label ml-3" for="p_motorista">
                        NÃO
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <label for="p_cliente">CLIENTE</label>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="radio" name="p_cliente" id="p_cliente" value="1" <?php echo $p_cliente; ?> >
                    <label class="form-check-label ml-3" for="p_cliente">
                        SIM
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="p_cliente" id="p_cliente" value="0" <?php echo $p_clienten; ?> >
                    <label class="form-check-label ml-3" for="p_cliente">
                        NÃO
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <label for="p_fornecedor">FORNECEDOR</label>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="radio" name="p_fornecedor" id="p_fornecedor" value="1" <?php echo $p_fornecedor; ?>>
                    <label class="form-check-label ml-3" for="p_fornecedor">
                        SIM
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="p_fornecedor" id="p_fornecedor" value="0" <?php echo $p_fornecedorn; ?>>
                    <label class="form-check-label ml-3" for="p_fornecedor">
                        NÃO
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <label for="p_corretor">CORRETOR</label>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="radio" name="p_corretor" id="p_corretor" value="1" <?php echo $p_corretor; ?>>
                    <label class="form-check-label ml-3" for="p_corretor">
                        SIM
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="p_corretor" id="p_corretor" value="0" <?php echo $p_corretorn; ?>>
                    <label class="form-check-label ml-3" for="p_corretor">
                        NÃO
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <label for="p_responsavel">RESPONSÁVEL</label>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="radio" name="p_responsavel" id="p_responsavel" value="1" <?php echo $p_responsavel; ?> >
                    <label class="form-check-label ml-3" for="p_responsavel">
                        SIM
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="p_responsavel" id="p_responsavel" value="0" <?php echo $p_responsaveln; ?> >
                    <label class="form-check-label ml-3" for="p_responsavel">
                        NÃO
                    </label>
                </div>
            </div>
            <div class="col-md-2">
                <label for="p_transportadora">TRANSPORTADORA</label>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="radio" name="p_transportadora" id="p_transportadora" value="1" <?php echo $p_transportadora ?> >
                    <label class="form-check-label ml-3" for="p_responsavel">
                        SIM
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="p_transportadora" id="p_transportadora" value="0" <?php echo $p_transportadoran; ?> >
                    <label class="form-check-label ml-3" for="p_transportadora">
                        NÃO
                    </label>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>