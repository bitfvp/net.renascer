<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
        <div class="col-8">
            <h2>RELATÓRIO DE VENDAS</h2>
            <h4>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h4>
        </div>
    </div>
    <hr class="hrgrosso">

    <?php
    $sql = "SELECT * FROM "
        ."ren_vendas "
        ."WHERE "
        ."ren_vendas.id <> 0 "
        ." AND ((ren_vendas.data_ts)>=:inicial) And ((ren_vendas.data_ts)<=:final) "
        ."order by ren_vendas.data_ts ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
    $consulta->execute();
    $vendas = $consulta->fetchAll();
    $vendas_quant = $consulta->rowCount();
        foreach ($vendas as $dados){
            $ve_id = $dados["id"];
            $prefixo = $dados["prefixo"];
            $contrato = $dados["contrato"];
            $cliente = fncgetpessoa($dados["cliente"])['nome'];
            $transportadora = fncgetpessoa($dados["transportadora"])['nome'];
            $data_pedido = $dados["data_pedido"];
            $descricao = $dados["descricao"];
            $nota_fiscal = $dados["nota_fiscal"];
            $retorno_de_estoque = $dados["retorno_de_estoque"];
            $data_ts=dataRetiraHora($dados["data_ts"]);
            $usuario = $dados["usuario"];
            ?>
            <table class="table table-sm table-stripe table-hover table-condensed">
                <thead class="thead-dark">
                <tr>
                    <th scope="col"><small>CONTRATO</small></th>
                    <th scope="col" colspan="2"><small>CLIENTE</small></th>
                    <th scope="col" colspan="2"><small>TRANSPORTADORA</small></th>
                    <th scope="col"><small>DATA</small></th>
                </tr>
                </thead>
                <tbody>
                <tr id="<?php echo $ve_id;?>" class="">
                    <th scope="row" id="">
                            <?php echo $prefixo."-".$contrato; ?>
                    </th>
                    <td colspan="2"><?php echo strtoupper($cliente); ?></td>
                    <td colspan="2"><?php echo strtoupper($transportadora); ?></td>
                    <td><?php echo $data_ts; ?></td>
                </tr>
                <tr id="<?php echo $ve_id;?>" class="">
                    <th colspan="2">
                        <?php echo $descricao; ?>
                    </th>
                    <td colspan="2"><?php echo $nota_fiscal; ?></td>
                    <td colspan="2"><?php echo $retorno_de_estoque; ?></td>
                </tr>

                <tr>
                    <td colspan="6">
                        <h4>PESAGENS</h4>
                    </td>
                </tr>

                        <?php
                        $sql = "SELECT * FROM "
                            ."ren_vendas_pesagens "
                            ."WHERE venda=:venda "
                            ."order by ren_vendas_pesagens.data_ts DESC ";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->bindValue(":venda", $ve_id);
                        $consulta->execute();
                        $motoristas = $consulta->fetchAll();
                        $motoristas_quant = $consulta->rowCount();
                        ?>
                                <tr>
                                    <th colspan="2">MOTORISTA</th>
                                    <th>PLACA</th>
                                    <th>PESO INICIAL</th>
                                    <th>PESO FINAL</th>
                                    <th>PESO LIQUIDO</th>
                                </tr>

                            <?php
                                $peso_total=0;
                                foreach ($motoristas as $dados2){
                                $motorista = fncgetpessoa($dados2["motorista"])['nome'];
                                $placa = $dados2["placa"];
                                $peso_entrada = mask($dados2["peso_entrada"],'######')." Kg";
                                $peso_saida = mask($dados2["peso_saida"],'######')." Kg";
                                $peso_liquido=$dados2["peso_saida"]-$dados2["peso_entrada"];
                                $peso_total=$peso_total+$peso_liquido;
                                $sacas=$peso_liquido/60;
                                $sacas=number_format($sacas, 2, '.', ',');
                                $peso_liquido.=" Kg";
                                ?>
                                <tr id="" class="">
                                    <td colspan="2"><?php echo strtoupper($motorista); ?></td>
                                    <td><?php echo $placa; ?></td>
                                    <td><?php echo $peso_entrada; ?></td>
                                    <td><?php echo $peso_saida;?></td>
                                    <td style="white-space: nowrap;"><?php echo $peso_liquido. ", ".$sacas."v";?></td>
                                </tr>
                                    <?php
                                }
                                ?>


                <tr>
                    <td colspan="6">
                        <h4>LOTES</h4>
                    </td>
                </tr>

                        <?php
                        try{
                            $sql = "SELECT * FROM "
                                ."ren_entradas_lotes_saidas "
                                ."WHERE id_destino=:id_destino and tipo_saida=1 "
                                ."order by ren_entradas_lotes_saidas.data_ts DESC ";
                            global $pdo;
                            $consulta = $pdo->prepare($sql);
                            $consulta->bindValue(":id_destino", $ve_id);
                            $consulta->execute();
                            global $LQ;
                            $LQ->fnclogquery($sql);
                        }catch ( PDOException $error_msg){
                            echo 'Erro'. $error_msg->getMessage();
                        }
                        $saidas = $consulta->fetchAll();
                        $saidas_quant = $consulta->rowCount();
                        $sql = null;
                        $consulta = null;
                        ?>
                                <tr>
                                    <th>DATA</th>
                                    <th>PRODUTO</th>
                                    <th>LOTE</th>
                                    <th>FORNECEDOR</th>
                                    <th>PESO</th>
                                    <th>ORIGEM</th>
                                </tr>

                            <?php
                                $peso_total=0;
                                foreach ($saidas as $dados3){
                                $peso_entrada = mask($dados3["peso_entrada"],'######')." Kg";
                                $obs_entrada = $dados3["obs_entrada"];
                                $peso_saida = mask($dados3["peso_saida"],'######')." Kg";
                                $obs_saida = $dados3["obs_saida"];

                                $data_ts = datahoraBanco2data($dados3["data_ts"]);
                                $tipo_cafe = fncgetprodutos($dados3["tipo_cafe"])['abrev'];
                                $lote = fncgetlote($dados3["lote"]);
                                $entrada=fncgetentrada($lote["romaneio"]);
                                    $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

                                $letra=fncgetletra($lote["letra"]);
                                $loteinfo=$romaneio_tipo.$entrada['ref'].$entrada['romaneio']." ".$letra;
                                $fornecedor = fncgetpessoa($entrada['fornecedor'])['nome'];

                                $peso_total=$peso_total+$dados3["peso"];
                                $peso = $dados3["peso"];
                                $sacas=$peso/60;
                                $sacas=number_format($sacas, 2, '.', ',');

                                $origem = fncgetlocal($dados3["origem"])['nome'];
                                $p_bo = $dados3["p_bo"];
                                $bo = $dados3["bo"];
                                ?>

                                    <tr id="" class="">
                                        <td><?php echo $data_ts ?></td>
                                        <td><?php echo $tipo_cafe; ?></td>
                                        <td><?php echo $loteinfo; ?></td>
                                        <td><?php echo strtoupper($fornecedor); ?></td>
                                        <td><?php echo $peso. "KG, ".$sacas."v";?></td>
                                        <td><?php echo $origem; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>


                </tbody>
            </table>
            <hr class="hrgrosso">
            <?php
        }
        ?>

    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>



</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print()
</SCRIPT>