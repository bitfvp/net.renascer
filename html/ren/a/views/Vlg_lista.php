<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vct_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT  "
        ."ren_entradas.id, "
        ."ren_entradas.data_ts, "
        ."ren_entradas.usuario, "
        ."ren_entradas.status, "
        ."ren_entradas.possui_lote, "
        ."ren_entradas.romaneio_tipo, "
        ."ren_entradas.romaneio, "
        ."ren_entradas.nota, "
        ."ren_entradas.fornecedor, "
        ."ren_entradas.corretor, "
        ."ren_entradas.motorista, "
        ."ren_entradas.placa, "
        ."ren_entradas.controle_interno, "
        ."ren_entradas.obs "
        ."FROM "
        ."ren_entradas "
        ."INNER JOIN ren_pessoas AS pessoas_fornecedor ON pessoas_fornecedor.id = ren_entradas.fornecedor "
        ." "
        ."WHERE "
        ."ren_entradas.id <> 0 and ren_entradas.romaneio_tipo=5 ";

        if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
            $sql .="AND ren_entradas.romaneio LIKE :romaneio ";
//            echo "true1";
        }
        if (isset($_GET['scb']) and $_GET['scb']!='') {
            $inicial=$_GET['scb'];
            $inicial.=" 00:00:01";
            $final=$_GET['scb'];
            $final.=" 23:59:59";

            $sql .=" AND ((ren_entradas.data_ts)>=:inicial) And ((ren_entradas.data_ts)<=:final) ";
//            echo "true2";
        }

    $sql .="order by ren_entradas.data_ts DESC LIMIT 0,500 ";

    global $pdo;
    $consulta = $pdo->prepare($sql);
        if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
        $consulta->bindValue(":romaneio", "%".$_GET['sca']."%");
        }
        if (isset($_GET['scb']) and $_GET['scb']!='') {
            $consulta->bindValue(":inicial", $inicial);
            $consulta->bindValue(":final", $final);
        }

    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container-fluid"><!--todo conteudo-->
    <h3 class="form-cadastro-heading">ROMANEIO DE LIGA INTERNA</h3>
    <hr>

    <form action="index.php" method="get">
        <div class="input-group mb-3 col-md-8 float-left">
            <div class="input-group-prepend">
                <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
            </div>
            <input name="pg" value="Vlg_lista" hidden/>
            <input type="number" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Romaneio..." aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
            <input type="date" name="scb" id="scb" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
        </div>
    </form>

    <script type="text/javascript">
        function selecionaTexto()
        {
            document.getElementById("sca").select();
        }
        window.onload = selecionaTexto();
    </script>

    <div class="row">
        <div class="col-md-7">
            <a href="index.php?pg=Vlg_editar" class="btn btn-block btn-success mb-2" >Novo romaneio</a>
        </div>
    </div>

    <table class="table table-stripe table-sm table-hover table-condensed">
        <thead class="thead-dark">
        <tr>
            <th scope="col" class="text-center">#</th>
            <th scope="col"><small>ROMANEIO</small></th>
            <th scope="col"><small>DATA</small></th>
            <th scope="col"><small>FORNECEDOR</small></th>
            <th scope="col" class="text-center"><small>INFO</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="2" >
            </th>
            <td colspan="4"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        foreach ($entradas as $dados){
            $en_id = $dados["id"];
            $romaneio = $dados["romaneio"];
            $romaneio_tipo=fncgetromaneiotipo($dados["romaneio_tipo"]);

            $fornecedor = fncgetpessoa($dados["fornecedor"])['nome'];
            $data_ts = $dados["data_ts"];
            $usuario = $dados["usuario"];

            if ($dados["possui_lote"]!=0){
                $possui_lote= "<i class='fas fa-boxes fa-2x' title='possui lotes'></i>";
            }else{
                $possui_lote= "";
            }
            if ($dados["status"]==0 or $dados["status"]==""){
                $status= "<i class='fas fa-lock fa-2x ml-1' title='lg finalizada'></i>";
            }else{
                $status= "";
            }
            ?>

            <tr id="<?php echo $en_id;?>" class="bg-success">
                <th scope="row" id="">
                    <small><?php echo $en_id; ?></small>
                </th>
                <th scope="row" id="">
                    <a href="index.php?pg=Vlg&id_e=<?php echo $en_id; ?>" class="badge badge-info text-dark">
                        <h6>
                        <?php echo $romaneio_tipo.$romaneio; ?>
                        </h6>
                    </a>
                </th>
                <td><?php echo dataRetiraHora($data_ts); ?></td>
                <td><?php echo strtoupper($fornecedor); ?></td>
                <td class="text-center"><?php echo $possui_lote.$status; ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vlg&id_e=<?php echo $en_id; ?>" title="Entrada" class="btn btn-sm btn- btn-outline-warning fab fa-searchengin fa-2x text-dark"></a>
                        <a href="index.php?pg=Vlg_editar&id=<?php echo $en_id; ?>" title="Editar entrada" class="btn btn-sm btn- btn-outline-primary fas fa-pen fa-2x text-dark"></a>
                    </div>

                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>