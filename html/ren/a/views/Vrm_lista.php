<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT  "
        ."ren_entradas.id, "
        ."ren_entradas.data_ts, "
        ."ren_entradas.usuario, "
        ."ren_entradas.possui_lote, "
        ."ren_entradas.romaneio_tipo, "
        ."ren_entradas.ref, "
        ."ren_entradas.romaneio, "
        ."ren_entradas.nota, "
        ."ren_entradas.fornecedor, "
        ."ren_entradas.corretor, "
        ."ren_entradas.motorista, "
        ."ren_entradas.placa, "
        ."ren_entradas.controle_interno, "
        ."ren_entradas.obs "
        ."FROM "
        ."ren_entradas "
        ."INNER JOIN ren_pessoas AS pessoas_fornecedor ON pessoas_fornecedor.id = ren_entradas.fornecedor "
        ."INNER JOIN ren_pessoas AS pessoas_motorista ON pessoas_motorista.id = ren_entradas.motorista "
        ."WHERE "
        ."ren_entradas.id <> 0 and ren_entradas.romaneio_tipo=1 ";

        if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
            $sql .="AND (ren_entradas.romaneio LIKE :romaneio or ren_entradas.nota LIKE :nota) ";
//            echo "true1";
        }
        if (isset($_GET['scb']) and $_GET['scb']!='') {
            $inicial=$_GET['scb'];
            $inicial.=" 00:00:01";
            $final=$_GET['scb'];
            $final.=" 23:59:59";

            $sql .=" AND ((ren_entradas.data_ts)>=:inicial) And ((ren_entradas.data_ts)<=:final) ";
//            echo "true2";
        }
    if (isset($_GET['scc']) and $_GET['scc']!='') {
        $scc=$_GET['scc'];
        $sql .=" AND pessoas_fornecedor.nome LIKE '%$scc%' ";
//        echo "true3";
    }
    if (isset($_GET['scd']) and $_GET['scd']!='') {
        $scd=$_GET['scd'];
        $sql .=" AND pessoas_motorista.nome LIKE '%$scd%' ";
//        echo "true4";
    }

    $sql .="order by ren_entradas.data_ts DESC LIMIT 0,500 ";

    global $pdo;
    $consulta = $pdo->prepare($sql);
        if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
        $consulta->bindValue(":romaneio", "%".$_GET['sca']."%");
        $consulta->bindValue(":nota", "%".$_GET['sca']."%");
        }
        if (isset($_GET['scb']) and $_GET['scb']!='') {
            $consulta->bindValue(":inicial", $inicial);
            $consulta->bindValue(":final", $final);
        }

    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container-fluid">
    <h3 class="form-cadastro-heading"><a href="index.php?pg=Vrm_lista" class="text-decoration-none ">ROMANEIO DE ENTRADA</a></h3>
    <hr>



    <div class="row">
        <div class="col-md-9">
            <form action="index.php" method="get">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
                    </div>
                    <input name="pg" value="Vrm_lista" hidden/>
                    <input type="number" name="sca" id="sca" autofocus="true" autocomplete="off" class="form-control" placeholder="Romaneio ou nota" aria-label="" aria-describedby="basic-addon1" value="<?php if (isset($_GET['sca'])) {echo $_GET['sca'];} ?>" />
                    <input type="date" name="scb" id="scb" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
                    <input type="text" name="scc" id="scc" autocomplete="off" class="form-control" placeholder="Fornecedor..." value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
                    <input type="text" name="scd" id="scd" autocomplete="off" class="form-control" placeholder="Motorista..." value="<?php if (isset($_GET['scd'])) {echo $_GET['scd'];} ?>" />
                </div>
            </form>

            <script type="text/javascript">
                function selecionaTexto()
                {
                    document.getElementById("sca").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>
        <div class="col-md-3">
            <a href="index.php?pg=Vrm_editar" class="btn btn-block btn-success mb-2" >Novo romaneio</a>
        </div>
    </div>

    <table class="table table-stripe table-sm table-hover table-condensed">
        <thead class="thead-dark">
            <tr>
                <th scope="col" class="text-left">#</th>
                <th scope="col"><small>ROM./NOTA</small></th>
                <th scope="col"><small>DATA</small></th>
                <th scope="col"><small>FORNECEDOR</small></th>
                <th scope="col"><small>MOTORISTA</small></th>
                <th scope="col"><small>PLACAS</small></th>
                <th scope="col"><small>CORRETOR</small></th>
                <th scope="col" class="text-center"><small>INFO</small></th>
                <th scope="col" class="text-center"><small>AÇÕES</small></th>
            </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="row" colspan="5" >
            </th>
            <td colspan="4"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</td>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        if($_GET['sca']!="" and isset($_GET['sca'])) {
            $sta = strtoupper($_GET['sca']);
            define('CSA', $sta);//TESTE
        }
        foreach ($entradas as $dados){
            $en_id = $dados["id"];
            $romaneio = $dados["romaneio"];
            $romaneio_tipo = $dados["romaneio_tipo"];
            $romaneio_tipo=fncgetromaneiotipo($dados["romaneio_tipo"]);
            $ref = $dados["ref"];
            $nota = $dados["nota"];

            $temp_romaneio=$romaneio_tipo.$ref.$romaneio;

            $motorista = fncgetpessoa($dados["motorista"])['nome'];
            $placa = $dados["placa"];
            $fornecedor = fncgetpessoa($dados["fornecedor"])['nome'];
            $corretor = fncgetpessoa($dados["corretor"])['nome'];
            $data_ts = $dados["data_ts"];
            $usuario = $dados["usuario"];

            if ($dados["possui_lote"]!=0){
                $possui_lote= "<i class='fas fa-boxes fa-2x' title='possui lotes'></i>";
            }else{
                $possui_lote= "";
            }

            ?>

            <tr id="<?php echo $en_id;?>" class="bg-success">
                <th scope="row" id="">
                    <small><?php echo $en_id; ?></small>
                </th>
                <th scope="row" style="white-space: nowrap;">
                    <a href="index.php?pg=Vrm&id_e=<?php echo $en_id; ?>" class="badge badge-info text-dark">
                        <h6>

                        <?php
                        if($_GET['sca']!="" and isset($_GET['sca'])) {
                            $sta = CSA;
                            $ccc = $temp_romaneio;
                            $cc = explode(CSA, $ccc);
                            $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                            echo $c;
                        }else{
                            echo $temp_romaneio;
                        }

                        if ($nota!=0){
                            echo "<br>";
                            if($_GET['sca']!="" and isset($_GET['sca'])) {
                                $sta = CSA;
                                $ccc = $nota;
                                $cc = explode(CSA, $ccc);
                                $c = implode("<span class='text-danger'>{$sta}</span>", $cc);
                                echo $c;
                            }else{
                                echo $nota;
                            }
                        }
                         ?>
                        </h6>
                    </a>
                </th>
                <td><small><?php echo dataRetiraHora($data_ts); ?></small></td>
                <td><?php echo strtoupper($fornecedor); ?></td>
                <td><?php echo strtoupper($motorista); ?></td>
                <td><?php echo $placa; ?></td>
                <td><?php echo strtoupper($corretor); ?></td>
                <td><?php echo $possui_lote; ?></td>
                <td class="text-center">
                    <div class="btn-group" role="group" aria-label="">
                        <a href="index.php?pg=Vrm&id_e=<?php echo $en_id; ?>" title="Entrada" class="btn btn-sm btn- btn-outline-warning fab fa-searchengin fa-2x text-dark"></a>
                        <a href="index.php?pg=Vrm_editar&id=<?php echo $en_id; ?>" title="Editar entrada" class="btn btn-sm btn- btn-outline-primary fas fa-pen fa-2x text-dark"></a>
                    </div>

                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>