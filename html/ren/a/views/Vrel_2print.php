<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Relatório de estoque-".$env->env_titulo;
$css="print";

include_once("{$env->env_root}includes/head.php");

// Recebe

    $sql = "SELECT * FROM ren_entradas_lotes WHERE ren_entradas_lotes.peso_atual > 0 ORDER BY ";
    switch ($_GET['order']){
        case "local":
            $sql .= "ren_entradas_lotes.localizacao ASC, ren_entradas_lotes.data_ts ASC";
            break;
        case "tipo":
            $sql .= "ren_entradas_lotes.tipo_cafe ASC, ren_entradas_lotes.data_ts ASC";
            break;
        case "data":
            $sql .= "ren_entradas_lotes.data_ts ASC";
            break;
        default:
            $sql .= "ren_entradas_lotes.localizacao ASC, ren_entradas_lotes.data_ts ASC ";
            break;
    }
    global $pdo;
    $consulta = $pdo->prepare($sql);

    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $lotes = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
?>
<div class="container-fluid">
    <?php
    switch ($_GET['order']){
        case "local":
            echo "<h3>ESTOQUE ATUAL ORDENADO POR LOCALIZAÇÃO, DATA DE ENTRADA</h3>";
            break;
        case "tipo":
            echo "<h3>ESTOQUE ATUAL ORDENADO POR TIPO DE PRODUTO, DATA DE ENTRADA</h3>";
            break;
        case "data":
            echo "<h3>ESTOQUE ATUAL ORDENADO POR DATA DE ENTRADA</h3>";
            break;
        default:
            echo "<h3>ESTOQUE ATUAL ORDENADO POR LOCALIZAÇÃO, DATA DE ENTRADA </h3>";
            break;
    }
    ?>

<!--    <h5>--><?php //echo fncgetsetor($setor)['setor']?><!--</h5>-->
    <h5><?php echo datahoraBanco2data(dataNow());?></h5>

    <table class="table table-bordered table-hover table-sm">
        <thead>
        <th>LOTE</th>
        <th>FORNECEDOR</th>
        <th>VOLUME ENTRADA</th>
        <th>VOLUME ATUAL</th>
        <th>PRODUTO</th>
        <th>LOCAL</th>
        </thead>
        <tbody>
        <?php
        foreach ($lotes as $lote){
            $entrada=fncgetentrada($lote['romaneio']);
            $cordalinha = "  ";
                if ($lote['p_bo']==1){
                    $cordalinha = " text-dark bg-warning ";
                }
            $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

            $letra=fncgetletra($lote["letra"]);

            $peso_entrada = $lote["peso_entrada"];
            $sacas_entrada=$peso_entrada/60;
            $sacas_entrada=number_format($sacas_entrada, 2, '.', ',');
            $peso_entrada.=" Kg";
            $bags_entrada = $lote["bags_entrada"];

            $peso_atual = $lote["peso_atual"];
            $sacas_atual=$peso_atual/60;
            $sacas_atual=number_format($sacas_atual, 2, '.', ',');
            $peso_atual.=" Kg";
            $bags_atual = $lote["bags_atual"];

            echo "<tr class='{$cordalinha}'>";

            echo "<td style='white-space: nowrap;'><small>".$romaneio_tipo.$entrada['ref'].$entrada['romaneio']."-".$letra." ".dataRetiraHora($lote['data_ts'])."</small></td>";
            echo "<td><small>".strtoupper(fncgetpessoa($entrada['fornecedor'])['nome'])."</small></td>";

//            echo "<td>".$peso_entrada. ", ".$sacas_entrada."s<br>".$bags_entrada." bag(s)</td>";
            echo "<td><small>".$sacas_entrada."v ".$bags_entrada." bag(s)</small></td>";
//            echo "<td>".$peso_atual. ", ".$sacas_atual."s<br>".$bags_atual." bags</td>";
            echo "<td><small>".$sacas_atual."v ".$bags_atual." bags</small></td>";
            echo "<td><small>".fncgetprodutos($lote['tipo_cafe'])['abrev']."</small></td>";
            echo "<td style='white-space: nowrap;'><small>".fncgetlocal($lote["localizacao"])['nome']." ".$lote["localizacao_obs"]."</small></td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table><!-- style='white-space: nowrap;'-->

</div>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print()
</SCRIPT>