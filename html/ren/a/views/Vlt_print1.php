<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vct_lista");
    exit();
}
$romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);
?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
        <div class="col-8">
            <h2>CONTROLE DE LOTES GERADOS</h2>
            <h4 class="">RESPECTIVO AO ROMANEIO: <strong><?php echo $romaneio_tipo.$entrada['ref'].$entrada['romaneio']; ?></strong></h4>
            <h5>FORNECEDOR: <strong><?php echo strtoupper(fncgetpessoa($entrada['fornecedor'])['nome']); ?></strong></h5>
            <?php
            if ($entrada['motorista']!=0){
                echo "<h5>MOTORISTA: <strong>";
                echo strtoupper(fncgetpessoa($entrada['motorista'])['nome']);;
                echo "</strong></h5>";
            }
            ?>
            <h4><strong><?php echo datahoraBanco2data($entrada['data_ts']); ?></strong></h4>

        </div>
    </div>


    <hr class="hrgrosso">
    <h3>Lotes gerados apartir de processo</h3>
    <?php
    //1 venda
    //2ct
    //3 devolucao
    try{
        $sql = "SELECT * \n"
            . "FROM ren_entradas_lotes \n"
            . "WHERE (((ren_entradas_lotes.romaneio)=:id_destino) and status=1)\n"
            . "ORDER BY ren_entradas_lotes.letra ASC";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":id_destino", $_GET['id_e']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $lotes = $consulta->fetchAll();
    $lotes_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-hover table-striped">
                <thead class="thead-inverse thead-dark">
                <tr>
                    <th scope="col">LOTE</th>
                    <th scope="col">FORNECEDOR</th>
                    <th scope="col">PRODUTO</th>
                    <th scope="col">PESO INICIAL</th>
                    <th scope="col">LOCALIZAÇÃO</th>
                    <th scope="col">B.O.</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $peso_total=0;
                foreach ($lotes as $dados){
                    $entrada=fncgetentrada($dados['romaneio']);
                    $fornecedor = fncgetpessoa($entrada["fornecedor"])['nome'];
                    $romaneio = $entrada["romaneio"];
                    $ref = $entrada["ref"];
                    $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

                    $letra=fncgetletra($dados["letra"]);

                    $tipo_cafe = $dados["tipo_cafe"];
                    $id_e = $dados["id"];
                    $peso_entrada = $dados["peso_entrada"];
                    $sacas_entrada=$peso_entrada/60;
                    $peso_total=$peso_total+$dados["peso_entrada"];
                    $bags_entrada = $dados["bags_entrada"];
                    $localizacao = $dados["localizacao"];
                    $localizacao_obs = $dados["localizacao_obs"];
                    $p_bo = $dados["p_bo"];
                    $bo = $dados["bo"];
                    $obs = $dados["obs"];
                    $responsavel = fncgetpessoa($dados["responsavel"])['nome'];
                    ?>

                    <tr class="">
                        <th scope="row" id="<?php echo $id_l;  ?>">
                                <?php
                                    echo $romaneio_tipo.$ref.$romaneio." ".$letra;
                                ?>
                        </th>
                        <td>
                            <?php
                                echo strtoupper($fornecedor);
                            ?>
                        </td>
                        <td class="small">
                            <?php echo fncgetprodutos($tipo_cafe)['nome'];?>
                        </td>
                        <td>
                            <?php echo number_format($peso_entrada, 3, ',', ' '). "Kg ou ".number_format($sacas_entrada, 2, ',', ' ')."V ";?>
                        </td>


                        <td>
                            <?php echo fncgetlocal($localizacao)['nome']. " ". $localizacao_obs;?>
                        </td>

                        <td>
                            <?php echo $bo;?>
                        </td>
                    </tr>
                    <?php
                }
                $sacastotais=$peso_total/60;
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-6 border">
            PESAGEM LÍQUIDO TOTAL DOS LOTES: <strong><?php echo number_format($peso_total,3,',',' ')." Kg" ?></strong>
        </div>
        <div class="col-6 border">
            QUANTIDADE TOTAL EM VOLUMES: <strong><?php echo number_format($sacastotais, 2, ',', ' '); ?></strong>
        </div>
    </div>

    <br>




    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php
            if ($entrada['ass_lotes']!=0){
                $sql = "SELECT * FROM tbl_users_assinatura WHERE user_id=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$entrada['ass_lotes']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $assinatura = $consulta->fetch();
                $assinaturacount = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                if ($assinaturacount>0){
                    echo "<img class='mt-0' height='50' id='' src='{$assinatura['assinatura']}' />";
                }else{
                    echo "<h4>_______________________</h4>";
                }
            }else{
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Assinatura do responsável por cadastrar lotes
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Operador: <strong><?php echo fncgetusuario($_SESSION['id'])['nome']." ".date('d/m/Y H:i:s'); ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>