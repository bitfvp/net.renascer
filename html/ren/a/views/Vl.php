<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_l']) and is_numeric($_GET['id_l'])){
    $a="lotesave";
    $lote=fncgetlote($_GET['id_l']);

}else{
    $a="lotenew";

}
$cordalinha = "info ";

?>
<!--/////////////////////////////////////////////////////-->

<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">

        <?php
        include_once("includes/vl_cab.php");
        ?>

        <!-- esquerdo -->
        <div class="col-md-7">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Opções de lotes
                </div>
                <div class="card-body">

                    <a href="index.php?pg=Vl_ml&id_l=<?php echo $_GET['id_l']; ?>" class="btn btn-block btn-warning">MOVER LOTE</a>
                    <a href="index.php?pg=Vl_editarprova&id_l=<?php echo $_GET['id_l']; ?>" class="btn btn-block btn-success">VERIFICAR PROVA</a>
                    <a href="index.php?pg=Vl_sv&id_l=<?php echo $_GET['id_l']; ?>" class="btn btn-block btn-primary">SAÍDA PARA VENDA</a>
                    <a href="index.php?pg=Vl_ct&id_l=<?php echo $_GET['id_l']; ?>" class="btn btn-block btn-info">SAÍDA PARA MAQUINAÇÃO</a>
                    <a href="index.php?pg=Vl_de&id_l=<?php echo $_GET['id_l']; ?>" class="btn btn-block btn-info">SAÍDA PARA DEVOLUÇÃO AO CLIENTE</a>
                    <a href="index.php?pg=Vl_lg&id_l=<?php echo $_GET['id_l']; ?>" class="btn btn-block btn-info">SAÍDA PARA LIGA INTERNA</a>
                </div>
            </div>
        </div>

        <?php
        include_once("includes/vl_saidas.php");
        ?>



    </div>
</div>
<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>