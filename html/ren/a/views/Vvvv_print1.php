<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_GET['id_v']) and is_numeric($_GET['id_v'])){
    $venda=fncgetvenda($_GET['id_v']);
    $cliente=fncgetpessoa($venda['cliente']);
    $transportadora=fncgetpessoa($venda['transportadora']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vv_lista");
    exit();
}
?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
        <div class="col-8">
            <h2>VENDA</h2>
            <h4>CLIENTE: <strong><?php echo strtoupper($cliente['nome']); ?></strong></h4>
            <h6>TELEFONE: <strong><?php echo $cliente['telefone']; ?></strong> E-MAIL: <strong><?php echo strtoupper($cliente['email']); ?></strong></h6>
            <h6>CNPJ: <strong><?php echo mask($cliente['cnpj'],'##.###.###/####-##'); ?></strong></h6>
            <h6>ENDEREÇO: <strong><?php echo strtoupper($cliente['endereco'])."  ".$cliente['numero']; ?></strong> BAIRRO: <strong><?php echo strtoupper($cliente['bairro']); ?></strong> CIDADE: <strong><?php echo strtoupper($cliente['cidade']); ?></strong></h6>
            <h6>COMPLEMENTO: <strong><?php echo $cliente['complemento']; ?></strong> CEP: <strong><?php echo $cliente['cep']; ?></strong></h6>
            <h4>TRANSPORTADORA: <strong><?php echo strtoupper($transportadora['nome'])." ".mask($transportadora['cnpj'],'##.###.###/####-##'); ?></strong></h4>
            <h4>NOTAS: <strong><?php echo $venda['nota_fiscal']; ?></strong></h4>
            <h4>RETORNO DE ESTOQUE: <strong><?php echo $venda['retorno_de_estoque']; ?></strong></h4>
        </div>
        <div class="col-4 text-right">
            <h4 class="">CONTRATO: <strong><?php echo $venda['prefixo']."-".$venda['contrato']." ".dataRetiraHora($venda['data_ts']); ?></strong></h4>
            <h4>DESCRIÇÃO: <strong><?php echo $venda['descricao']; ?></strong></h4>
        </div>
    </div>
    <hr class="hrgrosso">
    <h3>PESAGENS</h3>
    <?php
    $sql = "SELECT * FROM "
        ."ren_vendas_pesagens "
        ."WHERE venda=:venda "
        ."order by ren_vendas_pesagens.data_ts DESC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":venda", $_GET['id_v']);
    $consulta->execute();
    $motoristas = $consulta->fetchAll();
    $motoristas_quant = $consulta->rowCount();

    ?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-hover table-striped">
                <thead class="thead-inverse thead-dark">
                <tr>
                    <th>MOTORISTA</th>
                    <th>PLACA</th>
                    <th>PESO INICIAL</th>
                    <th>PESO FINAL</th>
                    <th>PESO LIQUIDO</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $peso_total=0;
                foreach ($motoristas as $dados){
                    $motorista = fncgetpessoa($dados["motorista"])['nome'];
                    $placa = $dados["placa"];
                    $peso_entrada = mask($dados["peso_entrada"],'######')." Kg";
                    $peso_saida = mask($dados["peso_saida"],'######')." Kg";
                    $peso_liquido=$dados["peso_saida"]-$dados["peso_entrada"];
                    $peso_total=$peso_total+$peso_liquido;
                    $sacas=$peso_liquido/60;
                    $sacas=number_format($sacas, 2, ',', ' ');
                    ?>

                    <tr id="" class="">
                        <td><?php echo strtoupper($motorista); ?></td>
                        <td><?php echo $placa; ?></td>
                        <td><?php echo $peso_entrada; ?></td>
                        <td><?php echo $peso_saida;?></td>
                        <td><?php echo number_format($peso_liquido,3,',',' '). "Kg<br>".number_format($sacas,2,',',' ')."v";?></td>
                    </tr>
                    <?php
                }
                $sacastotais=$peso_total/60;
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-6 border">
            PESAGEM LÍQUIDO TOTAL: <strong><?php echo number_format($peso_total,3,',',' ')." Kg" ?></strong>
        </div>
        <div class="col-6 border">
            QUANTIDADE TOTAL EM VOLUMES: <strong><?php echo number_format($sacastotais,2,',',' '); ?></strong>
        </div>
    </div>




    <hr class="hrgrosso">
    <h3>Lotes</h3>
    <?php
    //1 venda
    //2ct
    //3 devolucao
    try{
        $sql = "SELECT * FROM "
            ."ren_entradas_lotes_saidas "
            ."WHERE id_destino=:id_destino and tipo_saida=1 "
            ."order by ren_entradas_lotes_saidas.data_ts DESC ";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindValue(":id_destino", $_GET['id_v']);
        $consulta->execute();
        global $LQ;
        $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erro'. $error_msg->getMessage();
    }
    $pesagens = $consulta->fetchAll();
    $pesagens_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>


    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm ">
                <thead class="thead-inverse thead-dark">
                <tr>
                    <th>DATA</th>
                    <th>PRODUTO</th>
                    <th>LOTE</th>
                    <th>FORNECEDOR</th>
                    <th>PESO</th>
                    <th>ORIGEM</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $peso_total=0;
                foreach ($pesagens as $dados){
                    $peso_entrada = mask($dados["peso_entrada"],'######')." Kg";
                    $obs_entrada = $dados["obs_entrada"];
                    $peso_saida = mask($dados["peso_saida"],'######')." Kg";
                    $obs_saida = $dados["obs_saida"];

                    $data_ts = datahoraBanco2data($dados["data_ts"]);
                    $tipo_cafe = fncgetprodutos($dados["tipo_cafe"])['abrev'];
                    $lote = fncgetlote($dados["lote"]);
                    $entrada=fncgetentrada($lote["romaneio"]);
                    $romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);
                    $letra=fncgetletra($lote["letra"]);

                    $loteinfo=$romaneio_tipo.$entrada['ref'].$entrada['romaneio']." ".$letra;
                    $fornecedor = fncgetpessoa($entrada['fornecedor'])['nome'];

                    $peso_total=$peso_total+$dados["peso"];
                    $peso = $dados["peso"];
                    $sacas=$peso/60;

                    $origem = fncgetlocal($dados["origem"])['nome'];
                    $p_bo = $dados["p_bo"];
                    $bo = $dados["bo"];
                    ?>

                    <tr id="" class="" style="border-bottom: 2pt solid black;">
                        <td><?php echo $data_ts ?></td>
                        <td><?php echo $tipo_cafe; ?></td>
                        <td style='white-space: nowrap;'><?php echo $loteinfo; ?></td>
                        <td><?php echo strtoupper($fornecedor); ?></td>
                        <td><?php echo number_format($peso,3,',',' '). "Kg <br>".number_format($sacas,2,',',' ')."v";?></td>
                        <td><?php echo $origem; ?></td>
                    </tr>
                    <?php
                }
                $sacastotais=$peso_total/60;
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-6 border">
            PESAGEM LÍQUIDO TOTAL DOS LOTES: <strong><?php echo number_format($peso_total,3,',',' ')." Kg" ?></strong>
        </div>
        <div class="col-6 border">
            QUANTIDADE TOTAL EM VOLUMES: <strong><?php echo number_format($sacastotais,2,',',' '); ?></strong>
        </div>
    </div>

    <br>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php
            if ($venda['usuario']!=0){
                $sql = "SELECT * FROM tbl_users_assinatura WHERE user_id=?";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1,$venda['usuario']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $assinatura = $consulta->fetch();
                $assinaturacount = $consulta->rowCount();
                $sql=null;
                $consulta=null;
                if ($assinaturacount>0){
                    echo "<img class='mt-0' height='50' id='' src='{$assinatura['assinatura']}' />";
                }else{
                    echo "<h4>_______________________</h4>";
                }
            }else{
                echo "<h4>_______________________</h4>";
            }
            ?>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Operador: <strong><?php echo fncgetusuario($venda['usuario'])['nome']; ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print();
</SCRIPT>