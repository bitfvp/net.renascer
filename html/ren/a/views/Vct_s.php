<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Ve_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id_e']) and is_numeric($_GET['id_e'])){
//    $a="entradasave";
    $entrada=fncgetentrada($_GET['id_e']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Ve_lista");
    exit();
}
if (isset($_GET['id_p']) and is_numeric($_GET['id_p'])){
    $a="pesagemsave";
    $pesagem=fncgetpesagem($_GET['id_p']);
}else{
    $a="pesagemnew";
}
$romaneio_tipo=fncgetromaneiotipo($entrada["romaneio_tipo"]);

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container-fluid"><!--todo conteudo-->
    <div class="row">
        <?php
        include_once("includes/ct_cab.php");
        ?>

        <div class="col-md-6">

                    <?php
                    //1 venda
                    //2ct
                    //3 devolucao
                    try{
                        $sql = "SELECT * FROM "
                            ."ren_entradas_lotes_saidas "
                            ."WHERE id_destino=:id_destino and tipo_saida=2 "
                            ."order by ren_entradas_lotes_saidas.data_ts DESC ";
                        global $pdo;
                        $consulta = $pdo->prepare($sql);
                        $consulta->bindValue(":id_destino", $_GET['id_e']);
                        $consulta->execute();
                        global $LQ;
                        $LQ->fnclogquery($sql);
                    }catch ( PDOException $error_msg){
                        echo 'Erro'. $error_msg->getMessage();
                    }
                    $pesagens = $consulta->fetchAll();
                    $pesagens_quant = $consulta->rowCount();
                    $sql = null;
                    $consulta = null;
                    ?>

                            <h3>Lotes selecionados para maquinação</h3>
                            <table class="table table-sm table-hover table-striped">
                                <thead class="thead-inverse thead-dark">
                                <tr>
                                    <th>DATA</th>
                                    <th>PRODUTO</th>
                                    <th>LOTE</th>
                                    <th>FORNECEDOR</th>
                                    <th>PESO</th>
                                    <th>ORIGEM</th>
                                    <th>BO</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th scope="row" colspan="4" >
                                    </th>
                                    <td colspan="4"><?php echo $pesagens_quant;?> Saída(s) encontrada(s)</td>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php
                                foreach ($pesagens as $dados){
                                    $peso_entrada = mask($dados["peso_entrada"],'######')." Kg";
                                    $obs_entrada = $dados["obs_entrada"];
                                    $peso_saida = mask($dados["peso_saida"],'######')." Kg";
                                    $obs_saida = $dados["obs_saida"];

                                    $data_ts = datahoraBanco2data($dados["data_ts"]);
                                    $tipo_cafe = fncgetprodutos($dados["tipo_cafe"])['abrev'];
                                    $lote = fncgetlote($dados["lote"]);
                                    $entradax=fncgetentrada($lote["romaneio"]);
                                    $romaneio_tipo=fncgetromaneiotipo($entradax["romaneio_tipo"]);
                                    $ref = $entradax["ref"];

                                    $letra=fncgetletra($lote["letra"]);

                                    $loteinfo=$romaneio_tipo.$ref.$entradax['romaneio']." ".$letra;
                                    $fornecedor = fncgetpessoa($entradax['fornecedor'])['nome'];

                                    $peso = $dados["peso"];
                                    $sacas=$peso/60;
                                    $sacas=number_format($sacas, 1, '.', ',');

                                    $origem = fncgetlocal($dados["origem"])['nome'];
                                    $p_bo = $dados["p_bo"];
                                    $bo = $dados["bo"];

                                    $usuario = fncgetusuario($dados["usuario"])['nome'];
                                    ?>

                                    <tr id="" class="<?php echo $cordalinha; ?>">
                                        <td><?php echo $data_ts ?></td>
                                        <td><?php echo $tipo_cafe; ?></td>
                                        <td style='white-space: nowrap;'><?php echo $loteinfo; ?></td>
                                        <td><?php echo $fornecedor; ?></td>
                                        <td><?php echo $peso. "KG ou ".$sacas." volumes";?></td>
                                        <td><?php echo $origem; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>

        </div>

        <div class="col-md-3">
            <section class="sidebar-offcanvas" id="sidebar">
                <div class="list-group">
                    <?php
                    switch ($entrada["romaneio_tipo"]){
                        case 1:
//                            $romaneio_tipo="RM-";
                            echo "<a href='?pg=Vrm&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            echo "<a href='?pg=Vrm_p&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-balance-scale'></i>    PESSAGENS</a>";
                            break;
                        case 2:
//                            $romaneio_tipo="RE-";
                            echo "<a href='?pg=Vre&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            break;
                        case 3:
//                            $romaneio_tipo="CT-";
                            echo "<a href='?pg=Vct&id_e={$_GET['id_e']}' class='list-group-item'><i class='fa fa-folder'></i>    LOTES</a>";
                            echo "<a href='?pg=Vct_s&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-list'></i>    SELEÇÃO DE LOTES</a>";
                            break;
                        case 4:
//                            $romaneio_tipo="DE-";
                            echo "<a href='?pg=Vde&id_e={$_GET['id_e']}' class='list-group-item'><i class='fas fa-undo-alt'></i>    PARA DEVOLUÇÃO</a>";
                            break;
                    }
                    ?>
                </div>
            </section>
            <script type="application/javascript">
                var offset = $('#sidebar').offset().top;
                var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance
                $(document).on('scroll', function () {
                    if (offset <= $(window).scrollTop()) {
                        $meuMenu.addClass('fixarmenu');
                    } else {
                        $meuMenu.removeClass('fixarmenu');
                    }
                });
            </script>

        </div>


    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>