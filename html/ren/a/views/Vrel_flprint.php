<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
if (isset($_POST['fornecedor']) and is_numeric($_POST['fornecedor'])){
    $pessoa=fncgetpessoa($_POST['fornecedor']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}

$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";
?>

<div class="container">
    <?php
    include_once("includes/renascer_cab.php");
    ?>

    <div class="row">
        <div class="col-8">
            <h2>RELATÓRIO DE LOTES</h2>
            <h4>FORNECEDOR: <strong><?php echo strtoupper($pessoa['nome']); ?></strong></h4>
            <h5>
            </h5>
            <h5>
                CPF: <strong>
                    <?php
                    if($pessoa['cpf']!="" and $pessoa['cpf']!=0) {
                        echo "<span class='text-info'>";
                        echo mask($pessoa['cpf'],'###.###.###-##');
                        echo "</span>";
                    }else{
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?>
                </strong>
                CNPJ: <strong>
                    <?php
                    if($pessoa['cnpj']!="" and $pessoa['cnpj']!=0) {
                        echo "<span class='text-info'>";
                        echo mask($pessoa['cnpj'],'##.###.###/####-##');
                        echo "</span>";
                    }else{
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?>
                </strong>
                TELEFONE: <strong>
                    <?php
                    if($pessoa['telefone']!="") {
                        echo "<span class='text-info'>";
                        echo $pessoa['telefone'];
                        echo "</span>";
                    }else{
                        echo "<span class='text-muted'>";
                        echo "[---]";
                        echo "</span>";
                    }
                    ?>
                </strong>
            </h5>

        </div>
        <div class="col-4 text-right">
            <h5 class="">PERÍODO: <strong><?php echo dataBanco2data($_POST['data_inicial'])." a ".dataBanco2data($_POST['data_final']); ?></strong></h5>
        </div>
    </div>


    <hr class="hrgrosso">
    <?php
    try{
        $sql = "SELECT "
            . "ren_entradas_lotes.id, "
            . "ren_entradas.id as id_e, "
            . "ren_entradas.romaneio, "
            . "ren_entradas.ref, "
            . "ren_entradas_lotes.data_ts, "
            . "ren_entradas_lotes.letra, "
            . "ren_entradas_lotes.tipo_cafe, "
            . "ren_entradas_lotes.verificado, "
            . "ren_entradas_lotes.peso_entrada, "
            . "ren_entradas_lotes.bags_entrada, "
            . "ren_entradas_lotes.localizacao, "
            . "ren_entradas_lotes.localizacao_obs, "
            . "ren_entradas_lotes.p_bo, "
            . "ren_entradas_lotes.bo, "
            . "ren_entradas_lotes.peso_atual, "
            . "ren_entradas_lotes.bags_atual, "
            . "ren_entradas_lotes.obs, "
            . "ren_entradas.romaneio_tipo, "
            . "ren_pessoas.nome AS fornecedor "
            . "FROM "
            . "ren_entradas_lotes "
            . "INNER JOIN ren_entradas ON ren_entradas_lotes.romaneio = ren_entradas.id "
            . "INNER JOIN ren_pessoas ON ren_entradas.fornecedor = ren_pessoas.id ";
        $sql_where = "WHERE ren_entradas_lotes.id>0 and ren_entradas.fornecedor=:fornecedor "
            ."AND ((ren_entradas_lotes.data_ts)>=:inicial) And ((ren_entradas_lotes.data_ts)<=:final) ";
        $sql_orderby= "ORDER BY ren_entradas_lotes.data_ts DESC LIMIT 0,1000";
        global $pdo;
        $consulta=$pdo->prepare($sql.$sql_where.$sql_orderby);
        $consulta->bindValue(":fornecedor", $_POST['fornecedor']);
        $consulta->bindValue(":inicial", $inicial);
        $consulta->bindValue(":final", $final);
        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    }catch ( PDOException $error_msg){
        echo 'Erroff'. $error_msg->getMessage();
    }
    $lotes = $consulta->fetchAll();
    $lotes_quant = $consulta->rowCount();
    $sql = null;
    $consulta = null;
    ?>

    <table class="table table-striped table-hover table-sm">
        <thead class="thead-dark">
        <tr>
            <th scope="col"><small>LOTE</small></th>
            <th scope="col"><small>DATA</small></th>
            <th scope="col"><small>PRODUTO</small></th>
            <th scope="col"><small>PESO ENTRADA</small></th>
            <th scope="col"><small>PESO ATUAL</small></th>
            <th scope="col"><small>LOCALIZAÇÃO</small></th>
            <th scope="col"><small>B.O.</small></th>
        </tr>
        </thead>

        <?php

        $temppesoatual=0;
        $temppesoentrada=0;
        foreach ($lotes as $dados){
        $cordalinha = " font-weight-bold ";
        if ($dados['peso_atual']==null or $dados['peso_atual']==0 or $dados['peso_atual']==""){
            $cordalinha = " font-weight-lighter ";
        }else{
            if ($dados['p_bo']==1){
                $cordalinha = " font-weight-lighter font-italic ";
            }
        }

        $id_l = $dados["id"];
        $romaneio = $dados["romaneio"];
        $ref = $dados["ref"];
        $romaneio_tipo=fncgetromaneiotipo($dados["romaneio_tipo"]);

        $letra=fncgetletra($dados["letra"]);

        $entrada_id = $dados["id_e"];
        $data_l = $dados["data_ts"];

        $tipo_cafe = $dados["tipo_cafe"];
        $tipo_cafe_entrada = $dados["tipo_cafe_entrada"];
        if ($dados["verificado"]==0){
            $bebida="<small class='text-danger'>NÃO VERIFICADA</small>";
        }else{
            $bebida="<small class='text-success'>VERIFICADA</small>";
        }
        $peso_entrada = $dados["peso_entrada"];
        $sacas_entrada=$peso_entrada/60;
        $sacas_entrada=number_format($sacas_entrada, 2, '.', ',');
        $peso_entrada.=" Kg";
        $bags_entrada = $dados["bags_entrada"];
        $localizacao = $dados["localizacao"];
        $localizacao_obs = $dados["localizacao_obs"];
        $p_bo = $dados["p_bo"];
        $bo = $dados["bo"];
        $peso_atual = $dados["peso_atual"];
        $sacas_atual=$peso_atual/60;
        $sacas_atual=number_format($sacas_atual, 2, '.', ',');
        $peso_atual.=" Kg";
        $bags_atual = $dados["bags_atual"];
        $obs = $dados["obs"];
        $fornecedor = strtoupper($dados["fornecedor"]);

        $temppesoentrada+=$dados["peso_entrada"];
        $temppesoatual+=$dados["peso_atual"];

        ?>
        <tbody>
        <tr class="small <?php echo $cordalinha; ?>" id='<?php echo $romaneio_tipo; ?>'>
            <th scope="row" id="<?php echo $id_l;  ?>" style="white-space: nowrap;">
                <?php echo $romaneio_tipo.$ref.$romaneio." ".$letra; ?>
            </th>
            <td title="<?php echo datahoraBanco2data($dados["data_ts"]);?>">
                <?php echo dataRetiraHora($dados["data_ts"]); ?>
            </th>
            <td>
                <?php echo fncgetprodutos($tipo_cafe)['abrev'];?>
            </td>

            <td>
                <?php echo $peso_entrada. ", ".$sacas_entrada."v";?>
            </td>

            <td>
                <?php echo $peso_atual. ", ".$sacas_atual."v";?>
            </td>

            <td>
                <?php echo fncgetlocal($localizacao)['nome']. " ". $localizacao_obs;?>
            </td>

            <td>
                <?php echo $bo;?>
            </td>
        </tr>
        <?php
        }
        $tempsacasentrada=$temppesoentrada/60;
        $tempsacasatual=$temppesoatual/60;
        ?>
        </tbody>
    </table>
    <br>
    <div class="row">
        <div class="col-6 border">
            PESAGEM LÍQUIDO TOTAL DE ENTRADA: <strong><?php echo number_format($temppesoentrada,2,',','.')." Kg" ?></strong>
        </div>
        <div class="col-6 border">
            QUANTIDADE TOTAL EM VOLUMES: <strong><?php echo number_format($tempsacasentrada,2,',','.'); ?></strong>
        </div>
        <div class="col-12 border">
            <hr class="hrgrosso">
        </div>
        <div class="col-6 border">
            PESAGEM LÍQUIDO TOTAL ATUAL: <strong><?php echo number_format($temppesoatual,2,',','.')." Kg" ?></strong>
        </div>
        <div class="col-6 border">
            QUANTIDADE TOTAL EM VOLUMES: <strong><?php echo number_format($tempsacasatual,2,',','.'); ?></strong>
        </div>
    </div>



    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura do responsável
            </h4>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-12">
            <h6>
                Usuário: <strong><?php echo fncgetusuario($_SESSION['id'])['nome']." ".date('d/m/Y H:i:s'); ?></strong>
            </h6>
        </div>
    </div>


</div>

</body>
</html>
<SCRIPT LANGUAGE="JavaScript">
    window.print()
</SCRIPT>