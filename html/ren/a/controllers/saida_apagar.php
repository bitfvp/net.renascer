<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="apagarsaida"){

    if (isset($_GET["id_l"]) and is_numeric($_GET["id_l"])){
        $id_l=$_GET["id_l"];
    }else{
        $_SESSION['fsh']=[
            "flash"=>"Preencha Corretamente o cliente!!",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vl_lista");
        exit();
    }

    if (isset($_GET["peso"]) and is_numeric($_GET["peso"])){
        $peso=$_GET["peso"];
    }else{
        $_SESSION['fsh']=[
            "flash"=>"Preencha Corretamente o cliente!!",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vl_lista");
        exit();
    }

    if (isset($_GET["id"]) and is_numeric($_GET["id"])){
        $id_s=$_GET["id"];
    }else{
        $_SESSION['fsh']=[
            "flash"=>"Preencha Corretamente o cliente!!",
            "type"=>"warning",
        ];
        header("Location: index.php?pg=Vl_lista");
        exit();
    }

    $lote=fncgetlote($id_l);
    //$lote['peso_atual']

    $novopeso=$lote['peso_atual']+$peso;
    try {
        $sql="UPDATE ren_entradas_lotes";
        $sql.=" SET";
        $sql .= " peso_atual=:peso_atual WHERE id=:id";
        global $pdo;
        $atualiza = $pdo->prepare($sql);
        $atualiza->bindValue(":peso_atual", $novopeso);
        $atualiza->bindValue(":id", $id_l);
        $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

    } catch (PDOException $error_msg) {
        echo 'Erro' . $error_msg->getMessage();
    }


//    echo $id_l;
//    echo $peso;
//    echo $id_s;


    try {
        $sql = "DELETE FROM `ren_entradas_lotes_saidas` WHERE id = :id ";
        global $pdo;
        $exclui = $pdo->prepare($sql);
        $exclui->bindValue(":id", $id_s);
        $exclui->execute();
    } catch (PDOException $error_msg) {
        echo 'Erro:' . $error_msg->getMessage();
    }

    $_SESSION['fsh']=[
        "flash"=>"Saída apagada, estoque devolvido!!",
        "type"=>"success",
    ];
    header("Location: index.php?pg=Vl&id_l={$id_l}");
    exit();


}