<?php

function fncgetcaixa($id){
    $sql = "SELECT * FROM ren_caixas WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getcaixa = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcaixa;
}

function fnccaixaslista(){
    $sql = "SELECT * FROM ren_caixas WHERE status=1 order by `id` asc";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $caixaslista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $caixaslista;
}

