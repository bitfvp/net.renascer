<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_9"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vf_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="provaupdate";
    $prova=fncgetprova($_GET['id']);
}else{
    $a="provainsert";
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">


    $(document).ready(function () {
        $('#serchvendedor input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultvendedor");
            if (inputValf.length) {
                $.get("includes/cliente.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultvendedor p", function () {
            var vendedor = this.id;
            $.when(
                // $(this).parents("#serchvendedor").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchvendedor").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultvendedor").empty()

                $(this).parents("#serchvendedor").find('input[type="text"]').val($(this).text()),
                $('#vendedor').val(vendedor),
                $(this).parent(".resultvendedor").empty()

            ).then(
                // function() {
                //     // roda depois de acao1 e acao2 terminarem
                //     setTimeout(function() {
                //         //do something special
                //         $( "#gogo" ).click();
                //     }, 500);
                // }
                );
        });
    });




</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vfechamento_lista&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Prova</h3>
        <hr>
        <div class="row">

            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $prova['id']; ?>"/>


            <div class="col-md-12" id="serchvendedor">
                <label for="vendedor">Quem deixou a amostra:</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        if ($prova['vendedor']==0){
                         //se não tiver o id do vendedor usar 0 pra id e vendedor2 pra apresentar
                            $v_vendedor_id=0;
                            $v_vendedor=$prova['vendedor2'];
                        }else{
                            //se tiver id do vendedor buscar o nome pelo banco
                            $v_vendedor_id=$prova['vendedor'];
                            $v_vendedor=fncgetpessoa($prova['vendedor'])['nome'];
                        }


                    }else{
                        $v_vendedor_id="";
                        $v_vendedor="";
                    }
                    $c_vendedor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="v<?php echo $c_vendedor;?>" id="v<?php echo $c_vendedor;?>" value="<?php echo $v_vendedor; ?>" placeholder="Click no resultado ao aparecer"  required />
                    <input id="vendedor" autocomplete="false" type="hidden" class="form-control" name="vendedor" value="<?php echo $v_vendedor_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_vendedor">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultvendedor"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_vendedor").click(function(ev){
                            document.getElementById('vendedor').value='';
                            document.getElementById('v<?php echo $c_vendedor;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-7">
                <label for="endereco">ENDEREÇO:</label>
                <input autocomplete="off" id="endereco" placeholder="" type="text" class="form-control" name="endereco" value="<?php echo $prova['endereco']; ?>" />
            </div>

            <div class="col-md-5">
                <label for="telefone">TELEFONE:</label>
                <input autocomplete="off" id="telefone" placeholder="" type="text" class="form-control" name="telefone" value="<?php echo $prova['telefone']; ?>" />
            </div>

            <div class="col-md-6">
                <label for="multi1">CAMPO MULTIPROPÓSITO:</label>
                <input autocomplete="off" id="multi1" placeholder="" type="text" class="form-control" name="multi1" value="<?php echo $prova['multi1']; ?>" />
            </div>

            <div class="col-md-6">
                <label for="multi2">CAMPO MULTIPROPÓSITO:</label>
                <input autocomplete="off" id="multi2" placeholder="" type="text" class="form-control" name="multi2" value="<?php echo $prova['multi2']; ?>" />
            </div>



            <div class="col-md-2">
                <label for="quantidade">Quantidade:</label>
                <input type="number" name="quantidade" id="quantidade" class="form-control" value="<?php echo $prova['quantidade'];?>" min="0" step="any">
            </div>
            <div class="col-md-10">
                <label for="descricao">Descrição:</label>
                <textarea id="descricao" onkeyup="limite_textarea(this.value,1000,descricao,'cont')" maxlength="1000" class="form-control" rows="2" name="descricao" required><?php echo $prova['descricao']; ?></textarea>
                <span id="cont">1000</span>/1000
            </div>

            <div class="col-md-12">
                <hr>
            </div>

        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="finalizado">Foi feito a prova:</label>
                <select name="finalizado" id="finalizado" class="form-control">
                    <option selected="" value="<?php if ($prova['finalizado'] == "") {
                        echo 0;
                    } else {
                        echo $prova['finalizado'];
                    } ?>">
                        <?php
                        if ($prova['finalizado'] == null or $prova['finalizado'] == 0) {
                            echo "NÃO";
                        }

                        if ($prova['finalizado'] == 1) {
                            echo "SIM";
                        }
                        ?>
                    </option>
                    <option value="1">SIM</option>
                    <option value="0">NÃO</option>
                </select>
            </div>

            <div class="col-md-3">
                <label for="cata">CATA:</label>
                <input autocomplete="off" id="cata" placeholder="" type="number" class="form-control" name="cata" value="<?php echo $prova['cata']; ?>" min="0" max="98" step="1" />
            </div>

            <div class="col-md-3">
                <label for="peneira">PENEIRA:</label>
                <input autocomplete="off" id="peneira" placeholder="" type="text" class="form-control" name="peneira" value="<?php echo $prova['peneira']; ?>" />
            </div>


            <div class="col-md-3">
                <label for="processo">PROCESSO:</label>
                <select name="processo" id="processo" class="form-control">
                    <option selected="" value="<?php
                    if ($prova['processo'] == 1 or $prova['processo'] == 1) {
                        echo $prova['processo'];
                    }else{
                        echo 1;
                    }

                    ?>">
                        <?php
                        if ($prova['processo'] != 1 and $prova['processo'] != 2) {
                            echo "NATURAL";
                        }

                        if ($prova['processo'] == 1) {
                            echo "NATURAL";
                        }

                        if ($prova['processo'] == 2) {
                            echo "CD";
                        }
                        ?>
                    </option>
                    <option value="1">NATURAL</option>
                    <option value="2">CD</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label for="observacao">Resultado da prova:</label>
                <textarea id="observacao" onkeyup="limite_textarea(this.value,1000,observacao,'cont2')" maxlength="1000" class="form-control" rows="2" name="observacao"><?php echo $prova['observacao']; ?></textarea>
                <span id="cont2">1000</span>/1000
            </div>

            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" name="gogo" id="gogo" class="btn btn-block btn-success mt-4">
            </div>

            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>


        </div>

    </form>
</div>

<?php
//for ($i = 1; $i <= 1000000; $i++) {
//    echo $i."  ".get_criptografa64($i)."<br>";
//}
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>