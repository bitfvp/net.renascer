<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_9"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vprova_lista'>";
include_once("includes/topo.php");

try{
    $sql = "SELECT "
        ."ren_provas.id, "
        ."ren_provas.`status`, "
        ."ren_provas.data_ts, "
        ."ren_provas.imprimir, "
        ."ren_provas.finalizado, "
        ."ren_provas.quantidade, "
        ."ren_provas.descricao, "
        ."ren_provas.observacao, "
        ."ren_provas.cata, "
        ."ren_provas.processo, "
        ."ren_provas.peneira, "
        ."ren_provas.endereco, "
        ."ren_provas.telefone, "
        ."ren_provas.multi2, "
        ."ren_provas.multi2, "
        ."ren_provas.vendedor as id_vendedor, "
        ."ren_provas.vendedor2, "
        ."pessoas_vendedor.nome AS vendedor "
        ."FROM "
        ."ren_provas "
        ."LEFT JOIN ren_pessoas AS pessoas_vendedor ON pessoas_vendedor.id = ren_provas.vendedor "
        ."WHERE "
        ."ren_provas.id <> 0 ";


    if (isset($_GET['scb']) and $_GET['scb']!='') {
        $scb=$_GET['scb'];
        $sql .=" AND (pessoas_vendedor.nome LIKE '%$scb%' or ren_provas.vendedor2 LIKE '%$scb%')  ";
    }

    if (isset($_GET['scc']) and $_GET['scc']!=0 and $_GET['scc']!='') {
        $inicial=$_GET['scc'];
        $inicial.=" 00:00:01";
        $final=$_GET['scc'];
        $final.=" 23:59:59";
        $sql .=" AND ((ren_provas.data_ts)>=:inicial) And ((ren_provas.data_ts)<=:final) ";
    }else{
        if (((isset($_GET['sca']) and $_GET['sca']!='') or (isset($_GET['scb']) and $_GET['scb']!='')) or ((isset($_GET['scc']) and $_GET['scc']!='') or (isset($_GET['scc']) and $_GET['scc']!=''))){
            //tempo todo
            $inicial="2021-01-01";
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((ren_provas.data_ts)>=:inicial) And ((ren_provas.data_ts)<=:final) ";
        }else{
            //apenas dia anterior
            $inicial=date("Y-m-d",strtotime("-90 days"));
            $inicial.=" 00:00:01";
            $final=date("Y-m-d");;
            $final.=" 23:59:59";
            $sql .=" AND ((ren_provas.data_ts)>=:inicial) And ((ren_provas.data_ts)<=:final) ";
        }

    }

    $sql .="order by ren_provas.data_ts DESC LIMIT 0,50 ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial", $inicial);
    $consulta->bindValue(":final", $final);
//    if (isset($_GET['sca']) and  is_numeric($_GET['sca']) and $_GET['sca']!=0) {
//        $consulta->bindValue(":romaneio", "%".$_GET['sca']."%");
//    }
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
}catch ( PDOException $error_msg){
    echo 'Erro'. $error_msg->getMessage();
}

$entradas = $consulta->fetchAll();
$entradas_quant = $consulta->rowCount();
$sql = null;
$consulta = null;
?>

<main class="container-fluid"><!--todo conteudo-->
    <h3 class="form-cadastro-heading"><a href="index.php?pg=Vprova_lista" class="text-decoration-none ">PROVAS</a></h3>
    <hr>

    <div class="row">
        <div class="col-md-8">
            <form action="index.php" method="get">
                <div class="input-group mb-3 col-md-9 float-left">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-success" type="submit"><i class="fa fa-search animated swing infinite"></i></button>
                    </div>
                    <input name="pg" value="Vprova_lista" hidden/>
                    <input type="text" name="scb" id="scb" placeholder="vendedor" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scb'])) {echo $_GET['scb'];} ?>" />
                    <input type="date" name="scc" id="scc" autocomplete="off" class="form-control" value="<?php if (isset($_GET['scc'])) {echo $_GET['scc'];} ?>" />
                </div>
            </form>

            <script type="text/javascript">
                function selecionaTexto()
                {
                    document.getElementById("sca").select();
                }
                window.onload = selecionaTexto();
            </script>
        </div>


        <div class="col-md-4">
            <a href="index.php?pg=Vprova_editar" class="btn btn-block btn-primary mb-2" ><i class="fas fa-level-down-alt"></i> Novo</a>
        </div>
<!--        <div class="col-md-2">-->
<!--            <a href="index.php?pg=Vprova_roteiro" class="btn btn-block btn-success mb-2" target="_blank"> Impressão</a>-->
<!--        </div>-->
<!--        <div class="col-md-2">-->
<!--            <a href='index.php?pg=Vprova_lista&aca=resetroimprimir' class="btn btn-block btn-warning btn-block mb-2"> Desmarcar Todos</a>-->
<!--        </div>-->
    </div>

    <table class="table table-sm table-stripe table-hover table-bordered">
        <thead class="thead-dark">
        <tr>
            <th class="text-center"><small>#</small></th>
            <th scope="col"><small>EMISSÃO</small></th>
             <th scope="col"><small>VENDEDOR</small></th>
             <th scope="col"><small>DESCRIÇÃO</small></th>
             <th scope="col"><small>RESULTADO</small></th>
            <th scope="col" class="text-center"><small>AÇÕES</small></th>
        </tr>
        </thead>
        <tfoot>
        <tr class="bg-warning">
            <th colspan="6" class="bg-info text-right"><?php echo $entradas_quant;?> Entrada(s) encontrada(s)</th>
        </tr>
        </tfoot>

        <tbody>
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?php
        if($_GET['scb']!="" and isset($_GET['scb'])) {
            $stb = strtoupper($_GET['scb']);
            define('CSB', $stb);
        }
        foreach ($entradas as $dados){
            $pr_id = $dados["id"];

            if ($dados['id_vendedor']==0){
                $vendedor = strtoupper($dados["vendedor2"]);
            }else{
                $vendedor = strtoupper($dados["vendedor"]);
            }

            $data_ts = dataRetiraHora($dados["data_ts"]);
            $usuario = $dados["usuario"];
            $descricao = $dados["descricao"];
            if ($dados["quantidade"]>0){
                $quantidade=" - ".$dados["quantidade"]." sacas";
            }else{
                $quantidade="";
            }
            $resultado = $dados["observacao"];

            if ($dados["processo"]==1){
                $processo=" PROCESSO NATURAL";
            }
            if ($dados["processo"]==2){
                $processo=" PROCESSO CD";
            }

            if (strlen($dados["peneira"])>1){
                $peneira=" PENEIRA ".$dados["peneira"];
            }else{
                $peneira=" ";
            }

            if (strlen($dados["endereco"])>0){
                $endereco=" <br>".$dados["endereco"];
            }else{
                $endereco=" ";
            }

            if (strlen($dados["telefone"])>0){
                $telefone=" <br>".$dados["telefone"];
            }else{
                $telefone=" ";
            }

            if (strlen($dados["multi1"])>0){
                $multi1=" <br>".$dados["multi1"];
            }else{
                $multi1=" ";
            }

            if (strlen($dados["multi2"])>0){
                $multi2=" <br>".$dados["multi2"];
            }else{
                $multi2=" ";
            }


            if ($dados["cata"]>0){
                $cata=" - ".$dados["cata"]."% DE CATA, ";
            }else{
                $cata="";
            }

            $imprimir=$dados["imprimir"];

            if ($dados["finalizado"]==0){
                $corlinha="";
            }else{
                $corlinha=" bg-secondary text-light ";
            }
            ?>

            <tr id="<?php echo $pr_id;?>" class="<?php echo $corlinha;?>">

                <td class="text-center"><?php echo utf8_encode(strftime('%Y', strtotime("{$dados['data_ts']}")))."-".$dados['id']; ?></td>
                <td><?php echo $data_ts; ?></td>

                <td><?php
                    if($_GET['scb']!="" and isset($_GET['scb'])) {
                        $stb = CSB;
                        $aaa = $vendedor;
                        $aa = explode(CSB, $aaa);
                        $a = implode("<span class='text-success'>{$stb}</span>", $aa);
                        echo $a;
                    }else{
                        echo $vendedor;
                    }

                    echo $endereco.$telefone.$multi1.$multi2;
                    ?>
                </td>

                <td><?php echo $descricao."".$quantidade; ?></td>
                <td><?php echo $resultado."<BR>".$processo."".$cata."".$peneira; ?></td>

                <td class="text-center">

                        <div class="btn-group" role="group" aria-label="">
                            <a href="index.php?pg=Vprova_editar&id=<?php echo $pr_id; ?>" title="Editar registro" class="btn btn-sm btn-primary">
                                <i class="fas fa-pen">EDITAR</i>
                            </a>


<!--                            --><?php
//                            if($imprimir==0){
//                                echo "<a href='index.php?pg=Vfechamento_lista&id={$fe_id}&aca=imprimirrosim&sca={$_GET['sca']}&b={$_GET['scb']}&scc={$_GET['scc']}' title='Click para alterar para sim' class='btn btn-outline-danger text-danger'>IMPRIMIR</a>";
//                            }
//                            if($imprimir==1){
//                                echo "<a href='index.php?pg=Vfechamento_lista&id={$fe_id}&aca=imprimirronao&sca={$_GET['sca']}&b={$_GET['scb']}&scc={$_GET['scc']}' title='Click para alterar para não' class='btn btn-outline-success fas fa-print text-success'></a>";
//                            }
//                            ?>


                        </div>

                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>


</main>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>