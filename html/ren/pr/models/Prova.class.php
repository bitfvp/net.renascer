<?php
class Prova{
    public function fncprovainsert( $vendedor, $vendedor2, $endereco, $telefone, $multi1, $multi2, $descricao, $quantidade, $finalizado, $observacao, $cata, $peneira, $processo, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO ren_provas ";
            $sql.="(id, vendedor, vendedor2, endereco, telefone, multi1, multi2, descricao, quantidade, finalizado, observacao, cata, peneira, processo, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :vendedor, :vendedor2, :endereco, :telefone, :multi1, :multi2, :descricao, :quantidade, :finalizado, :observacao, :cata, :peneira, :processo, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":vendedor", $vendedor);
            $insere->bindValue(":vendedor2", $vendedor2);
            $insere->bindValue(":endereco", $endereco);
            $insere->bindValue(":telefone", $telefone);
            $insere->bindValue(":multi1", $multi1);
            $insere->bindValue(":multi2", $multi2);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":quantidade", $quantidade);
            $insere->bindValue(":finalizado", $finalizado);
            $insere->bindValue(":observacao", $observacao);
            $insere->bindValue(":cata", $cata);
            $insere->bindValue(":peneira", $peneira);
            $insere->bindValue(":processo", $processo);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];


            header("Location: index.php?pg=Vprova_lista");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncprovaupdate( $id, $vendedor, $vendedor2, $endereco, $telefone, $multi1, $multi2, $descricao, $quantidade, $finalizado, $observacao, $cata, $peneira, $processo ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="ren_provas ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE ren_provas ";
                $sql.="SET ";
                $sql .= "vendedor=:vendedor,
                vendedor2=:vendedor2,
                endereco=:endereco,
                telefone=:telefone,
                multi1=:multi1,
                multi2=:multi2,
                descricao=:descricao,
                quantidade=:quantidade,
finalizado=:finalizado,
observacao=:observacao,
cata=:cata,
peneira=:peneira,
processo=:processo
WHERE id=:id ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":vendedor", $vendedor);
                $atualiza->bindValue(":vendedor2", $vendedor2);
                $atualiza->bindValue(":endereco", $endereco);
                $atualiza->bindValue(":telefone", $telefone);
                $atualiza->bindValue(":multi1", $multi1);
                $atualiza->bindValue(":multi2", $multi2);
                $atualiza->bindValue(":descricao", $descricao);
                $atualiza->bindValue(":quantidade", $quantidade);
                $atualiza->bindValue(":finalizado", $finalizado);
                $atualiza->bindValue(":observacao", $observacao);
                $atualiza->bindValue(":cata", $cata);
                $atualiza->bindValue(":peneira", $peneira);
                $atualiza->bindValue(":processo", $processo);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse cadastro em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vprova_lista");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao



}//fim da classe