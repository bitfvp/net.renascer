<?php
class Allow{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncallowedit($pessoa,
                                 $admin,
                                 $allow_1,
                                 $allow_2,
                                 $allow_3,
                                 $allow_4,
                                 $allow_5,
                                 $allow_6,
                                 $allow_7,
                                 $allow_8,
                                 $allow_9,
                                 $allow_10,
                                 $allow_11,
                                 $allow_12,
                                 $allow_13,
                                 $allow_14,
                                 $allow_15,
                                 $allow_16,
                                 $allow_17,
                                 $allow_18,
                                 $allow_19,
                                 $allow_20
    )
    {
        //inserção no banco
        try{
            $sql = "UPDATE rcafe_allow SET ";
            $sql .= "admin = {$admin}, ";
            $sql .= "allow_1 = {$allow_1}, ";
            $sql .= "allow_2 = {$allow_2}, ";
            $sql .= "allow_3 = {$allow_3}, ";
            $sql .= "allow_4 = {$allow_4}, ";
            $sql .= "allow_5 = {$allow_5}, ";
            $sql .= "allow_6 = {$allow_6}, ";
            $sql .= "allow_7 = {$allow_7}, ";
            $sql .= "allow_8 = {$allow_8}, ";
            $sql .= "allow_9 = {$allow_9}, ";
            $sql .= "allow_10 = {$allow_10}, ";
            $sql .= "allow_11 = {$allow_11}, ";
            $sql .= "allow_12 = {$allow_12}, ";
            $sql .= "allow_13 = {$allow_13}, ";
            $sql .= "allow_14 = {$allow_14}, ";
            $sql .= "allow_15 = {$allow_15}, ";
            $sql .= "allow_16 = {$allow_16}, ";
            $sql .= "allow_17 = {$allow_17}, ";
            $sql .= "allow_18 = {$allow_18}, ";
            $sql .= "allow_19 = {$allow_19}, ";
            $sql .= "allow_20 = {$allow_20} ";
            $sql .= "WHERE rcafe_allow.pessoa = {$pessoa}";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }


}
