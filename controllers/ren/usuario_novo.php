<?php
//metodo de acao cadastro de usuario
if($startactiona==1 && $aca=="usuarionew"){
    $nome = remover_caracter(ucwords(strtolower($_POST["nome"])));
    $status = $_POST["status"];
    $nick = strtolower(limpadocumento($_POST["nick"]));
    $email = strtolower($_POST["email"]);
    $nascimento = $_POST["nascimento"];
    if($nascimento==""){
        $nascimento="1000-01-01";
    }
    $cpf = limpadocumento($_POST["cpf"]);


    $matriz=1;

    if(empty($nome) || empty($email) || empty($nascimento) || $nascimento==0 || empty($matriz)){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];
    }else{
        //valida email
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
            //executa classe cadastro
            $conec= new Usuario();
            $conec=$conec->fncnewusuario(
                $nome,
                $status,
                $nick,
                $email,
                $nascimento,
                $cpf,
                $matriz
            );
            header("Location: index.php");
            exit();
        }else{
            //EMAIL ERRADO
            $_SESSION['fsh']=[
                "flash"=>"Preencha corretamente o email",
                "type"=>"warning",
            ];
        }
    }
}
